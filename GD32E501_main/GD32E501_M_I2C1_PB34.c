#include "gd32e501.h"
#include <stdio.h>

// GD Sample code Suggest
//#define I2C_SHORT_TIMEOUT 0x3ffff
//#define I2C_LONG_TIMEOUT  0x5fff
#define I2C_SHORT_TIMEOUT   0x200
#define I2C_LONG_TIMEOUT    0x200
#define I2C1_TIMEOUT        0x200

typedef enum
{
  I2C_OK                                          = 0,
  I2C_FAIL                                        = 1
}I2C1_Status;

void Delay_I2C1(uint32_t i)
{
  while(i--);
}

void GPIO_Configuration_I2C1_PB34()
{  
    rcu_periph_reset_enable(RCU_I2C1RST);
    rcu_periph_reset_disable(RCU_I2C1RST);
    /* enable GPIOB clock */
    rcu_periph_clock_enable(RCU_GPIOB);
    /* enable BOARD_I2C APB1 clock */
    rcu_periph_clock_enable(RCU_I2C1);
    /* connect PB10 to I2C1_SCL */
    gpio_af_set(GPIOB, GPIO_AF_7, GPIO_PIN_3);
    /* connect PB11 to I2C1_SDA */
    gpio_af_set(GPIOB, GPIO_AF_7, GPIO_PIN_4);
    /* configure  I2C2 GPIO */
    gpio_mode_set(GPIOB, GPIO_MODE_AF, GPIO_PUPD_PULLUP, GPIO_PIN_3);
    gpio_output_options_set(GPIOB, GPIO_OTYPE_OD, GPIO_OSPEED_50MHZ, GPIO_PIN_3);
    gpio_mode_set(GPIOB, GPIO_MODE_AF, GPIO_PUPD_PULLUP, GPIO_PIN_4);
    gpio_output_options_set(GPIOB, GPIO_OTYPE_OD, GPIO_OSPEED_50MHZ, GPIO_PIN_4);
}

void Master_I2C1_PB34_Init(uint8_t Device_ADR)
{
    GPIO_Configuration_I2C1_PB34();
    /* configure I2C timing */
    i2c_timing_config(I2C1,0,0x9,0);
    i2c_master_clock_config(I2C1,0x78,0x78);
    i2c_address_config(I2C1,0x82,I2C_ADDFORMAT_7BITS);
    /* send slave address to I2C bus */
    i2c_master_addressing(I2C1,Device_ADR,I2C_MASTER_TRANSMIT);
    /* enable I2Cx */
    i2c_enable(I2C1);
}

void Resume_IIC_I2C1( uint32_t Timeout ,uint8_t Device_ADR)
{
    uint32_t GPIO_SDA;
    uint32_t GPIO_SCL;
    uint32_t GPIO_Pin_SDA,GPIO_Pin_SCL;
    
    /* enable GPIOB clock */
    rcu_periph_clock_enable(RCU_GPIOB);
    /* enable BOARD_I2C APB1 clock */
    rcu_periph_clock_disable(RCU_I2C1);
    GPIO_SCL=GPIOB;
    GPIO_Pin_SCL=GPIO_PIN_3;
    GPIO_SDA=GPIOB;
    GPIO_Pin_SDA=GPIO_PIN_4;

    do
    {    
        gpio_mode_set(GPIO_SCL, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLUP, GPIO_Pin_SCL);
        gpio_output_options_set(GPIO_SCL, GPIO_OTYPE_OD, GPIO_OSPEED_50MHZ, GPIO_Pin_SCL);
        gpio_mode_set(GPIO_SDA, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLUP, GPIO_Pin_SDA);
        gpio_output_options_set(GPIO_SDA, GPIO_OTYPE_OD, GPIO_OSPEED_50MHZ, GPIO_Pin_SDA);  
        gpio_bit_reset(GPIO_SCL, GPIO_Pin_SCL);
        Delay_I2C1(20);
        gpio_bit_reset(GPIO_SDA, GPIO_Pin_SDA);
        Delay_I2C1(20);
        gpio_bit_set(GPIO_SCL, GPIO_Pin_SCL);
        Delay_I2C1(20);
        gpio_bit_set(GPIO_SDA, GPIO_Pin_SDA);
        Delay_I2C1(20);
        if(Timeout-- == 0) return;
    }
    while((!gpio_input_bit_get(GPIO_SDA, GPIO_Pin_SDA))||(!gpio_input_bit_get(GPIO_SCL, GPIO_Pin_SCL)));
		
    Master_I2C1_PB34_Init(Device_ADR);
}

uint8_t Master_I2C1_ByteREAD_PB34(uint8_t SADR ,uint8_t Mem_REG )
{
    uint32_t I2C1_Timeout = I2C_SHORT_TIMEOUT;
    uint8_t  READ_DATA = 0x00;
    uint8_t Timeout_flag = 1;
    
    i2c_master_addressing( I2C1 , SADR , I2C_MASTER_TRANSMIT );
    i2c_transfer_byte_number_config( I2C1 , 1 );
    I2C1_Timeout = I2C_SHORT_TIMEOUT;
    while(i2c_flag_get( I2C1 , I2C_FLAG_I2CBSY))
    {
        if((I2C1_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            break;
        }
    }
    // step1 : I2C start + Address 
    i2c_start_on_bus(I2C1);
    // step2 : write data flag setting
    I2C_STAT(I2C1) |= I2C_STAT_TBE;
    
    I2C1_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get( I2C1 , I2C_FLAG_TBE))	
    {
        if((I2C1_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            break;
        }
    }
    // Step3 : write memory reg. address
    i2c_data_transmit( I2C1 , Mem_REG );

    I2C1_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get( I2C1 , I2C_FLAG_TC))	
    {
        if((I2C1_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            break;
        }
    }
    // Step4 : GD MCU I2C setting mode receive
	i2c_master_addressing( I2C1 , SADR , I2C_MASTER_RECEIVE);
    // Step5 : Read byte count setting
    i2c_transfer_byte_number_config( I2C1 , 1 );
    // Step6 : RESTART
    i2c_start_on_bus(I2C1);
    // step7 : Disable reload
    i2c_reload_disable(I2C1);
    // step8 : Enable I2C automatic end mode in master mode 
    i2c_automatic_end_enable(I2C1);
    // step9 : check flag read i2c temp buffer
    I2C1_Timeout = I2C_SHORT_TIMEOUT;
    
    while(!i2c_flag_get( I2C1 , I2C_FLAG_RBNE))
    {
        if((I2C1_Timeout--) == 0)
        {
            Timeout_flag = 0;
            break;
        }
    }
    
    if(Timeout_flag)
        READ_DATA = i2c_data_receive(I2C1);

    I2C1_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get(I2C1, I2C_FLAG_STPDET))
    {
        if((I2C1_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            break;
        }
    }
    // step10 : clear stop flag
    i2c_flag_clear(I2C1, I2C_FLAG_STPDET);
    i2c_automatic_end_disable(I2C1);
    //i2c_deinit(I2C1);
    return READ_DATA;
}


void Master_I2C1_ByteWrite_PB34( uint8_t SADR , uint8_t Mem_REG , uint8_t Write_DATA)
{
    uint32_t I2C1_Timeout = I2C_SHORT_TIMEOUT;
    // MCU I2C mode setting 
    i2c_master_addressing(I2C1, SADR, I2C_MASTER_TRANSMIT);
    i2c_transfer_byte_number_config(I2C1,3);
    i2c_enable(I2C1);
    // Check I2c BUS is free
    while(i2c_flag_get(I2C1, I2C_FLAG_I2CBSY))
    {
        if((I2C1_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            break;
        }
    }
    // Step1 : Start + slave address 
    i2c_start_on_bus(I2C1);
    // step2 : write data flag setting
    I2C_STAT(I2C1) |= I2C_STAT_TBE;
    I2C1_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get( I2C1 , I2C_FLAG_TBE))
    {
        if((I2C1_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            break;
        }
    }
    // Step3 : write memory reg. address
    i2c_data_transmit( I2C1 , Mem_REG );
    I2C1_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get( I2C1 , I2C_FLAG_TI))
    {
        if((I2C1_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            break;
        }
    }
    // Step4 : write data
    i2c_data_transmit( I2C1 , Write_DATA );
    // Step5 : Send a stop condition to I2C Bus
    i2c_stop_on_bus(I2C1);
    I2C1_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get(I2C1, I2C_FLAG_STPDET))
    {
        if((I2C1_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            break;
        }
    }
    // step6 : clear sotp flag
    i2c_flag_clear(I2C1, I2C_FLAG_STPDET);
}

void Master_I2C1_TwoByte_PB34( uint8_t SADR , uint8_t Mem_REG , uint8_t WD_MSB , uint8_t WD_LSB)
{
    uint32_t I2C1_Timeout = I2C_SHORT_TIMEOUT;
    // MCU I2C mode setting
    i2c_master_addressing(I2C1, SADR, I2C_MASTER_TRANSMIT);
    i2c_transfer_byte_number_config(I2C1,4);
    i2c_enable(I2C1);
    // Check I2c BUS is free
    while(i2c_flag_get(I2C1, I2C_FLAG_I2CBSY))
    {
        if((I2C1_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            break;
        }
    }
    // Step1 : Start + slave address
    i2c_start_on_bus(I2C1);
    // step2 : write data flag setting
    /* wait until the transmit data buffer is empty */
    I2C_STAT(I2C1) |= I2C_STAT_TBE;
    I2C1_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get( I2C0 , I2C_FLAG_TBE))
    {
        if((I2C1_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            break;
        }
    }
    // Step3 : write memory reg. address
    i2c_data_transmit( I2C1 , Mem_REG );
    I2C1_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get( I2C1 , I2C_FLAG_TI))
    {
        if((I2C1_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            break;
        }
    }
    // Step4 : write data
    i2c_data_transmit( I2C1 , WD_MSB );
    /* decrement the read bytes counter */
    I2C1_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get( I2C1 , I2C_FLAG_TI))
    {
        if((I2C1_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            break;
        }
    }
    // Step5 : write data
    i2c_data_transmit( I2C1 , WD_LSB );
    // Step6 : Send a stop condition to I2C Bus
    /* send a stop condition to I2C bus */
    i2c_stop_on_bus(I2C1);
    I2C1_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get(I2C1, I2C_FLAG_STPDET))
    {
        if((I2C1_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            break;
        }
    }
    // step7 : clear sotp flag
    i2c_flag_clear(I2C1, I2C_FLAG_STPDET);
}

void MI2C1_WordADR_W_PB34( uint8_t SADR , uint16_t MemADR , uint16_t DATA_Value )
{
    uint32_t I2C1_Timeout = I2C_SHORT_TIMEOUT;
    uint8_t TempMemADR_MSB = 0;
    uint8_t TempMemADR_LSB = 0;
    uint8_t TempDATD_MSB = 0;
    uint8_t TempDATD_LSB = 0;
    //----------------------------------------------------------//
    // SADR MSB_MADR LSB MADR LSBDATA MSBDATA
    //----------------------------------------------------------//
    TempMemADR_LSB = MemADR;
    TempMemADR_MSB = MemADR>>8;
    TempDATD_LSB = DATA_Value;
    TempDATD_MSB = DATA_Value>>8;
    
    // MCU I2C mode setting
    i2c_master_addressing(I2C1, SADR, I2C_MASTER_TRANSMIT);
    i2c_transfer_byte_number_config(I2C1,5);
    i2c_enable(I2C1);
    // Check I2c BUS is free
    while(i2c_flag_get(I2C1, I2C_FLAG_I2CBSY))
    {
        if((I2C1_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            break;
        }
    }
    // Step1 : Start + slave address
    i2c_start_on_bus(I2C1);
    // step2 : write data flag setting
    /* wait until the transmit data buffer is empty */
    I2C_STAT(I2C1) |= I2C_STAT_TBE;
    I2C1_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get( I2C1 , I2C_FLAG_TBE))
    {
        if((I2C1_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            break;
        }
    }
    // Step3 : write memory reg. address
    i2c_data_transmit( I2C1 , TempMemADR_MSB );
    I2C1_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get( I2C1 , I2C_FLAG_TI))
    {
        if((I2C1_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            break;
        }
    }
    i2c_data_transmit( I2C1 , TempMemADR_LSB );
    
    I2C1_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get( I2C1 , I2C_FLAG_TI))
    {
        if((I2C1_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            break;
        }
    }
    // Step4 : write data
    i2c_data_transmit( I2C1 , TempDATD_LSB );
    I2C1_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get( I2C1 , I2C_FLAG_TI))
    {
        if((I2C1_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            break;
        }
    }
    // Step5 : write data
    i2c_data_transmit( I2C1 , TempDATD_MSB );
    // Step6 : Send a stop condition to I2C Bus
    /* send a stop condition to I2C bus */
    i2c_stop_on_bus(I2C1);
    I2C1_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get(I2C1, I2C_FLAG_STPDET))
    {
        if((I2C1_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            break;
        }
    }
    // step7 : clear sotp flag
    i2c_flag_clear(I2C1, I2C_FLAG_STPDET);
}

uint16_t MI2C1_WordADR_R_PB34(uint8_t SADR ,uint16_t MemADR )
{
    uint32_t I2C1_Timeout = I2C_SHORT_TIMEOUT;
    uint8_t TempMemADR_MSB = 0;
    uint8_t TempMemADR_LSB = 0;
    uint16_t READ_DATA = 0x00;
    uint8_t Timeout_flag = 1;
    uint8_t DATA_MSB = 0;
    uint8_t DATA_LSB = 0;
    //----------------------------------------------------------//
    // SADR MSB_MADR LSB MADR LSBDATA MSBDATA
    //----------------------------------------------------------//
    TempMemADR_LSB = MemADR;
    TempMemADR_MSB = MemADR>>8;
    
    i2c_master_addressing( I2C1 , SADR , I2C_MASTER_TRANSMIT );
    i2c_transfer_byte_number_config( I2C1 , 2 );
    I2C1_Timeout = I2C_SHORT_TIMEOUT;
    while(i2c_flag_get( I2C1 , I2C_FLAG_I2CBSY))
    {
        if((I2C1_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            break;
        }
    }
    // step1 : I2C start + Address 
    i2c_start_on_bus(I2C1);
    // step2 : write data flag setting
    I2C_STAT(I2C1) |= I2C_STAT_TBE;
    I2C1_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get( I2C1 , I2C_FLAG_TBE))
    {
        if((I2C1_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            break;
        }
    }
    // Step3 : write memory reg. address
    i2c_data_transmit( I2C1 , TempMemADR_MSB );
    I2C1_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get( I2C1 , I2C_FLAG_TI))
    {
        if((I2C1_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            break;
        }
    }
    i2c_data_transmit( I2C1 , TempMemADR_LSB );
    I2C1_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get( I2C1 , I2C_FLAG_TC))
    {
        if((I2C1_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            break;
        }
    }
    // Step4 : GD MCU I2C setting mode receive
	i2c_master_addressing( I2C1 , SADR , I2C_MASTER_RECEIVE); 
    // Step5 : Read byte count setting
    i2c_transfer_byte_number_config( I2C1 , 2 );
    // Step6 : RESTART 
    i2c_start_on_bus(I2C1);
    // step7 : Disable reload
    i2c_reload_disable(I2C1);
    // step8 : Enable I2C automatic end mode in master mode 
    i2c_automatic_end_enable(I2C1);
    // step9 : check flag read i2c temp buffer
    I2C1_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get( I2C1 , I2C_FLAG_RBNE))
    {
        if((I2C1_Timeout--) == 0)
        {
            Timeout_flag = 0;
            break;
        }
    }
    
    if(Timeout_flag)
        DATA_LSB = i2c_data_receive(I2C1);
    
    I2C1_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get( I2C1 , I2C_FLAG_RBNE))
    {
        if((I2C1_Timeout--) == 0)
        {
            Timeout_flag = 0;
            break;
        }
    }
    if(Timeout_flag)
        DATA_MSB = i2c_data_receive(I2C1);
    
    I2C1_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get(I2C1, I2C_FLAG_STPDET))
    {
        if((I2C1_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            break;
        }
    }
    // step10 : clear stop flag
    i2c_flag_clear(I2C1, I2C_FLAG_STPDET);
    i2c_automatic_end_disable(I2C1);
    //i2c_deinit(I2C1);
    
    READ_DATA = (uint16_t)( DATA_MSB<<8 | DATA_LSB );
    
    return READ_DATA;
}

uint8_t MI2C1_WordADR_HLBW_PB34(uint8_t SADR, uint16_t MemADR , uint8_t MSB ,uint8_t LSB)
{
    uint32_t I2C1_Timeout = I2C_SHORT_TIMEOUT;
    uint8_t TempMemADR_MSB = 0;
    uint8_t TempMemADR_LSB = 0;
    //----------------------------------------------------------//
    // SADR MSB_MADR LSB MADR LSBDATA MSBDATA
    //----------------------------------------------------------//
    TempMemADR_LSB = MemADR;
    TempMemADR_MSB = MemADR>>8;
    // MCU I2C mode setting 
	i2c_master_addressing(I2C1, SADR, I2C_MASTER_TRANSMIT);
  	i2c_transfer_byte_number_config(I2C1,5);
	i2c_enable(I2C1);  
    while(i2c_flag_get(I2C1, I2C_FLAG_I2CBSY))
    {
        if((I2C1_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            return I2C_FAIL;
        }
    }
	i2c_start_on_bus(I2C1);	
    /* wait until the transmit data buffer is empty */
    I2C_STAT(I2C1) |= I2C_STAT_TBE;
	I2C1_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get(I2C1, I2C_FLAG_TBE))	
    {
        if((I2C1_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            return I2C_FAIL;
        }
    }
    i2c_data_transmit(I2C1, TempMemADR_MSB);
    I2C1_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get(I2C1, I2C_FLAG_TI))
    {
        if((I2C1_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            return I2C_FAIL;
        }
    }
    i2c_data_transmit(I2C1, TempMemADR_LSB);      
    I2C1_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get(I2C1, I2C_FLAG_TI))
    {
        if((I2C1_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            return I2C_FAIL;
        }
    }
    i2c_data_transmit(I2C1, LSB);    
    I2C1_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get(I2C1, I2C_FLAG_TI))
    {
        if((I2C1_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            return I2C_FAIL;
        }
    }
    i2c_data_transmit(I2C1, MSB);
    /* send a stop condition to I2C bus */
    i2c_stop_on_bus(I2C1);
    I2C1_Timeout = I2C_SHORT_TIMEOUT;
    while(!i2c_flag_get(I2C1, I2C_FLAG_STPDET))
    {
        if((I2C1_Timeout--) == 0)
        {
            Resume_IIC_I2C1(I2C_LONG_TIMEOUT,SADR);
            return I2C_FAIL;
        }
    }
    i2c_flag_clear(I2C1, I2C_FLAG_STPDET);
    return I2C_OK; 
}






