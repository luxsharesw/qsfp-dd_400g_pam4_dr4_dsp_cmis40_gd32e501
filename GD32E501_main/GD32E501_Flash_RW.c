#include "gd32e501.h"
#include "gd32e501_fmc.h"

//-------------------------------------------------------//
// Read me
// This is Flash Bank0 only
// If you use Bank1, you need to modify the fmc_bank1 & FMC_CTL1
//-------------------------------------------------------//

uint32_t *ptrd;

uint16_t Swap_Bytes(uint16_t Data)
{
    uint16_t SWAP_TempBuffer;
    
    SWAP_TempBuffer = ( ( Data & 0xFF ) << 8 ) | (( Data >> 8 ) & 0xFF ) ;
    
    return SWAP_TempBuffer ;
}

void GDMCU_Flash_Erase(uint32_t FMC_ADR)
{
    //Step1:unlock the flash program/erase controller
    fmc_unlock();    
    //Step2: clear all pending flags 
    fmc_bank0_flag_clear(FMC_BANK0_FLAG_END | FMC_BANK0_FLAG_WPERR | FMC_BANK0_FLAG_PGERR | FMC_BANK0_FLAG_PGAERR);
    //Step3: erase the flash pages   
    fmc_page_erase(FMC_ADR);
    //Step4: clear all pending flags 
    fmc_bank0_flag_clear(FMC_BANK0_FLAG_END | FMC_BANK0_FLAG_WPERR | FMC_BANK0_FLAG_PGERR | FMC_BANK0_FLAG_PGAERR);
    //Step5: lock the main FMC after the erase operation
    fmc_lock();
}

void GDMCU_Flasg_DoubleWord(uint32_t FMC_ADR,uint64_t DoubleWord)
{  
    //Step1:unlock the flash program/erase controller
    fmc_unlock();  
    //Step2:program flash
    fmc_doubleword_program(FMC_ADR, DoubleWord);    
    fmc_bank0_flag_clear(FMC_BANK0_FLAG_END | FMC_BANK0_FLAG_WPERR | FMC_BANK0_FLAG_PGERR | FMC_BANK0_FLAG_PGAERR);
    //Step3: lock the main FMC after the erase operation
    fmc_lock();
}

void GDMCU_FLash_HIGHLOEWordWrite(uint32_t FMC_ADR,uint32_t High_WriteData,uint32_t Low_WriteData)
{ 
    //Step1:unlock the flash program/erase controller
    fmc_unlock(); 
    
    fmc_state_enum fmc_state;
    fmc_state = fmc_bank0_ready_wait(FMC_TIMEOUT_COUNT);
    /* configure program width */
    if(FMC_READY == fmc_state)
    {
        /* set the PG bit to start program */
        FMC_CTL0 |= FMC_CTL0_PG;
        REG32(FMC_ADR) = High_WriteData;
        REG32(FMC_ADR+4U) = Low_WriteData;
        /* wait for the FMC ready */
        fmc_state = fmc_bank0_ready_wait(FMC_TIMEOUT_COUNT);
        /* reset the PG bit */
        FMC_CTL0 &= ~FMC_CTL0_PG;
    }
    //Step3: lock the main FMC after the erase operation
    fmc_lock();
}

void GDMCU_FLash_WordWrite(uint32_t FMC_ADR,uint16_t Data0,uint16_t Data1,uint16_t Data2,uint16_t Data3)
{ 
    //Step1:unlock the flash program/erase controller
    fmc_unlock(); 
    
    fmc_state_enum fmc_state;
    fmc_state = fmc_bank0_ready_wait(FMC_TIMEOUT_COUNT);
    /* configure program width */
    if(FMC_READY == fmc_state)
    {
        /* set the PG bit to start program */
        FMC_CTL0 |= FMC_CTL0_PG;
        REG16(FMC_ADR)    = Data0;
        REG16(FMC_ADR+2U) = Data1;
        REG16(FMC_ADR+4U) = Data2;
        REG16(FMC_ADR+6U) = Data3;

        /* wait for the FMC ready */
        fmc_state = fmc_bank0_ready_wait(FMC_TIMEOUT_COUNT);
        /* reset the PG bit */
        FMC_CTL0 &= ~FMC_CTL0_PG;
    }
    //Step3: lock the main FMC after the erase operation
    fmc_lock();
}

void GDMCU_FMC_WRITE_FUNCTION(uint32_t FMC_ADR,uint16_t *Write_data,uint16_t DATAL)
{
    uint16_t Data_count = 0 ;
    uint16_t Dobule_word_L_Count = (DATAL/4) ;
    
    uint16_t DATA_BUFFER0,DATA_BUFFER1,DATA_BUFFER2,DATA_BUFFER3;
    
    for(Data_count=0;Data_count<Dobule_word_L_Count;Data_count++)
    {
        DATA_BUFFER0 = Swap_Bytes(*Write_data++);
        DATA_BUFFER1 = Swap_Bytes(*Write_data++);
        DATA_BUFFER2 = Swap_Bytes(*Write_data++);
        DATA_BUFFER3 = Swap_Bytes(*Write_data++);
        GDMCU_FLash_WordWrite( FMC_ADR + Data_count*8 ,DATA_BUFFER0,DATA_BUFFER1,DATA_BUFFER2,DATA_BUFFER3 );
    }
}

void GDMCU_FMC_BytesWRITE_FUNCTION(uint32_t FMC_ADR,uint8_t *Write_data,uint8_t DATAL)
{
    uint16_t Data_count = 0 ;
    uint16_t Dobule_word_L_Count = (DATAL/8) ;
    uint16_t DATA_BUFFER_H,DATA_BUFFER_L;
    uint16_t DATA_BUFFER0,DATA_BUFFER1,DATA_BUFFER2,DATA_BUFFER3;
    
   
    for(Data_count=0;Data_count<Dobule_word_L_Count;Data_count++)
    {
        DATA_BUFFER_L = *Write_data++ ;
        DATA_BUFFER_H = *Write_data++ ;
        DATA_BUFFER0  = ( DATA_BUFFER_H << 8 | DATA_BUFFER_L);
        DATA_BUFFER_L = *Write_data++ ;
        DATA_BUFFER_H = *Write_data++ ;
        DATA_BUFFER1 = ( DATA_BUFFER_H << 8 | DATA_BUFFER_L);
        DATA_BUFFER_L = *Write_data++ ;
        DATA_BUFFER_H = *Write_data++ ;
        DATA_BUFFER2 = ( DATA_BUFFER_H << 8 | DATA_BUFFER_L);
        DATA_BUFFER_L = *Write_data++ ;
        DATA_BUFFER_H = *Write_data++ ;
        DATA_BUFFER3 = ( DATA_BUFFER_H << 8 | DATA_BUFFER_L);
        GDMCU_FLash_WordWrite( FMC_ADR + Data_count*8 ,DATA_BUFFER0,DATA_BUFFER1,DATA_BUFFER2,DATA_BUFFER3 );
    }
}


uint8_t GDMCU_FMC_READ_DATA(uint32_t FMC_ADR)
{
    uint32_t *READ_ADDR;
    uint8_t READ_DATA;
    
    READ_ADDR = (uint32_t *)FMC_ADR;
    READ_DATA = *READ_ADDR;
    
    return READ_DATA;
}

void GDMCU_FMC_READ_FUNCTION(uint32_t FMC_ADR,uint8_t *DataBuffer,uint16_t DATAL)
{
    uint16_t Data_count = 0 ;
    
    for(Data_count=0;Data_count<DATAL;Data_count++)
    {
        *DataBuffer++ = GDMCU_FMC_READ_DATA( FMC_ADR + Data_count );
    }    
}


