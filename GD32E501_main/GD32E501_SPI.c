#include "gd32e501.h"
#include "GD32E501_SPI_Control.h"

#define  SET_SPI_CS_LOW()            gpio_bit_reset(GPIOA, GPIO_PIN_4)
#define  SET_SPI_CS_HIGH()           gpio_bit_set(GPIOA, GPIO_PIN_4)


uint8_t GET_SPI_Buffer[10]={0};

void SPI_CS_Contorl(uint8_t HighLow)
{
    if(HighLow)
        SET_SPI_CS_HIGH();
    else
        SET_SPI_CS_LOW();
}

uint8_t SPI_RW_byteData(uint8_t byte)
{
    // loop while data register in not empty 
    while (RESET == spi_i2s_flag_get(SPI0,SPI_FLAG_TBE));
    // send byte through the SPI0 peripheral 
    spi_i2s_data_transmit(SPI0,byte);
    // wait to receive a byte 
    while(RESET == spi_i2s_flag_get(SPI0,SPI_FLAG_RBNE));
    // return the byte read from the SPI bus 
    return(spi_i2s_data_receive(SPI0));
}

void TEST_Flash_READ_ID()
{
    // SPI Chip CS push low
    SET_SPI_CS_LOW();
    SPI_RW_byteData(0x9F);  
    // Read also needs to write the value CLK will output
    GET_SPI_Buffer[0] = SPI_RW_byteData(READ_CLK_OUT);
    GET_SPI_Buffer[1] = SPI_RW_byteData(READ_CLK_OUT);
    GET_SPI_Buffer[2] = SPI_RW_byteData(READ_CLK_OUT);
    // SPI Chip CS push high
    SET_SPI_CS_HIGH();
}



