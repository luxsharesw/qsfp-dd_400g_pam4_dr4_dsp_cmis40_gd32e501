#include "gd32e501.h"
#include "core_cm33.h"
#include "systick.h"

#define GDLDD_PC10_SCL   GPIO_PIN_10
#define GDLDD_PC11_SDA   GPIO_PIN_11

#define High     0x01
#define Low      0x00


void Delay_1us()
{
    uint16_t i;
    for(i=0;i<2;i++);
}

void Delay_us(uint16_t time_us)
{
	int i;
	for(i=0;i<time_us;i++)
		Delay_1us();
}

//-----------------------------------------------------------------------------------//
// I2C ALL CH SDA / SCL High Low Setting
//-----------------------------------------------------------------------------------//
void I2C_SDA_SCL_SET(uint8_t HighLow )
{
    if(HighLow)
    {
        // SDA & SCL High
        gpio_bit_set(GPIOC, GDLDD_PC10_SCL);
        gpio_bit_set(GPIOC, GDLDD_PC11_SDA);
    }
    else
    {
        // SDA & SCL Low
        gpio_bit_reset(GPIOC, GDLDD_PC10_SCL);
        gpio_bit_reset(GPIOC, GDLDD_PC11_SDA);    
    }
}

void I2C_SDA_SET(uint8_t HighLow )
{
    if(HighLow)
        gpio_bit_set(GPIOC, GDLDD_PC11_SDA);
    else
        gpio_bit_reset(GPIOC, GDLDD_PC11_SDA);
}

void I2C_SCL_SET(uint8_t HighLow)
{
    if(HighLow)
        gpio_bit_set(GPIOC, GDLDD_PC10_SCL);
    else
        gpio_bit_reset(GPIOC, GDLDD_PC10_SCL);
}

uint8_t I2C_GET_SDA_Status()
{
	uint8_t buffer;
    gpio_mode_set(GPIOC, GPIO_MODE_INPUT, GPIO_PUPD_NONE, GPIO_PIN_11);
    Delay_1us();
    buffer = gpio_input_bit_get(GPIOC,GDLDD_PC11_SDA) ;
    gpio_mode_set(GPIOC, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE  , GPIO_PIN_11); 
	return buffer;
}
//-----------------------------------------------------------------------------------//
//MASTER UP I2C
//-----------------------------------------------------------------------------------//
void I2C_Idle()
{
	// SDA = High
	// SCL = High
	I2C_SDA_SCL_SET( High );
	Delay_us(1);
}

void I2C_Start()
{
	Delay_1us();
	// SDA = Low
	I2C_SDA_SET( Low );
	Delay_us(2);
}

void I2C_Stop()
{
	// SDA = Low
	I2C_SDA_SET( Low );
	Delay_us(2);
	// SCL = High
	I2C_SCL_SET( High );
	Delay_us(2);
	// SDA = High
	I2C_SDA_SET( High );
	Delay_1us();
}

//-----------------------------------------------------------------------------------//
// I2C Read Byte
//-----------------------------------------------------------------------------------//
uint8_t I2C_ReadByte()
{
	uint8_t buffer=0;
	int8_t i;
	uint8_t Rec_Data=0;
    
    I2C_SDA_SET( High );
    gpio_mode_set(GPIOC, GPIO_MODE_INPUT, GPIO_PUPD_NONE, GPIO_PIN_11);     
    for(i = 0; i < 8; i++)
    {
        I2C_SCL_SET( Low );
        Delay_1us();
        Delay_1us();
		I2C_SCL_SET( High );
		Delay_1us();
		Delay_1us();

        Rec_Data <<=1;
        
        if(gpio_input_bit_get(GPIOC,GDLDD_PC11_SDA)==High)
           Rec_Data++; 
        
        I2C_SCL_SET( Low );
        Delay_1us();
        Delay_1us();
    }
    gpio_mode_set(GPIOC, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE  , GPIO_PIN_11); 
	return Rec_Data;
}

uint8_t I2C_ReadByte_ST()
{
	uint8_t buffer=0;
	int8_t i;
	uint8_t Rec_Data=0;
    gpio_mode_set(GPIOC, GPIO_MODE_INPUT, GPIO_PUPD_NONE, GPIO_PIN_11); 
	// SCL = Low ( Clock setting to Low )
	I2C_SCL_SET( Low );

	Delay_1us();
	Delay_1us();
	Delay_1us();
	Delay_1us();

	for(i=7;i>=0;i--)
	{
		Delay_1us();
		// SCL = High ( Clock setting to High for Get SDA Data )
		I2C_SCL_SET( High );
		Delay_1us();
		Delay_1us();
		// Get SDA is High or Low
        /*if(gpio_output_bit_get(GPIOC,GDLDD_PC11_SDA)==High)
            buffer = 1;
        else
            buffer = 0;*/
        buffer = gpio_input_bit_get(GPIOC,GDLDD_PC11_SDA) ;
		Rec_Data |= (buffer << i);
		//	Delay_1us();
		// SCL = Low ( Clock setting to Low )
		I2C_SCL_SET( Low );

		Delay_1us();
	}
    gpio_mode_set(GPIOC, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE  , GPIO_PIN_11);  
	return Rec_Data;
}
//-----------------------------------------------------------------------------------//
// I2C Write Byte
//-----------------------------------------------------------------------------------//
void I2C_WriteByte(uint8_t Reg_Data )
{
	int8_t i;
	uint8_t Temp_Data=Reg_Data;
	// SCL = Low ( Clock setting to Low )
	I2C_SCL_SET( Low );

	Delay_1us();
	Delay_1us();
	Delay_1us();
	Delay_1us();

	for(i=7;i>=0;i--)
	{
		Temp_Data = ((Reg_Data >> i) & 0x01);
		if(Temp_Data)
			I2C_SDA_SET( High );
		else
			I2C_SDA_SET( Low );
		//	Delay_1us();
		// SCL = High ( Clock setting )
		I2C_SCL_SET( High );
		Delay_1us();
		Delay_1us();
		// SCL = Low ( Clock setting to Low )
		I2C_SCL_SET( Low );
		Delay_1us();
	}
}
//-------------------------------------------------------//
// UP I2C nACK / ACK
// Master_SDA_UP
// Master_SCL_UP
// TX CH4 - CH7 MAOM38053 UP AFSI-N74C4S2
// RX CH0 - CH3 MASC38040 UP AFSI-R74C4S1
//-------------------------------------------------------//
void I2C_nACK_Master()
{
	Delay_1us();
	// SDA = High
	I2C_SDA_SET( High );
	Delay_1us();
	// SCL = High
	I2C_SCL_SET( High );
	Delay_1us();
	Delay_1us();
	// SCL = Low
	I2C_SCL_SET( Low );
	Delay_1us();
	// SDA = Low
	I2C_SDA_SET( Low );
	Delay_1us();
}

void I2C_ACK_Slave()
{
	Delay_1us();
	// SCL = High
	I2C_SCL_SET( High );
	Delay_1us();
	Delay_1us();

	if( I2C_GET_SDA_Status() )
	{
		// SCL = Low
		I2C_SCL_SET( Low );
		Delay_1us();
		// SDA = Low
		I2C_SDA_SET( Low );
		Delay_1us();
	}
	else
	{
		// SCL = Low
		I2C_SCL_SET( Low );
		Delay_1us();
		// SDA = High
		I2C_SDA_SET( High );
		Delay_1us();
	}
}

void I2C_MRSlave_Ack()
{
	// SDA = Low
	I2C_SDA_SET( Low );
	// SCL = High
	I2C_SCL_SET( High );
	Delay_1us();
	Delay_1us();
	// SCL = Low
	I2C_SCL_SET( Low );
	// SDA = High
	I2C_SDA_SET( High );
}

void I2C_MRSlave_NAck()
{
	// SDA = High
	I2C_SDA_SET( High );
	// SCL = High
	I2C_SCL_SET( High );
	Delay_1us();
	Delay_1us();
	// SCL = Low
	I2C_SCL_SET( Low );
}

//-------------------------------------------------------//
// Master I2C Read/Write Function
// I2C_Idle
// I2C_Start
// I2C_Stop
// I2C_ReadByte
// I2C_WriteByte
// I2C_nACK_Master
// I2C_ACK_Slave
// I2C_MRSlave_Ack
// I2C_MRSlave_NAck
//-------------------------------------------------------//
uint8_t Master_I2C_PC1011_ReadByte(uint8_t Slave_address,uint8_t Regcommand_Address)
{
	uint8_t Temp_Data;
	I2C_Idle();
	I2C_Start();
	I2C_WriteByte(Slave_address);		// Write
	I2C_ACK_Slave();

	I2C_WriteByte(Regcommand_Address);
	I2C_ACK_Slave();
	// SCL = High
	I2C_SCL_SET( High );
	Delay_1us();
	I2C_Idle();

	I2C_Start();
	I2C_WriteByte( Slave_address + 1 );		// Read
	I2C_ACK_Slave();

	Temp_Data = I2C_ReadByte();
	I2C_nACK_Master();

	I2C_Stop();
	I2C_Idle();
	return Temp_Data;
}

void Master_I2C_PC1011_WriteByte(uint8_t Slave_address,uint8_t Regcommand_Address,uint8_t SendData)
{
	I2C_Idle();
	I2C_Start();
	I2C_WriteByte(Slave_address);		// Write
	I2C_ACK_Slave();

	I2C_WriteByte(Regcommand_Address);
	I2C_ACK_Slave();

	I2C_WriteByte(SendData);
	I2C_ACK_Slave();
	I2C_Stop();
	I2C_Idle();
}







