#include "gd32e501.h"
#include "core_cm33.h"
#include "systick.h"
#include "CMIS_MSA.h"
#include "Calibration_Struct.h"
#include "Calibration_Struct_2.h"
#include "GD_FlahMap.h"
#include "GD32E501_GPIO_Customize_Define.h"
#include "GD32E501_M_I2C1_PB3_PB4.h"
#include "GD32E501_M_I2C2_PC78.h"
#include "LDD_Control.h"
#include "TIA_Control.h"
#include "TEC_APC_Control.h"
//--------------------------------------------------------//
// Chip ICs .h                                            //
//--------------------------------------------------------//
#include "DSP.h"

uint8_t RESET_Count = 0;
uint16_t Power_check_flag = 0;

void Delay_for_Loop(uint16_t DataCount)
{
    uint16_t IIcount,JJcount;
    for(IIcount=0;IIcount<DataCount;DataCount++)
        for(JJcount=0;JJcount<3000;JJcount++);
}

void CheckSum_Calculate()
{
	uint16_t TEMP_BUF=0;
	uint32_t IIcount;
    uint16_t CHECKSUM = 0;

	for( IIcount=0x08000000 ; IIcount < MCU_Code_End ; IIcount++ )
	{
		TEMP_BUF = GDMCU_FMC_READ_DATA(IIcount);
		CHECKSUM = CHECKSUM + TEMP_BUF ;
	}
	CALIB_MEMORY_MAP.CHECKSUM_V = Swap_Bytes( CHECKSUM );
	CALIB_MEMORY_MAP.FW_VERSION = Swap_Bytes(MCU_FW_VERSION); 
    CALIB_MEMORY_MAP.CheckSum_EN = 0x00 ;
    GDMCU_Flash_Erase(FS_Cal0_P90);
	GDMCU_FMC_BytesWRITE_FUNCTION( FS_Cal0_P90  , &CALIB_MEMORY_MAP.VCC_SCALEM    , 128 );
}

void Internal_Voltage_Power_Down()
{
    // DSP & TRX Power Down
    P3V3_DSP_PowerDown();
    P3V3_RX_PowerDown();
    P3V3_TX_PowerDown();   
    // LDD & TIA Power Down
    P3V3_OERF_EN_Low();
    P3V6_REBIAS_EN_Low();
    P3V6_REMODE_EN_Low();
    MP5490_LDEN_Low();
    MP5490_RESET_High();
    P3V6_DRV_EN_Low();
    P1V8_LB_EN_Low();
    // Acacia TIA & LDD Reset
    TIA_CH12_RESET_Low();
    TIA_CH34_RESET_Low();
    DRV_RESET_Low();
    RESET_L_DSP_Low();    
    OSCEN_Pin_Low();
    delay_1ms(100);
    
//    P3V3_RX_PowerUp();
//    delay_1ms(10);
}

void Internal_Voltage_Power_up_Seq()
{    
    P3V3_DSP_PowerUp(); 
    delay_1ms(10);  
    P3V3_RX_PowerUp();
    delay_1ms(10);
    P3V3_TX_PowerUp();
    delay_1ms(10);
    P3V3_OERF_EN_High();
    delay_1ms(1);
    P3V6_REBIAS_EN_High();
    delay_1ms(1);
    P3V6_REMODE_EN_High();
    delay_1ms(1);
    P3V6_DRV_EN_High();
    delay_1ms(1);
    P1V8_LB_EN_High();
    delay_1ms(5);
    MP5490_LDEN_High();
    delay_1ms(1);
    MP5490_RESET_Low();
    delay_1ms(1);
    // Acacis TIA & LDD Enable
    TIA_CH12_RESET_High();
    delay_1ms(1);
    TIA_CH34_RESET_High();
    delay_1ms(1);
    DRV_RESET_High();
    delay_1ms(1);
}

void DSP_PowerON_Seq_Control()
{
    // OSC Enable ( High active )
    OSCEN_Pin_High();
    delay_1ms(5);
    // DSP RESET is Low
    RESET_L_DSP_Low();
    delay_1ms(100);
    // MODESELB Low is I2C intface 
    MODSELB_DSP_Low();
    delay_1ms(5);
    // DSP LPMODE ( High active )
    LPMODE_DSP_High();
    //DSP unReset , DSP Download SPI EEPROM Image
    RESET_L_DSP_High();
    delay_1ms(100);
}

void PowerOn_Sequencing_Control()
{
    // Device RESET POR
    Internal_Voltage_Power_Down();
    // Device Power on POR
    //Internal_Voltage_Power_up_Seq();
    // DSP Power on for Spi eepeorm fw upgrade
    //DSP_PowerON_Seq_Control();
}

void LPMDOE_POWER_OFF()
{
    Internal_Voltage_Power_Down();
}

void LPMODE_POWER_ON()
{
    // Device Power on POR
    Internal_Voltage_Power_up_Seq();
    // DSP Power on for Spi eepeorm fw upgrade
    DSP_PowerON_Seq_Control();
}


uint8_t Get_Power_C_Status()
{
	// bit 0 P3V3_DSP_EN_GPIOA
	// bit 1 P3V3_RX_EN_GPIOA
	// bit 2 P3V3_TX_EN_GPIOA
	// bit 3 P1V8_TX_EN_GPIOB
	// bit 4 OSC_EN_GPIOC
	// bit 5 RESET_L_DSP_GPIOB
	// bit 6 LPMODE_DSP_GPIOB
	// bit 7 MODSELB_DSP_GPIOB
	uint8_t Power_Status = 0 ;
	uint8_t Temp_data = 0 ;

	Temp_data = gpio_output_bit_get(GPIOA, P3V3_DSP_EN_GPIOA) ;
	Power_Status = Power_Status + Temp_data ;
	Temp_data = gpio_output_bit_get(GPIOA, P3V3_RX_EN_GPIOA) ;
	Power_Status = Power_Status + ( Temp_data << 1 );
	Temp_data = gpio_output_bit_get(GPIOA, P3V3_TX_EN_GPIOA) ;
	Power_Status = Power_Status + ( Temp_data << 2 );
	Temp_data = gpio_output_bit_get(GPIOF, P1V8_LB_EN_GPIOF) ;
	Power_Status = Power_Status + ( Temp_data << 3 );   
	Temp_data = gpio_output_bit_get(GPIOC, OSC_EN_GPIOC) ;
	Power_Status = Power_Status + ( Temp_data << 4 ); 
	Temp_data = gpio_output_bit_get(GPIOB, RESET_L_DSP_GPIOB) ;
	Power_Status = Power_Status + ( Temp_data << 5 );    
	Temp_data = gpio_output_bit_get(GPIOB, LPMODE_DSP_GPIOB) ;
	Power_Status = Power_Status + ( Temp_data << 6 );   
	Temp_data = gpio_output_bit_get(GPIOB, MODSELB_DSP_GPIOB) ;
	Power_Status = Power_Status + ( Temp_data << 7 );

	return Power_Status;
}

void SET_Power_Control(uint8_t SET_Value)
{    
    // bit 0 P3V3_DSP_EN_GPIOA
	// bit 1 P3V3_RX_EN_GPIOA
	// bit 2 P3V3_TX_EN_GPIOA
	// bit 3 P1V8_TX_EN_GPIOB
	// bit 4 OSC_EN_GPIOC
	// bit 5 RESET_L_DSP_GPIOB
	// bit 6 LPMODE_DSP_GPIOB
	// bit 7 MODSELB_DSP_GPIOB
	if( ( SET_Value & 0x01 ) == 0x01 )
		P3V3_DSP_PowerUp();
	else
		P3V3_DSP_PowerDown();

	if( ( SET_Value & 0x02 ) == 0x02 )
		P3V3_RX_PowerUp();
	else
		P3V3_RX_PowerDown();

	if( ( SET_Value & 0x04 ) == 0x04 )
		P3V3_TX_PowerUp();
	else
		P3V3_TX_PowerDown();
    
	if( ( SET_Value & 0x08 ) == 0x08 )
		P1V8_LB_EN_High();
	else
		P1V8_LB_EN_Low();
    
	if( ( SET_Value & 0x10 ) == 0x10 )
		OSCEN_Pin_High();
	else
		OSCEN_Pin_Low();

	if( ( SET_Value & 0x20 ) == 0x20 )
		RESET_L_DSP_High();
	else
		RESET_L_DSP_Low();

	if( ( SET_Value & 0x40 ) == 0x40 )
		LPMODE_DSP_High();
	else
		LPMODE_DSP_Low();

	if( ( SET_Value & 0x80 ) == 0x80 )
		MODSELB_DSP_High();
	else
		MODSELB_DSP_Low();
}

void MCU_READ_WRITE_DEIVCE_COMMAND_CONTROL()
{
    TEST_DSP_Command_Direct_Control();
	// Power Pin Control
	if( CALIB_MEMORY_1_MAP.Power_SET_Start == 0xAA )
	{
		CALIB_MEMORY_1_MAP.Power_SET_Start = 0x00;
		SET_Power_Control( CALIB_MEMORY_1_MAP.Power_SET_Value );
	}
    // MCU Flash CheckSum Calculation
	if( CALIB_MEMORY_MAP.CheckSum_EN == 0xAA )
	{
        i2c_disable(I2C0); 
		CheckSum_Calculate();
		CALIB_MEMORY_MAP.CheckSum_EN = 0x00 ;
        i2c_enable(I2C0);
	}
    
    if( DSP_Direct_Control_MEMORY_MAP.Trigger_CMD == 0xAA )
	{
		Trigger_CMD_Update_DSP_REG();
		DSP_Direct_Control_MEMORY_MAP.Trigger_CMD = 0x00 ;
	}

    if( CALIB_MEMORY_MAP.Error_Flag == 0xAA )
    {
        GetDebug_SP_data(CALIB_MEMORY_MAP.DEBUG_TEMP1);
        CALIB_MEMORY_MAP.Error_Flag = 0x00;
    }
    else if( CALIB_MEMORY_MAP.Error_Flag == 0xBB )
    {
        Acacia_TIA_GetSP();
        CALIB_MEMORY_MAP.Error_Flag = 0x00;
    }
    else if( CALIB_MEMORY_MAP.Error_Flag == 0xCC )
    {
        // Reset all device
        Device_RESET();
        CALIB_MEMORY_MAP.Error_Flag = 0x00;
    }
    else if( CALIB_MEMORY_MAP.Error_Flag == 0xDD )
    {
        // Disable Reset device 
        Device_UnReset();
        CALIB_MEMORY_MAP.Error_Flag = 0x00;
    }
    else if( CALIB_MEMORY_MAP.Error_Flag == 0x55 )
    {
        GetSP_LawData(CALIB_MEMORY_MAP.DEBUG_TEMP1,0);
        CALIB_MEMORY_MAP.Error_Flag = 0x00;
    }
    else if( CALIB_MEMORY_MAP.Error_Flag == 0x66 ) 
    {
        GetSP_LawData(CALIB_MEMORY_MAP.DEBUG_TEMP1,1);
        CALIB_MEMORY_MAP.Error_Flag = 0x00;
    }
    else if( CALIB_MEMORY_MAP.Error_Flag == 0x77 ) 
    {
        Update_dia_temp();
        CALIB_MEMORY_MAP.Error_Flag = 0x00;
    }

}

// PC15 Digital OP  - ModselL_G
void ModSelL_Function()
{
	if( gpio_input_bit_get(GPIOC, ModselL_G_GPIOC) == 0 )
		i2c_enable(I2C0);
	else
		i2c_disable(I2C0);  
}


void PowerOn_Reset_Check()
{
    RESET_Count = 0 ;
    //ResetL Function
    if( gpio_input_bit_get(GPIOF, RESETL_G_GPIOF) == 0)
    {
        while(1)
        {
            if( gpio_input_bit_get(GPIOF, RESETL_G_GPIOF) == 1)  
                RESET_Count ++;
            if(RESET_Count>10)
                break;
        }
    }
}

void RESET_L_Function()
{
    if( gpio_input_bit_get(GPIOF, RESETL_G_GPIOF) == 0)
        RESET_Count ++ ;
    else
        RESET_Count = 0 ;
    
    if( RESET_Count > 10 )
        nvic_system_reset();      
}

uint8_t LPMODE_Function()
{
    uint8_t feedback_IO_status;
    uint16_t High_Count = 0;
    uint16_t Low_Count = 0;
    
    while(1)
    {
        if(gpio_input_bit_get(GPIOF, LPMODE_G_GPIOF))
        {
            High_Count++;
            Low_Count = 0;
        }
        else
        {
            High_Count = 0;
            Low_Count++;
        }
        
        if(Power_check_flag)
        {
            if( (High_Count>500) || (Low_Count>500))
            {
                Power_check_flag = 0;
                break;
            }
        }
        else
        {
            if( (High_Count>10) || (Low_Count>10 ))//30
                break;
        }
    }
    
    if(High_Count>=10)
        feedback_IO_status = 1;
    else if(Low_Count>=10)
        feedback_IO_status = 0;
    
    return feedback_IO_status;
}
