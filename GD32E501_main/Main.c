#include "gd32e501.h"
#include "core_cm33.h"
#include "systick.h"
#include "string.h"
#include "GD_FlahMap.h"
#include "DSP.h"
#include "Calibration_Struct.h"
#include "Calibration_Struct_2.h"
#include "CMIS_MSA.h"
#include "LDD_Control.h"
#include "TIA_Control.h"
#include "TEC_APC_Control.h"
//V0148:修改Page10 config-error控制，增加9700交换机控制，修改DataPath statemachine,添加tec Updata_DAC_Current限制，太大或太小初始化Updata_DAC_Current为中间值；
//231124：增加P3读写功能，增加从0x7E地址开始切换page功能；

int main()
{
	  uint8_t TEC_count = 0;
    // GD MCU Initialize
    GD32E501_Power_on_Initial();
    // Power on timing control
    PowerOn_Sequencing_Control();
    // Power on HW RESET PIN Check
    PowerOn_Reset_Check();
    // Flash & SRAM Upload
    PowerON_Table_Init();
    // Power ready
    delay_1ms(100);
	
	  
    LPMODE_POWER_ON();
    // LDD Initiialize  
    MP5490_Write_Control_Data();
	  LDD_Trun_on();
    Acacia_LDD_Write_ALL();
    DAC_TO_TOPS_SETTING();
    // TIA Initilaize     
    Acacia_TIA_Write_ALL();
    
    for(TEC_count=0;TEC_count<50;TEC_count++)
    {
        TEC_Control(0);
        TEC_Control(1);
        TEC_Control(2);
        TEC_Control(3);
    }
		
    while(1)
    {
        // I2C Enable Control
        ModSelL_Function();
        // MSA StateMachine Control
        QSFPDD_MSA_StateMachine(); 
			  // CMIS DataPath State Machine
			  MSA_DataPath_StateMachine();
        // Luxshare-OET internal control
        MCU_READ_WRITE_DEIVCE_COMMAND_CONTROL();
        // Move Sram & Flash read/write
        SRMA_Flash_Function();
        // Report SW and HW IntL Status By all of interrupt Flags
		if(IntL_Trigger_Flag)
		{
			IntL_Function();
			IntL_Trigger_Flag=0;
		}
    }
}

