#include "gd32e501.h"
#include "core_cm33.h"
#include "systick.h"
#include "system_gd32e501.h"

__IO uint32_t prescaler_a = 0, prescaler_s = 0;
//----------------------------------------//
// Rtc pre Config
//----------------------------------------//
void rtc_pre_config(void)
{
    rcu_rtc_clock_config(RCU_RTCSRC_IRC40K);
    prescaler_s = 0x18F;
    prescaler_a = 0x63; 
    rcu_periph_clock_enable(RCU_RTC);   
    rtc_register_sync_wait();    
}
//----------------------------------------//
// Ruc periph clock initialize
//----------------------------------------//
void irc40k_config(void)
{
    /* enable IRC40K */
    rcu_osci_on(RCU_IRC40K);
    /* wait till IRC40K is ready */
    rcu_osci_stab_wait(RCU_IRC40K);
}

void GD_System_Periph_Clock_Enable()
{
    //System CLK 
    systick_config();
    irc40k_config();
    // GPIO CLK
    rcu_periph_clock_enable(RCU_GPIOA);
    rcu_periph_clock_enable(RCU_GPIOB);
    rcu_periph_clock_enable(RCU_GPIOC);
    rcu_periph_clock_enable(RCU_GPIOF);
    // CFGCMP CLK
    rcu_periph_clock_enable(RCU_CFGCMP);
    // ADC CLK
    rcu_periph_clock_enable(RCU_ADC);
    rcu_adc_clock_config(RCU_ADCCK_APB2_DIV6);
    // DAC CLK for FR4 
    rcu_periph_clock_enable(RCU_DAC0);
    rcu_periph_clock_enable(RCU_DAC1);
    //rcu_periph_clock_enable(RCU_DAC2);
    //rcu_periph_clock_enable(RCU_DAC3);
    // Interface rcu clk enable 
    rcu_periph_reset_enable(RCU_I2C1RST);  
    rcu_periph_reset_disable(RCU_I2C1RST);    
    rcu_periph_reset_enable(RCU_I2C2RST);
    rcu_periph_reset_disable(RCU_I2C2RST);   
    rcu_periph_clock_enable(RCU_I2C1);
    rcu_periph_clock_enable(RCU_I2C2);
    // SPI Clock
    //rcu_periph_clock_enable(RCU_SPI0);
}
//----------------------------------------//
// GPIO initialize
// Digital op
//----------------------------------------//
// PC4  Digital input OP  - MDC_DSP
// PC5  Digital input OP  - MDIO_DSP
// PC9  Digital input OP  - IntL_G
// PC15 Digital input OP  - ModselL_G
// PF1  Digital input OP  - RESETL_G
// PF0  Digital input OP  - LPMODE_G
void GD32E501_GPIO_OP_initial()
{
    // GD MCU GPIO C  
    gpio_mode_set(GPIOC, GPIO_MODE_INPUT, GPIO_PUPD_NONE, GPIO_PIN_4);    
    gpio_mode_set(GPIOC, GPIO_MODE_INPUT, GPIO_PUPD_NONE, GPIO_PIN_5); 
    gpio_mode_set(GPIOC, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO_PIN_9);    
    gpio_mode_set(GPIOC, GPIO_MODE_INPUT, GPIO_PUPD_NONE, GPIO_PIN_15); 
    // GD MCU GPIO F       
    gpio_mode_set(GPIOF, GPIO_MODE_INPUT, GPIO_PUPD_NONE, GPIO_PIN_0);  
    gpio_mode_set(GPIOF, GPIO_MODE_INPUT, GPIO_PUPD_NONE, GPIO_PIN_1);     
}
//----------------------------------------//
// GPIO initialize
// Digital pp
//----------------------------------------//
// PA8  Digital OUTPUT PP  - TIA_CH12_RESET
// PA9  Digital OUTPUT PP  - TIA_CH34_RESET
// PA10 Digital OUTPUT PP  - DRV_RESET
// PA11 Digital OUTPUT PP  - P3V3_DSP_EN
// PA12 Digital OUTPUT PP  - P3V3_RX_EN
// PA15 Digital OUTPUT PP  - P3V3_TX_EN

// PB10 Digital OUTPUT PP  - LOS_To_DSP
// PB11 Digital OUTPUT PP  - P3V3_OERF_EN
// PB12 Digital OUTPUT PP  - MODSELB DSP
// PB13 Digital OUTPUT PP  - RESET_L DSP
// PB14 Digital INPUT  PP  - LASI_DSP/Interrupt
// PB15 Digital OUTPUT PP  - INITMODE_DSP/LPMODE

// PC6  Digital OUTPUT PP  - OSC_EN
// PC10 Digital OUTPUT PP  - P3V6_REBASI_EN
// PC11 Digital OUTPUT PP  - P3V6_REMODE_EN
// PC12 Digital OUTPUT PP  - MP5490_LDEN
// PC13 Digital OUTPUT PP  - MP5490_RESER
// PF6  Digital OUTPUT PP  - P3V6_DRV_EN
// PF7  Digital OUTPUT PP  - P1V8_LB_EN

void GD32E501_GPIO_PP_initial()
{
    // GD MCU GPIO A 
    gpio_mode_set(GPIOA, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLUP, GPIO_PIN_8); 
    gpio_mode_set(GPIOA, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLUP, GPIO_PIN_9); 
    gpio_mode_set(GPIOA, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLUP, GPIO_PIN_10); 
    gpio_mode_set(GPIOA, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLDOWN, GPIO_PIN_11); 
    gpio_mode_set(GPIOA, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLDOWN, GPIO_PIN_12);   
    gpio_mode_set(GPIOA, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLDOWN, GPIO_PIN_15);
    // GD MCU GPIO B  
    gpio_mode_set(GPIOB, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLUP, GPIO_PIN_10); 
    gpio_mode_set(GPIOB, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLUP, GPIO_PIN_11);
    gpio_mode_set(GPIOB, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLUP, GPIO_PIN_12);
    gpio_mode_set(GPIOB, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLDOWN, GPIO_PIN_13); 
    gpio_mode_set(GPIOB, GPIO_MODE_INPUT,  GPIO_PUPD_PULLUP, GPIO_PIN_14);
    gpio_mode_set(GPIOB, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLUP, GPIO_PIN_15);  
    // GD MCU GPIO C         
    gpio_mode_set(GPIOC, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLDOWN, GPIO_PIN_6);  
    gpio_mode_set(GPIOC, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLUP, GPIO_PIN_10);   
    gpio_mode_set(GPIOC, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLUP, GPIO_PIN_11);  
    gpio_mode_set(GPIOC, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLUP, GPIO_PIN_12);
    gpio_mode_set(GPIOC, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLUP, GPIO_PIN_13);       
    // GD MCU GPIO F 
    gpio_mode_set(GPIOF, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLUP, GPIO_PIN_6);
    gpio_mode_set(GPIOF, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLUP, GPIO_PIN_7);    
}

//----------------------------------------//
// GPIO initialize
// Analogy
//----------------------------------------//
// PA0  Analogy     - TX1DMPD          - ADC_IN0
// PA1  Analogy     - TX2DMPD          - ADC_IN1
// PA2  Analogy     - TX3DMPD          - ADC_IN2
// PA3  Analogy     - TX4DMPD          - ADC_IN3
// PB0  Analogy     - P3V3_RX_MON      - ADC_IN8
// PB1  Analogy     - REF_D3_BIAS      - ADC_IN9
// PB2  Analogy     - REF_D4_BIAS      - ADC_IN4
// PB5  Analogy     - P1V8_DSP         - ADC_IN7
// PC0  Analogy     - TX1MPDANODE      - ADC_IN10
// PC1  Analogy     - TX2MPDANODE      - ADC_IN11
// PC2  Analogy     - TX3MPDANODE      - ADC_IN12
// PC3  Analogy     - TX4MPDANODE      - ADC_IN13
// PF4  Analogy     - REFD1_BIAS       - ADC_IN14
// PF5  Analogy     - REFD2_BIAS       - ADC_IN15
void GD32E501_GPIO_Analogy_initial()
{
    // GD MCU GPIO A
    gpio_mode_set(GPIOA, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO_PIN_0);
    gpio_mode_set(GPIOA, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO_PIN_1);
    gpio_mode_set(GPIOA, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO_PIN_2);
    gpio_mode_set(GPIOA, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO_PIN_3);
    // GD MCU GPIO B
    gpio_mode_set(GPIOB, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO_PIN_0); 
    gpio_mode_set(GPIOB, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO_PIN_1); 
    gpio_mode_set(GPIOB, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO_PIN_2); 
    gpio_mode_set(GPIOB, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO_PIN_5); 
    // GD MCU GPIO C
    gpio_mode_set(GPIOC, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO_PIN_0);
    gpio_mode_set(GPIOC, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO_PIN_1);
    gpio_mode_set(GPIOC, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO_PIN_2);
    gpio_mode_set(GPIOC, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO_PIN_3);    
    // GD MCU GPIO F
    gpio_mode_set(GPIOF, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO_PIN_4);
    gpio_mode_set(GPIOF, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO_PIN_5);
}
//-----------------------------------------------//
// Interface
// I2C
//-----------------------------------------------//
// I2C  Master
// PB3  Digital OP  - SCL_LDD
// PB4  Digital OP  - SDA_LDD
// PC7  Digital OP  - SCL_TIA
// PC8  Digital OP  - SDA_TIA
void Master_I2C1_PB34_Initial()
{  
    /* connect PB3 to I2C1_SCL */
    gpio_af_set(GPIOB, GPIO_AF_7, GPIO_PIN_3);
    /* connect PB4 to I2C1_SDA */
    gpio_af_set(GPIOB, GPIO_AF_7, GPIO_PIN_4);    
    /* configure  I2C2 GPIO */
    gpio_mode_set(GPIOB, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO_PIN_3);
    gpio_output_options_set(GPIOB, GPIO_OTYPE_OD, GPIO_OSPEED_50MHZ, GPIO_PIN_3); 
    gpio_mode_set(GPIOB, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO_PIN_4);
    gpio_output_options_set(GPIOB, GPIO_OTYPE_OD, GPIO_OSPEED_50MHZ, GPIO_PIN_4);
     
    /* configure I2C timing */
    i2c_timing_config(I2C1,0,0x9,0);
    i2c_master_clock_config(I2C1,0x78,0x78);
    i2c_address_config(I2C1,0x82,I2C_ADDFORMAT_7BITS);
    /* send slave address to I2C bus */
    //i2c_master_addressing(I2C1,Device_ADR,I2C_MASTER_TRANSMIT);
    /* enable I2Cx */
    i2c_enable(I2C1);	   
}

void Master_I2C2_PC78_Initial()
{    
    /* connect PB10 to I2C1_SCL */
    gpio_af_set(GPIOC, GPIO_AF_1, GPIO_PIN_7);
    /* connect PB11 to I2C1_SDA */
    gpio_af_set(GPIOC, GPIO_AF_1, GPIO_PIN_8);    
    /* configure  I2C2 GPIO */
    gpio_mode_set(GPIOC, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO_PIN_7);
    gpio_output_options_set(GPIOC, GPIO_OTYPE_OD, GPIO_OSPEED_50MHZ, GPIO_PIN_7);
    gpio_mode_set(GPIOC, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO_PIN_8);
    gpio_output_options_set(GPIOC, GPIO_OTYPE_OD, GPIO_OSPEED_50MHZ, GPIO_PIN_8);   
    /* configure I2C timing */
    i2c_timing_config(I2C2,0,0x9,0);
    i2c_master_clock_config(I2C2,0x78,0x78);
    /* configure I2C timing 800K HZ  */
    //i2c_timing_config(I2C2,0,0x5,0);
    //i2c_master_clock_config(I2C2,0x2B,0x4F);
    i2c_address_config(I2C2,0x82,I2C_ADDFORMAT_7BITS);
    /* send slave address to I2C bus */
    //i2c_master_addressing(I2C2,0xA0,I2C_MASTER_TRANSMIT);
    /* enable I2Cx */
    i2c_enable(I2C2);	
}
// I2C Slave
// PB6  Digital OP  - SCL_G
// PB7  Digital OP  - SDA_G
void Slave_I2C0_PB67_Initial()
{
    rcu_periph_reset_enable(RCU_I2C0RST);
    rcu_periph_reset_disable(RCU_I2C0RST);
    /* enable GPIOB clock */
    /* enable I2C0 clock */
    rcu_periph_clock_enable(RCU_I2C0);
    /* connect PB6 to I2C0_SCL */
    gpio_af_set(GPIOB, GPIO_AF_1, GPIO_PIN_6);
    /* connect PB7 to I2C0_SDA */
    gpio_af_set(GPIOB, GPIO_AF_1, GPIO_PIN_7);
    /* configure GPIO pins of I2C0 */
    gpio_mode_set(GPIOB, GPIO_MODE_AF, GPIO_PUPD_PULLUP, GPIO_PIN_6);
    gpio_output_options_set(GPIOB, GPIO_OTYPE_OD, GPIO_OSPEED_50MHZ, GPIO_PIN_6);
    gpio_mode_set(GPIOB, GPIO_MODE_AF, GPIO_PUPD_PULLUP, GPIO_PIN_7);
    gpio_output_options_set(GPIOB, GPIO_OTYPE_OD, GPIO_OSPEED_50MHZ, GPIO_PIN_7);
    
    /* configure I2C timing , delay time to max*/
    i2c_timing_config(I2C0,0,0x0F,0);
    
    /* configure I2C address */
    i2c_address_config(I2C0, 0xA0, I2C_ADDFORMAT_7BITS);
    /* configure number of bytes to be transferred */
    /* enable I2C reload mode */
    i2c_reload_enable(I2C0);
    // Setting Nvic irq
    nvic_irq_enable(I2C0_EV_IRQn, 2);
    nvic_irq_enable(I2C0_ER_IRQn, 1);
    
    i2c_idle_clock_timeout_config( I2C0 , BUSTOA_DETECT_IDLE );
    
    /* enable the I2C0 interrupt */
    i2c_interrupt_enable(I2C0, I2C_INT_ERR | I2C_INT_STPDET | I2C_INT_ADDM | I2C_INT_RBNE | I2C_INT_TI);
}

void ADC_Config_Initial()
{
    /* ADC temperature and Vrefint enable */
    adc_tempsensor_vrefint_enable();
    /* ADC continuous function enable */
    adc_special_function_config(ADC_SCAN_MODE, ENABLE);
    /* ADC trigger config */
    adc_external_trigger_source_config(ADC_INSERTED_CHANNEL, ADC_EXTTRIG_INSERTED_NONE); 
    /* ADC data alignment config */
    adc_data_alignment_config(ADC_DATAALIGN_RIGHT);
    /* ADC channel length config */
    adc_channel_length_config(ADC_INSERTED_CHANNEL, 0U);
    /* ADC external trigger enable */
    adc_external_trigger_config(ADC_INSERTED_CHANNEL, ENABLE);    
    /* enable ADC interface */
    adc_enable();  
    
    delay_1ms(1U);
    /* ADC calibration and reset calibration */
    adc_calibration_enable();
}

void DAC_Config_Initial()
{
    // PA4/PA5  DAC0
    dac_deinit(DAC0);
    /* configure the DAC0_OUT0 */
    dac_trigger_disable(DAC0, DAC_OUT_0);
    dac_wave_mode_config(DAC0, DAC_OUT_0, DAC_WAVE_DISABLE);
    dac_output_buffer_enable(DAC0, DAC_OUT_0);    
    /* configure the DAC0_OUT1 */
    dac_trigger_disable(DAC0, DAC_OUT_1);
    dac_wave_mode_config(DAC0, DAC_OUT_1, DAC_WAVE_DISABLE);
    dac_output_buffer_enable(DAC0, DAC_OUT_1);  
    /* enable DAC concurrent mode and set data */
    dac_concurrent_enable(DAC0);
    dac_concurrent_data_set(DAC0, DAC_ALIGN_12B_R, 0x0000 , 0x0000 );
    
    // PA6/PA7  DAC1
    dac_deinit(DAC1);
    /* configure the DAC0_OUT0 */
    dac_trigger_disable(DAC1, DAC_OUT_0);
    dac_wave_mode_config(DAC1, DAC_OUT_0, DAC_WAVE_DISABLE);
    dac_output_buffer_enable(DAC1, DAC_OUT_0);    
    /* configure the DAC0_OUT1 */
    dac_trigger_disable(DAC1, DAC_OUT_1);
    dac_wave_mode_config(DAC1, DAC_OUT_1, DAC_WAVE_DISABLE);
    dac_output_buffer_enable(DAC1, DAC_OUT_1);  
    /* enable DAC concurrent mode and set data */
    dac_concurrent_enable(DAC1);
    dac_concurrent_data_set(DAC1, DAC_ALIGN_12B_R, 0x0000 , 0x0000 ); 
}
//----------------------------------------//
// GD32E501 SPI0 Initial
//----------------------------------------//
void SPI0_Initial()
{
    spi_parameter_struct spi_init_struct;
    // SPI0 GPIO config: CS/PA4 SCK/PA5, MISO/PA6, MOSI/PA7 
    gpio_mode_set(GPIOA, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLUP, GPIO_PIN_4); 
    gpio_af_set(GPIOA, GPIO_AF_0, GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7);
    gpio_mode_set(GPIOA, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7);
    gpio_output_options_set(GPIOA, GPIO_OTYPE_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7);
    
    rcu_periph_clock_enable(RCU_SPI0);
    // deinitilize SPI and the parameters 
    spi_i2s_deinit(SPI0);
    spi_struct_para_init(&spi_init_struct);    
    // SPI0 parameter config 
    spi_init_struct.trans_mode           = SPI_TRANSMODE_FULLDUPLEX;
    spi_init_struct.device_mode          = SPI_MASTER;
    spi_init_struct.frame_size           = SPI_FRAMESIZE_8BIT;
    spi_init_struct.clock_polarity_phase = SPI_CK_PL_HIGH_PH_2EDGE;
    spi_init_struct.nss                  = SPI_NSS_SOFT;
    spi_init_struct.prescale             = SPI_PSC_8;
    spi_init_struct.endian               = SPI_ENDIAN_MSB;
    spi_init(SPI0, &spi_init_struct);
    
    spi_enable(SPI0);
}

//----------------------------------------//
// GD32E501 Power On Initial
//----------------------------------------//
void GD32E501_Power_on_Initial()
{
    // Enable SR4 Function periph clk
    // GPIO , ADC , I2C , DAC , CFGCMP , SPI
    GD_System_Periph_Clock_Enable();
    // GPIo initial
    GD32E501_GPIO_OP_initial();
    GD32E501_GPIO_PP_initial();
    GD32E501_GPIO_Analogy_initial();
    // User internal vref
    syscfg_vref_enable();
    // User external vref 
    // disable internal vref
    //syscfg_vref_disable();
    // ADC Initialize
    ADC_Config_Initial();
    // DAC Initialize
    DAC_Config_Initial();
    // Interface initial  
    Master_I2C1_PB34_Initial();
    Master_I2C2_PC78_Initial();
    Slave_I2C0_PB67_Initial();
    // SPI Interface Initial
    //SPI0_Initial();
}






