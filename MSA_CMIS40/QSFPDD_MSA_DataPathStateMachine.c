#include "gd32e501.h"
#include "core_cm33.h"
#include "CMIS_MSA.h"
#include <stdint.h>
#include "systick.h"
#include "GD32E501_GPIO_Customize_Define.h"
#include "DSP.h"
#include "DSP_Line_Side.h"

uint8_t DataPath_State[8]={DataPathDeactivated,DataPathDeactivated,DataPathDeactivated,DataPathDeactivated,DataPathDeactivated,DataPathDeactivated,DataPathDeactivated,DataPathDeactivated};
uint8_t Bef_DataPathTxDisableT = 0x00;
uint8_t Bef_DataPathTxForceSquelchT = 0x00;
//------------------------------------------------------------------------------------------//
// Low Page Flags
//------------------------------------------------------------------------------------------//
// Module_Global_Control is 0x00
uint8_t Bef_ForceLowPwr = 0x00 ;
uint8_t Bef_Tx_Disable_Flag = 0x00;

uint8_t ModuleReadyT()
{
    uint8_t Result=0;
    // Module State bit1-3, 011b is Module Ready
    if((QSFPDD_A0[3]&0x0E)==0x06)
        Result=1;
    
    return Result;
}

//------------------------------------------------------------//
// Module Global and Squelch Mode Controls 
// Byte26 Bit4:ForceLowPwr
//------------------------------------------------------------//
void ForceLowPwr()
{
	if( Bef_ForceLowPwr!=QSFPDD_A0[26] )
	{
		if( QSFPDD_A0[26] & 0x08 )
            __NVIC_SystemReset();
	}

	Bef_ForceLowPwr = QSFPDD_A0[26];
}

void ResetS()
{
    // ResetL Hardware signal
    RESET_L_Function();
	// Software Reset
	ForceLowPwr();
}

// LowPwrS = ForceLowPwr OR (LowPwr AND LPMode)
uint8_t LowPwrS()
{
    uint8_t Result=0;
    // ForceLowPwr
    if(QSFPDD_A0[26]&0x10)
        Result=1;
    // LowPwr AND LPMode
    else if((QSFPDD_A0[26]&0x40)&&(LPMODE_Function()))
        Result=1;
    
    return Result;
}

// ModuleDeactivatedT = (Lane0 Data Path State = DataPathDeactivated) AND (Lane1 Data Path State = DataPathDeactivated) AND...
uint8_t ModuleDeactivatedT()
{
    uint8_t Result=0;
    uint8_t i=0,Count=0;

    for(i=0;i<4;i++)
    {
    	if(QSFPDD_P11[i+0]==0x11)
    		Count++;
    }
    if(Count==4)
    	Result=1;

    return Result;
}
// LowPwrExS = LowPwrS AND ModuleDeactivatedT
uint8_t LowPwrExS()
{
    uint8_t Result=0;

    if((LowPwrS()==1)&&(ModuleDeactivatedT()==1))
		Result=1;

    return Result;
}
// DataPathDeintT = DataPathDeinit Lane0 OR DataPathDeinit Lane1 OR DataPathDeinit Lane2 OR DataPathDeinit Lane3..
uint8_t DataPathDeintT(uint8_t Lane)
{
    // Page10 Byte128
    uint8_t Result=0;
    // now must put 0xFF just return 1
    if(QSFPDD_P10[0]&(0x01<<Lane))
        Result=1;
    
    return Result;
}

// DataPathDeinitS = NOT ModuleReadyT OR LowPwrS OR DataPathDeinitT
uint8_t DataPathDeinitS(uint8_t Lane)
{
    uint8_t Result=0;
	  if(ModuleReadyT()==0 || LowPwrS() || DataPathDeintT(Lane))
			Result=1;
    
    return Result;
}

// DataPathReinitT = Lane0 Apply_DataPathInit OR Lane1 Apply_DataPathInit OR Lane2 Apply_DataPathInit OR Lane3 Apply_DataPathInit..
uint8_t DataPathReinitT(uint8_t Lane)
{
    uint8_t Result=0;
    // now must put 0xFF just return 1
    if(QSFPDD_P10[15]&(0x01<<Lane))
        Result=1;
    
    return Result;
}

// DataPathReDeinitS = DataPathDeinitS OR DataPathReinitT
uint8_t DataPathReDeinitS(uint8_t Lane)
{
    uint8_t Result=0;
    // *DataPathDeinitS and DataPathReinitT cannot be 1 at the same time
    if(DataPathDeinitS(Lane) || DataPathReinitT(Lane))
        Result=1;
        
    return Result;
}

uint8_t Get_DataPathReDeinitS()
{
	uint8_t Result=0,Result_Count=0;
	for(uint8_t Lane=0;Lane<8;Lane++)
	{
		if(DataPathReDeinitS(Lane))
			Result_Count++;
	}
	if(Result_Count>0)
		Result=1;
	
	return Result;
}

// DataPathTxDisableT = Tx1 Disable OR Tx2 Disable OR Tx3 Disable OR Tx4 Disable
uint8_t DataPathTxDisableT(uint8_t Lane)
{
    uint8_t Result=0;
	  //page10 control data
	  if(QSFPDD_P10[2] & (0x01<<Lane))
			return 1;
		
    return Result;
}

// DataPathTxForceSquelchT = Tx1 Force Squelch OR Tx2 Force Squelch OR Tx3 Force Squelch OR Tx4 Force Squelch
uint8_t DataPathTxForceSquelchT(uint8_t Lane)
{
    uint8_t Result=0;
	  if(QSFPDD_P10[4]&(0x01<<Lane))
			Result=1;
//    uint8_t i=0;
//    if(Bef_DataPathTxForceSquelchT!=QSFPDD_P10[4])
//    {
//    	for(i=0;i<8;i++)
//    	{
//    		if(QSFPDD_P10[4]&0x01<<i)
//    			MSA_Lane_DataPathState(i, DataPathInitialized);
//    		else
//    			MSA_Lane_DataPathState(i, DataPathActivated);
//    	}
//        if((QSFPDD_P10[4]&0xFF)>0)
//            Result=1;
//        // CH0-7
//    	for(i=0;i<8;i++)
//        {
//            if(QSFPDD_P10[4]&(0x01<<i))
//                DSP_LineSide_TX_Squelch_SET(i ,Squelch);
//            else
//                DSP_LineSide_TX_Squelch_SET(i ,Unsquelch);
//        }
//        Bef_DataPathTxForceSquelchT=QSFPDD_P10[4];
//    }
    return Result;
}

// DataPathDeactivateS = DataPathReDeinitS OR DataPathTxDisableT OR DataPathTxForceSquelchT
uint8_t DataPathDeactivateS(uint8_t Lane)
{
    uint8_t Result=0;
	  if(DataPathReDeinitS(Lane))
			Result=1;
		else
		{
			if(DataPathTxDisableT(Lane)||DataPathTxForceSquelchT(Lane)||(Auto_Squelch_Tx_Flag & (0x01<<Lane)))
        Result=1;
		}

    return Result;
}

void MSA_DataPath_StateMachine()
{
	for(uint8_t Lane=0;Lane<8;Lane++)
	{
		switch( DataPath_State[Lane] )
    {
		// Steady State
		case DataPathDeactivated:

			// Exit Condition 1 DataPathDeinitS transition signal becomes FALSE
			if(DataPathDeinitS(Lane)==0)
			{
				MSA_Lane_DataPathState (Lane,DataPathInitialized);
				DataPath_State[Lane]=DataPathInit;;
			}
//			else
//			{
//				//tx disable
//				if(DataPathTxDisableT(Lane))
//				{
//					if((Bef_Tx_Disable_Flag & (0x01<<Lane)))
//					{
//						Tx_Disable(Lane,1);
//						Bef_Tx_Disable_Flag &= ~(0x01<<Lane);
//					}
//				}
//				else
//				{
//					if((Bef_Tx_Disable_Flag & (0x01<<Lane))==0)
//					{
//						Tx_Disable(Lane,0);
//						Bef_Tx_Disable_Flag|=(0x01<<Lane);
//					}
//				}
//			}
			break;

     	// Transient State
     case DataPathInit:
			
        // Exit Condition 3 Module completes data path initialization
		    // Tx output state is Quiescent(disable)
		    if(DataPathDeinitS(Lane)==0)
				{
					DataPath_State[Lane]=DataPathInitialized;
					//for dsp_tx_los_check check again to change data path state by tx lol
					Tx_Los_Enable_Flag=1;
				}
				// Exit Condition 2 DataPathDeinitS transition signal becomes TRUE
				else
					DataPath_State[Lane]=DataPathDeinit;
        break;

		// Transient State
     case DataPathDeinit:
            // Exit Condition 2 Data path deinitialization complete
					if(Lane==7) DDMI_Trigger_Flag=1;
					DataPath_State[Lane]=DataPathDeactivated;
					MSA_Lane_DataPathState(Lane,DataPathDeactivated);
          break;

		// Steady State
     case DataPathInitialized:

            // Exit Condition 2 DataPathReDeinitS transition signal becomes TRUE
            if(DataPathReDeinitS(Lane))
            {
                DataPath_State[Lane]=DataPathDeinit;
            }
            // Exit Condition 3 DataPathDeactivateS transition signal becomes FALSE
            else if(DataPathDeactivateS(Lane)==0)
            {
                DataPath_State[Lane]=DataPathTxTurnOn;
            }
            
            break;

		// Transient State
     case DataPathTxTurnOn:
        	// Tx output state is In transition(enable)
          // Exit Condition 3 Module Tx output is enabled and stable
        	// Disable squelch
					if(DataPathDeactivateS(Lane)==0)
					{
						Tx_Disable(Lane,0);
						Tx_Force_Squelch(Lane,0);
						DataPath_State[Lane]=DataPathActivated;
						MSA_Lane_DataPathState(Lane,DataPathActivated);
					}
					// Exit Condition 2 DataPathDeactivateS transition signal becomes TRUE
					else
							DataPath_State[Lane]=DataPathTxTurnOff;

            break;

		// Transient State
      case DataPathTxTurnOff:
            // Exit Condition 2 Module Tx output is squelched or disabled
        	if((DataPathTxForceSquelchT(Lane))&&(DataPathTxDisableT(Lane)==0))
						Tx_Force_Squelch(Lane,1);
					else
						Tx_Disable(Lane,1);

					DataPath_State[Lane]=DataPathInitialized;
					MSA_Lane_DataPathState(Lane,DataPathInitialized);
          break;

		// Steady State
        case DataPathActivated:
            // Exit Condition 2 DataPathDeactivateS transition signal becomes TRUE
            if(DataPathDeactivateS(Lane))
            {
                DataPath_State[Lane]=DataPathTxTurnOff;
                MSA_Lane_DataPathState(Lane,DataPathTxTurnOff);
            }
            break;
    }
	}
    
}


