#include "gd32e501.h"
#include "core_cm33.h"
#include "CMIS_MSA.h"
#include "Calibration_Struct.h"
#include "GD_FlahMap.h"
#include "string.h"
//--------------------------------------------------------//
// Chip ICs .h                                            //
//--------------------------------------------------------//
#include "DSP.h"
#include "DSP_Line_Side.h"
#include "DSP_System_Side.h"
//------------------------------------------------------------------------------------------//
// Page14h Flags
//------------------------------------------------------------------------------------------//
uint8_t Trigger_Media_SNR_Flag = 0x0F;
uint8_t Trigger_Host_SNR_Flag = 0xFF;
uint8_t Media_SNR_Real_Data = 0;
uint8_t Host_SNR_Real_Data = 0;
uint8_t Update_LineSide_Flash = 0x00;
uint8_t Update_SystemSide_Flash = 0x00;
//-----------------------------------------------------------------------------------------------------//
// Page14h Controls                                                                                    //
//-----------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------//
// Media Side SNR Diagnostics Selector 06h Byte208-221
//------------------------------------------------------------------------------------------//
void Media_Side_SNR()
{
    uint8_t Data_Buffer[128];
    uint16_t Min_SNR=0x0800;
    
    if(QSFPDD_P14[0]==0x06)
    {
        // Media Lane 0-3 SNR
        // Read Page86 Data from Flash
        GDMCU_FMC_READ_FUNCTION( FS_DSP_LS_P86 , &Data_Buffer[0] , 128 );
        // Copy Read Data to Page86
        memcpy(  &DSP_Line_Side_PHY0_MEMORY_MAP   , &Data_Buffer[0] , 128 );
        // Lane0
        if(Rx_LOL_Buffer&0x01)
        {
            QSFPDD_P14[112]=0x00;
            QSFPDD_P14[113]=0x00;
            Trigger_Media_SNR_Flag|=0x01;
        }
        // Get Real Data
        else if(Trigger_Media_SNR_Flag&0x01)
        {
            if(Media_SNR_Real_Data&0x01)
            {
                DSP_LineSide_SNR_LTP_GET(0);
                // MSB
                QSFPDD_P14[113]=DSP_LineSide_SNR>>8;
                // LSB
                QSFPDD_P14[112]=DSP_LineSide_SNR;
                // Save last value to flash
                DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_SNR_MSB_CH0=QSFPDD_P14[113];
                DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_SNR_LSB_CH0=QSFPDD_P14[112];
                // For CH signal turn off on to make sure get stable value
                if(DSP_LineSide_SNR>Min_SNR)
                {
                    Trigger_Media_SNR_Flag&=~0x01;
                    Media_SNR_Real_Data&=~0x01;
                }
                Update_LineSide_Flash|=1;
            }
            else
            {
                // Get last vaule to show first
                QSFPDD_P14[113]=DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_SNR_MSB_CH0;
                QSFPDD_P14[112]=DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_SNR_LSB_CH0;
                Media_SNR_Real_Data|=0x01;
            }
        }
        // Lane1
        if(Rx_LOL_Buffer&0x02)
        {
            QSFPDD_P14[114]=0x00;
            QSFPDD_P14[115]=0x00;
            Trigger_Media_SNR_Flag|=0x02;
        }
        else if(Trigger_Media_SNR_Flag&0x02)
        {
            if(Media_SNR_Real_Data&0x02)
            {
                DSP_LineSide_SNR_LTP_GET(1);
                // MSB
                QSFPDD_P14[115]=DSP_LineSide_SNR>>8;
                // LSB
                QSFPDD_P14[114]=DSP_LineSide_SNR;
                // Save last value to flash
                DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_SNR_MSB_CH1=QSFPDD_P14[115];
                DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_SNR_LSB_CH1=QSFPDD_P14[114];
                // For CH signal turn off on to make sure get stable value
                if(DSP_LineSide_SNR>Min_SNR)
                {
                    Trigger_Media_SNR_Flag&=~0x02;
                    Media_SNR_Real_Data&=~0x02;
                }
                Update_LineSide_Flash|=1;
            }
            else
            {
                // Get last vaule to show first
                QSFPDD_P14[115]=DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_SNR_MSB_CH1;
                QSFPDD_P14[114]=DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_SNR_LSB_CH1;
                Media_SNR_Real_Data|=0x02;
            }
        }
        // Lane2
        if(Rx_LOL_Buffer&0x04)
        {
            QSFPDD_P14[116]=0x00;
            QSFPDD_P14[117]=0x00;
            Trigger_Media_SNR_Flag|=0x04;
        }
        else if(Trigger_Media_SNR_Flag&0x04)
        {
            if(Media_SNR_Real_Data&0x04)
            {
                DSP_LineSide_SNR_LTP_GET(2);
                // MSB
                QSFPDD_P14[117]=DSP_LineSide_SNR>>8;
                // LSB
                QSFPDD_P14[116]=DSP_LineSide_SNR;
                // Save last value to flash
                DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_SNR_MSB_CH2=QSFPDD_P14[117];
                DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_SNR_LSB_CH2=QSFPDD_P14[116];
                // For CH signal turn off on to make sure get stable value
                if(DSP_LineSide_SNR>Min_SNR)
                {
                    Trigger_Media_SNR_Flag&=~0x04;
                    Media_SNR_Real_Data&=~0x04;
                }
                Update_LineSide_Flash|=1;
            }
            else
            {
                // Get last vaule to show first
                QSFPDD_P14[117]=DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_SNR_MSB_CH2;
                QSFPDD_P14[116]=DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_SNR_LSB_CH2;
                Media_SNR_Real_Data|=0x04;
            }
        }
        // Lane3
        if(Rx_LOL_Buffer&0x08)
        {
            QSFPDD_P14[118]=0x00;
            QSFPDD_P14[119]=0x00;
            Trigger_Media_SNR_Flag|=0x08;
        }
        else if(Trigger_Media_SNR_Flag&0x08)
        {
            if(Media_SNR_Real_Data&0x08)
            {
                DSP_LineSide_SNR_LTP_GET(3);
                // MSB
                QSFPDD_P14[119]=DSP_LineSide_SNR>>8;
                // LSB
                QSFPDD_P14[118]=DSP_LineSide_SNR;
                // Save last value to flash
                DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_SNR_MSB_CH3=QSFPDD_P14[119];
                DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_SNR_LSB_CH3=QSFPDD_P14[118];
                // For CH signal turn off on to make sure get stable value
                if(DSP_LineSide_SNR>Min_SNR)
                {
                    Trigger_Media_SNR_Flag&=~0x08;
                    Media_SNR_Real_Data&=~0x08;
                }
                Update_LineSide_Flash|=1;
            }
            else
            {
                // Get last vaule to show first
                QSFPDD_P14[119]=DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_SNR_MSB_CH3;
                QSFPDD_P14[118]=DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_SNR_LSB_CH3;
                Media_SNR_Real_Data|=0x08;
            }
        }
        if(Update_LineSide_Flash)
        {
            // Save Last SNR Data to Flash
            GDMCU_Flash_Erase(FS_DSP_LS_P86);
            memcpy(  &Data_Buffer[0]   , &DSP_Line_Side_PHY0_MEMORY_MAP , 128 );
            GDMCU_FMC_BytesWRITE_FUNCTION( FS_DSP_LS_P86  , &Data_Buffer[0] , 128 );
            Update_LineSide_Flash=0;
        }
    }
}
//------------------------------------------------------------------------------------------//
// Host Side SNR Diagnostics Selector 06h Byte208-221
//------------------------------------------------------------------------------------------//
void Host_Side_SNR()
{
    uint8_t CH14_Data_Buffer[128],CH58_Data_Buffer[128];
    uint16_t Min_SNR=0x2600;
    
    if(QSFPDD_P14[0]==0x06)
    {
        // Host Lane 0-7 SNR
        // Read Page87 Data from Flash
        GDMCU_FMC_READ_FUNCTION( FS_DSP_SS_P87 , &CH14_Data_Buffer[0] , 128 );
        // Copy Read Data to Page86
        memcpy(  &DSP_System_Side_PHY0_MEMORY_MAP   , &CH14_Data_Buffer[0] , 128 );
        // Read Page87 Data from Flash
        GDMCU_FMC_READ_FUNCTION( FS_DSP_SS_P8E , &CH58_Data_Buffer[0] , 128 );
        // Copy Read Data to Page86
        memcpy(  &DSP_System_Side_PHY1_MEMORY_MAP   , &CH58_Data_Buffer[0] , 128 );
        // Lane0
        if(Tx_LOS_Buffer&0x01)
        {
            QSFPDD_P14[80]=0x00;
            QSFPDD_P14[81]=0x00;
            Trigger_Host_SNR_Flag|=0x01;
        }
        // Get Real Data
        else if(Trigger_Host_SNR_Flag&0x01)
        {
            if(Host_SNR_Real_Data&0x01)
            {
                DSP_SystemSide_SNR_LTP_GET(0);
                // MSB
                QSFPDD_P14[81]=DSP_SystemSide_SNR>>8;
                // LSB
                QSFPDD_P14[80]=DSP_SystemSide_SNR;
                // Save last value to flash
                DSP_System_Side_PHY0_MEMORY_MAP.System_side_SNR_MSB_CH0=QSFPDD_P14[81];
                DSP_System_Side_PHY0_MEMORY_MAP.System_side_SNR_LSB_CH0=QSFPDD_P14[80];
                // For CH signal turn off on to make sure get stable value
                if(DSP_SystemSide_SNR>Min_SNR)
                {
                    Trigger_Host_SNR_Flag&=~0x01;
                    Host_SNR_Real_Data&=~0x01;
                }
                Update_SystemSide_Flash|=1;
            }
            else
            {
                // Get last vaule to show first
                QSFPDD_P14[81]=DSP_System_Side_PHY0_MEMORY_MAP.System_side_SNR_MSB_CH0;
                QSFPDD_P14[80]=DSP_System_Side_PHY0_MEMORY_MAP.System_side_SNR_LSB_CH0;
                Host_SNR_Real_Data|=0x01;
            }
        }
        // Lane1
        if(Tx_LOS_Buffer&0x02)
        {
            QSFPDD_P14[82]=0x00;
            QSFPDD_P14[83]=0x00;
            Trigger_Host_SNR_Flag|=0x02;
        }
        else if(Trigger_Host_SNR_Flag&0x02)
        {
            if(Host_SNR_Real_Data&0x02)
            {
                DSP_SystemSide_SNR_LTP_GET(1);
                // MSB
                QSFPDD_P14[83]=DSP_SystemSide_SNR>>8;
                // LSB
                QSFPDD_P14[82]=DSP_SystemSide_SNR;
                // Save last value to flash
                DSP_System_Side_PHY0_MEMORY_MAP.System_side_SNR_MSB_CH1=QSFPDD_P14[83];
                DSP_System_Side_PHY0_MEMORY_MAP.System_side_SNR_LSB_CH1=QSFPDD_P14[82];
                // For CH signal turn off on to make sure get stable value
                if(DSP_SystemSide_SNR>Min_SNR)
                {
                    Trigger_Host_SNR_Flag&=~0x02;
                    Host_SNR_Real_Data&=~0x02;
                }
                Update_SystemSide_Flash|=1;
            }
            else
            {
                // Get last vaule to show first
                QSFPDD_P14[83]=DSP_System_Side_PHY0_MEMORY_MAP.System_side_SNR_MSB_CH1;
                QSFPDD_P14[82]=DSP_System_Side_PHY0_MEMORY_MAP.System_side_SNR_LSB_CH1;
                Host_SNR_Real_Data|=0x02;
            }
        }
        // Lane2
        if(Tx_LOS_Buffer&0x04)
        {
            QSFPDD_P14[84]=0x00;
            QSFPDD_P14[85]=0x00;
            Trigger_Host_SNR_Flag|=0x04;
        }
        else if(Trigger_Host_SNR_Flag&0x04)
        {
            if(Host_SNR_Real_Data&0x04)
            {
                DSP_SystemSide_SNR_LTP_GET(2);
                // MSB
                QSFPDD_P14[85]=DSP_SystemSide_SNR>>8;
                // LSB
                QSFPDD_P14[84]=DSP_SystemSide_SNR;
                // Save last value to flash
                DSP_System_Side_PHY0_MEMORY_MAP.System_side_SNR_MSB_CH2=QSFPDD_P14[85];
                DSP_System_Side_PHY0_MEMORY_MAP.System_side_SNR_LSB_CH2=QSFPDD_P14[84];
                // For CH signal turn off on to make sure get stable value
                if(DSP_SystemSide_SNR>Min_SNR)
                {
                    Trigger_Host_SNR_Flag&=~0x04;
                    Host_SNR_Real_Data&=~0x04;
                }
                Update_SystemSide_Flash|=1;
            }
            else
            {
                // Get last vaule to show first
                QSFPDD_P14[85]=DSP_System_Side_PHY0_MEMORY_MAP.System_side_SNR_MSB_CH2;
                QSFPDD_P14[84]=DSP_System_Side_PHY0_MEMORY_MAP.System_side_SNR_LSB_CH2;
                Host_SNR_Real_Data|=0x04;
            }
        }
        // Lane3
        if(Tx_LOS_Buffer&0x08)
        {
            QSFPDD_P14[86]=0x00;
            QSFPDD_P14[87]=0x00;
            Trigger_Host_SNR_Flag|=0x08;
        }
        else if(Trigger_Host_SNR_Flag&0x08)
        {
            if(Host_SNR_Real_Data&0x08)
            {
                DSP_SystemSide_SNR_LTP_GET(3);
                // MSB
                QSFPDD_P14[87]=DSP_SystemSide_SNR>>8;
                // LSB
                QSFPDD_P14[86]=DSP_SystemSide_SNR;
                // Save last value to flash
                DSP_System_Side_PHY0_MEMORY_MAP.System_side_SNR_MSB_CH3=QSFPDD_P14[87];
                DSP_System_Side_PHY0_MEMORY_MAP.System_side_SNR_LSB_CH3=QSFPDD_P14[86];
                // For CH signal turn off on to make sure get stable value
                if(DSP_SystemSide_SNR>Min_SNR)
                {
                    Trigger_Host_SNR_Flag&=~0x08;
                    Host_SNR_Real_Data&=~0x08;
                }
                Update_SystemSide_Flash|=1;
            }
            else
            {
                // Get last vaule to show first
                QSFPDD_P14[87]=DSP_System_Side_PHY0_MEMORY_MAP.System_side_SNR_MSB_CH3;
                QSFPDD_P14[86]=DSP_System_Side_PHY0_MEMORY_MAP.System_side_SNR_LSB_CH3;
                Host_SNR_Real_Data|=0x08;
            }
        }
        // Lane4
        if(Tx_LOS_Buffer&0x10)
        {
            QSFPDD_P14[88]=0x00;
            QSFPDD_P14[89]=0x00;
            Trigger_Host_SNR_Flag|=0x10;
        }
        // Get Real Data
        else if(Trigger_Host_SNR_Flag&0x10)
        {
            if(Host_SNR_Real_Data&0x10)
            {
                DSP_SystemSide_SNR_LTP_GET(4);
                // MSB
                QSFPDD_P14[89]=DSP_SystemSide_SNR>>8;
                // LSB
                QSFPDD_P14[88]=DSP_SystemSide_SNR;
                // Save last value to flash
                DSP_System_Side_PHY1_MEMORY_MAP.System_side_SNR_MSB_CH0=QSFPDD_P14[89];
                DSP_System_Side_PHY1_MEMORY_MAP.System_side_SNR_LSB_CH0=QSFPDD_P14[88];
                // For CH signal turn off on to make sure get stable value
                if(DSP_SystemSide_SNR>Min_SNR)
                {
                    Trigger_Host_SNR_Flag&=~0x10;
                    Host_SNR_Real_Data&=~0x10;
                }
                Update_SystemSide_Flash|=1;
            }
            else
            {
                // Get last vaule to show first
                QSFPDD_P14[89]=DSP_System_Side_PHY1_MEMORY_MAP.System_side_SNR_MSB_CH0;
                QSFPDD_P14[88]=DSP_System_Side_PHY1_MEMORY_MAP.System_side_SNR_LSB_CH0;
                Host_SNR_Real_Data|=0x10;
            }
        }
        // Lane5
        if(Tx_LOS_Buffer&0x20)
        {
            QSFPDD_P14[90]=0x00;
            QSFPDD_P14[91]=0x00;
            Trigger_Host_SNR_Flag|=0x20;
        }
        else if(Trigger_Host_SNR_Flag&0x20)
        {
            if(Host_SNR_Real_Data&0x20)
            {
                DSP_SystemSide_SNR_LTP_GET(5);
                // MSB
                QSFPDD_P14[91]=DSP_SystemSide_SNR>>8;
                // LSB
                QSFPDD_P14[90]=DSP_SystemSide_SNR;
                // Save last value to flash
                DSP_System_Side_PHY1_MEMORY_MAP.System_side_SNR_MSB_CH1=QSFPDD_P14[91];
                DSP_System_Side_PHY1_MEMORY_MAP.System_side_SNR_LSB_CH1=QSFPDD_P14[90];
                // For CH signal turn off on to make sure get stable value
                if(DSP_SystemSide_SNR>Min_SNR)
                {
                    Trigger_Host_SNR_Flag&=~0x20;
                    Host_SNR_Real_Data&=~0x20;
                }
                Update_SystemSide_Flash|=1;
            }
            else
            {
                // Get last vaule to show first
                QSFPDD_P14[91]=DSP_System_Side_PHY1_MEMORY_MAP.System_side_SNR_MSB_CH1;
                QSFPDD_P14[90]=DSP_System_Side_PHY1_MEMORY_MAP.System_side_SNR_LSB_CH1;
                Host_SNR_Real_Data|=0x20;
            }
        }
        // Lane6
        if(Tx_LOS_Buffer&0x40)
        {
            QSFPDD_P14[92]=0x00;
            QSFPDD_P14[93]=0x00;
            Trigger_Host_SNR_Flag|=0x40;
        }
        else if(Trigger_Host_SNR_Flag&0x40)
        {
            if(Host_SNR_Real_Data&0x40)
            {
                DSP_SystemSide_SNR_LTP_GET(6);
                // MSB
                QSFPDD_P14[93]=DSP_SystemSide_SNR>>8;
                // LSB
                QSFPDD_P14[92]=DSP_SystemSide_SNR;
                // Save last value to flash
                DSP_System_Side_PHY1_MEMORY_MAP.System_side_SNR_MSB_CH2=QSFPDD_P14[93];
                DSP_System_Side_PHY1_MEMORY_MAP.System_side_SNR_LSB_CH2=QSFPDD_P14[92];
                // For CH signal turn off on to make sure get stable value
                if(DSP_SystemSide_SNR>Min_SNR)
                {
                    Trigger_Host_SNR_Flag&=~0x40;
                    Host_SNR_Real_Data&=~0x40;
                }
                Update_SystemSide_Flash|=1;
            }
            else
            {
                // Get last vaule to show first
                QSFPDD_P14[93]=DSP_System_Side_PHY1_MEMORY_MAP.System_side_SNR_MSB_CH2;
                QSFPDD_P14[92]=DSP_System_Side_PHY1_MEMORY_MAP.System_side_SNR_LSB_CH2;
                Host_SNR_Real_Data|=0x40;
            }
        }
        // Lane7
        if(Tx_LOS_Buffer&0x80)
        {
            QSFPDD_P14[94]=0x00;
            QSFPDD_P14[95]=0x00;
            Trigger_Host_SNR_Flag|=0x80;
        }
        else if(Trigger_Host_SNR_Flag&0x80)
        {
            if(Host_SNR_Real_Data&0x80)
            {
                DSP_SystemSide_SNR_LTP_GET(7);
                // MSB
                QSFPDD_P14[95]=DSP_SystemSide_SNR>>8;
                // LSB
                QSFPDD_P14[94]=DSP_SystemSide_SNR;
                // Save last value to flash
                DSP_System_Side_PHY1_MEMORY_MAP.System_side_SNR_MSB_CH3=QSFPDD_P14[95];
                DSP_System_Side_PHY1_MEMORY_MAP.System_side_SNR_LSB_CH3=QSFPDD_P14[94];
                // For CH signal turn off on to make sure get stable value
                if(DSP_SystemSide_SNR>Min_SNR)
                {
                    Trigger_Host_SNR_Flag&=~0x80;
                    Host_SNR_Real_Data&=~0x80;
                }
                Update_SystemSide_Flash|=1;
            }
            else
            {
                // Get last vaule to show first
                QSFPDD_P14[95]=DSP_System_Side_PHY1_MEMORY_MAP.System_side_SNR_MSB_CH3;
                QSFPDD_P14[94]=DSP_System_Side_PHY1_MEMORY_MAP.System_side_SNR_LSB_CH3;
                Host_SNR_Real_Data|=0x80;
            }
        }
        if(Update_SystemSide_Flash)
        {
            // Save Last SNR Data to Flash
            GDMCU_Flash_Erase(FS_DSP_SS_P87);
            memcpy(  &CH14_Data_Buffer[0]   , &DSP_System_Side_PHY0_MEMORY_MAP , 128 );
            GDMCU_FMC_BytesWRITE_FUNCTION( FS_DSP_SS_P87  , &CH14_Data_Buffer[0] , 128 );
            GDMCU_Flash_Erase(FS_DSP_SS_P8E);
            memcpy(  &CH58_Data_Buffer[0]   , &DSP_System_Side_PHY1_MEMORY_MAP , 128 );
            GDMCU_FMC_BytesWRITE_FUNCTION( FS_DSP_SS_P8E  , &CH58_Data_Buffer[0] , 128 );
            Update_SystemSide_Flash=0;
        }
    }
}

void CMIS_PAGE14h_Diagnostics_Function()
{
    Media_Side_SNR();
    Host_Side_SNR();
}