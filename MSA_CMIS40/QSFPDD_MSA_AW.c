#include "gd32e501.h"
#include "CMIS_MSA.h"
#include <stdint.h>
#include "Calibration_Struct.h"
#include "Calibration_Struct_2.h"
#include "systick.h"
#include "GD32E501_GPIO_Customize_Define.h"
//--------------------------------------------------------//
// Chip ICs .h                                            //
//--------------------------------------------------------//
#include "DSP.h"
#include "DSP_Line_Side.h"
#include "DSP_System_Side.h"

uint8_t Temperature_LATCH = 0;
uint8_t VCC_Temperature_LATCH = 0;
uint8_t TxP_HA_LATCH = 0;
uint8_t TxP_LA_LATCH = 0;
uint8_t TxP_HW_LATCH = 0;
uint8_t TxP_LW_LATCH = 0;

uint8_t BIAS_HA_LATCH = 0;
uint8_t BIAS_LA_LATCH = 0;
uint8_t BIAS_HW_LATCH = 0;
uint8_t BIAS_LW_LATCH = 0;

uint8_t RxP_HA_LATCH = 0;
uint8_t RxP_LA_LATCH = 0;
uint8_t RxP_HW_LATCH = 0;
uint8_t RxP_LW_LATCH = 0;


uint8_t Rx_LOS_Buffer = 0x00;
uint8_t Rx_LOL_Buffer = 0x00;
uint8_t Tx_LOS_Buffer = 0x00;

uint8_t IntL_Trigger_Flag=0;


// CMIS40_RXLOS_LOL_AW()
uint8_t Bef_Rx_LOL_LATCH_R;

uint8_t Get_Lane_DataPathState(uint8_t Lane)
{
	uint8_t DataPathState=0,Address=0;;
    // Page11 128 QSFPDD_P11[0] Bit0-3 Lan0/Bit4-7 Lan1
	// Page11 129 QSFPDD_P11[1] Bit0-3 Lan2/Bit4-7 Lan3
	// Page11 130 QSFPDD_P11[2] Bit0-3 Lan4/Bit4-7 Lan5
	// Page11 131 QSFPDD_P11[3] Bit0-3 Lan6/Bit4-7 Lan7

	if(Lane==0)
		DataPathState = QSFPDD_P11[0] & 0x0F ;
	else if(Lane==1)
		DataPathState = (QSFPDD_P11[0] & 0xF0)>>4 ;
	else if(Lane==2)
		DataPathState = QSFPDD_P11[1] & 0x0F ;
	else if(Lane==3)
		DataPathState = (QSFPDD_P11[1] & 0xF0)>>4 ;
	else if(Lane==4)
		DataPathState = QSFPDD_P11[2] & 0x0F ;
	else if(Lane==5)
		DataPathState = (QSFPDD_P11[2] & 0xF0)>>4 ;
	else if(Lane==6)
		DataPathState = QSFPDD_P11[3] & 0x0F ;
	else if(Lane==7)
		DataPathState = (QSFPDD_P11[3] & 0xF0)>>4 ;

	return DataPathState;
}

uint8_t Get_Support_DataPathState(uint8_t Lane)
{
	uint8_t Result=1,Address=0,Data_Buffer;

	Data_Buffer=Get_Lane_DataPathState(Lane);
	// Flag not allowed on DataPathDeactivated¡BDataPathInit and DataPathDeinit
	if((Data_Buffer==DataPathDeactivated)||(Data_Buffer==DataPathInit)||(Data_Buffer==DataPathDeinit))
		Result=0;

	return Result;
}

void IntL_Function()
{
	uint8_t Interrupt_Flag=0,Data_Buffer,i,Lane_Count;
	//--------------------------------------------------------------------//
	// Module State Changed Flag				  						  //
	// Low Page Byte8 Bit0 				 	  							  //
	//--------------------------------------------------------------------//
	// Module State Changed Flag Mask Low Page Byte31 Bit0
	if((QSFPDD_A0[31]&0x01)==0)
	{
		// L-Module State Changed Flag
		if(QSFPDD_A0[8]&0x01)
			Interrupt_Flag|=1;
	}
	//--------------------------------------------------------------------//
	// Vcc and Temp Alarm Warning Flags									  //
	// Byte9 Bit7 Vcc Low Warning										  //
	//       Bit6 Vcc High Warning										  //
	//       Bit5 Vcc Low Alarm											  //
	//       Bit4 Vcc High Alarm										  //
	//       Bit3 Temp Low Warning									      //
	//       Bit2 Temp High Warning										  //
	//       Bit1 Temp Low Alarm										  //
	//       Bit0 Temp High Alarm										  //
	//--------------------------------------------------------------------//
	// Vcc and Temp Mask Low Page Byte32
	if(Interrupt_Flag==0)
	{
		if(QSFPDD_A0[32]!=0xFF)
		{
			// Vcc and Temp L-Alarm Warning Flags
			Data_Buffer=QSFPDD_A0[9];
			if(QSFPDD_A0[32]>0x00)
			{
				// Check Mask bit by lane
				for(i=0;i<8;i++)
				{
					if(QSFPDD_A0[32]&(0x01<<i))
						Data_Buffer&=~(0x01<<i);
				}
			}
			if(Data_Buffer>0x00)
                Interrupt_Flag|=1;
		}
	}
	//--------------------------------------------------------------------//
	// L-Data Path State Changed Flags					  				  //
	// Page11 Byte134 Bit7-Bit0 Lane7-Lane0								  //
	//--------------------------------------------------------------------//
	// L-Data Path State Changed Mask Page10 Byte215
	if(Interrupt_Flag==0)
	{
		if((QSFPDD_P10[85]!=0xFF)&&(Module_Status != MODULE_LOW_PWR))
		{
			// L-Data Path State Changed Flags
			Data_Buffer=QSFPDD_P11[6];
			if(QSFPDD_P10[85]>0x00)
			{
				// Check Mask bit by lane
				for(Lane_Count=0;Lane_Count<8;Lane_Count++)
				{
					if(QSFPDD_P10[85]&(0x01<<Lane_Count))
						Data_Buffer&=~(0x01<<Lane_Count);
				}
			}
			if(Data_Buffer>0x00)
                Interrupt_Flag|=1;
		}
	}
	//--------------------------------------------------------------------//
	// Tx LOS Interrupt Flags					  						  //
	// Page11 Byte136 Bit7-Bit0 Tx8-Tx1									  //
	//--------------------------------------------------------------------//
	// Tx LOS Mask Page10 Byte215
	if(Interrupt_Flag==0)
	{
		if(QSFPDD_P10[87]!=0xFF)
		{
			// Tx LOS L-Alarm Warning Flags
			Data_Buffer=QSFPDD_P11[8];
			if(QSFPDD_P10[87]>0x00)
			{
				// Check Mask bit by lane
				for(Lane_Count=0;Lane_Count<8;Lane_Count++)
				{
					if(QSFPDD_P10[87]&(0x01<<Lane_Count))
						Data_Buffer&=~(0x01<<Lane_Count);
				}
			}
			if(Data_Buffer>0x00)
                Interrupt_Flag|=1;
		}
	}
	//--------------------------------------------------------------------//
	// Tx CDR LOL Interrupt Flags										  //
	// Page11 Byte137 Bit7-Bit0 Tx8-Tx1									  //
	//--------------------------------------------------------------------//
	// Tx CDR LOL Mask Page10 Byte216
	if(Interrupt_Flag==0)
	{
		if(QSFPDD_P10[88]!=0xFF)
		{
			// Tx LOS L-Alarm Warning Flags
			Data_Buffer=QSFPDD_P11[9];
			if(QSFPDD_P10[88]>0x00)
			{
				// Check Mask bit by lane
				for(Lane_Count=0;Lane_Count<8;Lane_Count++)
				{
					if(QSFPDD_P10[88]&(0x01<<Lane_Count))
						Data_Buffer&=~(0x01<<Lane_Count);
				}
			}
			if(Data_Buffer>0x00)
                Interrupt_Flag|=1;
		}
	}
	//--------------------------------------------------------------------//
	// Tx Power Alarm Warning Flags				  						  //
	// Page11 Byte139 High Alarm   Bit7-Bit0 Tx8-Tx1				 	  //
	//        Byte140 Low Alarm    Bit7-Bit0 Tx8-Tx1				 	  //
	//        Byte141 High Warning Bit7-Bit0 Tx8-Tx1					  //
	//        Byte142 Low Warning  Bit7-Bit0 Tx8-Tx1					  //
	//--------------------------------------------------------------------//
	// Tx Power Mask
	if(Interrupt_Flag==0)
	{
		//Page10 Byte218-Byte221
		for(i=0;i<4;i++)
		{
			if(QSFPDD_P10[90+i]!=0xFF)
			{
				// Tx Power L-Alarm Warning Flags
				Data_Buffer=QSFPDD_P11[11+i];
				if(QSFPDD_P10[90+i]>0x00)
				{
					// Check Mask bit by lane
					for(Lane_Count=0;Lane_Count<8;Lane_Count++)
					{
						if(QSFPDD_P10[90+i]&(0x01<<Lane_Count))
							Data_Buffer&=~(0x01<<Lane_Count);
					}
				}
				// If already interrupt stop the loop
				if(Data_Buffer>0x00)
				{
                    Interrupt_Flag|=1;
					break;
				}
			}
		}
	}
	//--------------------------------------------------------------------//
	// Tx Bias Alarm Warning Flags				  						  //
	// Page11 Byte143 High Alarm   Bit7-Bit0 Tx8-Tx1				 	  //
	//        Byte144 Low Alarm    Bit7-Bit0 Tx8-Tx1				 	  //
	//        Byte145 High Warning Bit7-Bit0 Tx8-Tx1					  //
	//        Byte146 Low Warning  Bit7-Bit0 Tx8-Tx1					  //
	//--------------------------------------------------------------------//
	//Tx Bias Mask
	if(Interrupt_Flag==0)
	{
		//Page10 Byte222-Byte225
		for(i=0;i<4;i++)
		{
			if(QSFPDD_P10[94+i]!=0xFF)
			{
				// Tx Power L-Alarm Warning Flags
				Data_Buffer=QSFPDD_P11[15+i];
				if(QSFPDD_P10[94+i]>0x00)
				{
					// Check Mask bit by lane
					for(Lane_Count=0;Lane_Count<8;Lane_Count++)
					{
						if(QSFPDD_P10[94+i]&(0x01<<Lane_Count))
							Data_Buffer&=~(0x01<<Lane_Count);
					}
				}
				// If already interrupt stop the loop
				if(Data_Buffer>0x00)
				{
					Interrupt_Flag|=1;
					break;
				}
			}
		}
	}
	//--------------------------------------------------------------------//
	// Rx LOS Interrupt Flags					  						  //
	// Page11 Byte147 Bit7-Bit0 Tx8-Tx1									  //
	//--------------------------------------------------------------------//
	// Rx LOS Mask Page10 Byte226
	if(Interrupt_Flag==0)
	{
		if(QSFPDD_P10[98]!=0xFF)
		{
			// Rx LOS L-Alarm Warning Flags
			Data_Buffer=QSFPDD_P11[19];
			if(QSFPDD_P10[98]>0x00)
			{
				// Check Mask bit by lane
				for(Lane_Count=0;Lane_Count<8;Lane_Count++)
				{
					if(QSFPDD_P10[98]&(0x01<<Lane_Count))
						Data_Buffer&=~(0x01<<Lane_Count);
				}
			}
			if(Data_Buffer>0x00)
				Interrupt_Flag|=1;
		}
	}
	//--------------------------------------------------------------------//
	// Rx CDR LOL Interrupt Flags										  //
	// Page11 Byte148 Bit7-Bit0 Tx8-Tx1									  //
	//--------------------------------------------------------------------//
	// Rx CDR LOL Mask Page10 Byte227
	if(Interrupt_Flag==0)
	{
		if(QSFPDD_P10[99]!=0xFF)
		{
			// Rx LOS L-Alarm Warning Flags
			Data_Buffer=QSFPDD_P11[20];
			if(QSFPDD_P10[99]>0x00)
			{
				// Check Mask bit by lane
				for(Lane_Count=0;Lane_Count<8;Lane_Count++)
				{
					if(QSFPDD_P10[99]&(0x01<<Lane_Count))
						Data_Buffer&=~(0x01<<Lane_Count);
				}
			}
			if(Data_Buffer>0x00)
				Interrupt_Flag|=1;
		}
	}
	//--------------------------------------------------------------------//
	// Rx Power Alarm Warning Flags				  						  //
	// Page11 Byte149 High Alarm   Bit7-Bit0 Rx8-Rx1				 	  //
	//        Byte150 Low Alarm    Bit7-Bit0 Rx8-Rx1				 	  //
	//        Byte151 High Warning Bit7-Bit0 Rx8-Rx1					  //
	//        Byte152 Low Warning  Bit7-Bit0 Rx8-Rx1					  //
	//--------------------------------------------------------------------//
	// Rx Power Mask
	if(Interrupt_Flag==0)
	{
		//Page10 Byte228-Byte231
		for(i=0;i<3;i++)
		{
			if(QSFPDD_P10[100+i]!=0xFF)
			{
				// Rx Power L-Alarm Warning Flags
				Data_Buffer=QSFPDD_P11[21+i];
				if(QSFPDD_P10[100+i]>0x00)
				{
					// Check Mask bit by lane
					for(Lane_Count=0;Lane_Count<8;Lane_Count++)
					{
						if(QSFPDD_P10[100+i]&(0x01<<Lane_Count))
							Data_Buffer&=~(0x01<<Lane_Count);
					}
				}
				// If already interrupt stop the loop
				if(Data_Buffer>0x00)
				{
					Interrupt_Flag|=1;
					break;
				}
			}
		}
	}
	// Some interrupt flags happen
	if(Interrupt_Flag)
	{
		// HW IntL Status
        IntL_G_Low();
		// SW IntL Status
		QSFPDD_A0[3] &= ~0x01;
	}
	// Normal Flag when no interrupt flag happen
	else
	{
		// HW IntL Status
        IntL_G_High();
		// SW IntL Status
		QSFPDD_A0[3] |= 0x01;
	}
}
void QSFPDD_Temperature_VCC_AW()
{
	int16_t T_Temp;
	int16_t High_Alarm_Value, Low_Alarm_Value, High_Warning, Low_Warning;
	uint16_t VCC_Temp;
	uint16_t VCC_High_Alarm_Value, VCC_Low_Alarm_Value, VCC_High_Warning, VCC_Low_Warning;
	uint8_t LATCH = 0;

	T_Temp           = ((int16_t)( QSFPDD_A0[14]<< 8 ) | QSFPDD_A0[15] );
	High_Alarm_Value = ((int16_t)( QSFPDD_P2[0] << 8 ) | QSFPDD_P2[1] );
	Low_Alarm_Value  = ((int16_t)( QSFPDD_P2[2] << 8 ) | QSFPDD_P2[3] );
	High_Warning     = ((int16_t)( QSFPDD_P2[4] << 8 ) | QSFPDD_P2[5] );
	Low_Warning      = ((int16_t)( QSFPDD_P2[6] << 8 ) | QSFPDD_P2[7] );

	VCC_Temp             = ((uint16_t)( QSFPDD_A0[16] << 8 ) | QSFPDD_A0[17] );
	VCC_High_Alarm_Value = ((uint16_t)( QSFPDD_P2[8]  << 8 ) | QSFPDD_P2[9] );
	VCC_Low_Alarm_Value  = ((uint16_t)( QSFPDD_P2[10] << 8 ) | QSFPDD_P2[11] );
	VCC_High_Warning     = ((uint16_t)( QSFPDD_P2[12] << 8 ) | QSFPDD_P2[13] );
	VCC_Low_Warning      = ((uint16_t)( QSFPDD_P2[14] << 8 ) | QSFPDD_P2[15] );

	if( T_Temp < Low_Warning )               // set Low  warning flag bit4
		LATCH |= 0x08;
	if( T_Temp > High_Warning )              // set high warning flag bit3
		LATCH |= 0x04;
	if( T_Temp < Low_Alarm_Value )           // set Low  alarm   flag bit2
		LATCH |= 0x02;
	if( T_Temp > High_Alarm_Value )          // set high alarm   flag bit1
		LATCH |= 0x01;

	if ( VCC_Temp < VCC_Low_Warning )        // set Low  warning flag bit7
		LATCH |= 0x80;
	if ( VCC_Temp > VCC_High_Warning )       // set High warning flag bit6
		LATCH |= 0x40;
	if ( VCC_Temp < VCC_Low_Alarm_Value )    // set Low  alarm   flag bit5
		LATCH |= 0x20;
	if ( VCC_Temp > VCC_High_Alarm_Value )   // set High alarm   flag bit4
		LATCH |= 0x10;

	if ((QSFPDD_A0[9] > 0) && (LATCH == 0))
		LATCH = QSFPDD_A0[9];

	VCC_Temperature_LATCH = LATCH;

	QSFPDD_A0[9] = LATCH;
    IntL_Trigger_Flag=1;
}

void QSFPDD_TxPower_AW()
{
	uint16_t TXPOWER_Temp[8];
	uint8_t Channel;
	uint16_t High_Alarm_Value, Low_Alarm_Value, High_Warning_Value, Low_Warning_Value;
	uint8_t LOW_A_LATCH  = 0;
	uint8_t HIGH_A_LATCH = 0;
	uint8_t LOW_W_LATCH  = 0;
	uint8_t HIGH_W_LATCH = 0;

	TXPOWER_Temp[0] = ( (uint16_t)( QSFPDD_P11[26] << 8) | QSFPDD_P11[27] );
	TXPOWER_Temp[1] = ( (uint16_t)( QSFPDD_P11[28] << 8) | QSFPDD_P11[29] );
	TXPOWER_Temp[2] = ( (uint16_t)( QSFPDD_P11[30] << 8) | QSFPDD_P11[31] );
	TXPOWER_Temp[3] = ( (uint16_t)( QSFPDD_P11[32] << 8) | QSFPDD_P11[33] );
	TXPOWER_Temp[4] = ( (uint16_t)( QSFPDD_P11[34] << 8) | QSFPDD_P11[35] );
	TXPOWER_Temp[5] = ( (uint16_t)( QSFPDD_P11[36] << 8) | QSFPDD_P11[37] );
	TXPOWER_Temp[6] = ( (uint16_t)( QSFPDD_P11[38] << 8) | QSFPDD_P11[39] );
	TXPOWER_Temp[7] = ( (uint16_t)( QSFPDD_P11[40] << 8) | QSFPDD_P11[41] );

	High_Alarm_Value   = ( (uint16_t)( QSFPDD_P2[48] << 8)| QSFPDD_P2[49] );
	Low_Alarm_Value    = ( (uint16_t)( QSFPDD_P2[50] << 8)| QSFPDD_P2[51] );
	High_Warning_Value = ( (uint16_t)( QSFPDD_P2[52] << 8)| QSFPDD_P2[53] );
	Low_Warning_Value  = ( (uint16_t)( QSFPDD_P2[54] << 8)| QSFPDD_P2[55] );
    //Only use four channel
	for(Channel = 0; Channel <= 3; Channel++)
	{
		// CH0 - CH 7 High Alarm Flag setting
		if( TXPOWER_Temp[Channel] > High_Alarm_Value )
			HIGH_A_LATCH |= ( 1 << Channel );
		// CH0 - CH 7 Low Alarm Flag setting
		if( TXPOWER_Temp[Channel] < Low_Alarm_Value )
			LOW_A_LATCH  |= ( 1 << Channel );
		// CH0 - CH 7 High Warning Flag setting
		if( TXPOWER_Temp[Channel] > High_Warning_Value )
			HIGH_W_LATCH |= ( 1 << Channel );
		// CH0 - CH 7 Low Warning Flag setting
		if( TXPOWER_Temp[Channel] < Low_Warning_Value )
			LOW_W_LATCH  |= ( 1 << Channel );
	}

	//-------------------------------------------------------------//
	// High / Low Alarm & Warning Flag write Value
	//-------------------------------------------------------------//
	if ( ( QSFPDD_P11[11] > 0 ) && ( HIGH_A_LATCH == 0 ))
		HIGH_A_LATCH = QSFPDD_P11[11];
	if ( ( QSFPDD_P11[12] > 0 ) && ( LOW_A_LATCH  == 0 ))
		LOW_A_LATCH = QSFPDD_P11[12];
	if ( ( QSFPDD_P11[13] > 0 ) && ( HIGH_W_LATCH == 0 ))
		HIGH_W_LATCH = QSFPDD_P11[13];
	if ( ( QSFPDD_P11[14] > 0 ) && ( LOW_W_LATCH  == 0 ))
		LOW_W_LATCH = QSFPDD_P11[14];

	TxP_HA_LATCH = HIGH_A_LATCH ;
	TxP_LA_LATCH = LOW_A_LATCH ;
	TxP_HW_LATCH = HIGH_W_LATCH ;
	TxP_LW_LATCH = LOW_W_LATCH ;

	QSFPDD_P11[11] = HIGH_A_LATCH ;
	QSFPDD_P11[12] = LOW_A_LATCH ;
	QSFPDD_P11[13] = HIGH_W_LATCH ;
	QSFPDD_P11[14] = LOW_W_LATCH ;

	IntL_Trigger_Flag=1;
}

void QSFPDD_Bias_AW()
{
	uint16_t Bias_Temp[8];
	uint8_t Channel;
	uint16_t High_Alarm_Value, Low_Alarm_Value, High_Warning_Value, Low_Warning_Value;
	uint8_t LOW_A_LATCH  = 0;
	uint8_t HIGH_A_LATCH = 0;
	uint8_t LOW_W_LATCH  = 0;
	uint8_t HIGH_W_LATCH = 0;

	Bias_Temp[0] = ((uint16_t)( QSFPDD_P11[42] << 8 ) | QSFPDD_P11[43] );
	Bias_Temp[1] = ((uint16_t)( QSFPDD_P11[44] << 8 ) | QSFPDD_P11[45] );
	Bias_Temp[2] = ((uint16_t)( QSFPDD_P11[46] << 8 ) | QSFPDD_P11[47] );
	Bias_Temp[3] = ((uint16_t)( QSFPDD_P11[48] << 8 ) | QSFPDD_P11[49] );
	Bias_Temp[4] = ((uint16_t)( QSFPDD_P11[50] << 8 ) | QSFPDD_P11[51] );
	Bias_Temp[5] = ((uint16_t)( QSFPDD_P11[52] << 8 ) | QSFPDD_P11[53] );
	Bias_Temp[6] = ((uint16_t)( QSFPDD_P11[54] << 8 ) | QSFPDD_P11[55] );
	Bias_Temp[7] = ((uint16_t)( QSFPDD_P11[56] << 8 ) | QSFPDD_P11[57] );

	High_Alarm_Value   = ( (uint16_t)( QSFPDD_P2[56] << 8)| QSFPDD_P2[57] );
	Low_Alarm_Value    = ( (uint16_t)( QSFPDD_P2[58] << 8)| QSFPDD_P2[59] );
	High_Warning_Value = ( (uint16_t)( QSFPDD_P2[60] << 8)| QSFPDD_P2[61] );
	Low_Warning_Value  = ( (uint16_t)( QSFPDD_P2[62] << 8)| QSFPDD_P2[63] );
    //Only use four channel
	for( Channel = 0; Channel <= 3; Channel++ )
	{
		// CH0 - CH 7 High Alarm Flag setting
		if( Bias_Temp[Channel] > High_Alarm_Value )
			HIGH_A_LATCH |= ( 1 << Channel );
		// CH0 - CH 7 Low Alarm Flag setting
		if( Bias_Temp[Channel] < Low_Alarm_Value )
			LOW_A_LATCH  |= ( 1 << Channel );
		// CH0 - CH 7 High Warning Flag setting
		if( Bias_Temp[Channel] > High_Warning_Value )
			HIGH_W_LATCH |= ( 1 << Channel );
		// CH0 - CH 7 Low Warning Flag setting
		if( Bias_Temp[Channel] < Low_Warning_Value )
			LOW_W_LATCH  |= ( 1 << Channel );
	}

	//-------------------------------------------------------------//
	// High / Low Alarm & Warning Flag write Value
	//-------------------------------------------------------------//
	if ( ( QSFPDD_P11[15] > 0 ) && ( HIGH_A_LATCH == 0 ))
		HIGH_A_LATCH = QSFPDD_P11[15];

	if ( ( QSFPDD_P11[16] > 0 ) && ( LOW_A_LATCH  == 0 ))
		LOW_A_LATCH = QSFPDD_P11[16];

	if ( ( QSFPDD_P11[17] > 0 ) && ( HIGH_W_LATCH == 0 ))
		HIGH_W_LATCH = QSFPDD_P11[17];

	if ( ( QSFPDD_P11[18] > 0 ) && ( LOW_W_LATCH  == 0 ))
		LOW_W_LATCH = QSFPDD_P11[18];

	BIAS_HA_LATCH = HIGH_A_LATCH ;
	BIAS_LA_LATCH = LOW_A_LATCH ;
	BIAS_HW_LATCH = HIGH_W_LATCH ;
	BIAS_LW_LATCH = LOW_W_LATCH ;

	QSFPDD_P11[15] = HIGH_A_LATCH ;
	QSFPDD_P11[16] = LOW_A_LATCH ;
	QSFPDD_P11[17] = HIGH_W_LATCH ;
	QSFPDD_P11[18] = LOW_W_LATCH ;

	IntL_Trigger_Flag=1;
}

void QSFPDD_RxPower_AW()
{
	uint16_t RXPOWER_Temp[8];
	uint8_t Channel;
	uint16_t High_Alarm_Value, Low_Alarm_Value, High_Warning_Value, Low_Warning_Value ;
	uint8_t HIGH_A_LATCH = 0;
	uint8_t LOW_A_LATCH = 0;
	uint8_t HIGH_W_LATCH = 0;
	uint8_t LOW_W_LATCH = 0;

	RXPOWER_Temp[0] = ((uint16_t)( QSFPDD_P11[58] << 8 ) | QSFPDD_P11[59] );
	RXPOWER_Temp[1] = ((uint16_t)( QSFPDD_P11[60] << 8 ) | QSFPDD_P11[61] );
	RXPOWER_Temp[2] = ((uint16_t)( QSFPDD_P11[62] << 8 ) | QSFPDD_P11[63] );
	RXPOWER_Temp[3] = ((uint16_t)( QSFPDD_P11[64] << 8 ) | QSFPDD_P11[65] );
	RXPOWER_Temp[4] = ((uint16_t)( QSFPDD_P11[66] << 8 ) | QSFPDD_P11[67] );
	RXPOWER_Temp[5] = ((uint16_t)( QSFPDD_P11[68] << 8 ) | QSFPDD_P11[69] );
	RXPOWER_Temp[6] = ((uint16_t)( QSFPDD_P11[70] << 8 ) | QSFPDD_P11[71] );
	RXPOWER_Temp[7] = ((uint16_t)( QSFPDD_P11[72] << 8 ) | QSFPDD_P11[73] );

	High_Alarm_Value   = ( (uint16_t)( QSFPDD_P2[64] << 8)| QSFPDD_P2[65] );
	Low_Alarm_Value    = ( (uint16_t)( QSFPDD_P2[66] << 8)| QSFPDD_P2[67] );
	High_Warning_Value = ( (uint16_t)( QSFPDD_P2[68] << 8)| QSFPDD_P2[69] );
	Low_Warning_Value  = ( (uint16_t)( QSFPDD_P2[70] << 8)| QSFPDD_P2[71] );
    //Only use four channel
	for( Channel = 0; Channel <= 3; Channel++ )
	{
		// CH0 - CH 7 High Alarm Flag setting
		if( RXPOWER_Temp[Channel] > High_Alarm_Value )
			HIGH_A_LATCH |= ( 1 << Channel );
        // Not support datapath do not return flag status
		if(Get_Support_DataPathState(Channel))
		{
			// CH0 - CH 7 Low Alarm Flag setting
			if( RXPOWER_Temp[Channel] < Low_Alarm_Value )
				LOW_A_LATCH  |= ( 1 << Channel );
		}
		// CH0 - CH 7 High Warning Flag setting
		if( RXPOWER_Temp[Channel] > High_Warning_Value )
			HIGH_W_LATCH |= ( 1 << Channel );
        // Not support datapath do not return flag status
		if(Get_Support_DataPathState(Channel))
		{
			// CH0 - CH 7 Low Warning Flag setting
			if( RXPOWER_Temp[Channel] < Low_Warning_Value )
				LOW_W_LATCH  |= ( 1 << Channel );
		}
	}
	//-------------------------------------------------------------//
	// High / Low Alarm & Warning Flag write Value
	//-------------------------------------------------------------//
	if ( ( QSFPDD_P11[21] > 0 ) && ( HIGH_A_LATCH == 0 ))
		HIGH_A_LATCH = QSFPDD_P11[21];
	if ( ( QSFPDD_P11[22] > 0 ) && ( LOW_A_LATCH  == 0 ))
		LOW_A_LATCH = QSFPDD_P11[22];
	if ( ( QSFPDD_P11[23] > 0 ) && ( HIGH_W_LATCH == 0 ))
		HIGH_W_LATCH = QSFPDD_P11[23];
	if ( ( QSFPDD_P11[24] > 0 ) && ( LOW_W_LATCH  == 0 ))
		LOW_W_LATCH = QSFPDD_P11[24];

	RxP_HA_LATCH  = HIGH_A_LATCH ;
	RxP_LA_LATCH  = LOW_A_LATCH ;
	RxP_HW_LATCH  = HIGH_W_LATCH ;
	RxP_LW_LATCH  = LOW_W_LATCH ;

	QSFPDD_P11[21] = HIGH_A_LATCH ;
	QSFPDD_P11[22] = LOW_A_LATCH ;
	QSFPDD_P11[23] = HIGH_W_LATCH ;
	QSFPDD_P11[24] = LOW_W_LATCH ;

	IntL_Trigger_Flag=1;
}

void CMIS40_TXLOS_LOL_AW()
{
    uint8_t Tx_LOS_LATCH_R = 0,Tx_LOL_LATCH_R = 0,i;
    //Get LOL From System Side Input
    //Tx_LOL_LATCH_R = DSP_System_Side_LOL();
    Tx_LOS_LATCH_R = Tx_LOL_LATCH_R;

    //Use for DSP_Tx_Los_Check to get real status to enable or disable tx output
    Tx_LOS_Buffer = Tx_LOL_LATCH_R ;
    
    // Lane-Specific Flag Conformance
	for(i=0;i<8;i++)
	{
		// Not support datapath do not return flag status
		if(Get_Support_DataPathState(i)==0)
		{
			Tx_LOL_LATCH_R&=~(0x01<<i);
			Tx_LOS_LATCH_R&=~(0x01<<i);
		}
	}

	if( ( QSFPDD_P11[8] > 0 ) && ( Tx_LOS_LATCH_R == 0x00 ) )
		Tx_LOS_LATCH_R = QSFPDD_P11[8];

	if( ( QSFPDD_P11[9] > 0 ) && ( Tx_LOL_LATCH_R == 0x00 ) )
		Tx_LOL_LATCH_R = QSFPDD_P11[9];

	QSFPDD_P11[8] |= Tx_LOS_LATCH_R ;
	QSFPDD_P11[9] |= Tx_LOL_LATCH_R ;

	IntL_Trigger_Flag=1;
}

void CMIS40_RXLOS_LOL_AW()
{
	uint8_t Rx_LOL_LATCH_R,Rx_LOS_LATCH_R,i;
    //Get LOL From Line Side Input
	Rx_LOL_LATCH_R = DSP_Line_Side_LOL();
    Rx_LOS_LATCH_R = Rx_LOL_LATCH_R;
    
    // Triggered to config dsp bandwidth for BER performance
    if(Bef_Rx_LOL_LATCH_R!=Rx_LOL_LATCH_R)
    {
        if(Rx_LOL_LATCH_R==0)
        {
            DSP_LineSide_RX_Squelch_SET(All_CH ,Squelch);
            delay_1ms(1);
            DSP_PLL_BANDWIDTH_CONFIG(Normal_Bandwidth);
            delay_1ms(1);
            DSP_LineSide_RX_Squelch_SET(All_CH ,Unsquelch);
            Bef_Rx_LOL_LATCH_R=Rx_LOL_LATCH_R;
        }
    }
    else
    {
        //Use for MSA_Rx_Los_Check to get real status
        Rx_LOS_Buffer=Rx_LOS_LATCH_R;
        // Use for DSP Line Side SNR
        Rx_LOL_Buffer=Rx_LOL_LATCH_R;
    }
   	// Lane-Specific Flag Conformance
	for(i=0;i<8;i++)
	{
		// Not support datapath do not return flag status
		if(Get_Support_DataPathState(i)==0)
			Rx_LOL_LATCH_R&=~(0x01<<i);
	}

	if( ( QSFPDD_P11[19]>0 ) && ( Rx_LOS_LATCH_R==0x00 ))
		Rx_LOS_LATCH_R = QSFPDD_P11[19] ;

	if( ( QSFPDD_P11[20]>0 ) && ( Rx_LOL_LATCH_R==0x00 ))
		Rx_LOL_LATCH_R = QSFPDD_P11[20];

	QSFPDD_P11[20] |= Rx_LOL_LATCH_R;
    QSFPDD_P11[19] |= Rx_LOS_LATCH_R;

	IntL_Trigger_Flag=1;
}

void Clear_Flag(uint8_t Current_AD)
{
	switch(Current_AD)
	{
		case ADDR_DPCF :
                        // Data Path state changed flag clear
						QSFPDD_P11[ADDR_DPCF] = 0 ;
					    break;
		case ADDR_TXFAULT:
						QSFPDD_P11[ADDR_TXFAULT] = 0 ;
						break;
		case ADDR_TXLOS:
						QSFPDD_P11[ADDR_TXLOS] = 0 ;
						break;
		case ADDR_TXCDR_LOL:
						QSFPDD_P11[ADDR_TXCDR_LOL] = 0 ;
						break;
		case ADDR_TX_EQ_FAULT:
						QSFPDD_P11[ADDR_TX_EQ_FAULT] = 0 ;
						break;
		case ADDR_TXP_HA:
						QSFPDD_P11[ADDR_TXP_HA] &= ~TxP_HA_LATCH ;
						break;
		case ADDR_TXP_LA:
						QSFPDD_P11[ADDR_TXP_LA] = 0x00 ;
						//QSFPDD_P11[ADDR_TXP_LA] &= ~TxP_LA_LATCH ;
						break;
		case ADDR_TXP_HW:
						QSFPDD_P11[ADDR_TXP_HW] &= ~TxP_HW_LATCH ;
						break;
		case ADDR_TXP_LW:
						QSFPDD_P11[ADDR_TXP_LW] = 0x00;
						//QSFPDD_P11[ADDR_TXP_LW] &= ~TxP_LW_LATCH ;
						break;
		case ADDR_TXB_HA:
						QSFPDD_P11[ADDR_TXB_HA] &= ~BIAS_HA_LATCH ;
						break;
		case ADDR_TXB_LA:
						QSFPDD_P11[ADDR_TXB_LA] = 0x00;
						//QSFPDD_P11[ADDR_TXB_LA] &= ~BIAS_LA_LATCH ;
						break;
		case ADDR_TXB_HW:
						QSFPDD_P11[ADDR_TXB_HW] &= ~BIAS_HW_LATCH ;
						break;
		case ADDR_TXB_LW:
						QSFPDD_P11[ADDR_TXB_LW] = 0x00;
						//QSFPDD_P11[ADDR_TXB_LW] &= ~BIAS_LW_LATCH ;
						break;
		case ADDR_RXLOS:
						QSFPDD_P11[ADDR_RXLOS] = 0 ;
						break;
		case ADDR_RXCDR_LOL:
						QSFPDD_P11[ADDR_RXCDR_LOL] = 0 ;
						break;
		case ADDR_RXP_HA:
						QSFPDD_P11[ADDR_RXP_HA] &= ~RxP_HA_LATCH ;
						break;
		case ADDR_RXP_LA:
						QSFPDD_P11[ADDR_RXP_LA] &= ~RxP_LA_LATCH ;
						break;
		case ADDR_RXP_HW:
						QSFPDD_P11[ADDR_RXP_HW] &= ~RxP_HW_LATCH ;
						break;
		case ADDR_RXP_LW:
						QSFPDD_P11[ADDR_RXP_LW] &= ~RxP_LW_LATCH ;
						break;
	}
	IntL_Trigger_Flag=1;
}

void Clear_VCC_TEMP_Flag()
{
	QSFPDD_A0[9]    &= ~VCC_Temperature_LATCH ;
	IntL_Trigger_Flag=1;
}

void Clear_Module_state_Byte8()
{
	QSFPDD_A0[8] = 0 ;
	IntL_Trigger_Flag=1;
}


void DDMI_AW_Monitor()
{
	QSFPDD_Temperature_VCC_AW();
	QSFPDD_RxPower_AW();
    QSFPDD_Bias_AW();
    QSFPDD_TxPower_AW();
}




