#include "gd32e501.h"
#include "core_cm33.h"
#include "CMIS_MSA.h"
#include "Calibration_Struct.h"
#include "GD_FlahMap.h"
#include "string.h"
#include "LDD_Control.h"
//--------------------------------------------------------//
// Chip ICs .h                                            //
//--------------------------------------------------------//
#include "DSP.h"
#include "DSP_Line_Side.h"
#include "DSP_System_Side.h"
//--------------------------------------------------------//
// MSA AppSelCode Define 
//--------------------------------------------------------//
// LowPage AppSelCode 1-8
uint8_t IB_PAM_2PORT_425G_DR4[4]={0x32,0x1C,0x44,0x11};
uint8_t IB_PAM_4PORT_212G_DR2[4]={0x32,0x17,0x22,0x55};
uint8_t PAM_1PORT_850G_L_DR8[4]={0x52,0x56,0x88,0x01};
uint8_t PAM_1PORT_850G_S_DR8[4]={0x51,0x56,0x88,0x01};
uint8_t PAM_2PORT_425G_L_DR4[4]={0x50,0x1C,0x44,0x11};
uint8_t PAM_2PORT_425G_S_DR4[4]={0x4F,0x1C,0x44,0x11};
uint8_t PAM_4PORT_212G_L_DR2[4]={0x4E,0x17,0x22,0x55};
uint8_t PAM_4PORT_212G_S_DR2[4]={0x4D,0x17,0x22,0x55};
uint8_t PAM_1PORT_425G_C2M_DR4[4]={0x11,0x1C,0x84,0x01};
// Page01h AppSelCode 9-15
uint8_t PAM_8PORT_106G_L_DR1[4]={0x4C,0x14,0x11,0xFF};
uint8_t PAM_8PORT_106G_S_DR1[4]={0x4B,0x14,0x11,0xFF};
uint8_t IB_PAM_8PORT_106G_DR1[4]={0x32,0x14,0x11,0xFF};
uint8_t PAM_1PORT_850G_L_DR8_2[4]={0x52,0x57,0x88,0x01};
uint8_t PAM_1PORT_850G_S_DR8_2[4]={0x51,0x57,0x88,0x01};
uint8_t IB_PAM_1PORT_850G_DR8[4]={0x32,0x56,0x88,0x01};
uint8_t IB_PAM_1PORT_850G_DR8_2[4]={0x32,0x57,0x88,0x01};
//------------------------------------------------------------------------------------------//
// Page10h Flags
//------------------------------------------------------------------------------------------//
// Update from Get_AppSelCode_Result(), use on Get_PAM_Code() and Get_NRZ_Code()
uint8_t Max_AppSelCode=0;
// Tx Disable Byte130
uint8_t TxDIS_Power_flag=0;
uint8_t Bef_Tx_Disable = 0xFF;
uint8_t Tx_Disable_Flag=0xFF;//
// Auto_Squelch_Tx_Output
uint8_t Bef_Tx_LOS_Buffer=0x00;
uint8_t Bef_Tx_Squelch_Buffer=0x00;
uint8_t Tx_Los_Enable_Flag=0;
uint8_t Auto_Squelch_Tx_Flag=0x00;
// Auto_Squelch_Rx_Output
uint8_t Rx_Los_Enable_Flag=0;
uint8_t Bef_Rx_LOS_Buffer = 0x00;
uint8_t Bef_Rx_Squelch_Buffer=0x00;
// Update from Rx_Output_Disable
uint8_t Rx_output_flag=0;
uint8_t Bef_Rx_output;
// Update from AppSel_Control_Feedback()
uint8_t Active_AppSelCode[8]={0};
uint8_t AppSelCode_Buffer[8]={0};
uint8_t Internal_Config_Error_Code[4]={0x11,0x11,0x11,0x11};
uint8_t MSA_Config_Error_Code=1;
uint8_t Explicit_Control_Buffer=0;
uint8_t Invalid_Lane = 0;
// Update from Signal_Integrity_Control_Feedback()
uint8_t Config_Error_flag = 0x00;
// Update from Rx Pre Post Amp Control
uint8_t	System_Side_Tab_Load = 0x00;
//uint8_t Auto_Trigger_Mode = 0;
// TRx Polarity
uint8_t Tx_Polarify_flag=0;
uint8_t Rx_Polarify_flag=0;
uint8_t Bef_Tx_Polarify_flag=0;
uint8_t Bef_Rx_Polarify_flag=0;
// AppSelCode
uint8_t AppSelCode_Lane[8]={0};
uint8_t Control_Set=0;
//------------------------------------------------------------------------------------------//
// Page11h Flags
//------------------------------------------------------------------------------------------//
// Update from MSA_DataPathState() and MSA_DataPathState()
uint8_t Bef_Lane_DataPath[8]={0};
//-----------------------------------------------------------------------------------------------------//
// Functions
//-----------------------------------------------------------------------------------------------------//
uint8_t Get_AppSelCode_Result(uint8_t Code_Value)
{
	uint8_t Result=1,Data_Buffer,End_Code=0,i=0;

    Data_Buffer=((Code_Value&0xF0)>>4);
    // Scan support AppSelCode from low page, 0xFF will be end of code on Host Electrical Interface Code
		for(i=0;i<15;i++)
    {
        // Low Page AppSelCode 1-8
        if(i<8)
        {
            // Scan end of AppSelCode
            if(QSFPDD_A0[86+(i*4)]==0xFF)
						{
							break;
						}
            else
						{
							End_Code++;
						}
        }
        // Page01 AppSelCode 9-15
        else
        {
            // Scan end of AppSelCode
            if(QSFPDD_P1[95+((i-8)*4)]==0xFF)
                break;
            else
                End_Code++;
        }
    }
    if(Control_Set==0)
    {
        // Range1-8
        if(End_Code>8)
            Max_AppSelCode=8;
        else
            Max_AppSelCode=End_Code;
    }
    else if(Control_Set==1)
    {
        // Range 9-15
        if(Data_Buffer<9)
            Max_AppSelCode=0;
        else
            Max_AppSelCode=End_Code;
    }
    // Mean not support code
	if(Data_Buffer>Max_AppSelCode)
		Result=0;

    return Result;
}
//------------------------------------------------------//
// Scan AppSelCode from low page by port value
//------------------------------------------------------//
uint8_t Get_53G_PAM_Code(uint8_t Port_Value)
{
    // For skip verify when code value is 0x00
	uint8_t AppSelCode=-1;
	uint8_t Code_Value,i,Correct_Times;

	for(Code_Value=0;Code_Value<=Max_AppSelCode;Code_Value++)
	{
		// Clear every time to start search AppSelCode
		Correct_Times=0;
		// Search to support 1Port 8 lanes
		if(Port_Value==1)
		{
			for(i=0;i<4;i++)
			{
                // Low Page AppSelcode 1-8
                if(Code_Value<8)
                {
                    if(PAM_1PORT_425G_C2M_DR4[i]==QSFPDD_A0[86+(Code_Value*4)+i])
                        Correct_Times++;
                }
                // Page01 AppSelcode 9-15
                else
                {
                    if(PAM_1PORT_425G_C2M_DR4[i]==QSFPDD_P1[95+((Code_Value-8)*4)+i])
                        Correct_Times++;
                }
			}
		}
		// Search to support 2Port 4 lanes
		else if(Port_Value==2)
		{
			for(i=0;i<4;i++)
			{
				//if(PAM_2PORT_200G_SR4[i]==QSFPDD_A0[86+(Code_Value*4)+i])
					//Correct_Times++;
			}
		}
		// Search to support 4Port 2 lanes
		else if(Port_Value==4)
		{
			for(i=0;i<4;i++)
			{
				//if(PAM_4PORT_100G_SR2[i]==QSFPDD_A0[86+(Code_Value*4)+i])
					//Correct_Times++;
			}
		}
		// Search to support 8Port 1 lanes
		else if(Port_Value==8)
		{
			for(i=0;i<4;i++)
			{
                // Low Page AppSelcode 1-8
                if(Code_Value<8)
                {
                    //if(PAM_8PORT_53G_ALB[i]==QSFPDD_A0[86+(Code_Value*4)+i])
                        //Correct_Times++;
                }
                // Page01 AppSelcode 9-15
                else
                {
                    //if(PAM_8PORT_53G_ALB[i]==QSFPDD_P1[95+((Code_Value-8)*4)+i])
                        //Correct_Times++;
                }
			}
		}
		else
			break;
		// All value correct
		if(Correct_Times==4)
		{
			AppSelCode=Code_Value+1;
			break;
        }
	}
	return AppSelCode;
}

uint8_t Get_106G_PAM_S_Code(uint8_t Port_Value)
{
    // For skip verify when code value is 0x00
	uint8_t AppSelCode=-1;
	uint8_t Code_Value,i,Correct_Times;

	for(Code_Value=0;Code_Value<=Max_AppSelCode;Code_Value++)
	{
		// Clear every time to start search AppSelCode
		Correct_Times=0;
		// Search to support 1Port 8 lanes
		if(Port_Value==1)
		{
			for(i=0;i<4;i++)
			{
                // Low Page AppSelcode 1-8
                if(Code_Value<8)
                {
                    if(PAM_1PORT_850G_S_DR8[i]==QSFPDD_A0[86+(Code_Value*4)+i])
                        Correct_Times++;
                }
                // Page01 AppSelcode 9-15
                else
                {
                    if(PAM_1PORT_850G_S_DR8[i]==QSFPDD_P1[95+((Code_Value-8)*4)+i])
                        Correct_Times++;
                }
			}
		}
		// Search to support 2Port 4 lanes
		else if(Port_Value==2)
		{
			for(i=0;i<4;i++)
			{
                // Low Page AppSelcode 1-8
                if(Code_Value<8)
                {
                    if(PAM_2PORT_425G_S_DR4[i]==QSFPDD_A0[86+(Code_Value*4)+i])
                        Correct_Times++;
                }
                // Page01 AppSelcode 9-15
                else
                {
                    if(PAM_2PORT_425G_S_DR4[i]==QSFPDD_P1[95+((Code_Value-8)*4)+i])
                        Correct_Times++;
                }
			}
		}
		// Search to support 4Port 2 lanes
		else if(Port_Value==4)
		{
			for(i=0;i<4;i++)
			{
                // Low Page AppSelcode 1-8
                if(Code_Value<8)
                {
                    if(PAM_4PORT_212G_S_DR2[i]==QSFPDD_A0[86+(Code_Value*4)+i])
                        Correct_Times++;
                }
                // Page01 AppSelcode 9-15
                else
                {
                    if(PAM_4PORT_212G_S_DR2[i]==QSFPDD_P1[95+((Code_Value-8)*4)+i])
                        Correct_Times++;
                }
			}
		}
		// Search to support 8Port 1 lanes
		else if(Port_Value==8)
		{
			for(i=0;i<4;i++)
			{
                // Low Page AppSelcode 1-8
                if(Code_Value<8)
                {
                    if(PAM_8PORT_106G_S_DR1[i]==QSFPDD_A0[86+(Code_Value*4)+i])
                        Correct_Times++;
                }
                // Page01 AppSelcode 9-15
                else
                {
                    if(PAM_8PORT_106G_S_DR1[i]==QSFPDD_P1[95+((Code_Value-8)*4)+i])
                        Correct_Times++;
                }
			}
		}
		else
			break;
		// All value correct
		if(Correct_Times==4)
		{
			AppSelCode=Code_Value+1;
			break;
        }
	}
	return AppSelCode;
}

uint8_t Get_106G_PAM_S_2_Code(uint8_t Port_Value)
{
    // For skip verify when code value is 0x00
	uint8_t AppSelCode=-1;
	uint8_t Code_Value,i,Correct_Times;

	for(Code_Value=0;Code_Value<=Max_AppSelCode;Code_Value++)
	{
		// Clear every time to start search AppSelCode
		Correct_Times=0;
		// Search to support 1Port 8 lanes
		if(Port_Value==1)
		{
			for(i=0;i<4;i++)
			{
                // Low Page AppSelcode 1-8
                if(Code_Value<8)
                {
                    if(PAM_1PORT_850G_S_DR8_2[i]==QSFPDD_A0[86+(Code_Value*4)+i])
                        Correct_Times++;
                }
                // Page01 AppSelcode 9-15
                else
                {
                    if(PAM_1PORT_850G_S_DR8_2[i]==QSFPDD_P1[95+((Code_Value-8)*4)+i])
                        Correct_Times++;
                }
			}
		}
		else
			break;
		// All value correct
		if(Correct_Times==4)
		{
			AppSelCode=Code_Value+1;
			break;
        }
	}
	return AppSelCode;
}

uint8_t Get_106G_PAM_L_Code(uint8_t Port_Value)
{
    // For skip verify when code value is 0x00
	uint8_t AppSelCode=-1;
	uint8_t Code_Value,i,Correct_Times;

	for(Code_Value=0;Code_Value<=Max_AppSelCode;Code_Value++)
	{
		// Clear every time to start search AppSelCode
		Correct_Times=0;
		// Search to support 1Port 8 lanes
		if(Port_Value==1)
		{
			for(i=0;i<4;i++)
			{
                // Low Page AppSelcode 1-8
                if(Code_Value<8)
                {
                    if(PAM_1PORT_850G_L_DR8[i]==QSFPDD_A0[86+(Code_Value*4)+i])
                        Correct_Times++;
                }
                // Page01 AppSelcode 9-15
                else
                {
                    if(PAM_1PORT_850G_L_DR8[i]==QSFPDD_P1[95+((Code_Value-8)*4)+i])
                        Correct_Times++;
                }
			}
		}
		// Search to support 2Port 4 lanes
		else if(Port_Value==2)
		{
			for(i=0;i<4;i++)
			{
                // Low Page AppSelcode 1-8
                if(Code_Value<8)
                {
                    if(PAM_2PORT_425G_L_DR4[i]==QSFPDD_A0[86+(Code_Value*4)+i])
                        Correct_Times++;
                }
                // Page01 AppSelcode 9-15
                else
                {
                    if(PAM_2PORT_425G_L_DR4[i]==QSFPDD_P1[95+((Code_Value-8)*4)+i])
                        Correct_Times++;
                }
			}
		}
		// Search to support 4Port 2 lanes
		else if(Port_Value==4)
		{
			for(i=0;i<4;i++)
			{
                // Low Page AppSelcode 1-8
                if(Code_Value<8)
                {
                    if(PAM_4PORT_212G_L_DR2[i]==QSFPDD_A0[86+(Code_Value*4)+i])
                        Correct_Times++;
                }
                // Page01 AppSelcode 9-15
                else
                {
                    if(PAM_4PORT_212G_L_DR2[i]==QSFPDD_P1[95+((Code_Value-8)*4)+i])
                        Correct_Times++;
                }
			}
		}
		// Search to support 8Port 1 lanes
		else if(Port_Value==8)
		{
			for(i=0;i<4;i++)
			{
                // Low Page AppSelcode 1-8
                if(Code_Value<8)
                {
                    if(PAM_8PORT_106G_L_DR1[i]==QSFPDD_A0[86+(Code_Value*4)+i])
                        Correct_Times++;
                }
                // Page01 AppSelcode 9-15
                else
                {
                    if(PAM_8PORT_106G_L_DR1[i]==QSFPDD_P1[95+((Code_Value-8)*4)+i])
                        Correct_Times++;
                }
			}
		}
		else
			break;
		// All value correct
		if(Correct_Times==4)
		{
			AppSelCode=Code_Value+1;
			break;
        }
	}
	return AppSelCode;
}

uint8_t Get_106G_PAM_L_2_Code(uint8_t Port_Value)
{
    // For skip verify when code value is 0x00
	uint8_t AppSelCode=-1;
	uint8_t Code_Value,i,Correct_Times;

	for(Code_Value=0;Code_Value<=Max_AppSelCode;Code_Value++)
	{
		// Clear every time to start search AppSelCode
		Correct_Times=0;
		// Search to support 1Port 8 lanes
		if(Port_Value==1)
		{
			for(i=0;i<4;i++)
			{
                // Low Page AppSelcode 1-8
                if(Code_Value<8)
                {
                    if(PAM_1PORT_850G_L_DR8_2[i]==QSFPDD_A0[86+(Code_Value*4)+i])
                        Correct_Times++;
                }
                // Page01 AppSelcode 9-15
                else
                {
                    if(PAM_1PORT_850G_L_DR8_2[i]==QSFPDD_P1[95+((Code_Value-8)*4)+i])
                        Correct_Times++;
                }
			}
		}
		else
			break;
		// All value correct
		if(Correct_Times==4)
		{
			AppSelCode=Code_Value+1;
			break;
        }
	}
	return AppSelCode;
}

uint8_t Get_IB_PAM_Code(uint8_t Port_Value)
{
    // For skip verify when code value is 0x00
	uint8_t AppSelCode=-1;
	uint8_t Code_Value,i,Correct_Times;

	for(Code_Value=0;Code_Value<=Max_AppSelCode;Code_Value++)
	{
		// Clear every time to start search AppSelCode
		Correct_Times=0;
		// Search to support 1Port 8 lanes
		if(Port_Value==1)
		{
			for(i=0;i<4;i++)
			{
                // Low Page AppSelcode 1-8
                if(Code_Value<8)
                {
                    if(IB_PAM_1PORT_850G_DR8[i]==QSFPDD_A0[86+(Code_Value*4)+i])
                        Correct_Times++;
                }
                // Page01 AppSelcode 9-15
                else
                {
                    if(IB_PAM_1PORT_850G_DR8[i]==QSFPDD_P1[95+((Code_Value-8)*4)+i])
                        Correct_Times++;
                }
			}
		}
		// Search to support 2Port 4 lanes
		else if(Port_Value==2)
		{
			for(i=0;i<4;i++)
			{
                // Low Page AppSelcode 1-8
                if(Code_Value<8)
                {
                    if(IB_PAM_2PORT_425G_DR4[i]==QSFPDD_A0[86+(Code_Value*4)+i])
                        Correct_Times++;
                }
                // Page01 AppSelcode 9-15
                else
                {
                    if(IB_PAM_2PORT_425G_DR4[i]==QSFPDD_P1[95+((Code_Value-8)*4)+i])
                        Correct_Times++;
                }
			}
		}
		// Search to support 4Port 2 lanes
		else if(Port_Value==4)
		{
			for(i=0;i<4;i++)
			{
                // Low Page AppSelcode 1-8
                if(Code_Value<8)
                {
                    if(IB_PAM_4PORT_212G_DR2[i]==QSFPDD_A0[86+(Code_Value*4)+i])
                        Correct_Times++;
                }
                // Page01 AppSelcode 9-15
                else
                {
                    if(IB_PAM_4PORT_212G_DR2[i]==QSFPDD_P1[95+((Code_Value-8)*4)+i])
                        Correct_Times++;
                }
			}
		}
		// Search to support 8Port 1 lanes
		else if(Port_Value==8)
		{
			for(i=0;i<4;i++)
			{
                // Low Page AppSelcode 1-8
                if(Code_Value<8)
                {
                    if(IB_PAM_8PORT_106G_DR1[i]==QSFPDD_A0[86+(Code_Value*4)+i])
                        Correct_Times++;
                }
                // Page01 AppSelcode 9-15
                else
                {
                    if(IB_PAM_8PORT_106G_DR1[i]==QSFPDD_P1[95+((Code_Value-8)*4)+i])
                        Correct_Times++;
                }
			}
		}
		else
			break;
		// All value correct
		if(Correct_Times==4)
		{
			AppSelCode=Code_Value+1;
			break;
        }
	}
	return AppSelCode;
}
uint8_t Get_IB_PAM_2_Code(uint8_t Port_Value)
{
    // For skip verify when code value is 0x00
	uint8_t AppSelCode=-1;
	uint8_t Code_Value,i,Correct_Times;

	for(Code_Value=0;Code_Value<=Max_AppSelCode;Code_Value++)
	{
		// Clear every time to start search AppSelCode
		Correct_Times=0;
		// Search to support 1Port 8 lanes
		if(Port_Value==1)
		{
			for(i=0;i<4;i++)
			{
                // Low Page AppSelcode 1-8
                if(Code_Value<8)
                {
                    if(IB_PAM_1PORT_850G_DR8_2[i]==QSFPDD_A0[86+(Code_Value*4)+i])
                        Correct_Times++;
                }
                // Page01 AppSelcode 9-15
                else
                {
                    if(IB_PAM_1PORT_850G_DR8_2[i]==QSFPDD_P1[95+((Code_Value-8)*4)+i])
                        Correct_Times++;
                }
			}
		}
		else
			break;
		// All value correct
		if(Correct_Times==4)
		{
			AppSelCode=Code_Value+1;
			break;
        }
	}
	return AppSelCode;
}
//------------------------------------------------------//
// Scan AppSelCode from low page by port value
//------------------------------------------------------//
uint8_t Get_25G_NRZ_Code(uint8_t Port_Value)
{
    // For skip verify when code value is 0x00
	uint8_t AppSelCode=-1;
	uint8_t Code_Value,i,Correct_Times;

	for(Code_Value=0;Code_Value<=Max_AppSelCode;Code_Value++)
	{
		// Clear every time to start search AppSelCode
		Correct_Times=0;
        // Search to support 1Port 8 lanes
		if(Port_Value==1)
		{
			for(i=0;i<4;i++)
			{
                // Low Page AppSelcode 1-8
                if(Code_Value<8)
                {
                    //if(NRZ_1PORT_212G_ALB[i]==QSFPDD_A0[86+(Code_Value*4)+i])
                        //Correct_Times++;
                }
                // Page01 AppSelcode 9-15
                else
                {
                    //if(NRZ_1PORT_212G_ALB[i]==QSFPDD_P1[95+((Code_Value-8)*4)+i])
                        //Correct_Times++;
                }
			}
		}
		// Search to support 2Port 4 lanes
		else if(Port_Value==2)
		{
			for(i=0;i<4;i++)
			{
                // Low Page AppSelcode 1-8
                if(Code_Value<8)
                {
                    //if(NRZ_2PORT_103G_ALB[i]==QSFPDD_A0[86+(Code_Value*4)+i])
                        //Correct_Times++;
                }
                // Page01 AppSelcode 9-15
                else
                {
                    //if(NRZ_2PORT_103G_ALB[i]==QSFPDD_P1[95+((Code_Value-8)*4)+i])
                        //Correct_Times++;
                }
			}
		}
		// Search to support 8Port 1 lanes
		else if(Port_Value==8)
		{
			for(i=0;i<4;i++)
			{
                // Low Page AppSelcode 1-8
                if(Code_Value<8)
                {
                    //if(NRZ_8PORT_25G_ALB[i]==QSFPDD_A0[86+(Code_Value*4)+i])
                        //Correct_Times++;
                }
                // Page01 AppSelcode 9-15
                else
                {
                    //if(NRZ_8PORT_25G_ALB[i]==QSFPDD_P1[95+((Code_Value-8)*4)+i])
                        //Correct_Times++;
                }
			}
		}
		else
			break;
		// All value correct
		if(Correct_Times==4)
		{
			AppSelCode=Code_Value+1;
			break;
        }
	}

	return AppSelCode;
}
uint8_t Is_1Port_8Lanes_AppSelCode()
{
    uint8_t Result=0;
    // PAM4 53G
    if(((Active_AppSelCode[0])==(Get_53G_PAM_Code(1)<<4))&&
       ((Active_AppSelCode[1])==(Get_53G_PAM_Code(1)<<4))&&
       ((Active_AppSelCode[2])==(Get_53G_PAM_Code(1)<<4))&&
       ((Active_AppSelCode[3])==(Get_53G_PAM_Code(1)<<4))&&
       ((Active_AppSelCode[4])==(Get_53G_PAM_Code(1)<<4))&&
       ((Active_AppSelCode[5])==(Get_53G_PAM_Code(1)<<4))&&
       ((Active_AppSelCode[6])==(Get_53G_PAM_Code(1)<<4))&&
       ((Active_AppSelCode[7])==(Get_53G_PAM_Code(1)<<4)))
    {       
        Result=1;
    }
    // PAM4 106G S
    else if(((Active_AppSelCode[0])==(Get_106G_PAM_S_Code(1)<<4))&&
            ((Active_AppSelCode[1])==(Get_106G_PAM_S_Code(1)<<4))&&
            ((Active_AppSelCode[2])==(Get_106G_PAM_S_Code(1)<<4))&&
            ((Active_AppSelCode[3])==(Get_106G_PAM_S_Code(1)<<4))&&
            ((Active_AppSelCode[4])==(Get_106G_PAM_S_Code(1)<<4))&&
            ((Active_AppSelCode[5])==(Get_106G_PAM_S_Code(1)<<4))&&
            ((Active_AppSelCode[6])==(Get_106G_PAM_S_Code(1)<<4))&&
            ((Active_AppSelCode[7])==(Get_106G_PAM_S_Code(1)<<4)))
    {       
        Result=1;
    }
    // PAM4 106G S 2
    else if(((Active_AppSelCode[0])==(Get_106G_PAM_S_2_Code(1)<<4))&&
            ((Active_AppSelCode[1])==(Get_106G_PAM_S_2_Code(1)<<4))&&
            ((Active_AppSelCode[2])==(Get_106G_PAM_S_2_Code(1)<<4))&&
            ((Active_AppSelCode[3])==(Get_106G_PAM_S_2_Code(1)<<4))&&
            ((Active_AppSelCode[4])==(Get_106G_PAM_S_2_Code(1)<<4))&&
            ((Active_AppSelCode[5])==(Get_106G_PAM_S_2_Code(1)<<4))&&
            ((Active_AppSelCode[6])==(Get_106G_PAM_S_2_Code(1)<<4))&&
            ((Active_AppSelCode[7])==(Get_106G_PAM_S_2_Code(1)<<4)))
    {       
        Result=1;
    }
    // PAM4 106G L
    else if(((Active_AppSelCode[0])==(Get_106G_PAM_L_Code(1)<<4))&&
            ((Active_AppSelCode[1])==(Get_106G_PAM_L_Code(1)<<4))&&
            ((Active_AppSelCode[2])==(Get_106G_PAM_L_Code(1)<<4))&&
            ((Active_AppSelCode[3])==(Get_106G_PAM_L_Code(1)<<4))&&
            ((Active_AppSelCode[4])==(Get_106G_PAM_L_Code(1)<<4))&&
            ((Active_AppSelCode[5])==(Get_106G_PAM_L_Code(1)<<4))&&
            ((Active_AppSelCode[6])==(Get_106G_PAM_L_Code(1)<<4))&&
            ((Active_AppSelCode[7])==(Get_106G_PAM_L_Code(1)<<4)))
    {       
        Result=1;
    }
    // PAM4 106G L 2
    else if(((Active_AppSelCode[0])==(Get_106G_PAM_L_2_Code(1)<<4))&&
            ((Active_AppSelCode[1])==(Get_106G_PAM_L_2_Code(1)<<4))&&
            ((Active_AppSelCode[2])==(Get_106G_PAM_L_2_Code(1)<<4))&&
            ((Active_AppSelCode[3])==(Get_106G_PAM_L_2_Code(1)<<4))&&
            ((Active_AppSelCode[4])==(Get_106G_PAM_L_2_Code(1)<<4))&&
            ((Active_AppSelCode[5])==(Get_106G_PAM_L_2_Code(1)<<4))&&
            ((Active_AppSelCode[6])==(Get_106G_PAM_L_2_Code(1)<<4))&&
            ((Active_AppSelCode[7])==(Get_106G_PAM_L_2_Code(1)<<4)))
    {       
        Result=1;
    }
    // PAM4 IB
    else if(((Active_AppSelCode[0])==(Get_IB_PAM_Code(1)<<4))&&
            ((Active_AppSelCode[1])==(Get_IB_PAM_Code(1)<<4))&&
            ((Active_AppSelCode[2])==(Get_IB_PAM_Code(1)<<4))&&
            ((Active_AppSelCode[3])==(Get_IB_PAM_Code(1)<<4))&&
            ((Active_AppSelCode[4])==(Get_IB_PAM_Code(1)<<4))&&
            ((Active_AppSelCode[5])==(Get_IB_PAM_Code(1)<<4))&&
            ((Active_AppSelCode[6])==(Get_IB_PAM_Code(1)<<4))&&
            ((Active_AppSelCode[7])==(Get_IB_PAM_Code(1)<<4)))
    {       
        Result=1;
    }
    // PAM4 IB 2
    else if(((Active_AppSelCode[0])==(Get_IB_PAM_2_Code(1)<<4))&&
            ((Active_AppSelCode[1])==(Get_IB_PAM_2_Code(1)<<4))&&
            ((Active_AppSelCode[2])==(Get_IB_PAM_2_Code(1)<<4))&&
            ((Active_AppSelCode[3])==(Get_IB_PAM_2_Code(1)<<4))&&
            ((Active_AppSelCode[4])==(Get_IB_PAM_2_Code(1)<<4))&&
            ((Active_AppSelCode[5])==(Get_IB_PAM_2_Code(1)<<4))&&
            ((Active_AppSelCode[6])==(Get_IB_PAM_2_Code(1)<<4))&&
            ((Active_AppSelCode[7])==(Get_IB_PAM_2_Code(1)<<4)))
    {       
        Result=1;
    }
    // NRZ 25G
    else if(((Active_AppSelCode[0])==(Get_25G_NRZ_Code(1)<<4))&&
            ((Active_AppSelCode[1])==(Get_25G_NRZ_Code(1)<<4))&&
            ((Active_AppSelCode[2])==(Get_25G_NRZ_Code(1)<<4))&&
            ((Active_AppSelCode[3])==(Get_25G_NRZ_Code(1)<<4))&&
            ((Active_AppSelCode[4])==(Get_25G_NRZ_Code(1)<<4))&&
            ((Active_AppSelCode[5])==(Get_25G_NRZ_Code(1)<<4))&&
            ((Active_AppSelCode[6])==(Get_25G_NRZ_Code(1)<<4))&&
            ((Active_AppSelCode[7])==(Get_25G_NRZ_Code(1)<<4)))
    {       
        Result=1;
    }
    return Result;
}
uint8_t Is_1Port_4Lanes_AppSelCode(uint8_t Lane)
{
    uint8_t Result=0;
    // Lane 0-3
    if(Lane==Lane_0_3)
    {
        // PAM4 106G S
        if(((Active_AppSelCode[0])==(Get_106G_PAM_S_Code(2)<<4))&&
           ((Active_AppSelCode[1])==(Get_106G_PAM_S_Code(2)<<4))&&
           ((Active_AppSelCode[2])==(Get_106G_PAM_S_Code(2)<<4))&&
           ((Active_AppSelCode[3])==(Get_106G_PAM_S_Code(2)<<4)))
        {
            Result=1;
        }
        // PAM4 106G L
        else if(((Active_AppSelCode[0])==(Get_106G_PAM_L_Code(2)<<4))&&
                ((Active_AppSelCode[1])==(Get_106G_PAM_L_Code(2)<<4))&&
                ((Active_AppSelCode[2])==(Get_106G_PAM_L_Code(2)<<4))&&
                ((Active_AppSelCode[3])==(Get_106G_PAM_L_Code(2)<<4)))
        {
            Result=1;
        }
        // PAM4 IB
        else if(((Active_AppSelCode[0])==(Get_IB_PAM_Code(2)<<4))&&
                ((Active_AppSelCode[1])==(Get_IB_PAM_Code(2)<<4))&&
                ((Active_AppSelCode[2])==(Get_IB_PAM_Code(2)<<4))&&
                ((Active_AppSelCode[3])==(Get_IB_PAM_Code(2)<<4)))
        {
            Result=1;
        }
        // NRZ 25G
        else if(((Active_AppSelCode[0])==(Get_25G_NRZ_Code(2)<<4))&&
                ((Active_AppSelCode[1])==(Get_25G_NRZ_Code(2)<<4))&&
                ((Active_AppSelCode[2])==(Get_25G_NRZ_Code(2)<<4))&&
                ((Active_AppSelCode[3])==(Get_25G_NRZ_Code(2)<<4)))
        {
            Result=1;
        }
    }
     // Lane 4-7
    else if(Lane==Lane_4_7)
    {
        // PAM4 106G S
        if(((Active_AppSelCode[4])==((Get_106G_PAM_S_Code(2)<<4)|0x08))&&
           ((Active_AppSelCode[5])==((Get_106G_PAM_S_Code(2)<<4)|0x08))&&
           ((Active_AppSelCode[6])==((Get_106G_PAM_S_Code(2)<<4)|0x08))&&
           ((Active_AppSelCode[7])==((Get_106G_PAM_S_Code(2)<<4)|0x08)))
        {
            Result=1;
        }
        // PAM4 106G L
        else if(((Active_AppSelCode[4])==((Get_106G_PAM_L_Code(2)<<4)|0x08))&&
                ((Active_AppSelCode[5])==((Get_106G_PAM_L_Code(2)<<4)|0x08))&&
                ((Active_AppSelCode[6])==((Get_106G_PAM_L_Code(2)<<4)|0x08))&&
                ((Active_AppSelCode[7])==((Get_106G_PAM_L_Code(2)<<4)|0x08)))
        {
            Result=1;
        }
        // PAM4 IB
        else if(((Active_AppSelCode[4])==((Get_IB_PAM_Code(2)<<4)|0x08))&&
                ((Active_AppSelCode[5])==((Get_IB_PAM_Code(2)<<4)|0x08))&&
                ((Active_AppSelCode[6])==((Get_IB_PAM_Code(2)<<4)|0x08))&&
                ((Active_AppSelCode[7])==((Get_IB_PAM_Code(2)<<4)|0x08)))
        {
            Result=1;
        }
        // NRZ 25G
        else if(((Active_AppSelCode[4])==((Get_25G_NRZ_Code(2)<<4)|0x08))&&
                ((Active_AppSelCode[5])==((Get_25G_NRZ_Code(2)<<4)|0x08))&&
                ((Active_AppSelCode[6])==((Get_25G_NRZ_Code(2)<<4)|0x08))&&
                ((Active_AppSelCode[7])==((Get_25G_NRZ_Code(2)<<4)|0x08)))
        {
            Result=1;
        }
    }
    return Result;
}
uint8_t Is_1Port_2Lanes_AppSelCode(uint8_t Lane)
{
    uint8_t Result=0;
    // Lane0-1
    if(Lane==Lane_0_1)
    {
        // PAM4 53G
        if(((Active_AppSelCode[0])==(Get_53G_PAM_Code(4)<<4))&&
           ((Active_AppSelCode[1])==(Get_53G_PAM_Code(4)<<4)))
        {
            Result=1;
        }
        // PAM4 106G S
        else if(((Active_AppSelCode[0])==(Get_106G_PAM_S_Code(4)<<4))&&
                ((Active_AppSelCode[1])==(Get_106G_PAM_S_Code(4)<<4)))
        {
            Result=1;
        }
        // PAM4 106G L
        else if(((Active_AppSelCode[0])==(Get_106G_PAM_L_Code(4)<<4))&&
                ((Active_AppSelCode[1])==(Get_106G_PAM_L_Code(4)<<4)))
        {
            Result=1;
        }
        // PAM4 IB
        else if(((Active_AppSelCode[0])==(Get_IB_PAM_Code(4)<<4))&&
                ((Active_AppSelCode[1])==(Get_IB_PAM_Code(4)<<4)))
        {
            Result=1;
        }
        // NRZ 25G
        else if(((Active_AppSelCode[0])==(Get_25G_NRZ_Code(4)<<4))&&
                ((Active_AppSelCode[1])==(Get_25G_NRZ_Code(4)<<4)))
        {
            Result=1;
        }
    }
    // Lane2-3
    else if(Lane==Lane_2_3)
    {
        // PAM4 53G
        if(((Active_AppSelCode[2])==((Get_53G_PAM_Code(4)<<4)|0x04))&&
           ((Active_AppSelCode[3])==((Get_53G_PAM_Code(4)<<4)|0x04)))
        {
            Result=1;
        }
        // PAM4 106G S
        else if(((Active_AppSelCode[2])==((Get_106G_PAM_S_Code(4)<<4)|0x04))&&
                ((Active_AppSelCode[3])==((Get_106G_PAM_S_Code(4)<<4)|0x04)))
        {
            Result=1;
        }
        // PAM4 106G L
        else if(((Active_AppSelCode[2])==((Get_106G_PAM_L_Code(4)<<4)|0x04))&&
                ((Active_AppSelCode[3])==((Get_106G_PAM_L_Code(4)<<4)|0x04)))
        {
            Result=1;
        }
        // PAM4 IB
        else if(((Active_AppSelCode[2])==((Get_IB_PAM_Code(4)<<4)|0x04))&&
                ((Active_AppSelCode[3])==((Get_IB_PAM_Code(4)<<4)|0x04)))
        {
            Result=1;
        }
        // NRZ 25G
        else if(((Active_AppSelCode[2])==((Get_25G_NRZ_Code(4)<<4)|0x04))&&
                ((Active_AppSelCode[3])==((Get_25G_NRZ_Code(4)<<4)|0x04)))
        {
            Result=1;
        }
    }
    // Lane4-5
    else if(Lane==Lane_4_5)
    {
        // PAM4 53G
        if(((Active_AppSelCode[4])==((Get_53G_PAM_Code(4)<<4)|0x08))&&
           ((Active_AppSelCode[5])==((Get_53G_PAM_Code(4)<<4)|0x08)))
        {
            Result=1;
        }
        // PAM4 106G S
        else if(((Active_AppSelCode[4])==((Get_106G_PAM_S_Code(4)<<4)|0x08))&&
                ((Active_AppSelCode[5])==((Get_106G_PAM_S_Code(4)<<4)|0x08)))
        {
            Result=1;
        }
        // PAM4 106G L
        else if(((Active_AppSelCode[4])==((Get_106G_PAM_L_Code(4)<<4)|0x08))&&
                ((Active_AppSelCode[5])==((Get_106G_PAM_L_Code(4)<<4)|0x08)))
        {
            Result=1;
        }
        // PAM4 IB
        else if(((Active_AppSelCode[4])==((Get_IB_PAM_Code(4)<<4)|0x08))&&
                ((Active_AppSelCode[5])==((Get_IB_PAM_Code(4)<<4)|0x08)))
        {
            Result=1;
        }
        // NRZ 25G
        else if(((Active_AppSelCode[4])==((Get_25G_NRZ_Code(4)<<4)|0x08))&&
                ((Active_AppSelCode[5])==((Get_25G_NRZ_Code(4)<<4)|0x08)))
        {
            Result=1;
        }
    }
    // Lane6-7
    else if(Lane==Lane_6_7)
    {
        // PAM4 53G
        if(((Active_AppSelCode[6])==((Get_53G_PAM_Code(4)<<4)|0x0C))&&
           ((Active_AppSelCode[7])==((Get_53G_PAM_Code(4)<<4)|0x0C)))
        {
            Result=1;
        }
        // PAM4 106G S
        else if(((Active_AppSelCode[6])==((Get_106G_PAM_S_Code(4)<<4)|0x0C))&&
                ((Active_AppSelCode[7])==((Get_106G_PAM_S_Code(4)<<4)|0x0C)))
        {
            Result=1;
        }
        // PAM4 106G L
        else if(((Active_AppSelCode[6])==((Get_106G_PAM_L_Code(4)<<4)|0x0C))&&
                ((Active_AppSelCode[7])==((Get_106G_PAM_L_Code(4)<<4)|0x0C)))
        {
            Result=1;
        }
        // PAM4 IB
        else if(((Active_AppSelCode[6])==((Get_IB_PAM_Code(4)<<4)|0x0C))&&
                ((Active_AppSelCode[7])==((Get_IB_PAM_Code(4)<<4)|0x0C)))
        {
            Result=1;
        }
        // NRZ 25G
        else if(((Active_AppSelCode[6])==((Get_25G_NRZ_Code(4)<<4)|0x0C))&&
                ((Active_AppSelCode[7])==((Get_25G_NRZ_Code(4)<<4)|0x0C)))
        {
            Result=1;
        }
    }
    return Result;
}
//-----------------------------------------------------------------------------------------------------//
// Page11h Status                                                                                      //
//-----------------------------------------------------------------------------------------------------//
//-----------------------------------------------------------------------------------------------------//
// Data Path State Indicator Byte128-131
//-----------------------------------------------------------------------------------------------------//
//----------------------------------------------------------//
// Page11h Data Path State 0x80 and Lane State Changed 0x86 //
//----------------------State List--------------------------//
// Deactivated_DataPath                                     //
// Deactivated_DataPath                                     //
// Init_DataPath                                            //
// Deinit_DataPath                                          //
// Activated_DataPath                                       //
// TxTurnOn_DataPath                                        //
// TxTurnOFF_DataPath                                       //
// Initialized_DataPath                                     //
//----------------------------------------------------------//
void MSA_DataPathState(uint8_t DataPathState)
{
    // Update Bef Data Path
    Bef_Lane_DataPath[0] = DataPathState;
    Bef_Lane_DataPath[1] = DataPathState;
    Bef_Lane_DataPath[2] = DataPathState;
    Bef_Lane_DataPath[3] = DataPathState;
    Bef_Lane_DataPath[4] = DataPathState;
    Bef_Lane_DataPath[5] = DataPathState;
    Bef_Lane_DataPath[6] = DataPathState;
    Bef_Lane_DataPath[7] = DataPathState;
    
    // Only Steady State to show Lane State Changed
	if((DataPathState==DataPathDeactivated)||(DataPathState==DataPathInitialized)||(DataPathState==DataPathActivated))
		QSFPDD_P11[6] = 0xFF;
    
	DataPathState |= (DataPathState<<4);
	//Lane Data Path State
	QSFPDD_P11[0] = DataPathState;
	QSFPDD_P11[1] = DataPathState;
    QSFPDD_P11[2] = DataPathState;
	QSFPDD_P11[3] = DataPathState;
}

void MSA_Lane_DataPathState(uint8_t Lane, uint8_t DataPathState)
{
    if(Lane==0)
    {
        //Lane Data Path State
        QSFPDD_P11[0]=((QSFPDD_P11[0]&= ~0x0F)|DataPathState);
        if(Bef_Lane_DataPath[0]!=DataPathState)
        {
            //Lane State Changed
            QSFPDD_P11[6] |= 0x01;
            Bef_Lane_DataPath[0]=DataPathState;
        }
    }
    else if(Lane==1)
    {
        //Lane Data Path State
        QSFPDD_P11[0]=((QSFPDD_P11[0]&= ~0xF0)|DataPathState<<4);
        if(Bef_Lane_DataPath[1]!=DataPathState)
        {
            //Lane State Changed
            QSFPDD_P11[6] |= 0x02;
            Bef_Lane_DataPath[1]=DataPathState;
        }
    }
    else if(Lane==2)
    {
        //Lane Data Path State
        QSFPDD_P11[1]=((QSFPDD_P11[1]&= ~0x0F)|DataPathState);
        if(Bef_Lane_DataPath[2]!=DataPathState)
        {
            //Lane State Changed
            QSFPDD_P11[6] |= 0x04;
            Bef_Lane_DataPath[2]=DataPathState;
        }
    }
    else if(Lane==3)
    {
        //Lane Data Path State
        QSFPDD_P11[1]=((QSFPDD_P11[1]&= ~0xF0)|DataPathState<<4);
        if(Bef_Lane_DataPath[3]!=DataPathState)
        {
            //Lane State Changed
            QSFPDD_P11[6] |= 0x08;
            Bef_Lane_DataPath[3]=DataPathState;
        }
    }
    else if(Lane==4)
    {
        //Lane Data Path State
        QSFPDD_P11[2]=((QSFPDD_P11[2]&= ~0x0F)|DataPathState);
        if(Bef_Lane_DataPath[4]!=DataPathState)
        {
            //Lane State Changed
            QSFPDD_P11[6] |= 0x10;
            Bef_Lane_DataPath[4]=DataPathState;
        }
    }
    else if(Lane==5)
    {
        //Lane Data Path State
        QSFPDD_P11[2]=((QSFPDD_P11[2]&= ~0xF0)|DataPathState<<4);
        if(Bef_Lane_DataPath[5]!=DataPathState)
        {
            //Lane State Changed
            QSFPDD_P11[6] |= 0x20;
            Bef_Lane_DataPath[5]=DataPathState;
        }
    }
    else if(Lane==6)
    {
        //Lane Data Path State
        QSFPDD_P11[3]=((QSFPDD_P11[3]&= ~0x0F)|DataPathState);
        if(Bef_Lane_DataPath[6]!=DataPathState)
        {
            //Lane State Changed
            QSFPDD_P11[6] |= 0x40;
            Bef_Lane_DataPath[6]=DataPathState;
        }
    }
    else if(Lane==7)
    {
        //Lane Data Path State
        QSFPDD_P11[3]=((QSFPDD_P11[3]&= ~0xF0)|DataPathState<<4);
        if(Bef_Lane_DataPath[7]!=DataPathState)
        {
            //Lane State Changed
            QSFPDD_P11[6] |= 0x80;
            Bef_Lane_DataPath[7]=DataPathState;
        }
    }
}

//-----------------------------------------------------------------------------------------------------//
// Configuration Error Code Byte202-205
//-----------------------------------------------------------------------------------------------------//
uint8_t Verify_Config_Error_Code(uint8_t Error_Code,uint8_t Lane)
{
	uint8_t Data_Buffer,Result=0,i;
    uint8_t Error_Code_Buffer[4]={0};
    // Page11 202 QSFPDD_P11[74] Bit7-4 Lane1 , Bit3-0 Lane0
	// Page11 203 QSFPDD_P11[75] Bit7-4 Lane3 , Bit3-0 Lane2
	// Page11 204 QSFPDD_P11[76] Bit7-4 Lane5 , Bit3-0 Lane4
	// Page11 205 QSFPDD_P11[77] Bit7-4 Lane7 , Bit3-0 Lane6
	for(i=0;i<4;i++)
	{
		if(MSA_Config_Error_Code)
			Error_Code_Buffer[i]=QSFPDD_P11[74+i];
		else
			Error_Code_Buffer[i]=Internal_Config_Error_Code[i];
	}
	if(Lane==0)
		Data_Buffer=Error_Code_Buffer[0]&0x0F;
	else if(Lane==1)
		Data_Buffer=((Error_Code_Buffer[0]&0xF0)>>4);
	else if(Lane==2)
		Data_Buffer=Error_Code_Buffer[1]&0x0F;
	else if(Lane==3)
		Data_Buffer=((Error_Code_Buffer[1]&0xF0)>>4);
	else if(Lane==4)
		Data_Buffer=Error_Code_Buffer[2]&0x0F;
	else if(Lane==5)
		Data_Buffer=((Error_Code_Buffer[2]&0xF0)>>4);
	else if(Lane==6)
		Data_Buffer=Error_Code_Buffer[3]&0x0F;
	else if(Lane==7)
		Data_Buffer=((Error_Code_Buffer[3]&0xF0)>>4);

	if(Data_Buffer==Error_Code)
		Result=1;

	return Result;
}

void Configuration_Error_code_Feedback(uint8_t Lane_CH,uint8_t Write_Error_code)
{
	uint8_t Data_Buffer = 0;
	uint8_t Current_Address = 0 ;
    // Page11 202 QSFPDD_P11[74] Lan0/Lan1
	// Page11 203 QSFPDD_P11[75] Lan2/Lan3
	// Page11 204 QSFPDD_P11[76] Lan4/Lan5
	// Page11 205 QSFPDD_P11[77] Lan6/Lan7
	// Determine odd and even
	if (Lane_CH==8)
	{
        if(MSA_Config_Error_Code)
		{
			QSFPDD_P11[74] = ((Write_Error_code<<4)|Write_Error_code);
			QSFPDD_P11[75] = ((Write_Error_code<<4)|Write_Error_code);
			QSFPDD_P11[76] = ((Write_Error_code<<4)|Write_Error_code);
			QSFPDD_P11[77] = ((Write_Error_code<<4)|Write_Error_code);
		}
		else
		{
			Internal_Config_Error_Code[0] = ((Write_Error_code<<4)|Write_Error_code);
			Internal_Config_Error_Code[1] = ((Write_Error_code<<4)|Write_Error_code);
			Internal_Config_Error_Code[2] = ((Write_Error_code<<4)|Write_Error_code);
			Internal_Config_Error_Code[3] = ((Write_Error_code<<4)|Write_Error_code);
		}
	}
	else
	{
		if( Lane_CH%2 == 0 )
		{
			if(MSA_Config_Error_Code)
			{
				Current_Address = ( 74 + Lane_CH/2 ) ;
				Data_Buffer = QSFPDD_P11[Current_Address] & 0xF0 ;
				Data_Buffer = (Data_Buffer | Write_Error_code);
				QSFPDD_P11[Current_Address] = Data_Buffer ;
			}
			else
			{
				Current_Address = ( 0 + Lane_CH/2 ) ;
				Data_Buffer = Internal_Config_Error_Code[Current_Address] & 0xF0 ;
				Data_Buffer = (Data_Buffer | Write_Error_code);
				Internal_Config_Error_Code[Current_Address] = Data_Buffer ;
			}
		}
		else
		{
			if(MSA_Config_Error_Code)
			{
				Current_Address = ( 74 + Lane_CH/2 ) ;
				Data_Buffer = QSFPDD_P11[Current_Address] & 0x0F ;
				Data_Buffer = ( Data_Buffer | ( Write_Error_code << 4 ));
				QSFPDD_P11[Current_Address] = Data_Buffer ;
			}
			else
			{
				Current_Address = ( 0 + Lane_CH/2 ) ;
				Data_Buffer = Internal_Config_Error_Code[Current_Address] & 0x0F ;
				Data_Buffer = ( Data_Buffer | ( Write_Error_code << 4 ));
				Internal_Config_Error_Code[Current_Address] = Data_Buffer ;
			}
		}
	}
}
//-----------------------------------------------------------------------------------------------------//
// Page10h Controls                                                                                    //
//-----------------------------------------------------------------------------------------------------//
//-----------------------------------------------------------------------------------------------------//
// Tx Disable Byte130
//-----------------------------------------------------------------------------------------------------//
void Tx_Disable(uint8_t Lane,uint8_t Disable)
{
	if(Disable)
	{
		LDD_TX1_TX8_Tx_Disable(Lane,1);
		Tx_Disable_Flag|=(0x01<<Lane);
	}
	else
	{//
		LDD_TX1_TX8_Tx_Disable(Lane,0);
		Tx_Disable_Flag&=~(0x01<<Lane);
	}

}
//-----------------------------------------------------------------------------------------------------//
// Tx Squelch Disable Byte131
//-----------------------------------------------------------------------------------------------------//
void Auto_Squelch_Tx_Output()
{
	uint8_t Lane;
	//For DDMI Calibration station to show ddmi status
	if(CALIB_MEMORY_MAP.SKIP_DSP_Tx_Los_Check==0)
	{
		if(Tx_Los_Enable_Flag==0)
		{
			if(Bef_Tx_LOS_Buffer!=Tx_LOS_Buffer)
			{
				Tx_Los_Enable_Flag = 1 ;
				Bef_Tx_LOS_Buffer=Tx_LOS_Buffer;
			}
			//For Tx Squelch Disable
			if(Bef_Tx_Squelch_Buffer!=QSFPDD_P10[3])
			{
				Tx_Los_Enable_Flag = 1 ;
				Bef_Tx_Squelch_Buffer=QSFPDD_P10[3];
			}
		}
		else
		{
			// Update new Active AppSelCode to auto disable tx output
			for(Lane=0;Lane<8;Lane++)
			{
				// invalid config lane auto squelch tx output
				if(Active_AppSelCode[Lane]==0x00)
					Tx_LOS_Buffer|=(0x01<<Lane);
			}
			// 1 Port 8 Lanes Check Lane0-7
			if(Is_1Port_8Lanes_AppSelCode())
			{
				if(Tx_LOS_Buffer>0)
                    Auto_Squelch_Tx_Flag |= 0xFF ;
				else
                    Auto_Squelch_Tx_Flag = 0x00 ;
			}
			else
			{
				// 1 Port 4 Lanes Check Lane0-3
				if(Is_1Port_4Lanes_AppSelCode(Lane_0_3))
				{
	                // Lane0-Lane3
	                if( (Tx_LOS_Buffer&0x0F)>0 )
                		Auto_Squelch_Tx_Flag |= 0x0F ;
	                else
                        Auto_Squelch_Tx_Flag &= ~0x0F ;
				}
				else
				{
					// 1 Port 2 Lanes Check Lane0-1
					if(Is_1Port_2Lanes_AppSelCode(Lane_0_1))
					{
		                // Lane0-Lane1
		                if( (Tx_LOS_Buffer&0x03)>0 )
	                		Auto_Squelch_Tx_Flag |= 0x03 ;
		                else
	                        Auto_Squelch_Tx_Flag &= ~0x03 ;
					}
                    // 1 Lane 1Port PAM4 or NRZ Check Lane0-1
                    else
                    {
						// Lane0-Lane1
                        for(Lane=0;Lane<2;Lane++)
                        {
                            if(Tx_LOS_Buffer&(0x01<<Lane))
                            {
                                Auto_Squelch_Tx_Flag |= (0x01<<Lane);
                            }
                            else
                            {
                                Auto_Squelch_Tx_Flag &= ~(0x01<<Lane);
                            }
                            
                        }
                    }
					// 1 Port 2 Lanes Check Lane2-3
					if(Is_1Port_2Lanes_AppSelCode(Lane_2_3))
					{
		                // Lane2-Lane3
		                if( (Tx_LOS_Buffer&0x0C)>0 )
	                		Auto_Squelch_Tx_Flag |= 0x0C ;
		                else
	                        Auto_Squelch_Tx_Flag &= ~0x0C ;
					}
                    // 1 Lane 1Port PAM4 or NRZ Check Lane2-3
                    else
                    {
                        // Lane2-Lane3
                        for(Lane=0;Lane<2;Lane++)
                        {
                            if(Tx_LOS_Buffer&(0x04<<Lane))
                            {
                                Auto_Squelch_Tx_Flag |= (0x04<<Lane);
                            }
                            else
                            {
                                Auto_Squelch_Tx_Flag &= ~(0x04<<Lane);
                            }
                        }
                    }
				}
				// 1 Port 4 Lanes Check Lane4-7
				if(Is_1Port_4Lanes_AppSelCode(Lane_4_7))
				{
	                // Lane4-Lane7
	                if( (Tx_LOS_Buffer&0xF0)>0 )
                		Auto_Squelch_Tx_Flag |= 0xF0 ;
	                else
                        Auto_Squelch_Tx_Flag &= ~0xF0 ;
				}
				else
				{
					// 1 Port 2 Lanes Check Lane4-5
					if(Is_1Port_2Lanes_AppSelCode(Lane_4_5))
					{
		                // Lane4-Lane5
		                if( (Tx_LOS_Buffer&0x30)>0 )
	                		Auto_Squelch_Tx_Flag |= 0x30 ;
		                else
	                        Auto_Squelch_Tx_Flag &= ~0x30 ;
					}
                    // 1 Lane 1Port PAM4 or NRZ Check Lane4-5
                    else
                    {
						// Lane4-Lane5
						for(Lane=0;Lane<2;Lane++)
                        {
                            if(Tx_LOS_Buffer&(0x10<<Lane))
                            {
                                Auto_Squelch_Tx_Flag |= (0x10<<Lane);
                            }
                            else
                            {
                                Auto_Squelch_Tx_Flag &= ~(0x10<<Lane);
                            }
                        }
                    }
					// 1 Port 2 Lanes Check Lane6-7
					if(Is_1Port_2Lanes_AppSelCode(Lane_6_7))
					{
		                // Lane6-Lane7
		                if( (Tx_LOS_Buffer&0xC0)>0 )
	                		Auto_Squelch_Tx_Flag |= 0xC0 ;
		                else
	                        Auto_Squelch_Tx_Flag &= ~0xC0 ;
					}
                    // 1 Lane 1Port PAM4 or NRZ Check Lane6-7
                    else
                    {
						// Lane6-Lane7
						for(Lane=0;Lane<2;Lane++)
                        {
                            if(Tx_LOS_Buffer&(0x40<<Lane))
                            {
                                Auto_Squelch_Tx_Flag |= (0x40<<Lane);
                            }
                            else
                            {
                                Auto_Squelch_Tx_Flag &= ~(0x40<<Lane);
                            }
                        }
                    }
				}
			}
			//For Tx Squelch Disable
            if(QSFPDD_P10[3]!=0x00)
            {
            	//Lane0-Lane7
            	for(Lane=0;Lane<8;Lane++)
            	{
            		if(QSFPDD_P10[3]&(0x01<<Lane))
            			Auto_Squelch_Tx_Flag &= ~(0x01<<Lane);
            	}
            }
			Tx_Los_Enable_Flag = 0;
		}
	}
    //for calibration station
	else
		Tx_Disable_Flag = 0x00;
}

//------------------------------------------------------//
// Tx Force Squelch Enable 0x84
//------------------------------------------------------//
void Tx_Force_Squelch(uint8_t Lane,uint8_t Squelch_Value)
{
    if(Squelch_Value)
        DSP_LineSide_TX_Squelch_SET( Lane , Squelch );
    else
        DSP_LineSide_TX_Squelch_SET( Lane , Unsquelch );
}

//-----------------------------------------------------------------------------------------------------//
// Rx Output Disable Byte138
//-----------------------------------------------------------------------------------------------------//
void Rx_Output_Disable(uint8_t Control_Data)
{
    if(Rx_output_flag==0) 
    {
        if(Bef_Rx_output!=Control_Data)
        {
            Rx_output_flag=1;
            Bef_Rx_output=Control_Data;
        }
    }
    else
    {
        // Rx1
        if(Control_Data&0x01)
            DSP_System_Side_TX_Squelch_SET( 0 , Squelch );
        else
            DSP_System_Side_TX_Squelch_SET( 0 , Unsquelch );
        // Rx2
        if(Control_Data&0x02)
            DSP_System_Side_TX_Squelch_SET( 1 , Squelch );
        else
            DSP_System_Side_TX_Squelch_SET( 1 , Unsquelch );
        // Rx3
        if(Control_Data&0x04)
            DSP_System_Side_TX_Squelch_SET( 2 , Squelch );
        else
            DSP_System_Side_TX_Squelch_SET( 2 , Unsquelch );
        // Rx4
        if(Control_Data&0x08)
            DSP_System_Side_TX_Squelch_SET( 3 , Squelch );
        else
            DSP_System_Side_TX_Squelch_SET( 3 , Unsquelch );
        
        // Rx5
        if(Control_Data&0x10)
            DSP_System_Side_TX_Squelch_SET( 4 , Squelch );
        else
            DSP_System_Side_TX_Squelch_SET( 4 , Unsquelch );
        // Rx6
        if(Control_Data&0x20)
            DSP_System_Side_TX_Squelch_SET( 5 , Squelch );
        else
            DSP_System_Side_TX_Squelch_SET( 5 , Unsquelch );
        // Rx7
        if(Control_Data&0x40)
            DSP_System_Side_TX_Squelch_SET( 6 , Squelch );
        else
            DSP_System_Side_TX_Squelch_SET( 6 , Unsquelch );
        // Rx8
        if(Control_Data&0x80)
            DSP_System_Side_TX_Squelch_SET( 7 , Squelch );
        else
            DSP_System_Side_TX_Squelch_SET( 7 , Unsquelch );
        
        Rx_output_flag=0;
    }
}

//-----------------------------------------------------------------------------------------------------//
// Rx Squelch Disable Byte139
//-----------------------------------------------------------------------------------------------------//
// 1.Any rx los trigger then auto rx squelch
// 2.Page10h 0x8B is 0xFF then disable rx squelch
//-----------------------------------------------------------------------------------------------------//
void Auto_Squelch_Rx_Output()
{
	uint8_t Data_Buffer=0x00,i;

	if(Rx_Los_Enable_Flag==0)
	{
		if(Bef_Rx_LOS_Buffer!=Rx_LOS_Buffer)
		{
			Rx_Los_Enable_Flag = 1 ;
			Bef_Rx_LOS_Buffer=Rx_LOS_Buffer;
		}
		if(Bef_Rx_Squelch_Buffer!=QSFPDD_P10[11])
		{
			Rx_Los_Enable_Flag = 1 ;
			Bef_Rx_Squelch_Buffer=QSFPDD_P10[11];
		}
	}
	else
	{
        // Update new Active AppSelCode to auto disable rx output
        for(i=0;i<8;i++)
        {
            // invalid config lane auto squelch rx output
            if(Active_AppSelCode[i]==0x00)
                Rx_LOS_Buffer|=(0x01<<i);
        }
        // 1 Port 8 Lanes AppSelCode Auto Squelch
		if(Is_1Port_8Lanes_AppSelCode())
		{
			if(Rx_LOS_Buffer!=0)
			{
				Data_Buffer=0xFF;		//Rx Squelch all
			}
			else if(Rx_LOS_Buffer==0)
			{
				Data_Buffer=0x00;		//Rx Unsquelch all
			}
		}
		else
		{
			// 1 Port 4 Lanes AppSelCode Auto Squelch
			if(Is_1Port_4Lanes_AppSelCode(Lane_0_3))
			{
                // Lane0-Lane3
                if( (Rx_LOS_Buffer&0x0F)>0 )
                	Data_Buffer|=0x0F;
                else
                	Data_Buffer&=~0x0F;
			}
			// 1 Port 2 Lanes AppSelCode Auto Squelch
			else
			{
				if(Is_1Port_2Lanes_AppSelCode(Lane_0_1))
				{
	                // Lane0-Lane1
	                if( (Rx_LOS_Buffer&0x03)>0 )
	                	Data_Buffer |= 0x03 ;
	                else
	                	Data_Buffer &= ~0x03 ;
				}
				if(Is_1Port_2Lanes_AppSelCode(Lane_2_3))
				{
	                // Lane2-Lane3
	                if( (Rx_LOS_Buffer&0x0C)>0 )
	                	Data_Buffer |= 0x0C ;
	                else
	                	Data_Buffer &= ~0x0C ;
				}
			}
			// 1 Port 4 Lanes AppSelCode Auto Squelch
			if(Is_1Port_4Lanes_AppSelCode(Lane_4_7))
			{
				// Lane4-Lane7
				if( (Rx_LOS_Buffer&0xF0)>0 )
					Data_Buffer |= 0xF0 ;
				else
					Data_Buffer &= ~0xF0 ;
			}
			// 1 Port 2 Lanes AppSelCode Auto Squelch
			else
			{
                if(Is_1Port_2Lanes_AppSelCode(Lane_4_5))
				{
					// Lane4-Lane5
					if( (Rx_LOS_Buffer&0x30)>0 )
						Data_Buffer |= 0x30 ;
					else
						Data_Buffer &= ~0x30 ;
				}
                if(Is_1Port_2Lanes_AppSelCode(Lane_6_7))
				{
					// Lane6-Lane7
					if( (Rx_LOS_Buffer&0xC0)>0 )
						Data_Buffer |= 0xC0 ;
					else
						Data_Buffer &= ~0xC0 ;
				}
			}
		}
        //For Rx Squelch Disable
		if(QSFPDD_P10[11]!=0x00)
		{
			//Lane0-Lane3
			if(QSFPDD_P10[11]&0x01)
				Data_Buffer &= ~0x01;
			if(QSFPDD_P10[11]&0x02)
				Data_Buffer &= ~0x02;
			if(QSFPDD_P10[11]&0x04)
				Data_Buffer &= ~0x04;
			if(QSFPDD_P10[11]&0x08)
				Data_Buffer &= ~0x08;
			//Lane4-Lane7
			if(QSFPDD_P10[11]&0x10)
				Data_Buffer &= ~0x10;
			if(QSFPDD_P10[11]&0x20)
				Data_Buffer &= ~0x20;
			if(QSFPDD_P10[11]&0x40)
				Data_Buffer &= ~0x40;
			if(QSFPDD_P10[11]&0x80)
				Data_Buffer &= ~0x80;
		}
		Rx_output_flag=1;
		Rx_Output_Disable(Data_Buffer);
		Rx_Los_Enable_Flag=0;
	}
}

//-----------------------------------------------------------------------------------------------------//
// Staged Control Set 0, Application Select Controls Byte145-152
//-----------------------------------------------------------------------------------------------------//
uint8_t Verify_1Port_8Lane_AppSelCode()
{
    uint8_t Result=0;
    // PAM 53G
    if(((AppSelCode_Buffer[0]&0xF0)==(Get_53G_PAM_Code(1)<<4))&&
	   ((AppSelCode_Buffer[1]&0xF0)==(Get_53G_PAM_Code(1)<<4))&&
	   ((AppSelCode_Buffer[2]&0xF0)==(Get_53G_PAM_Code(1)<<4))&&
	   ((AppSelCode_Buffer[3]&0xF0)==(Get_53G_PAM_Code(1)<<4))&&
	   ((AppSelCode_Buffer[4]&0xF0)==(Get_53G_PAM_Code(1)<<4))&&
	   ((AppSelCode_Buffer[5]&0xF0)==(Get_53G_PAM_Code(1)<<4))&&
	   ((AppSelCode_Buffer[6]&0xF0)==(Get_53G_PAM_Code(1)<<4))&&
	   ((AppSelCode_Buffer[7]&0xF0)==(Get_53G_PAM_Code(1)<<4)))
    {
        Result=1;
    }
    // PAM 106G S
    else if(((AppSelCode_Buffer[0]&0xF0)==(Get_106G_PAM_S_Code(1)<<4))&&
            ((AppSelCode_Buffer[1]&0xF0)==(Get_106G_PAM_S_Code(1)<<4))&&
            ((AppSelCode_Buffer[2]&0xF0)==(Get_106G_PAM_S_Code(1)<<4))&&
            ((AppSelCode_Buffer[3]&0xF0)==(Get_106G_PAM_S_Code(1)<<4))&&
            ((AppSelCode_Buffer[4]&0xF0)==(Get_106G_PAM_S_Code(1)<<4))&&
            ((AppSelCode_Buffer[5]&0xF0)==(Get_106G_PAM_S_Code(1)<<4))&&
            ((AppSelCode_Buffer[6]&0xF0)==(Get_106G_PAM_S_Code(1)<<4))&&
            ((AppSelCode_Buffer[7]&0xF0)==(Get_106G_PAM_S_Code(1)<<4)))
    {
        Result=1;
    }
    // PAM 106G S 2
    else if(((AppSelCode_Buffer[0]&0xF0)==(Get_106G_PAM_S_2_Code(1)<<4))&&
            ((AppSelCode_Buffer[1]&0xF0)==(Get_106G_PAM_S_2_Code(1)<<4))&&
            ((AppSelCode_Buffer[2]&0xF0)==(Get_106G_PAM_S_2_Code(1)<<4))&&
            ((AppSelCode_Buffer[3]&0xF0)==(Get_106G_PAM_S_2_Code(1)<<4))&&
            ((AppSelCode_Buffer[4]&0xF0)==(Get_106G_PAM_S_2_Code(1)<<4))&&
            ((AppSelCode_Buffer[5]&0xF0)==(Get_106G_PAM_S_2_Code(1)<<4))&&
            ((AppSelCode_Buffer[6]&0xF0)==(Get_106G_PAM_S_2_Code(1)<<4))&&
            ((AppSelCode_Buffer[7]&0xF0)==(Get_106G_PAM_S_2_Code(1)<<4)))
    {
        Result=1;
    }
    // PAM 106G L
    else if(((AppSelCode_Buffer[0]&0xF0)==(Get_106G_PAM_L_Code(1)<<4))&&
            ((AppSelCode_Buffer[1]&0xF0)==(Get_106G_PAM_L_Code(1)<<4))&&
            ((AppSelCode_Buffer[2]&0xF0)==(Get_106G_PAM_L_Code(1)<<4))&&
            ((AppSelCode_Buffer[3]&0xF0)==(Get_106G_PAM_L_Code(1)<<4))&&
            ((AppSelCode_Buffer[4]&0xF0)==(Get_106G_PAM_L_Code(1)<<4))&&
            ((AppSelCode_Buffer[5]&0xF0)==(Get_106G_PAM_L_Code(1)<<4))&&
            ((AppSelCode_Buffer[6]&0xF0)==(Get_106G_PAM_L_Code(1)<<4))&&
            ((AppSelCode_Buffer[7]&0xF0)==(Get_106G_PAM_L_Code(1)<<4)))
    {
        Result=1;
    }
    // PAM 106G L 2
    else if(((AppSelCode_Buffer[0]&0xF0)==(Get_106G_PAM_L_2_Code(1)<<4))&&
            ((AppSelCode_Buffer[1]&0xF0)==(Get_106G_PAM_L_2_Code(1)<<4))&&
            ((AppSelCode_Buffer[2]&0xF0)==(Get_106G_PAM_L_2_Code(1)<<4))&&
            ((AppSelCode_Buffer[3]&0xF0)==(Get_106G_PAM_L_2_Code(1)<<4))&&
            ((AppSelCode_Buffer[4]&0xF0)==(Get_106G_PAM_L_2_Code(1)<<4))&&
            ((AppSelCode_Buffer[5]&0xF0)==(Get_106G_PAM_L_2_Code(1)<<4))&&
            ((AppSelCode_Buffer[6]&0xF0)==(Get_106G_PAM_L_2_Code(1)<<4))&&
            ((AppSelCode_Buffer[7]&0xF0)==(Get_106G_PAM_L_2_Code(1)<<4)))
    {
        Result=1;
    }
    // PAM IB
    else if(((AppSelCode_Buffer[0]&0xF0)==(Get_IB_PAM_Code(1)<<4))&&
            ((AppSelCode_Buffer[1]&0xF0)==(Get_IB_PAM_Code(1)<<4))&&
            ((AppSelCode_Buffer[2]&0xF0)==(Get_IB_PAM_Code(1)<<4))&&
            ((AppSelCode_Buffer[3]&0xF0)==(Get_IB_PAM_Code(1)<<4))&&
            ((AppSelCode_Buffer[4]&0xF0)==(Get_IB_PAM_Code(1)<<4))&&
            ((AppSelCode_Buffer[5]&0xF0)==(Get_IB_PAM_Code(1)<<4))&&
            ((AppSelCode_Buffer[6]&0xF0)==(Get_IB_PAM_Code(1)<<4))&&
            ((AppSelCode_Buffer[7]&0xF0)==(Get_IB_PAM_Code(1)<<4)))
    {
        Result=1;
    }
    // PAM IB 2
    else if(((AppSelCode_Buffer[0]&0xF0)==(Get_IB_PAM_2_Code(1)<<4))&&
            ((AppSelCode_Buffer[1]&0xF0)==(Get_IB_PAM_2_Code(1)<<4))&&
            ((AppSelCode_Buffer[2]&0xF0)==(Get_IB_PAM_2_Code(1)<<4))&&
            ((AppSelCode_Buffer[3]&0xF0)==(Get_IB_PAM_2_Code(1)<<4))&&
            ((AppSelCode_Buffer[4]&0xF0)==(Get_IB_PAM_2_Code(1)<<4))&&
            ((AppSelCode_Buffer[5]&0xF0)==(Get_IB_PAM_2_Code(1)<<4))&&
            ((AppSelCode_Buffer[6]&0xF0)==(Get_IB_PAM_2_Code(1)<<4))&&
            ((AppSelCode_Buffer[7]&0xF0)==(Get_IB_PAM_2_Code(1)<<4)))
    {
        Result=1;
    }
    // NRZ 25G
    else if(((AppSelCode_Buffer[0]&0xF0)==(Get_25G_NRZ_Code(1)<<4))&&
            ((AppSelCode_Buffer[1]&0xF0)==(Get_25G_NRZ_Code(1)<<4))&&
            ((AppSelCode_Buffer[2]&0xF0)==(Get_25G_NRZ_Code(1)<<4))&&
            ((AppSelCode_Buffer[3]&0xF0)==(Get_25G_NRZ_Code(1)<<4))&&
            ((AppSelCode_Buffer[4]&0xF0)==(Get_25G_NRZ_Code(1)<<4))&&
            ((AppSelCode_Buffer[5]&0xF0)==(Get_25G_NRZ_Code(1)<<4))&&
            ((AppSelCode_Buffer[6]&0xF0)==(Get_25G_NRZ_Code(1)<<4))&&
            ((AppSelCode_Buffer[7]&0xF0)==(Get_25G_NRZ_Code(1)<<4)))
    {
        Result=1;
    }
    return Result;
}
uint8_t Verify_1Port_4Lane_AppSelCode(uint8_t Lane)
{
    uint8_t Result=0;
    if(Lane==Lane_0_3)
    {
        // PAM 53G
        if(((AppSelCode_Buffer[0]&0xF0)==(Get_53G_PAM_Code(2)<<4))&&
           ((AppSelCode_Buffer[1]&0xF0)==(Get_53G_PAM_Code(2)<<4))&&
           ((AppSelCode_Buffer[2]&0xF0)==(Get_53G_PAM_Code(2)<<4))&&
           ((AppSelCode_Buffer[3]&0xF0)==(Get_53G_PAM_Code(2)<<4)))
        {
            Result=1;
        }
        // PAM 106G S
        else if(((AppSelCode_Buffer[0]&0xF0)==(Get_106G_PAM_S_Code(2)<<4))&&
                ((AppSelCode_Buffer[1]&0xF0)==(Get_106G_PAM_S_Code(2)<<4))&&
                ((AppSelCode_Buffer[2]&0xF0)==(Get_106G_PAM_S_Code(2)<<4))&&
                ((AppSelCode_Buffer[3]&0xF0)==(Get_106G_PAM_S_Code(2)<<4)))
        {
            Result=1;
        }
        // PAM 106G L
        else if(((AppSelCode_Buffer[0]&0xF0)==(Get_106G_PAM_L_Code(2)<<4))&&
                ((AppSelCode_Buffer[1]&0xF0)==(Get_106G_PAM_L_Code(2)<<4))&&
                ((AppSelCode_Buffer[2]&0xF0)==(Get_106G_PAM_L_Code(2)<<4))&&
                ((AppSelCode_Buffer[3]&0xF0)==(Get_106G_PAM_L_Code(2)<<4)))
        {
            Result=1;
        }
        // PAM IB
        else if(((AppSelCode_Buffer[0]&0xF0)==(Get_IB_PAM_Code(2)<<4))&&
                ((AppSelCode_Buffer[1]&0xF0)==(Get_IB_PAM_Code(2)<<4))&&
                ((AppSelCode_Buffer[2]&0xF0)==(Get_IB_PAM_Code(2)<<4))&&
                ((AppSelCode_Buffer[3]&0xF0)==(Get_IB_PAM_Code(2)<<4)))
        {
            Result=1;
        }
        // NRZ 25G
        else if(((AppSelCode_Buffer[0]&0xF0)==(Get_25G_NRZ_Code(2)<<4))&&
                ((AppSelCode_Buffer[1]&0xF0)==(Get_25G_NRZ_Code(2)<<4))&&
                ((AppSelCode_Buffer[2]&0xF0)==(Get_25G_NRZ_Code(2)<<4))&&
                ((AppSelCode_Buffer[3]&0xF0)==(Get_25G_NRZ_Code(2)<<4)))
        {
            Result=1;
        }
    }
    else if(Lane==Lane_4_7)
    {
        // PAM 53G
        if(((AppSelCode_Buffer[4]&0xF0)==(Get_53G_PAM_Code(2)<<4))&&
           ((AppSelCode_Buffer[5]&0xF0)==(Get_53G_PAM_Code(2)<<4))&&
           ((AppSelCode_Buffer[6]&0xF0)==(Get_53G_PAM_Code(2)<<4))&&
           ((AppSelCode_Buffer[7]&0xF0)==(Get_53G_PAM_Code(2)<<4)))
        {
            Result=1;
        }
        // PAM 106G S
        else if(((AppSelCode_Buffer[4]&0xF0)==(Get_106G_PAM_S_Code(2)<<4))&&
                ((AppSelCode_Buffer[5]&0xF0)==(Get_106G_PAM_S_Code(2)<<4))&&
                ((AppSelCode_Buffer[6]&0xF0)==(Get_106G_PAM_S_Code(2)<<4))&&
                ((AppSelCode_Buffer[7]&0xF0)==(Get_106G_PAM_S_Code(2)<<4)))
        {
            Result=1;
        }
        // PAM 106G L
        else if(((AppSelCode_Buffer[4]&0xF0)==(Get_106G_PAM_L_Code(2)<<4))&&
                ((AppSelCode_Buffer[5]&0xF0)==(Get_106G_PAM_L_Code(2)<<4))&&
                ((AppSelCode_Buffer[6]&0xF0)==(Get_106G_PAM_L_Code(2)<<4))&&
                ((AppSelCode_Buffer[7]&0xF0)==(Get_106G_PAM_L_Code(2)<<4)))
        {
            Result=1;
        }
        // PAM IB
        else if(((AppSelCode_Buffer[4]&0xF0)==(Get_IB_PAM_Code(2)<<4))&&
                ((AppSelCode_Buffer[5]&0xF0)==(Get_IB_PAM_Code(2)<<4))&&
                ((AppSelCode_Buffer[6]&0xF0)==(Get_IB_PAM_Code(2)<<4))&&
                ((AppSelCode_Buffer[7]&0xF0)==(Get_IB_PAM_Code(2)<<4)))
        {
            Result=1;
        }
        // NRZ 25G
        else if(((AppSelCode_Buffer[4]&0xF0)==(Get_25G_NRZ_Code(2)<<4))&&
                ((AppSelCode_Buffer[5]&0xF0)==(Get_25G_NRZ_Code(2)<<4))&&
                ((AppSelCode_Buffer[6]&0xF0)==(Get_25G_NRZ_Code(2)<<4))&&
                ((AppSelCode_Buffer[7]&0xF0)==(Get_25G_NRZ_Code(2)<<4)))
        {
            Result=1;
        }
    }
    return Result;
}
uint8_t Verify_1Port_2Lane_AppSelCode(uint8_t Lane)
{
    uint8_t Result=0;
    if(Lane==Lane_0_1)
    {
        // PAM 53G
        if(((AppSelCode_Buffer[0]&0xF0)==(Get_53G_PAM_Code(4)<<4))&&
           ((AppSelCode_Buffer[1]&0xF0)==(Get_53G_PAM_Code(4)<<4)))
        {
            Result=1;
        }
        // PAM 106G S
        else if(((AppSelCode_Buffer[0]&0xF0)==(Get_106G_PAM_S_Code(4)<<4))&&
                ((AppSelCode_Buffer[1]&0xF0)==(Get_106G_PAM_S_Code(4)<<4)))
        {
            Result=1;
        }
        // PAM 106G L
        else if(((AppSelCode_Buffer[0]&0xF0)==(Get_106G_PAM_L_Code(4)<<4))&&
                ((AppSelCode_Buffer[1]&0xF0)==(Get_106G_PAM_L_Code(4)<<4)))
        {
            Result=1;
        }
        // PAM IB
        else if(((AppSelCode_Buffer[0]&0xF0)==(Get_IB_PAM_Code(4)<<4))&&
                ((AppSelCode_Buffer[1]&0xF0)==(Get_IB_PAM_Code(4)<<4)))
        {
            Result=1;
        }
        // NRZ 25G
        else if(((AppSelCode_Buffer[0]&0xF0)==(Get_25G_NRZ_Code(4)<<4))&&
                ((AppSelCode_Buffer[1]&0xF0)==(Get_25G_NRZ_Code(4)<<4)))
        {
            Result=1;
        }
    }
    else if(Lane==Lane_2_3)
    {
        // PAM 53G
        if(((AppSelCode_Buffer[2]&0xF0)==(Get_53G_PAM_Code(4)<<4))&&
           ((AppSelCode_Buffer[3]&0xF0)==(Get_53G_PAM_Code(4)<<4)))
        {
            Result=1;
        }
        // PAM 106G S
        else if(((AppSelCode_Buffer[2]&0xF0)==(Get_106G_PAM_S_Code(4)<<4))&&
                ((AppSelCode_Buffer[3]&0xF0)==(Get_106G_PAM_S_Code(4)<<4)))
        {
            Result=1;
        }
        // PAM 106G L
        else if(((AppSelCode_Buffer[2]&0xF0)==(Get_106G_PAM_L_Code(4)<<4))&&
                ((AppSelCode_Buffer[3]&0xF0)==(Get_106G_PAM_L_Code(4)<<4)))
        {
            Result=1;
        }
        // PAM IB
        else if(((AppSelCode_Buffer[2]&0xF0)==(Get_IB_PAM_Code(4)<<4))&&
                ((AppSelCode_Buffer[3]&0xF0)==(Get_IB_PAM_Code(4)<<4)))
        {
            Result=1;
        }
        // NRZ 25G
        else if(((AppSelCode_Buffer[2]&0xF0)==(Get_25G_NRZ_Code(4)<<4))&&
                ((AppSelCode_Buffer[3]&0xF0)==(Get_25G_NRZ_Code(4)<<4)))
        {
            Result=1;
        }
    }
    else if(Lane==Lane_4_5)
    {
        // PAM 53G
        if(((AppSelCode_Buffer[4]&0xF0)==(Get_53G_PAM_Code(4)<<4))&&
           ((AppSelCode_Buffer[5]&0xF0)==(Get_53G_PAM_Code(4)<<4)))
        {
            Result=1;
        }
        // PAM 106G S
        else if(((AppSelCode_Buffer[4]&0xF0)==(Get_106G_PAM_S_Code(4)<<4))&&
                ((AppSelCode_Buffer[5]&0xF0)==(Get_106G_PAM_S_Code(4)<<4)))
        {
            Result=1;
        }
        // PAM 106G L
        else if(((AppSelCode_Buffer[4]&0xF0)==(Get_106G_PAM_L_Code(4)<<4))&&
                ((AppSelCode_Buffer[5]&0xF0)==(Get_106G_PAM_L_Code(4)<<4)))
        {
            Result=1;
        }
        // PAM IB
        else if(((AppSelCode_Buffer[4]&0xF0)==(Get_IB_PAM_Code(4)<<4))&&
                ((AppSelCode_Buffer[5]&0xF0)==(Get_IB_PAM_Code(4)<<4)))
        {
            Result=1;
        }
        // NRZ 25G
        else if(((AppSelCode_Buffer[4]&0xF0)==(Get_25G_NRZ_Code(4)<<4))&&
                ((AppSelCode_Buffer[5]&0xF0)==(Get_25G_NRZ_Code(4)<<4)))
        {
            Result=1;
        }
    }
    else if(Lane==Lane_6_7)
    {
        // PAM 53G
        if(((AppSelCode_Buffer[6]&0xF0)==(Get_53G_PAM_Code(4)<<4))&&
           ((AppSelCode_Buffer[7]&0xF0)==(Get_53G_PAM_Code(4)<<4)))
        {
            Result=1;
        }
        // PAM 106G S
        else if(((AppSelCode_Buffer[6]&0xF0)==(Get_106G_PAM_S_Code(4)<<4))&&
                ((AppSelCode_Buffer[7]&0xF0)==(Get_106G_PAM_S_Code(4)<<4)))
        {
            Result=1;
        }
        // PAM 106G L
        else if(((AppSelCode_Buffer[6]&0xF0)==(Get_106G_PAM_L_Code(4)<<4))&&
                ((AppSelCode_Buffer[7]&0xF0)==(Get_106G_PAM_L_Code(4)<<4)))
        {
            Result=1;
        }
        // PAM IB
        else if(((AppSelCode_Buffer[6]&0xF0)==(Get_IB_PAM_Code(4)<<4))&&
                ((AppSelCode_Buffer[7]&0xF0)==(Get_IB_PAM_Code(4)<<4)))
        {
            Result=1;
        }
        // NRZ 25G
        else if(((AppSelCode_Buffer[6]&0xF0)==(Get_25G_NRZ_Code(4)<<4))&&
                ((AppSelCode_Buffer[7]&0xF0)==(Get_25G_NRZ_Code(4)<<4)))
        {
            Result=1;
        }
    }
    return Result;
}

uint8_t Verify_1Port_1Lane_AppSelCode(uint8_t Lane)
{
    uint8_t Result=0;
    if(((AppSelCode_Buffer[Lane]&0xF0)==(Get_106G_PAM_S_Code(8)<<4))||
       ((AppSelCode_Buffer[Lane]&0xF0)==(Get_106G_PAM_L_Code(8)<<4))||
       ((AppSelCode_Buffer[Lane]&0xF0)==(Get_53G_PAM_Code(8)<<4))||
       ((AppSelCode_Buffer[Lane]&0xF0)==(Get_25G_NRZ_Code(8)<<4))||
       ((AppSelCode_Buffer[Lane]&0xF0)==(Get_IB_PAM_Code(8)<<4)))
    {
        Result=1;
    }
                    
    return Result;
}

//-----------------------------------------------------------------------------------------------------//
// Staged Control Set 0, Application Select Controls Byte145-152
//-----------------------------------------------------------------------------------------------------//
void AppSelCode_Verify_Process(uint8_t AppSelCode_Value[8])
{
    uint8_t Lane_Verify_Fail = 0;
    uint32_t Conflict_Lane_Flag = 0;
    uint8_t FOR_COUNT,NRZ_Lane_Count=0,PAM_106G_Lane_Count=0,PAM_53G_Lane_Count=0,IB_Lane_Count=0;
    
	  if(MSA_Config_Error_Code)
    {
    	for(FOR_COUNT=0;FOR_COUNT<8;FOR_COUNT++)
    		AppSelCode_Buffer[FOR_COUNT]=AppSelCode_Value[FOR_COUNT];
    }
    else
    {
    	for(FOR_COUNT=0;FOR_COUNT<8;FOR_COUNT++)
    	{
    		// Update previous AppSelCode if AppSelCode is 0x00 or Invalid code
    		if(((AppSelCode_Value[FOR_COUNT]&0xF0)==0)||(Invalid_Lane&(0x01<<FOR_COUNT)))
            {
    			AppSelCode_Buffer[FOR_COUNT]=Active_AppSelCode[FOR_COUNT];
                if(Explicit_Control_Buffer&(0x01<<FOR_COUNT))
                    AppSelCode_Buffer[FOR_COUNT]|=0x01;
            }
    		// Lane0,2,4,6 Bit0-3
    		else if((FOR_COUNT%2)==0)
    		{
    			// Update Accepted AppSelCode
    			if((QSFPDD_P11[74+(FOR_COUNT/2)]&0x0F)==Accepted)
    				AppSelCode_Buffer[FOR_COUNT]=QSFPDD_P10[17+FOR_COUNT];
    			// Update previous AppSelCode
    			else
    			{
    				AppSelCode_Buffer[FOR_COUNT]=Active_AppSelCode[FOR_COUNT];
    	    		if(Explicit_Control_Buffer&(0x01<<FOR_COUNT))
    	    			AppSelCode_Buffer[FOR_COUNT]|=0x01;
    			}
    		}
    		// Lane1,3,5,7 Bit4-7
    		else
    		{
    			// Update Accepted AppSelCode
    			if((QSFPDD_P11[74+(FOR_COUNT/2)]&0xF0)==(Accepted<<4))
    				AppSelCode_Buffer[FOR_COUNT]=QSFPDD_P10[17+FOR_COUNT];
    			// Update previous AppSelCode
    			else
    			{
    				AppSelCode_Buffer[FOR_COUNT]=Active_AppSelCode[FOR_COUNT];
    	    		if(Explicit_Control_Buffer&(0x01<<FOR_COUNT))
    	    			AppSelCode_Buffer[FOR_COUNT]|=0x01;
    			}
    		}
            // Get NRZ Count Flag
    		if(((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_25G_NRZ_Code(1)<<4))||
               ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_25G_NRZ_Code(2)<<4))||
               ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_25G_NRZ_Code(8)<<4)))
            {
    			NRZ_Lane_Count|=(0x01<<FOR_COUNT);
            }
            // Get 53G PAM Count Flag
    		else if(((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_53G_PAM_Code(1)<<4))||
    				((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_53G_PAM_Code(2)<<4))||
    				((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_53G_PAM_Code(4)<<4))||
    				((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_53G_PAM_Code(8)<<4)))
            {
    			PAM_53G_Lane_Count|=(0x01<<FOR_COUNT);
            }
            // Get 106G PAM Count Flag
    		else if(((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_106G_PAM_S_Code(1)<<4))||
    				((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_106G_PAM_S_Code(2)<<4))||
    				((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_106G_PAM_S_Code(4)<<4))||
    				((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_106G_PAM_S_Code(8)<<4))||
                    ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_106G_PAM_L_Code(1)<<4))||
    				((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_106G_PAM_L_Code(2)<<4))||
    				((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_106G_PAM_L_Code(4)<<4))||
    				((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_106G_PAM_L_Code(8)<<4))||
					((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_106G_PAM_S_2_Code(1)<<4))||
					((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_106G_PAM_L_2_Code(1)<<4)))
            {
    			PAM_106G_Lane_Count|=(0x01<<FOR_COUNT);
            }
            // Get IB Count Flag
    		else if(((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_IB_PAM_Code(1)<<4))||
    				((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_IB_PAM_Code(2)<<4))||
    				((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_IB_PAM_Code(4)<<4))||
    				((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_IB_PAM_Code(8)<<4))||
					((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_IB_PAM_2_Code(1)<<4)))
            {
    			IB_Lane_Count|=(0x01<<FOR_COUNT);
            }
    	}
    	//Previous Mode is PAM4 Clear it for Next modulation NRZ
    	if(NRZ_Lane_Count>0)
    	{
        	for(FOR_COUNT=0;FOR_COUNT<8;FOR_COUNT++)
        	{
        		if((AppSelCode_Value[17+FOR_COUNT]==0x00)||(Invalid_Lane&(0x01<<FOR_COUNT)))
        		{
        			if(((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_53G_PAM_Code(1)<<4))||
					   ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_53G_PAM_Code(2)<<4))||
					   ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_53G_PAM_Code(4)<<4))||
					   ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_53G_PAM_Code(8)<<4))||
                       ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_106G_PAM_S_Code(1)<<4))||
					   ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_106G_PAM_S_Code(2)<<4))||
					   ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_106G_PAM_S_Code(4)<<4))||
					   ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_106G_PAM_S_Code(8)<<4))||
                       ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_106G_PAM_L_Code(1)<<4))||
					   ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_106G_PAM_L_Code(2)<<4))||
					   ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_106G_PAM_L_Code(4)<<4))||
					   ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_106G_PAM_L_Code(8)<<4))||
                       ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_IB_PAM_Code(1)<<4))||
					   ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_IB_PAM_Code(2)<<4))||
					   ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_IB_PAM_Code(4)<<4))||
					   ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_IB_PAM_Code(8)<<4))||
					   ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_106G_PAM_S_2_Code(1)<<4))||
					   ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_106G_PAM_L_2_Code(1)<<4))||
					   ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_IB_PAM_2_Code(1)<<4)))
                    {
        				AppSelCode_Buffer[FOR_COUNT]=0x00;
                    }
        		}
        	}
    	}
    	//Previous Mode is NRZ Clear it for Next modulation PAM4
    	if(PAM_53G_Lane_Count>0)
    	{
        	for(FOR_COUNT=0;FOR_COUNT<8;FOR_COUNT++)
        	{
                if(((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_25G_NRZ_Code(1)<<4))||
                   ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_25G_NRZ_Code(2)<<4))||
                   ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_25G_NRZ_Code(8)<<4))||
                   ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_106G_PAM_S_Code(1)<<4))||
                   ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_106G_PAM_S_Code(2)<<4))||
                   ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_106G_PAM_S_Code(4)<<4))||
                   ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_106G_PAM_S_Code(8)<<4))||
                   ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_106G_PAM_L_Code(1)<<4))||
                   ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_106G_PAM_L_Code(2)<<4))||
                   ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_106G_PAM_L_Code(4)<<4))||
                   ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_106G_PAM_L_Code(8)<<4))||
                   ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_IB_PAM_Code(1)<<4))||
                   ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_IB_PAM_Code(2)<<4))||
                   ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_IB_PAM_Code(4)<<4))||
                   ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_IB_PAM_Code(8)<<4))||
				   ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_106G_PAM_S_2_Code(1)<<4))||
				   ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_106G_PAM_L_2_Code(1)<<4))||
				   ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_IB_PAM_2_Code(1)<<4)))
                {
                    AppSelCode_Buffer[FOR_COUNT]=0x00;
                }
        	}
    	}
    	//Previous Mode is NRZ or 53G PAM4 Clear it for Next modulation
    	if((PAM_106G_Lane_Count>0)||(IB_Lane_Count>0))
    	{
        	for(FOR_COUNT=0;FOR_COUNT<8;FOR_COUNT++)
        	{
        		if((AppSelCode_Value[FOR_COUNT]==0x00)||(Invalid_Lane&(0x01<<FOR_COUNT)))
        		{
        			if(((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_25G_NRZ_Code(1)<<4))||
                       ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_25G_NRZ_Code(2)<<4))||
                       ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_25G_NRZ_Code(8)<<4))||
                       ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_53G_PAM_Code(1)<<4))||
                       ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_53G_PAM_Code(2)<<4))||
                       ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_53G_PAM_Code(4)<<4))||
                       ((AppSelCode_Buffer[FOR_COUNT]&0xF0)==(Get_53G_PAM_Code(8)<<4)))
                    {
        				AppSelCode_Buffer[FOR_COUNT]=0x00;
                    }
        		}
        	}
    	}
    }
    // AppSelCode 1Port with 8Lanes - Lane0-Lane7
	if(Verify_1Port_8Lane_AppSelCode())
	{
		// This mode only can setting on lane0,if get other lane value to return error code <Rejected_Invalid_Combo>
		for(FOR_COUNT=0;FOR_COUNT<8;FOR_COUNT++)
		{
			if((AppSelCode_Buffer[FOR_COUNT]&0x0E)>0)
			{
				Configuration_Error_code_Feedback(FOR_COUNT,Rejected_Invalid_Combo);
				Lane_Verify_Fail|=(0x01<<FOR_COUNT);
			}
		}
		// Because some lane get wrong setting so other lane return error code <Rejected_Incomplete_Lane_Info>
		if(Lane_Verify_Fail>0)
		{
			for(FOR_COUNT=0;FOR_COUNT<8;FOR_COUNT++)
			{
				if((Lane_Verify_Fail&(0x01<<FOR_COUNT))==0)
					Configuration_Error_code_Feedback(FOR_COUNT,Rejected_Incomplete_Lane_Info);
			}
		}
	}
	else
	{
					
        // AppSelCode 2Port with 4Lanes - Lane0-Lane3
		if(Verify_1Port_4Lane_AppSelCode(Lane_0_3))
		{
			
			// For same lane setting, but different code
			for(FOR_COUNT=0;FOR_COUNT<4;FOR_COUNT++)
			{
				if((AppSelCode_Buffer[FOR_COUNT]&0xFE)==(Get_53G_PAM_Code(2)<<4))
					Conflict_Lane_Flag|=(0x00001<<FOR_COUNT);
 				if((AppSelCode_Buffer[FOR_COUNT]&0xFE)==(Get_25G_NRZ_Code(2)<<4))
					Conflict_Lane_Flag|=(0x00010<<FOR_COUNT);
				if((AppSelCode_Buffer[FOR_COUNT]&0xFE)==(Get_106G_PAM_S_Code(2)<<4))
					Conflict_Lane_Flag|=(0x00100<<FOR_COUNT);
				if((AppSelCode_Buffer[FOR_COUNT]&0xFE)==(Get_106G_PAM_L_Code(2)<<4))
					Conflict_Lane_Flag|=(0x01000<<FOR_COUNT);
				if((AppSelCode_Buffer[FOR_COUNT]&0xFE)==(Get_IB_PAM_Code(2)<<4))
					Conflict_Lane_Flag|=(0x10000<<FOR_COUNT);
			}

			if(((Conflict_Lane_Flag&0x0000F)>0)&&
               ((Conflict_Lane_Flag&0x000F0)>0)&&
               ((Conflict_Lane_Flag&0x00F00)>0)&&
               ((Conflict_Lane_Flag&0x0F000)>0)&&
               ((Conflict_Lane_Flag&0xF0000)>0))
			{
				// Check Port1 Lane0-Lane3
				for(FOR_COUNT=0;FOR_COUNT<4;FOR_COUNT++)
				{
					Configuration_Error_code_Feedback(FOR_COUNT,Rejected_Incomplete_Lane_Info);
				}
			}
			// Start Lane Check
			else
			{
				// Port1 Lane0-Lane3
				for(FOR_COUNT=0;FOR_COUNT<4;FOR_COUNT++)
				{
					if((AppSelCode_Buffer[FOR_COUNT]&0x0E)>0)
					{
                        if(Verify_Config_Error_Code(Rejected_Invalid_Code,FOR_COUNT)==0)
                        {
                            Configuration_Error_code_Feedback(FOR_COUNT,Rejected_Invalid_Combo);
                            Lane_Verify_Fail|=(0x01<<FOR_COUNT);
                        }
					}
				}

				// Rejected Incomplete Lane Info if lane check fail return Rejected_Invalid_Combo
				if(Lane_Verify_Fail&0x0F)
				{
					// Check Port1 Lane0-Lane3
					for(FOR_COUNT=0;FOR_COUNT<4;FOR_COUNT++)
					{
						if((Lane_Verify_Fail&(0x01<<FOR_COUNT))==0)
							Configuration_Error_code_Feedback(FOR_COUNT,Rejected_Incomplete_Lane_Info);
					}
				}
			}
		}
		// AppSelCode 4Port 100G with 2Lanes - Lane0-Lane1 and Lane2-Lane3
		else
		{
			// 4Port with 2Lanes Port1 Lane0-Lane1
			if(Verify_1Port_2Lane_AppSelCode(Lane_0_1))
			{
				// Lane Start Check Port1 Lane0-Lane1
				for(FOR_COUNT=0;FOR_COUNT<2;FOR_COUNT++)
				{
					if((AppSelCode_Buffer[FOR_COUNT]&0x0E)>0)
					{
                        if(Verify_Config_Error_Code(Rejected_Invalid_Code,FOR_COUNT)==0)
                        {
                            Configuration_Error_code_Feedback(FOR_COUNT,Rejected_Invalid_Combo);
                            Lane_Verify_Fail|=(0x01<<FOR_COUNT);
                        }
					}
				}
				if(Lane_Verify_Fail&0x03)
				{
					// Check Port1 Lane0-Lane1
					for(FOR_COUNT=0;FOR_COUNT<2;FOR_COUNT++)
					{
						if((Lane_Verify_Fail&(0x01<<FOR_COUNT))==0)
							Configuration_Error_code_Feedback(FOR_COUNT,Rejected_Incomplete_Lane_Info);
					}
				}
			}
			// AppSelCode 8PORT PAM4 & NRZ Lane0-Lane1
			else
			{
				// Lane0 value right
                if(Verify_Config_Error_Code(Rejected_Invalid_Code,0)==0)
                {
                    if(Verify_1Port_1Lane_AppSelCode(0))
                    {
                        if((AppSelCode_Buffer[0]&0x0E)==0)
                            Configuration_Error_code_Feedback(0,Accepted);
                        // Invalid Lane to feedback error code
                        else
                            Configuration_Error_code_Feedback(0,Rejected_Invalid_Combo);
                    }
                    // AppSelCode is 0x00
                    else if(((AppSelCode_Buffer[0]&0xF0)==0))
                    {
                        // DataPathInit is true keep config accepted
                        if(QSFPDD_P10[0]&0x01)
                            Configuration_Error_code_Feedback(0,Accepted);
                        else
                            Configuration_Error_code_Feedback(0,Rejected_In_Use);
                    }
                    // Rejected_Incomplete_Lane_Info when AppSelCode is not 0x00 or Rejected_Invalid_Code
                    else
                    {
                        Configuration_Error_code_Feedback(0,Rejected_Incomplete_Lane_Info);
                    }
                }
				// Lane1 value right
                if(Verify_Config_Error_Code(Rejected_Invalid_Code,1)==0)
                {
                    if(Verify_1Port_1Lane_AppSelCode(1))
                    {
                        if((AppSelCode_Buffer[1]&0x0E)==0x02)
                            Configuration_Error_code_Feedback(1,Accepted);
                        // Invalid Lane to feedback error code
                        else
                            Configuration_Error_code_Feedback(1,Rejected_Invalid_Combo);
                    }
                    // AppSelCode is 0x00
                    else if(((AppSelCode_Buffer[1]&0xF0)==0))
                    {
                        // DataPathInit is true keep config accepted
                        if(QSFPDD_P10[0]&0x02)
                            Configuration_Error_code_Feedback(1,Accepted);
                        else
                            Configuration_Error_code_Feedback(1,Rejected_In_Use);
                    }
                    // Rejected_Incomplete_Lane_Info when AppSelCode is not 0x00 or Rejected_Invalid_Code
                    else
                    {
                        Configuration_Error_code_Feedback(1,Rejected_Incomplete_Lane_Info);
                    }
                }
			}
			// 4Port with 2Lanes Port2 Lane2-Lane3
			if(Verify_1Port_2Lane_AppSelCode(Lane_2_3))
			{
				// Lane Start Check Port2 Lane2-Lane3
				for(FOR_COUNT=0;FOR_COUNT<2;FOR_COUNT++)
				{
					if((AppSelCode_Buffer[2+FOR_COUNT]&0x0E)!=4)
					{
                        if(Verify_Config_Error_Code(Rejected_Invalid_Code,2+FOR_COUNT)==0)
                        {
                            Configuration_Error_code_Feedback(FOR_COUNT+2,Rejected_Invalid_Combo);
                            Lane_Verify_Fail|=(0x04<<FOR_COUNT);
                        }
					}
				}
				if(Lane_Verify_Fail&0x0C)
				{
					// Check Port2 Lane2-Lane3
					for(FOR_COUNT=0;FOR_COUNT<2;FOR_COUNT++)
					{
						if((Lane_Verify_Fail&(0x04<<FOR_COUNT))==0)
							Configuration_Error_code_Feedback(FOR_COUNT+2,Rejected_Incomplete_Lane_Info);
					}
				}
			}
			// AppSelCode 8PORT PAM4 & NRZ Lane2-Lane3
			else
			{
				// Lane2 value right
                if(Verify_Config_Error_Code(Rejected_Invalid_Code,2)==0)
                {
                    if(Verify_1Port_1Lane_AppSelCode(2))
                    {
                        if((AppSelCode_Buffer[2]&0x0E)==0x04)
                            Configuration_Error_code_Feedback(2,Accepted);
                        // Invalid Lane to feedback error code
                        else
                            Configuration_Error_code_Feedback(2,Rejected_Invalid_Combo);
                    }
                    // AppSelCode is 0x00
                    else if(((AppSelCode_Buffer[2]&0xF0)==0))
                    {
                        // DataPathInit is true keep config accepted
                        if(QSFPDD_P10[0]&0x04)
                            Configuration_Error_code_Feedback(2,Accepted);
                        else
                            Configuration_Error_code_Feedback(2,Rejected_In_Use);
                    }
                    // Rejected_Incomplete_Lane_Info when AppSelCode is not 0x00 or Rejected_Invalid_Code
                    else
                    {
                        Configuration_Error_code_Feedback(2,Rejected_Incomplete_Lane_Info);
                    }
                }
				// Lane3 value right
                if(Verify_Config_Error_Code(Rejected_Invalid_Code,3)==0)
                {
                    if(Verify_1Port_1Lane_AppSelCode(3))
                    {
                        if((AppSelCode_Buffer[3]&0x0E)==0x06)
                            Configuration_Error_code_Feedback(3,Accepted);
                        // Invalid Lane to feedback error code
                        else
                            Configuration_Error_code_Feedback(3,Rejected_Invalid_Combo);
                    }
                    // AppSelCode is 0x00
                    else if(((AppSelCode_Buffer[3]&0xF0)==0))
                    {
                        // DataPathInit is true keep config accepted
                        if(QSFPDD_P10[0]&0x08)
                            Configuration_Error_code_Feedback(3,Accepted);
                        else
                            Configuration_Error_code_Feedback(3,Rejected_In_Use);
                    }
                    // Rejected_Incomplete_Lane_Info when AppSelCode is not 0x00 or Rejected_Invalid_Code
                    else
                    {
                        Configuration_Error_code_Feedback(3,Rejected_Incomplete_Lane_Info);
                    }
                }
			}
		}
		Conflict_Lane_Flag=0;
        // AppSelCode 2Port with 4Lanes - Lane4-Lane7
		if(Verify_1Port_4Lane_AppSelCode(Lane_4_7))
		{
			// For same lane setting, but different code
			for(FOR_COUNT=0;FOR_COUNT<4;FOR_COUNT++)
			{
				if((AppSelCode_Buffer[4+FOR_COUNT]&0xFE)==((Get_53G_PAM_Code(2)<<4)|0x08))
					Conflict_Lane_Flag|=(0x00001<<FOR_COUNT);
				if((AppSelCode_Buffer[4+FOR_COUNT]&0xFE)==((Get_25G_NRZ_Code(2)<<4)|0x08))
					Conflict_Lane_Flag|=(0x00010<<FOR_COUNT);
				if((AppSelCode_Buffer[4+FOR_COUNT]&0xFE)==((Get_106G_PAM_S_Code(2)<<4)|0x08))
					Conflict_Lane_Flag|=(0x00100<<FOR_COUNT);
				if((AppSelCode_Buffer[4+FOR_COUNT]&0xFE)==((Get_106G_PAM_L_Code(2)<<4)|0x08))
					Conflict_Lane_Flag|=(0x01000<<FOR_COUNT);
				if((AppSelCode_Buffer[4+FOR_COUNT]&0xFE)==((Get_IB_PAM_Code(2)<<4)|0x08))
					Conflict_Lane_Flag|=(0x10000<<FOR_COUNT);
			}
			if(((Conflict_Lane_Flag&0x0000F)>0)&&
               ((Conflict_Lane_Flag&0x000F0)>0)&&
               ((Conflict_Lane_Flag&0x00F00)>0)&&
               ((Conflict_Lane_Flag&0x0F000)>0)&&
               ((Conflict_Lane_Flag&0xF0000)>0))
			{
				// Check Port2 Lane4-Lane7
				for(FOR_COUNT=0;FOR_COUNT<4;FOR_COUNT++)
					Configuration_Error_code_Feedback(FOR_COUNT+4,Rejected_Incomplete_Lane_Info);
			}
			// Start Lane Check
			else
			{
				
				// Lane Start Check Port2 Lane4-Lane7
				for(FOR_COUNT=0;FOR_COUNT<4;FOR_COUNT++)
				{
					if((AppSelCode_Buffer[4+FOR_COUNT]&0x0E)!=8)
					{
                        if(Verify_Config_Error_Code(Rejected_Invalid_Code,4+FOR_COUNT)==0)
                        {
                            Configuration_Error_code_Feedback(FOR_COUNT+4,Rejected_Invalid_Combo);
                            Lane_Verify_Fail|=(0x10<<FOR_COUNT);
                        }
					}
				}
				
				// Rejected Incomplete Lane Info if lane check fail return Rejected_Invalid_Combo
				if(Lane_Verify_Fail&0xF0)
				{
					// Check Port2 Lane4-Lane7
					for(FOR_COUNT=0;FOR_COUNT<4;FOR_COUNT++)
					{
						if((Lane_Verify_Fail&(0x10<<FOR_COUNT))==0)
							Configuration_Error_code_Feedback(FOR_COUNT+4,Rejected_Incomplete_Lane_Info);
					}
				}
			}
		}
		// AppSelCode4 4Port with 2Lanes - Lane4-Lane5 and Lane6-Lane7
		else
		{
			// 4Port with 2Lanes Port3 Lane4-Lane5
			if(Verify_1Port_2Lane_AppSelCode(Lane_4_5))
			{
				// Lane Start Check Port3 Lane4-Lane5
				for(FOR_COUNT=0;FOR_COUNT<2;FOR_COUNT++)
				{
					if((AppSelCode_Buffer[4+FOR_COUNT]&0x0E)!=8)
					{
                        if(Verify_Config_Error_Code(Rejected_Invalid_Code,4+FOR_COUNT)==0)
                        {
                            Configuration_Error_code_Feedback(FOR_COUNT+4,Rejected_Invalid_Combo);
                            Lane_Verify_Fail|=(0x10<<FOR_COUNT);
                        }
					}
				}
				if(Lane_Verify_Fail&0x30)
				{
					// Check Port3 Lane4-Lane5
					for(FOR_COUNT=0;FOR_COUNT<2;FOR_COUNT++)
					{
						if((Lane_Verify_Fail&(0x10<<FOR_COUNT))==0)
							Configuration_Error_code_Feedback(FOR_COUNT+4,Rejected_Incomplete_Lane_Info);
					}
				}
			}
			// AppSelCode 8PORT PAM4 & NRZ Lane4-Lane5
			else
			{
				// Lane4 value right
                if(Verify_Config_Error_Code(Rejected_Invalid_Code,4)==0)
                {
                    if(Verify_1Port_1Lane_AppSelCode(4))
                    {
                        if((AppSelCode_Buffer[4]&0x0E)==0x08)
                            Configuration_Error_code_Feedback(4,Accepted);
                        // Invalid Lane to feedback error code
                        else
                            Configuration_Error_code_Feedback(4,Rejected_Invalid_Combo);
                    }
                    // AppSelCode is 0x00
                    else if(((AppSelCode_Buffer[4]&0xF0)==0))
                    {
                        // DataPathInit is true keep config accepted
                        if(QSFPDD_P10[0]&0x10)
                            Configuration_Error_code_Feedback(4,Accepted);
                        else
                            Configuration_Error_code_Feedback(4,Rejected_In_Use);
                    }
                    // Rejected_Incomplete_Lane_Info when AppSelCode is not 0x00 or Rejected_Invalid_Code
                    else
                    {
                        Configuration_Error_code_Feedback(4,Rejected_Incomplete_Lane_Info);
                    }
                }
				// Lane5 value right
                if(Verify_Config_Error_Code(Rejected_Invalid_Code,5)==0)
                {
                    if(Verify_1Port_1Lane_AppSelCode(5))
                    {
                        if((AppSelCode_Buffer[5]&0x0E)==0x0A)
                            Configuration_Error_code_Feedback(5,Accepted);
                        // Invalid Lane to feedback error code
                        else
                            Configuration_Error_code_Feedback(5,Rejected_Invalid_Combo);
                    }
                    // AppSelCode is 0x00
                    else if(((AppSelCode_Buffer[5]&0xF0)==0))
                    {
                        // DataPathInit is true keep config accepted
                        if(QSFPDD_P10[0]&0x20)
                            Configuration_Error_code_Feedback(5,Accepted);
                        else
                            Configuration_Error_code_Feedback(5,Rejected_In_Use);
                    }
                    // Rejected_Incomplete_Lane_Info when AppSelCode is not 0x00 or Rejected_Invalid_Code
                    else
                    {
                        Configuration_Error_code_Feedback(5,Rejected_Incomplete_Lane_Info);
                    }
                }
			}
			// 4Port with 2Lanes Port4 Lane6-Lane7
			if(Verify_1Port_2Lane_AppSelCode(Lane_6_7))
			{
				// Lane Start Check Lane6-Lane7
				for(FOR_COUNT=0;FOR_COUNT<2;FOR_COUNT++)
				{
					if((AppSelCode_Buffer[6+FOR_COUNT]&0x0E)!=0x0C)
					{
                        if(Verify_Config_Error_Code(Rejected_Invalid_Code,6+FOR_COUNT)==0)
                        {
                            Configuration_Error_code_Feedback(FOR_COUNT+6,Rejected_Invalid_Combo);
                            Lane_Verify_Fail|=(0x40<<FOR_COUNT);
                        }
					}
				}
				if(Lane_Verify_Fail&0xC0)
				{
					// Port4 Lane6-Lane7
					for(FOR_COUNT=0;FOR_COUNT<2;FOR_COUNT++)
					{
						if((Lane_Verify_Fail&(0x40<<FOR_COUNT))==0)
							Configuration_Error_code_Feedback(FOR_COUNT+6,Rejected_Incomplete_Lane_Info);
					}
				}
			}
			// AppSelCode 8PORT PAM4 & NRZ Lane6-Lane7
			else
			{
				// Lane6 value right
                if(Verify_Config_Error_Code(Rejected_Invalid_Code,6)==0)
                {
                    if(Verify_1Port_1Lane_AppSelCode(6))
                    {
                        if((AppSelCode_Buffer[6]&0x0E)==0x0C)
                            Configuration_Error_code_Feedback(6,Accepted);
                        // Invalid Lane to feedback error code
                        else
                            Configuration_Error_code_Feedback(6,Rejected_Invalid_Combo);
                    }
                    // AppSelCode is 0x00
                    else if(((AppSelCode_Buffer[6]&0xF0)==0))
                    {
                        // DataPathInit is true keep config accepted
                        if(QSFPDD_P10[0]&0x40)
                            Configuration_Error_code_Feedback(6,Accepted);
                        else
                            Configuration_Error_code_Feedback(6,Rejected_In_Use);
                    }
                    // Rejected_Incomplete_Lane_Info when AppSelCode is not 0x00 or Rejected_Invalid_Code
                    else
                    {
                        Configuration_Error_code_Feedback(6,Rejected_Incomplete_Lane_Info);
                    }
                }
				// Lane7 value right
                if(Verify_Config_Error_Code(Rejected_Invalid_Code,7)==0)
                {
                    if(Verify_1Port_1Lane_AppSelCode(7))
                    {
                        if((AppSelCode_Buffer[7]&0x0E)==0x0E)
                            Configuration_Error_code_Feedback(7,Accepted);
                        // Invalid Lane to feedback error code
                        else
                            Configuration_Error_code_Feedback(7,Rejected_Invalid_Combo);
                    }
                    // AppSelCode is 0x00
                    else if(((AppSelCode_Buffer[7]&0xF0)==0))
                    {
                        // DataPathInit is true keep config accepted
                        if(QSFPDD_P10[0]&0x80)
                            Configuration_Error_code_Feedback(7,Accepted);
                        else
                            Configuration_Error_code_Feedback(7,Rejected_In_Use);
                    }
                    // Rejected_Incomplete_Lane_Info when AppSelCode is not 0x00 or Rejected_Invalid_Code
                    else
                    {
                        Configuration_Error_code_Feedback(7,Rejected_Incomplete_Lane_Info);
                    }
                }
			}
		}
	}
}

uint8_t Clear_Active_AppSelCode(uint8_t Data_Rate,uint8_t Port)
{
    uint8_t Clear_Lane=0;
    if(Data_Rate==PAM_106G_S)
    {
        if(Port==Lane_0_7)
        {
            // Clear 1Port PAM Active AppSelCode
            for(uint8_t i=0;i<8;i++)
            {
                if(Active_AppSelCode[i]==(Get_106G_PAM_S_Code(1)<<4))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
                if(Active_AppSelCode[i]==(Get_106G_PAM_S_2_Code(1)<<4))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
        else if(Port==Lane_0_3)
        {
            // Clear 2Port PAM Active AppSelCode
            for(uint8_t i=0;i<4;i++)
            {
                // Clear 2Port 425G Lane0-Lane3
                if(Active_AppSelCode[i]==(Get_106G_PAM_S_Code(2)<<4))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
        else if(Port==Lane_4_7)
        {
            // Clear 2Port PAM Active AppSelCode
            for(uint8_t i=4;i<8;i++)
            {
                // Clear 2Port 425G Lane4-Lane7
                if(Active_AppSelCode[i]==((Get_106G_PAM_S_Code(2)<<4|0x08)))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
        else if(Port==Lane_0_1)
        {
            // Clear 2Port PAM Active AppSelCode
            for(uint8_t i=0;i<2;i++)
            {
                // Clear 2Port 212G Lane0-Lane1
                if(Active_AppSelCode[i]==(Get_106G_PAM_S_Code(4)<<4))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
        else if(Port==Lane_2_3)
        {
            // Clear 2Port PAM Active AppSelCode
            for(uint8_t i=2;i<4;i++)
            {
                // Clear 2Port 212G Lane2-Lane3
                if(Active_AppSelCode[i]==((Get_106G_PAM_S_Code(4)<<4)|0x04))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
        else if(Port==Lane_4_5)
        {
            // Clear 2Port PAM Active AppSelCode
            for(uint8_t i=4;i<6;i++)
            {
                // Clear 2Port 212G Lane4-Lane5
                if(Active_AppSelCode[i]==((Get_106G_PAM_S_Code(4)<<4)|0x08))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
        else if(Port==Lane_6_7)
        {
            // Clear 2Port PAM Active AppSelCode
            for(uint8_t i=4;i<6;i++)
            {
                // Clear 2Port 212G Lane6-Lane7
                if(Active_AppSelCode[i]==((Get_106G_PAM_S_Code(4)<<4)|0x0C))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
        else if(Port==Lane_Single)
        {
            // Clear 8Port PAM Active AppSelCode
            for(uint8_t i=0;i<8;i++)
            {
                if(Active_AppSelCode[i]==((Get_106G_PAM_S_Code(8)<<4)+(i*2)))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
    }
    else if(Data_Rate==PAM_106G_L)
    {
        if(Port==Lane_0_7)
        {
            // Clear 1Port PAM Active AppSelCode
            for(uint8_t i=0;i<8;i++)
            {
                if(Active_AppSelCode[i]==(Get_106G_PAM_L_Code(1)<<4))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
                if(Active_AppSelCode[i]==(Get_106G_PAM_L_2_Code(1)<<4))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
        else if(Port==Lane_0_3)
        {
            // Clear 2Port PAM Active AppSelCode
            for(uint8_t i=0;i<4;i++)
            {
                // Clear 2Port 425G Lane0-Lane3
                if(Active_AppSelCode[i]==(Get_106G_PAM_L_Code(2)<<4))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
        else if(Port==Lane_4_7)
        {
            // Clear 2Port PAM Active AppSelCode
            for(uint8_t i=4;i<8;i++)
            {
                // Clear 2Port 425G Lane4-Lane7
                if(Active_AppSelCode[i]==((Get_106G_PAM_L_Code(2)<<4|0x08)))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
        else if(Port==Lane_0_1)
        {
            // Clear 2Port PAM Active AppSelCode
            for(uint8_t i=0;i<2;i++)
            {
                // Clear 2Port 212G Lane0-Lane1
                if(Active_AppSelCode[i]==(Get_106G_PAM_L_Code(4)<<4))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
        else if(Port==Lane_2_3)
        {
            // Clear 2Port PAM Active AppSelCode
            for(uint8_t i=2;i<4;i++)
            {
                // Clear 2Port 212G Lane2-Lane3
                if(Active_AppSelCode[i]==((Get_106G_PAM_L_Code(4)<<4)|0x04))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
        else if(Port==Lane_4_5)
        {
            // Clear 2Port PAM Active AppSelCode
            for(uint8_t i=4;i<6;i++)
            {
                // Clear 2Port 212G Lane4-Lane5
                if(Active_AppSelCode[i]==((Get_106G_PAM_L_Code(4)<<4)|0x08))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
        else if(Port==Lane_6_7)
        {
            // Clear 2Port PAM Active AppSelCode
            for(uint8_t i=4;i<6;i++)
            {
                // Clear 2Port 212G Lane6-Lane7
                if(Active_AppSelCode[i]==((Get_106G_PAM_L_Code(4)<<4)|0x0C))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
        else if(Port==Lane_Single)
        {
            // Clear 8Port PAM Active AppSelCode
            for(uint8_t i=0;i<8;i++)
            {
                if(Active_AppSelCode[i]==((Get_106G_PAM_L_Code(8)<<4)+(i*2)))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
    }
    else if(Data_Rate==PAM_106G_IB)
    {
        if(Port==Lane_0_7)
        {
            // Clear 1Port PAM Active AppSelCode
            for(uint8_t i=0;i<8;i++)
            {
                if(Active_AppSelCode[i]==(Get_IB_PAM_Code(1)<<4))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
                if(Active_AppSelCode[i]==(Get_IB_PAM_2_Code(1)<<4))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
        else if(Port==Lane_0_3)
        {
            // Clear 2Port PAM Active AppSelCode
            for(uint8_t i=0;i<4;i++)
            {
                // Clear 2Port 425G Lane0-Lane3
                if(Active_AppSelCode[i]==(Get_IB_PAM_Code(2)<<4))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
        else if(Port==Lane_4_7)
        {
            // Clear 2Port PAM Active AppSelCode
            for(uint8_t i=4;i<8;i++)
            {
                // Clear 2Port 425G Lane4-Lane7
                if(Active_AppSelCode[i]==((Get_IB_PAM_Code(2)<<4|0x08)))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
        else if(Port==Lane_0_1)
        {
            // Clear 2Port PAM Active AppSelCode
            for(uint8_t i=0;i<2;i++)
            {
                // Clear 2Port 212G Lane0-Lane1
                if(Active_AppSelCode[i]==(Get_IB_PAM_Code(4)<<4))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
        else if(Port==Lane_2_3)
        {
            // Clear 2Port PAM Active AppSelCode
            for(uint8_t i=2;i<4;i++)
            {
                // Clear 2Port 212G Lane2-Lane3
                if(Active_AppSelCode[i]==((Get_IB_PAM_Code(4)<<4)|0x04))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
        else if(Port==Lane_4_5)
        {
            // Clear 2Port PAM Active AppSelCode
            for(uint8_t i=4;i<6;i++)
            {
                // Clear 2Port 212G Lane4-Lane5
                if(Active_AppSelCode[i]==((Get_IB_PAM_Code(4)<<4)|0x08))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
        else if(Port==Lane_6_7)
        {
            // Clear 2Port PAM Active AppSelCode
            for(uint8_t i=4;i<6;i++)
            {
                // Clear 2Port 212G Lane6-Lane7
                if(Active_AppSelCode[i]==((Get_IB_PAM_Code(4)<<4)|0x0C))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
        else if(Port==Lane_Single)
        {
            // Clear 8Port PAM Active AppSelCode
            for(uint8_t i=0;i<8;i++)
            {
                if(Active_AppSelCode[i]==((Get_IB_PAM_Code(8)<<4)+(i*2)))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
    }
    else if(Data_Rate==PAM_53G)
    {
        if(Port==Lane_0_7)
        {
            // Clear 1Port PAM Active AppSelCode
            for(uint8_t i=0;i<8;i++)
            {
                if(Active_AppSelCode[i]==(Get_53G_PAM_Code(1)<<4))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
        else if(Port==Lane_0_3)
        {
            // Clear 2Port PAM Active AppSelCode
            for(uint8_t i=0;i<4;i++)
            {
                // Clear 2Port 212G Lane0-Lane3
                if(Active_AppSelCode[i]==(Get_53G_PAM_Code(2)<<4))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
        else if(Port==Lane_4_7)
        {
            // Clear 2Port PAM Active AppSelCode
            for(uint8_t i=4;i<8;i++)
            {
                // Clear 2Port 212G Lane4-Lane7
                if(Active_AppSelCode[i]==((Get_53G_PAM_Code(2)<<4|0x08)))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
        else if(Port==Lane_0_1)
        {
            // Clear 2Port PAM Active AppSelCode
            for(uint8_t i=0;i<2;i++)
            {
                // Clear 2Port 106G Lane0-Lane1
                if(Active_AppSelCode[i]==(Get_53G_PAM_Code(4)<<4))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
        else if(Port==Lane_2_3)
        {
            // Clear 2Port PAM Active AppSelCode
            for(uint8_t i=2;i<4;i++)
            {
                // Clear 2Port 106G Lane2-Lane3
                if(Active_AppSelCode[i]==((Get_53G_PAM_Code(4)<<4)|0x04))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
        else if(Port==Lane_4_5)
        {
            // Clear 2Port PAM Active AppSelCode
            for(uint8_t i=4;i<6;i++)
            {
                // Clear 2Port 106G Lane4-Lane5
                if(Active_AppSelCode[i]==((Get_53G_PAM_Code(4)<<4)|0x08))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
        else if(Port==Lane_6_7)
        {
            // Clear 2Port PAM Active AppSelCode
            for(uint8_t i=4;i<6;i++)
            {
                // Clear 2Port 106G Lane6-Lane7
                if(Active_AppSelCode[i]==((Get_53G_PAM_Code(4)<<4)|0x0C))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
        else if(Port==Lane_Single)
        {
            // Clear 8Port PAM Active AppSelCode
            for(uint8_t i=0;i<8;i++)
            {
                if(Active_AppSelCode[i]==((Get_53G_PAM_Code(8)<<4)+(i*2)))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
    }
    else if(Data_Rate==NRZ_25G)
    {
        if(Port==Lane_0_7)
        {
            // Clear 1Port NRZ Active AppSelCode
            for(uint8_t i=0;i<8;i++)
            {
                if(Active_AppSelCode[i]==(Get_25G_NRZ_Code(1)<<4))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
        else if(Port==Lane_0_3)
        {
            // Clear 2Port NRZ Active AppSelCode
            for(uint8_t i=0;i<4;i++)
            {
                // Clear 2Port 103G Lane0-Lane3
                if(Active_AppSelCode[i]==(Get_25G_NRZ_Code(2)<<4))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
        else if(Port==Lane_4_7)
        {
            // Clear 2Port NRZ Active AppSelCode
            for(uint8_t i=4;i<8;i++)
            {
                // Clear 2Port 103G Lane4-Lane7
                if(Active_AppSelCode[i]==((Get_25G_NRZ_Code(2)<<4|0x08)))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
        else if(Port==Lane_0_1)
        {
            // Clear 2Port NRZ Active AppSelCode
            for(uint8_t i=0;i<2;i++)
            {
                // Clear 2Port 50G Lane0-Lane1
                if(Active_AppSelCode[i]==(Get_25G_NRZ_Code(4)<<4))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
        else if(Port==Lane_2_3)
        {
            // Clear 2Port NRZ Active AppSelCode
            for(uint8_t i=2;i<4;i++)
            {
                // Clear 2Port 50G Lane2-Lane3
                if(Active_AppSelCode[i]==((Get_25G_NRZ_Code(4)<<4)|0x04))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
        else if(Port==Lane_4_5)
        {
            // Clear 2Port NRZ Active AppSelCode
            for(uint8_t i=4;i<6;i++)
            {
                // Clear 2Port 50G Lane4-Lane5
                if(Active_AppSelCode[i]==((Get_25G_NRZ_Code(4)<<4)|0x08))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
        else if(Port==Lane_6_7)
        {
            // Clear 2Port NRZ Active AppSelCode
            for(uint8_t i=4;i<6;i++)
            {
                // Clear 2Port 50G Lane6-Lane7
                if(Active_AppSelCode[i]==((Get_25G_NRZ_Code(4)<<4)|0x0C))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
        else if(Port==Lane_Single)
        {
            // Clear 8Port NRZ Active AppSelCode
            for(uint8_t i=0;i<8;i++)
            {
                if(Active_AppSelCode[i]==((Get_25G_NRZ_Code(8)<<4)+(i*2)))
                {
                    Active_AppSelCode[i]=0x00;
                    Clear_Lane|=(0x01<<i);
                }
            }
        }
    }
    return Clear_Lane;
}

uint8_t AppSel_Control_Feedback()
{
	uint8_t Error_code_Pass = 0;
	uint8_t FOR_COUNT = 0;
	uint8_t NRZ_Code_Flag = 0,PAM_53G_Code_Flag = 0, PAM_106G_S_Code_Flag = 0,PAM_106G_L_Code_Flag = 0,PAM_IB_Code_Flag = 0,Accepted_Flag = 0,Accepted_8Port_Flag = 0,Accepted_4Port_Flag = 0;
    uint8_t Disable_Lane = 0;
    //-----------------------------------------------------//
    // Check Support AppSel Code Now 0x10-0xF0
	//-----------------------------------------------------//
	// Lane0-Lane7 page10 145-152
	//-----------------------------------------------------//
    // Get AppSelCode_Lane Value
    for(uint8_t lane=0;lane<8;lane++)
    {
        if(Control_Set==0)
        {
            AppSelCode_Lane[lane]=QSFPDD_P10[17+lane];
        }
        else if(Control_Set==1)
        {
            AppSelCode_Lane[lane]=QSFPDD_P10[52+lane];
        }
    }
    // Enable Feedback to MSA Config Error Code
    MSA_Config_Error_Code=1;
	Invalid_Lane=0;
		
	// Init value for AppSel_Control_Feedback and Signal_Integrity_Control_Feedback to return config error code
    for(FOR_COUNT=0;FOR_COUNT<8;FOR_COUNT++)
    {
    	// Not support AppSelCode to feedback <Rejected_Invalid_Code>
    	if(Get_AppSelCode_Result(AppSelCode_Lane[FOR_COUNT])==0)
        {
						Configuration_Error_code_Feedback(FOR_COUNT,Rejected_Invalid_Code);
            Invalid_Lane|=(0x01<<FOR_COUNT);
        }
        else
        {
        	Configuration_Error_code_Feedback(FOR_COUNT,Accepted);
            // For Update Page11h Active AppSelCode
        	if((AppSelCode_Lane[FOR_COUNT]&0xF0)==0)
        		Disable_Lane|=(0x01<<FOR_COUNT);
        }
    }

    AppSelCode_Verify_Process(&AppSelCode_Lane[0]);
    // All AppSelCode is 0x00 feedback Rejected_Incomplete_Lane_Info
    if(Disable_Lane==0xFF)
    {
		for(FOR_COUNT=0;FOR_COUNT<8;FOR_COUNT++)
		{
            // If DataPathInit if true feedback Accepted
            if(QSFPDD_P10[0]&(0x01<<FOR_COUNT))
            {
                Configuration_Error_code_Feedback(FOR_COUNT,Accepted);
            }
            else
                Configuration_Error_code_Feedback(FOR_COUNT,Rejected_In_Use);
			AppSelCode_Buffer[FOR_COUNT]=Active_AppSelCode[FOR_COUNT];
            if(Explicit_Control_Buffer&(0x01<<FOR_COUNT))
                AppSelCode_Buffer[FOR_COUNT]|=0x01;
		}
    	// Disable Feedback to MSA Config Error Code, data will to Internal_Config_Error_Code[4]
    	MSA_Config_Error_Code=0;
    }
    else if((Disable_Lane>0)||(Invalid_Lane>0))
    {
    	// Disable Feedback to MSA Config Error Code, data will to Internal_Config_Error_Code[4]
    	MSA_Config_Error_Code=0;
    	AppSelCode_Verify_Process(&AppSelCode_Lane[0]);
    }
    // Clear to default
    else
    {
    	for(FOR_COUNT=0;FOR_COUNT<4;FOR_COUNT++)
    		Internal_Config_Error_Code[FOR_COUNT]=0x11;
    }
	//-----------------------------------------------------//
	// Update Page11h Active AppSelCode
	//-----------------------------------------------------//
    for(FOR_COUNT=0;FOR_COUNT<8;FOR_COUNT++)
    {
        // If DataPathInit if true feedback AppSelcode Control Value
        if((QSFPDD_P10[0]&(0x01<<FOR_COUNT)))
        {
            QSFPDD_P11[78+FOR_COUNT]=AppSelCode_Lane[FOR_COUNT];
        }
        else
        {
            if(Verify_Config_Error_Code(Accepted,FOR_COUNT)==1)
                QSFPDD_P11[78+FOR_COUNT]=AppSelCode_Buffer[FOR_COUNT];
            else
                QSFPDD_P11[78+FOR_COUNT]=0x00;
        }
    }
    // Update Explicit_Control_Buffer to keep Bit0 data
    for(FOR_COUNT=0;FOR_COUNT<8;FOR_COUNT++)
    {
        if(QSFPDD_P11[78+FOR_COUNT]&0x01)
            Explicit_Control_Buffer|=(0x01<<FOR_COUNT);
        else
            Explicit_Control_Buffer&=~(0x01<<FOR_COUNT);
    }
	//-----------------------------------------------------//
	// Scan Result from Page11h Active AppSelCode
	//-----------------------------------------------------//
	for(FOR_COUNT=0;FOR_COUNT<8;FOR_COUNT++)
	{
        // 25G NRZ Support Modes
		if((QSFPDD_P11[78+FOR_COUNT]&0xF0)==(Get_25G_NRZ_Code(1)<<4)||
           (QSFPDD_P11[78+FOR_COUNT]&0xF0)==(Get_25G_NRZ_Code(2)<<4)||
           (QSFPDD_P11[78+FOR_COUNT]&0xF0)==(Get_25G_NRZ_Code(8)<<4))
        {
			NRZ_Code_Flag++;
        }
        // 53G PAM Support Modes
		else if((QSFPDD_P11[78+FOR_COUNT]&0xF0)==(Get_53G_PAM_Code(1)<<4)||
                (QSFPDD_P11[78+FOR_COUNT]&0xF0)==(Get_53G_PAM_Code(2)<<4)||
                (QSFPDD_P11[78+FOR_COUNT]&0xF0)==(Get_53G_PAM_Code(4)<<4)||
                (QSFPDD_P11[78+FOR_COUNT]&0xF0)==(Get_53G_PAM_Code(8)<<4))
        {
            PAM_53G_Code_Flag++;
        }
        // 106G S Support Modes
        else if((QSFPDD_P11[78+FOR_COUNT]&0xF0)==(Get_106G_PAM_S_Code(1)<<4)||
                (QSFPDD_P11[78+FOR_COUNT]&0xF0)==(Get_106G_PAM_S_Code(2)<<4)||
                (QSFPDD_P11[78+FOR_COUNT]&0xF0)==(Get_106G_PAM_S_Code(4)<<4)||
                (QSFPDD_P11[78+FOR_COUNT]&0xF0)==(Get_106G_PAM_S_Code(8)<<4)||
				(QSFPDD_P11[78+FOR_COUNT]&0xF0)==(Get_106G_PAM_S_2_Code(1)<<4))
        {
			PAM_106G_S_Code_Flag++;
        }
        // 106G L Support Modes
        else if((QSFPDD_P11[78+FOR_COUNT]&0xF0)==(Get_106G_PAM_L_Code(1)<<4)||
                (QSFPDD_P11[78+FOR_COUNT]&0xF0)==(Get_106G_PAM_L_Code(2)<<4)||
                (QSFPDD_P11[78+FOR_COUNT]&0xF0)==(Get_106G_PAM_L_Code(4)<<4)||
                (QSFPDD_P11[78+FOR_COUNT]&0xF0)==(Get_106G_PAM_L_Code(8)<<4)||
				(QSFPDD_P11[78+FOR_COUNT]&0xF0)==(Get_106G_PAM_L_2_Code(1)<<4))
        {
			PAM_106G_L_Code_Flag++;
        }
        // IB Support Modes
        else if((QSFPDD_P11[78+FOR_COUNT]&0xF0)==(Get_IB_PAM_Code(1)<<4)||
                (QSFPDD_P11[78+FOR_COUNT]&0xF0)==(Get_IB_PAM_Code(2)<<4)||
                (QSFPDD_P11[78+FOR_COUNT]&0xF0)==(Get_IB_PAM_Code(4)<<4)||
                (QSFPDD_P11[78+FOR_COUNT]&0xF0)==(Get_IB_PAM_Code(8)<<4)||
				(QSFPDD_P11[78+FOR_COUNT]&0xF0)==(Get_IB_PAM_2_Code(1)<<4))
        {
			PAM_IB_Code_Flag++;
        }
	}
	// Mix Pattern Result so return <Rejected_Invalid_Combo>
	if(((NRZ_Code_Flag>0)&&(PAM_53G_Code_Flag>0))||
       ((NRZ_Code_Flag>0)&&(PAM_106G_S_Code_Flag>0))||
       ((NRZ_Code_Flag>0)&&(PAM_106G_L_Code_Flag>0))||
       ((NRZ_Code_Flag>0)&&(PAM_IB_Code_Flag>0))||
       ((PAM_53G_Code_Flag>0)&&(PAM_106G_S_Code_Flag>0))||
       ((PAM_53G_Code_Flag>0)&&(PAM_106G_L_Code_Flag>0))||
       ((PAM_53G_Code_Flag>0)&&(PAM_IB_Code_Flag>0))||
       ((PAM_106G_S_Code_Flag>0)&&(PAM_106G_L_Code_Flag>0)&&(PAM_IB_Code_Flag>0)))
	{
        // Enable Feedback to MSA Config Error Code
		if((Disable_Lane>0)||Invalid_Lane>0)
			MSA_Config_Error_Code=1;
		for(FOR_COUNT=0;FOR_COUNT<8;FOR_COUNT++)
		{
			Configuration_Error_code_Feedback(FOR_COUNT,Rejected_Unknown);
			QSFPDD_P11[78+FOR_COUNT]=0x00;
		}
	}
	// 106G PAM4 Mode
	else if((PAM_106G_S_Code_Flag>0)||(PAM_106G_L_Code_Flag>0)||(PAM_IB_Code_Flag>0))
	{
		DSP_MODE_SET = Mode1_CHIP_MODE_2X53G_KP4PAM_TO_1X106G_KP4PAM;//Mode1_CHIP_MODE_1X106G_KP4PAM_TO_1X106G_KP4PAM ;
		// PAM4 MODE
        Signal_Status = PAM4_to_PAM4;
		Error_code_Pass = 1;
	}
	// 53G PAM4 Mode
	else if(PAM_53G_Code_Flag>0)
	{
		DSP_MODE_SET = Mode1_CHIP_MODE_2X53G_KP4PAM_TO_1X106G_KP4PAM;//Mode2_CHIP_MODE_1X53G_KP4PAM_TO_1X53G_KP4PAM ;
		// PAM4 MODE
        Signal_Status = PAM4_to_PAM4;
		Error_code_Pass = 1;
	}
    // 25G NRZ Mode
	else if(NRZ_Code_Flag>0)
	{
		//DSP_MODE_SET = Mode4_CHIP_MODE_1x25G_KR4NRZ_TO_1x25G_KR4NRZ ;
		// NRZ MODE
		Signal_Status = NRZ_to_NRZ;
		Error_code_Pass = 1;
	}
	
	//-----------------------------------------------------//
	// Initialize for select chip mode
	//-----------------------------------------------------//
	if(Error_code_Pass)
	{
	    if(Invalid_Lane==0)
	    {
			// For DSP_Tx_Los_Check and DSP_Rx_Los_Check to keep preious mode when whole lane config fail
			for(FOR_COUNT=0;FOR_COUNT<8;FOR_COUNT++)
			{
				// Update Active AppSelCode Codes from Accepted Lane
				if(Verify_Config_Error_Code(Accepted,FOR_COUNT)==1)
				{
					// AppSelCode 0x00 don't need to replace
					if(QSFPDD_P11[78+FOR_COUNT]!=0x00)
					{
						Active_AppSelCode[FOR_COUNT]=(QSFPDD_P11[78+FOR_COUNT]&0xFE);
						Accepted_Flag|=(0x01<<FOR_COUNT);
					}
				}
			}
	    }
	    // When some lane is invalid code , copy page11h code to  Active_AppSelCode[]
	    else
	    {
			for(FOR_COUNT=0;FOR_COUNT<8;FOR_COUNT++)
			{
				Active_AppSelCode[FOR_COUNT]=(QSFPDD_P11[78+FOR_COUNT]&0xFE);
				Accepted_Flag|=(0x01<<FOR_COUNT);
			}
	    }
        //-----------------------------------------------------//
        // Clear not support code on Active_AppSelCode[]
        //-----------------------------------------------------//
        if(Accepted_Flag!=0xFF)
        {
            // Clear 1Port PAM4 Active AppSelCode
            // PAM4 106G_S 106G_L PAM_100G_IB 53G
            for(uint8_t Data_Rate=0;Data_Rate<4;Data_Rate++)
            {
                if(Clear_Active_AppSelCode(Data_Rate,Lane_0_7)==0xFF)
                    break;
            }
            // Clear different modulation
            if(PAM_106G_S_Code_Flag>0)
            {
                // Clear Other Modulation
                if(Clear_Active_AppSelCode(PAM_106G_L,Lane_Single)!=0xFF)
                {
                    Clear_Active_AppSelCode(PAM_106G_L,Lane_0_3);
                    Clear_Active_AppSelCode(PAM_106G_L,Lane_4_7);
                }
                if(Clear_Active_AppSelCode(PAM_106G_IB,Lane_Single)!=0xFF)
                {
                    Clear_Active_AppSelCode(PAM_106G_IB,Lane_0_3);
                    Clear_Active_AppSelCode(PAM_106G_IB,Lane_4_7);
                }
                if(Clear_Active_AppSelCode(PAM_53G,Lane_Single)!=0xFF)
                {
                    Clear_Active_AppSelCode(PAM_53G,Lane_0_3);
                    Clear_Active_AppSelCode(PAM_53G,Lane_4_7);
                }
                if(Clear_Active_AppSelCode(NRZ_25G,Lane_Single)!=0xFF)
                {
                    Clear_Active_AppSelCode(NRZ_25G,Lane_0_3);
                    Clear_Active_AppSelCode(NRZ_25G,Lane_4_7);
                }
                // Clear Other Port if not support it
                // Get 8Port Lane_Accepted_Flag
                for(FOR_COUNT=0;FOR_COUNT<8;FOR_COUNT++)
                {
                    // PAM 106G
                    // Get 8Port Lane_Accepted_Flag
                	if(Active_AppSelCode[FOR_COUNT]==(Get_106G_PAM_S_Code(8)<<4)+(FOR_COUNT*2))
                		Accepted_8Port_Flag|=0x01<<FOR_COUNT;
                }
                 // Clear 2Port PAM Lane0-Lane7 if select port is 8Port
                if(Accepted_8Port_Flag&0x0F)
                {
                    // Clear 2Port 4Lanes Mode when lane is selected
                    for(uint8_t i=0;i<4;i++)
                    {
                        if((Accepted_8Port_Flag&(0x01<<i))==0)
                        {
                            if(Active_AppSelCode[i]==(Get_106G_PAM_S_Code(2)<<4))
                            {
                                Active_AppSelCode[i]=0x00;
                            }
                        }
                    }
                }
                if(Accepted_8Port_Flag&0xF0)
                {
                    // Clear 2Port 4Lanes Mode when lane is selected
                    for(uint8_t i=0;i<4;i++)
                    {
                        if((Accepted_8Port_Flag&(0x10<<i))==0)
                        {
                            if(Active_AppSelCode[i+4]==((Get_106G_PAM_S_Code(2)<<4)|0x08))
                            {
                                Active_AppSelCode[i+4]=0x00;
                            }
                        }
                    }
                }
            }
            if(PAM_106G_L_Code_Flag>0)
            {
                // Clear 2Port&8Port 106G_S 53G PAM & 25G NRZ Active AppSelCode
                if(Clear_Active_AppSelCode(PAM_106G_S,Lane_Single)!=0xFF)
                {
                    Clear_Active_AppSelCode(PAM_106G_S,Lane_0_3);
                    Clear_Active_AppSelCode(PAM_106G_S,Lane_4_7);
                }
                if(Clear_Active_AppSelCode(PAM_106G_IB,Lane_Single)!=0xFF)
                {
                    Clear_Active_AppSelCode(PAM_106G_IB,Lane_0_3);
                    Clear_Active_AppSelCode(PAM_106G_IB,Lane_4_7);
                }
                if(Clear_Active_AppSelCode(PAM_53G,Lane_Single)!=0xFF)
                {
                    Clear_Active_AppSelCode(PAM_53G,Lane_0_3);
                    Clear_Active_AppSelCode(PAM_53G,Lane_4_7);
                }
                if(Clear_Active_AppSelCode(NRZ_25G,Lane_Single)!=0xFF)
                {
                    Clear_Active_AppSelCode(NRZ_25G,Lane_0_3);
                    Clear_Active_AppSelCode(NRZ_25G,Lane_4_7);
                }
                // Clear Other Port if not support it
                // Get 8Port Lane_Accepted_Flag
                for(FOR_COUNT=0;FOR_COUNT<8;FOR_COUNT++)
                {
                    // PAM 106G
                    // Get 8Port Lane_Accepted_Flag
                	if(Active_AppSelCode[FOR_COUNT]==(Get_106G_PAM_L_Code(8)<<4)+(FOR_COUNT*2))
                		Accepted_8Port_Flag|=0x01<<FOR_COUNT;
                }
                // Clear 2Port PAM Lane0-Lane7 if select port is 8Port
                if(Accepted_8Port_Flag&0x0F)
                {
                    // Clear 2Port 4Lanes Mode when lane is selected
                    for(uint8_t i=0;i<4;i++)
                    {
                        if((Accepted_8Port_Flag&(0x01<<i))==0)
                        {
                            if(Active_AppSelCode[i]==(Get_106G_PAM_L_Code(2)<<4))
                            {
                                Active_AppSelCode[i]=0x00;
                            }
                        }
                    }
                }
                if(Accepted_8Port_Flag&0xF0)
                {
                    // Clear 2Port 4Lanes Mode when lane is selected
                    for(uint8_t i=0;i<4;i++)
                    {
                        if((Accepted_8Port_Flag&(0x10<<i))==0)
                        {
                            if(Active_AppSelCode[i+4]==((Get_106G_PAM_L_Code(2)<<4)|0x08))
                            {
                                Active_AppSelCode[i+4]=0x00;
                            }
                        }
                    }
                }
            }
            if(PAM_IB_Code_Flag>0)
            {
                // Clear 2Port&8Port 106G_S 53G PAM & 25G NRZ Active AppSelCode
                if(Clear_Active_AppSelCode(PAM_106G_S,Lane_Single)!=0xFF)
                {
                    Clear_Active_AppSelCode(PAM_106G_S,Lane_0_3);
                    Clear_Active_AppSelCode(PAM_106G_S,Lane_4_7);
                }
                if(Clear_Active_AppSelCode(PAM_106G_L,Lane_Single)!=0xFF)
                {
                    Clear_Active_AppSelCode(PAM_106G_L,Lane_0_3);
                    Clear_Active_AppSelCode(PAM_106G_L,Lane_4_7);
                }
                if(Clear_Active_AppSelCode(PAM_53G,Lane_Single)!=0xFF)
                {
                    Clear_Active_AppSelCode(PAM_53G,Lane_0_3);
                    Clear_Active_AppSelCode(PAM_53G,Lane_4_7);
                }
                if(Clear_Active_AppSelCode(NRZ_25G,Lane_Single)!=0xFF)
                {
                    Clear_Active_AppSelCode(NRZ_25G,Lane_0_3);
                    Clear_Active_AppSelCode(NRZ_25G,Lane_4_7);
                }
                // Clear Other Port if not support it
                // Get 8Port Lane_Accepted_Flag
                for(FOR_COUNT=0;FOR_COUNT<8;FOR_COUNT++)
                {
                    // PAM 106G
                    // Get 8Port Lane_Accepted_Flag
                	if(Active_AppSelCode[FOR_COUNT]==(Get_IB_PAM_Code(8)<<4)+(FOR_COUNT*2))
                		Accepted_8Port_Flag|=0x01<<FOR_COUNT;
                }
                // Clear 2Port PAM Lane0-Lane7 if select port is 8Port
                if(Accepted_8Port_Flag&0x0F)
                {
                    // Clear 2Port 4Lanes Mode when lane is selected
                    for(uint8_t i=0;i<4;i++)
                    {
                        if((Accepted_8Port_Flag&(0x01<<i))==0)
                        {
                            if(Active_AppSelCode[i]==(Get_IB_PAM_Code(2)<<4))
                            {
                                Active_AppSelCode[i]=0x00;
                            }
                        }
                    }
                }
                if(Accepted_8Port_Flag&0xF0)
                {
                    // Clear 2Port 4Lanes Mode when lane is selected
                    for(uint8_t i=0;i<4;i++)
                    {
                        if((Accepted_8Port_Flag&(0x10<<i))==0)
                        {
                            if(Active_AppSelCode[i+4]==((Get_IB_PAM_Code(2)<<4)|0x08))
                            {
                                Active_AppSelCode[i+4]=0x00;
                            }
                        }
                    }
                }
            }
            if(PAM_53G_Code_Flag>0)
            {
                // Clear 2Port&8Port 106G_S 106G_L PAM & 25G NRZ Active AppSelCode
                if(Clear_Active_AppSelCode(PAM_106G_S,Lane_Single)!=0xFF)
                {
                    Clear_Active_AppSelCode(PAM_106G_S,Lane_0_3);
                    Clear_Active_AppSelCode(PAM_106G_S,Lane_4_7);
                }
                if(Clear_Active_AppSelCode(PAM_106G_L,Lane_Single)!=0xFF)
                {
                    Clear_Active_AppSelCode(PAM_106G_L,Lane_0_3);
                    Clear_Active_AppSelCode(PAM_106G_L,Lane_4_7);
                }
                if(Clear_Active_AppSelCode(PAM_106G_IB,Lane_Single)!=0xFF)
                {
                    Clear_Active_AppSelCode(PAM_106G_IB,Lane_0_3);
                    Clear_Active_AppSelCode(PAM_106G_IB,Lane_4_7);
                }
                if(Clear_Active_AppSelCode(NRZ_25G,Lane_Single)!=0xFF)
                {
                    Clear_Active_AppSelCode(NRZ_25G,Lane_0_3);
                    Clear_Active_AppSelCode(NRZ_25G,Lane_4_7);
                }
                // Clear Other Port if not support it
                // Get 8Port Lane_Accepted_Flag
                for(FOR_COUNT=0;FOR_COUNT<8;FOR_COUNT++)
                {
                    // PAM 53G
                    // Get 8Port Lane_Accepted_Flag
                	if(Active_AppSelCode[FOR_COUNT]==(Get_53G_PAM_Code(8)<<4)+(FOR_COUNT*2))
                		Accepted_8Port_Flag|=0x01<<FOR_COUNT;
                }
                // Clear 2Port PAM Lane0-Lane7 if select port is 8Port
                if(Accepted_8Port_Flag&0x0F)
                {
                    // Clear 2Port 4Lanes Mode when lane is selected
                    for(uint8_t i=0;i<4;i++)
                    {
                        if((Accepted_8Port_Flag&(0x01<<i))==0)
                        {
                            if(Active_AppSelCode[i]==(Get_53G_PAM_Code(2)<<4))
                            {
                                Active_AppSelCode[i]=0x00;
                            }
                        }
                    }
                }
                if(Accepted_8Port_Flag&0xF0)
                {
                    // Clear 2Port 4Lanes Mode when lane is selected
                    for(uint8_t i=0;i<4;i++)
                    {
                        if((Accepted_8Port_Flag&(0x10<<i))==0)
                        {
                            if(Active_AppSelCode[i+4]==((Get_53G_PAM_Code(2)<<4)|0x08))
                            {
                                Active_AppSelCode[i+4]=0x00;
                            }
                        }
                    }
                }
            }
            if(NRZ_Code_Flag>0)
            {
                // Clear 2Port&8Port 106G_S 106G_L PAM & 25G NRZ Active AppSelCode
                if(Clear_Active_AppSelCode(PAM_106G_S,Lane_Single)!=0xFF)
                {
                    Clear_Active_AppSelCode(PAM_106G_S,Lane_0_3);
                    Clear_Active_AppSelCode(PAM_106G_S,Lane_4_7);
                }
                if(Clear_Active_AppSelCode(PAM_106G_L,Lane_Single)!=0xFF)
                {
                    Clear_Active_AppSelCode(PAM_106G_L,Lane_0_3);
                    Clear_Active_AppSelCode(PAM_106G_L,Lane_4_7);
                }
                if(Clear_Active_AppSelCode(PAM_106G_IB,Lane_Single)!=0xFF)
                {
                    Clear_Active_AppSelCode(PAM_106G_IB,Lane_0_3);
                    Clear_Active_AppSelCode(PAM_106G_IB,Lane_4_7);
                }
                if(Clear_Active_AppSelCode(PAM_53G,Lane_Single)!=0xFF)
                {
                    Clear_Active_AppSelCode(PAM_53G,Lane_0_3);
                    Clear_Active_AppSelCode(PAM_53G,Lane_4_7);

                }
                // Clear Other Port if not support it
                // Get 8Port Lane_Accepted_Flag
                for(FOR_COUNT=0;FOR_COUNT<8;FOR_COUNT++)
                {
                    // NRZ 25G
                    // Get 8Port Lane_Accepted_Flag
                	if(Active_AppSelCode[FOR_COUNT]==(Get_25G_NRZ_Code(8)<<4)+(FOR_COUNT*2))
                		Accepted_8Port_Flag|=0x01<<FOR_COUNT;
                }
                // Clear 2Port PAM Lane0-Lane7 if select port is 8Port
                if(Accepted_8Port_Flag&0x0F)
                {
                    // Clear 2Port 4Lanes Mode when lane is selected
                    for(uint8_t i=0;i<4;i++)
                    {
                        if((Accepted_8Port_Flag&(0x01<<i))==0)
                        {
                            if(Active_AppSelCode[i]==(Get_25G_NRZ_Code(2)<<4))
                            {
                                Active_AppSelCode[i]=0x00;
                            }
                        }
                    }
                }
                if(Accepted_8Port_Flag&0xF0)
                {
                    // Clear 2Port 4Lanes Mode when lane is selected
                    for(uint8_t i=0;i<4;i++)
                    {
                        if((Accepted_8Port_Flag&(0x10<<i))==0)
                        {
                            if(Active_AppSelCode[i+4]==((Get_25G_NRZ_Code(2)<<4)|0x08))
                            {
                                Active_AppSelCode[i+4]=0x00;
                            }
                        }
                    }
                }
            }
        }
	    // Use for run test script on I2C Script
	    if(CALIB_MEMORY_MAP.DEBUG_TEMP1==1)
	    {
	    	for(FOR_COUNT=0;FOR_COUNT<8;FOR_COUNT++)
	    		QSFPDD_P13[86+FOR_COUNT]=Active_AppSelCode[FOR_COUNT];
	        QSFPDD_P13[94]=Accepted_Flag;
	    }
	}
	// Enable Feedback to MSA Config Error Code
	MSA_Config_Error_Code=1;
	return Error_code_Pass;
}
//-----------------------------------------------------------------------------------------------------//
// Staged Control Set 0, Tx Controls
//-----------------------------------------------------------------------------------------------------//
//-----------------------------------------------------------------------------------------------------//
// Staged Control Set 0, Rx Controls
//-----------------------------------------------------------------------------------------------------//
//-----------------------------------------------------------------------------------------------------//
// Staged Control Set 0, Signal Integrity Controls Byte153-173
//-----------------------------------------------------------------------------------------------------//
uint8_t Signal_Integrity_Control_Feedback()
{
	uint8_t Pre_Cursor[8]={0};
	uint8_t Post_Cursor[8]={0};
	uint8_t AMP_C[8]={0};
	uint8_t Result = 0,Lane,Data_Index;

	Config_Error_flag=0x00;
	//Get Pre Post Amp Data from Page10h
	for(Lane=0;Lane<8;Lane++)
	{
		// Lane data in Bit0-3 & Bit4-7
		Data_Index=(Lane/2);
		Pre_Cursor[Lane]=Get_CH_Data(QSFPDD_P10[34+Data_Index],Lane);
		Post_Cursor[Lane]=Get_CH_Data(QSFPDD_P10[38+Data_Index],Lane);
		AMP_C[Lane]=Get_CH_Data(QSFPDD_P10[42+Data_Index],Lane);
	}
	// Pre Post Amp Range Check
	for(Lane=0;Lane<8;Lane++)
	{
		if(Pre_Cursor[Lane]>Pre_Cursor_Max_Vaule)
			Config_Error_flag |= (0x01<<Lane);
		if(Post_Cursor[Lane]>Post_Cursor_Max_Vaule)
			Config_Error_flag |= (0x01<<Lane);
		if(AMP_C[Lane]>AMP_Max_Vaule)
			Config_Error_flag |= (0x01<<Lane);
	}
	//Config Error Code Feedback when value out of range
	//CH1-4
	if((Verify_Config_Error_Code(Accepted,0)==1)&&(QSFPDD_P11[78]&0x01))
	{
		if(Config_Error_flag&0x01)
			Configuration_Error_code_Feedback( 0 , Rejected_Invalid_SI );
		// Not Support AppSelCode0
		else if(AppSelCode_Lane[0]!=0x00)
        {
			QSFPDD_P11[95]=((QSFPDD_P11[95]&= ~0x0F)|Pre_Cursor[0]);
			QSFPDD_P11[99]=((QSFPDD_P11[99]&= ~0x0F)|Post_Cursor[0]);
			QSFPDD_P11[103]=((QSFPDD_P11[103]&= ~0x0F)|AMP_C[0]);
			Configuration_Error_code_Feedback( 0 , Accepted );
		}
	}
	if((Verify_Config_Error_Code(Accepted,1)==1)&&(QSFPDD_P11[79]&0x01))
	{
		if(Config_Error_flag&0x02)
			Configuration_Error_code_Feedback( 1 , Rejected_Invalid_SI );
		// Not Support AppSelCode0
		else if(AppSelCode_Lane[1]!=0x00)
		{
			QSFPDD_P11[95]=((QSFPDD_P11[95]&= ~0xF0)|(Pre_Cursor[1]<<4));
			QSFPDD_P11[99]=((QSFPDD_P11[99]&= ~0xF0)|(Post_Cursor[1]<<4));
			QSFPDD_P11[103]=((QSFPDD_P11[103]&= ~0xF0)|(AMP_C[1]<<4));
			Configuration_Error_code_Feedback( 1 , Accepted );
		}
	}
	if((Verify_Config_Error_Code(Accepted,2)==1)&&(QSFPDD_P11[80]&0x01))
	{
		if(Config_Error_flag&0x04)
			Configuration_Error_code_Feedback( 2 , Rejected_Invalid_SI );
		// Not Support AppSelCode0
		else if(AppSelCode_Lane[2]!=0x00)
		{
			QSFPDD_P11[96]=((QSFPDD_P11[96]&= ~0x0F)|Pre_Cursor[2]);
			QSFPDD_P11[100]=((QSFPDD_P11[100]&= ~0x0F)|Post_Cursor[2]);
			QSFPDD_P11[104]=((QSFPDD_P11[104]&= ~0x0F)|AMP_C[2]);
			Configuration_Error_code_Feedback( 2 , Accepted );
		}
	}
	if((Verify_Config_Error_Code(Accepted,3)==1)&&(QSFPDD_P11[81]&0x01))
	{
		if(Config_Error_flag&0x08)
			Configuration_Error_code_Feedback( 3 , Rejected_Invalid_SI );
		// Not Support AppSelCode0
		else if(AppSelCode_Lane[3]!=0x00)
		{
			QSFPDD_P11[96]=((QSFPDD_P11[96]&= ~0xF0)|(Pre_Cursor[3]<<4));
			QSFPDD_P11[100]=((QSFPDD_P11[100]&= ~0xF0)|(Post_Cursor[3]<<4));
			QSFPDD_P11[104]=((QSFPDD_P11[104]&= ~0xF0)|(AMP_C[3]<<4));
			Configuration_Error_code_Feedback( 3 , Accepted );
		}
	}
	//CH5-8
	if((Verify_Config_Error_Code(Accepted,4)==1)&&(QSFPDD_P11[82]&0x01))
	{
		if(Config_Error_flag&0x10)
			Configuration_Error_code_Feedback( 4 , Rejected_Invalid_SI );
		// Not Support AppSelCode0
		else if(AppSelCode_Lane[4]!=0x00)
		{
			QSFPDD_P11[97]=((QSFPDD_P11[97]&= ~0x0F)|Pre_Cursor[4]);
			QSFPDD_P11[101]=((QSFPDD_P11[101]&= ~0x0F)|Post_Cursor[4]);
			QSFPDD_P11[105]=((QSFPDD_P11[105]&= ~0x0F)|AMP_C[4]);
			Configuration_Error_code_Feedback( 4 , Accepted );
		}
	}
	if((Verify_Config_Error_Code(Accepted,5)==1)&&(QSFPDD_P11[83]&0x01))
	{
		if(Config_Error_flag&0x20)
			Configuration_Error_code_Feedback( 5 , Rejected_Invalid_SI );
		// Not Support AppSelCode0
		else if(AppSelCode_Lane[5]!=0x00)
		{
			QSFPDD_P11[97]=((QSFPDD_P11[97]&= ~0xF0)|(Pre_Cursor[5]<<4));
			QSFPDD_P11[101]=((QSFPDD_P11[101]&= ~0xF0)|(Post_Cursor[5]<<4));
			QSFPDD_P11[105]=((QSFPDD_P11[105]&= ~0xF0)|(AMP_C[5]<<4));
			Configuration_Error_code_Feedback( 5 , Accepted );
		}
	}
	if((Verify_Config_Error_Code(Accepted,6)==1)&&(QSFPDD_P11[84]&0x01))
	{
		if(Config_Error_flag&0x40)
			Configuration_Error_code_Feedback( 6 , Rejected_Invalid_SI );
		// Not Support AppSelCode0
		else if(AppSelCode_Lane[6]!=0x00)
		{
			QSFPDD_P11[98]=((QSFPDD_P11[98]&= ~0x0F)|Pre_Cursor[6]);
			QSFPDD_P11[102]=((QSFPDD_P11[102]&= ~0x0F)|Post_Cursor[6]);
			QSFPDD_P11[106]=((QSFPDD_P11[106]&= ~0x0F)|AMP_C[6]);
			Configuration_Error_code_Feedback( 6 , Accepted );
		}
	}
	if((Verify_Config_Error_Code(Accepted,7)==1)&&(QSFPDD_P11[85]&0x01))
	{
		if(Config_Error_flag&0x80)
			Configuration_Error_code_Feedback( 7 , Rejected_Invalid_SI );
		// Not Support AppSelCode0
		else if(AppSelCode_Lane[7]!=0x00)
		{
			QSFPDD_P11[98]=((QSFPDD_P11[98]&= ~0xF0)|(Pre_Cursor[7]<<4));
			QSFPDD_P11[102]=((QSFPDD_P11[102]&= ~0xF0)|(Post_Cursor[7]<<4));
			QSFPDD_P11[106]=((QSFPDD_P11[106]&= ~0xF0)|(AMP_C[7]<<4));
			Configuration_Error_code_Feedback( 7 , Accepted );
		}
	}

	for(Lane=0;Lane<8;Lane++)
	{
		// ExplicitControl bit is 1
        if(QSFPDD_P11[78+Lane]&0x01)
        {
            // config error not equal Accepted , AppSelCode is 0x00 , 
            // Apply DataPathInit is not 1 , those condition do not do config SI
            if((Verify_Config_Error_Code(Accepted ,Lane)==0)||(AppSelCode_Lane[Lane]==0x00)||((QSFPDD_P10[15]&0x01<<Lane)==0))
            {
                Config_Error_flag|=(0x01<<Lane);
            }
        }
        else
            Config_Error_flag|=(0x01<<Lane);
	}

	if(Config_Error_flag!=0xFF)
		Result=1;

	return Result;
}
//-----------------------------------------------------------------------------------------------------//
// Rx Output EQ <Pre> Control Byte162-165
// Value only config by Apply_DataPathInit Byte143 or Apply_Immediate Byte144
//-----------------------------------------------------------------------------------------------------//
void RX_Pre_Control(uint8_t DSP_Control)
{
    uint8_t Pre_Cursor_RX1,Pre_Cursor_RX2,Pre_Cursor_RX3,Pre_Cursor_RX4;
    uint8_t Pre_Cursor_RX5,Pre_Cursor_RX6,Pre_Cursor_RX7,Pre_Cursor_RX8;
	// MSA PAGE10 Byte 162 RX12 Pre-cursor CONTROL
	// MSA PAGE10 Byte 163 RX34 Pre-cursor CONTROL
	// MSA PAGE10 Byte 164 RX56 Pre-cursor CONTROL
	// MSA PAGE10 Byte 165 RX78 Pre-cursor CONTROL
	// Device 3 level Control 09 / 0A / 0B
    //Get Rx Pre Channel Data
	Pre_Cursor_RX1 = Get_CH_Data(QSFPDD_P10[34],0);
	Pre_Cursor_RX2 = Get_CH_Data(QSFPDD_P10[34],1);
	Pre_Cursor_RX3 = Get_CH_Data(QSFPDD_P10[35],2);
	Pre_Cursor_RX4 = Get_CH_Data(QSFPDD_P10[35],3);
	Pre_Cursor_RX5 = Get_CH_Data(QSFPDD_P10[36],4);
	Pre_Cursor_RX6 = Get_CH_Data(QSFPDD_P10[36],5);
	Pre_Cursor_RX7 = Get_CH_Data(QSFPDD_P10[37],6);
	Pre_Cursor_RX8 = Get_CH_Data(QSFPDD_P10[37],7);
	//-----------------------------------------------------------------------------------------//
	// MSA Pre-Cursor RX1 - RX4
	//-----------------------------------------------------------------------------------------//
	QSFPDD_A0[12]=Config_Error_flag;
    // RX1 Pre_Cursor
	if((Config_Error_flag&0x01)==0)
    {
        if(Pre_Cursor_RX1==0)
            DSP_System_Side_PHY0_MEMORY_MAP.System_Side_PRE1_CH0 = 0x0000;
        // Get PAM4 Pre Value
        else if(Signal_Status==PAM4_to_PAM4)
            DSP_System_Side_PHY0_MEMORY_MAP.System_Side_PRE1_CH0 = (((uint16_t)PAM4_Pre_Table[Pre_Cursor_RX1]<<8 | 0x00FF));
        // Get NRZ Pre Value
        else
            DSP_System_Side_PHY0_MEMORY_MAP.System_Side_PRE1_CH0 = (((uint16_t)NRZ_Pre_Table[Pre_Cursor_RX1]<<8 | 0x00FF));
        
        if(DSP_Control)
		{
			Configuration_Error_code_Feedback(0,Config_In_Process);
			System_Side_Tab_Load |= 0x01;
		}
    }
    // RX2 Pre_Cursor
	if((Config_Error_flag&0x02)==0)
    {
		if(Pre_Cursor_RX2==0)
			DSP_System_Side_PHY0_MEMORY_MAP.System_Side_PRE1_CH1 = 0x0000;
        // Get PAM4 Pre Value
		else if(Signal_Status==PAM4_to_PAM4)
            DSP_System_Side_PHY0_MEMORY_MAP.System_Side_PRE1_CH1 = (((uint16_t)PAM4_Pre_Table[Pre_Cursor_RX2]<<8 | 0x00FF));
        // Get NRZ Pre Value
        else
            DSP_System_Side_PHY0_MEMORY_MAP.System_Side_PRE1_CH1 = (((uint16_t)NRZ_Pre_Table[Pre_Cursor_RX2]<<8 | 0x00FF));
        
        if(DSP_Control)
		{
			Configuration_Error_code_Feedback(1,Config_In_Process);
			System_Side_Tab_Load |= 0x02;
		}
    }
    // RX3 Pre_Cursor
	if((Config_Error_flag&0x04)==0)
    {
		if(Pre_Cursor_RX3==0)
			DSP_System_Side_PHY0_MEMORY_MAP.System_Side_PRE1_CH2 = 0x0000;
        // Get PAM4 Pre Value
		else if(Signal_Status==PAM4_to_PAM4)
            DSP_System_Side_PHY0_MEMORY_MAP.System_Side_PRE1_CH2 = (((uint16_t)PAM4_Pre_Table[Pre_Cursor_RX3]<<8 | 0x00FF));
        // Get NRZ Pre Value
        else
            DSP_System_Side_PHY0_MEMORY_MAP.System_Side_PRE1_CH2 = (((uint16_t)NRZ_Pre_Table[Pre_Cursor_RX3]<<8 | 0x00FF));
        
        if(DSP_Control)
		{
			Configuration_Error_code_Feedback(2,Config_In_Process);
			System_Side_Tab_Load |= 0x04;
		}
    }
    // RX4 Pre_Cursor
	if((Config_Error_flag&0x08)==0)
    {
		if(Pre_Cursor_RX4==0)
			DSP_System_Side_PHY0_MEMORY_MAP.System_Side_PRE1_CH3 = 0x0000;
        // Get PAM4 Pre Value
		else if(Signal_Status==PAM4_to_PAM4)
            DSP_System_Side_PHY0_MEMORY_MAP.System_Side_PRE1_CH3 = (((uint16_t)PAM4_Pre_Table[Pre_Cursor_RX4]<<8 | 0x00FF));
        // Get NRZ Pre Value
        else
            DSP_System_Side_PHY0_MEMORY_MAP.System_Side_PRE1_CH3 = (((uint16_t)NRZ_Pre_Table[Pre_Cursor_RX4]<<8 | 0x00FF));
        
        if(DSP_Control)
		{
			Configuration_Error_code_Feedback(3,Config_In_Process);
			System_Side_Tab_Load |= 0x08;
		}
    }
	//-----------------------------------------------------------------------------------------//
	// MSA Pre-Cursor RX5 - RX8
	//-----------------------------------------------------------------------------------------//
    // RX5 Pre_Cursor
	if((Config_Error_flag&0x10)==0)
    {
		if(Pre_Cursor_RX5==0)
			DSP_System_Side_PHY1_MEMORY_MAP.System_Side_PRE1_CH0 = 0x0000;
        // Get PAM4 Pre Value
		else if(Signal_Status==PAM4_to_PAM4)
            DSP_System_Side_PHY1_MEMORY_MAP.System_Side_PRE1_CH0 = (((uint16_t)PAM4_Pre_Table[Pre_Cursor_RX5]<<8 | 0x00FF));
        // Get NRZ Pre Value
        else
            DSP_System_Side_PHY1_MEMORY_MAP.System_Side_PRE1_CH0 = (((uint16_t)NRZ_Pre_Table[Pre_Cursor_RX5]<<8 | 0x00FF));
        
        if(DSP_Control)
		{
			Configuration_Error_code_Feedback(4,Config_In_Process);
			System_Side_Tab_Load |= 0x10;
		}
    }
    // RX6 Pre_Cursor
	if((Config_Error_flag&0x20)==0)
    {
		if(Pre_Cursor_RX6==0)
			DSP_System_Side_PHY1_MEMORY_MAP.System_Side_PRE1_CH1 = 0x0000;
        // Get PAM4 Pre Value
		else if(Signal_Status==PAM4_to_PAM4)
            DSP_System_Side_PHY1_MEMORY_MAP.System_Side_PRE1_CH1 = (((uint16_t)PAM4_Pre_Table[Pre_Cursor_RX6]<<8 | 0x00FF));
        // Get NRZ Pre Value
        else
            DSP_System_Side_PHY1_MEMORY_MAP.System_Side_PRE1_CH1 = (((uint16_t)NRZ_Pre_Table[Pre_Cursor_RX6]<<8 | 0x00FF));
        
        if(DSP_Control)
		{
			Configuration_Error_code_Feedback(5,Config_In_Process);
			System_Side_Tab_Load |= 0x20;
		}
    }
    // RX7 Pre_Cursor
	if((Config_Error_flag&0x40)==0)
    {
		if(Pre_Cursor_RX7==0)
			DSP_System_Side_PHY1_MEMORY_MAP.System_Side_PRE1_CH2 = 0x0000;
        // Get PAM4 Pre Value
		else if(Signal_Status==PAM4_to_PAM4)
            DSP_System_Side_PHY1_MEMORY_MAP.System_Side_PRE1_CH2 = (((uint16_t)PAM4_Pre_Table[Pre_Cursor_RX7]<<8 | 0x00FF));
        // Get NRZ Pre Value
        else
            DSP_System_Side_PHY1_MEMORY_MAP.System_Side_PRE1_CH2 = (((uint16_t)NRZ_Pre_Table[Pre_Cursor_RX7]<<8 | 0x00FF));
        
        if(DSP_Control)
		{
			Configuration_Error_code_Feedback(6,Config_In_Process);
			System_Side_Tab_Load |= 0x40;
		}
    }
    // RX8 Pre_Cursor
	if((Config_Error_flag&0x80)==0)
    {
		if(Pre_Cursor_RX8==0)
			DSP_System_Side_PHY1_MEMORY_MAP.System_Side_PRE1_CH3 = 0x0000;
        // Get PAM4 Pre Value
		else if(Signal_Status==PAM4_to_PAM4)
            DSP_System_Side_PHY1_MEMORY_MAP.System_Side_PRE1_CH3 = (((uint16_t)PAM4_Pre_Table[Pre_Cursor_RX8]<<8 | 0x00FF));
        // Get NRZ Pre Value
        else
            DSP_System_Side_PHY1_MEMORY_MAP.System_Side_PRE1_CH3 = (((uint16_t)NRZ_Pre_Table[Pre_Cursor_RX8]<<8 | 0x00FF));
        
        if(DSP_Control)
		{
			Configuration_Error_code_Feedback(7,Config_In_Process);
			System_Side_Tab_Load |= 0x80;
		}
    }
}
//-----------------------------------------------------------------------------------------------------//
// Rx Output EQ <Post> Control Byte166-169
// Value only config by Apply_DataPathInit Byte143 or Apply_Immediate Byte144
//-----------------------------------------------------------------------------------------------------//
void RX_Post_Control(uint8_t DSP_Control)
{
    uint8_t Post_Cursor_RX1,Post_Cursor_RX2,Post_Cursor_RX3,Post_Cursor_RX4;
	uint8_t Post_Cursor_RX5,Post_Cursor_RX6,Post_Cursor_RX7,Post_Cursor_RX8;
	// MSA PAGE10 Byte 166 RX12 Post-cursor CONTROL
	// MSA PAGE10 Byte 167 RX34 Post-cursor CONTROL
	// MSA PAGE10 Byte 168 RX56 Post-cursor CONTROL
	// MSA PAGE10 Byte 169 RX78 Post-cursor CONTROL
	// Device 3 level Control 0C / 0D / 0E
    //Get Rx Post Channel Data
	Post_Cursor_RX1 = Get_CH_Data(QSFPDD_P10[38],0);
	Post_Cursor_RX2 = Get_CH_Data(QSFPDD_P10[38],1);
	Post_Cursor_RX3 = Get_CH_Data(QSFPDD_P10[39],2);
	Post_Cursor_RX4 = Get_CH_Data(QSFPDD_P10[39],3);
	Post_Cursor_RX5 = Get_CH_Data(QSFPDD_P10[40],4);
	Post_Cursor_RX6 = Get_CH_Data(QSFPDD_P10[40],5);
	Post_Cursor_RX7 = Get_CH_Data(QSFPDD_P10[41],6);
	Post_Cursor_RX8 = Get_CH_Data(QSFPDD_P10[41],7);
	//-----------------------------------------------------------------------------------------//
	// MSA Post-Cursor RX1 - RX4
	//-----------------------------------------------------------------------------------------//
	// RX1 Post_Cursor
	if((Config_Error_flag&0x01)==0)
    {
		if(Post_Cursor_RX1 == 0)
			DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST1_CH0 = 0x0000;
        // Get PAM4 Post Value
		else if(Signal_Status==PAM4_to_PAM4)
            DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST1_CH0 = (((uint16_t)PAM4_Post_Table[Post_Cursor_RX1]<<8 | 0x00FF));
        // Get NRZ Post Value
        else
            DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST1_CH0 = (((uint16_t)NRZ_Post_Table[Post_Cursor_RX1]<<8 | 0x00FF));
        
        if(DSP_Control)
		{
			Configuration_Error_code_Feedback(0,Config_In_Process);
			System_Side_Tab_Load |= 0x01;
		}
    }
	// RX2 Post_Cursor
	if((Config_Error_flag&0x02)==0)
    {
		if(Post_Cursor_RX2 == 0)
			DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST1_CH1 = 0x0000;
        // Get PAM4 Post Value
		else if(Signal_Status==PAM4_to_PAM4)
            DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST1_CH1 = (((uint16_t)PAM4_Post_Table[Post_Cursor_RX2]<<8 | 0x00FF));
        // Get NRZ Post Value
        else
            DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST1_CH1 = (((uint16_t)NRZ_Post_Table[Post_Cursor_RX2]<<8 | 0x00FF));
        
		if(DSP_Control)
		{
			Configuration_Error_code_Feedback(1,Config_In_Process);
			System_Side_Tab_Load |= 0x02;
		}
    }
	// RX3 Post_Cursor
	if((Config_Error_flag&0x04)==0)
    {
		if(Post_Cursor_RX3 == 0)
			DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST1_CH2 = 0x0000;
        // Get PAM4 Post Value
		else if(Signal_Status==PAM4_to_PAM4)
            DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST1_CH2 = (((uint16_t)PAM4_Post_Table[Post_Cursor_RX3]<<8 | 0x00FF));
        // Get NRZ Post Value
        else
            DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST1_CH2 = (((uint16_t)NRZ_Post_Table[Post_Cursor_RX3]<<8 | 0x00FF));
        
        if(DSP_Control)
		{
			Configuration_Error_code_Feedback(2,Config_In_Process);
			System_Side_Tab_Load |= 0x04;
		}
    }
	// RX4 Post_Cursor
	if((Config_Error_flag&0x08)==0)
    {
		if(Post_Cursor_RX4 == 0)
			DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST1_CH3 = 0x0000;
        // Get PAM4 Post Value
		else if(Signal_Status==PAM4_to_PAM4)
            DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST1_CH3 = (((uint16_t)PAM4_Post_Table[Post_Cursor_RX4]<<8 | 0x00FF));
        // Get NRZ Post Value
        else
            DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST1_CH3 = (((uint16_t)NRZ_Post_Table[Post_Cursor_RX4]<<8 | 0x00FF));
        
        if(DSP_Control)
		{
			Configuration_Error_code_Feedback(3,Config_In_Process);
			System_Side_Tab_Load |= 0x08;
		}
    }
    //-----------------------------------------------------------------------------------------//
	// MSA Post-Cursor RX5 - RX8
	//-----------------------------------------------------------------------------------------//
    // RX5 Post_Cursor
	if((Config_Error_flag&0x10)==0)
    {
		if(Post_Cursor_RX5 == 0)
			DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST1_CH0 = 0x0000;
        // Get PAM4 Post Value
		else if(Signal_Status==PAM4_to_PAM4)
            DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST1_CH0 = (((uint16_t)PAM4_Post_Table[Post_Cursor_RX5]<<8 | 0x00FF));
        // Get NRZ Post Value
        else
            DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST1_CH0 = (((uint16_t)NRZ_Post_Table[Post_Cursor_RX5]<<8 | 0x00FF));
        
        if(DSP_Control)
		{
			Configuration_Error_code_Feedback(4,Config_In_Process);
			System_Side_Tab_Load |= 0x10;
		}
    }
	// RX6 Post_Cursor
	if((Config_Error_flag&0x20)==0)
    {
		if(Post_Cursor_RX6 == 0)
			DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST1_CH1 = 0x0000;
        // Get PAM4 Post Value
		else if(Signal_Status==PAM4_to_PAM4)
            DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST1_CH1 = (((uint16_t)PAM4_Post_Table[Post_Cursor_RX6]<<8 | 0x00FF));
        // Get NRZ Post Value
        else
            DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST1_CH1 = (((uint16_t)NRZ_Post_Table[Post_Cursor_RX6]<<8 | 0x00FF));
        
        if(DSP_Control)
		{
			Configuration_Error_code_Feedback(5,Config_In_Process);
			System_Side_Tab_Load |= 0x20;
		}
    }
	// RX7 Post_Cursor
	if((Config_Error_flag&0x40)==0)
    {
		if(Post_Cursor_RX7 == 0)
			DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST1_CH2 = 0x0000;
        // Get PAM4 Post Value
		else if(Signal_Status==PAM4_to_PAM4)
            DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST1_CH2 = (((uint16_t)PAM4_Post_Table[Post_Cursor_RX7]<<8 | 0x00FF));
        // Get NRZ Post Value
        else
            DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST1_CH2 = (((uint16_t)NRZ_Post_Table[Post_Cursor_RX7]<<8 | 0x00FF));
        
        if(DSP_Control)
		{
			Configuration_Error_code_Feedback(6,Config_In_Process);
			System_Side_Tab_Load |= 0x40;
		}
    }
	// RX8 Post_Cursor
	if((Config_Error_flag&0x80)==0)
    {
		if(Post_Cursor_RX8 == 0)
			DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST1_CH3 = 0x0000;
        // Get PAM4 Post Value
		else if(Signal_Status==PAM4_to_PAM4)
            DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST1_CH3 = (((uint16_t)PAM4_Post_Table[Post_Cursor_RX8]<<8 | 0x00FF));
        // Get NRZ Post Value
        else
            DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST1_CH3 = (((uint16_t)NRZ_Post_Table[Post_Cursor_RX8]<<8 | 0x00FF));
        
        if(DSP_Control)
		{
			Configuration_Error_code_Feedback(7,Config_In_Process);
			System_Side_Tab_Load |= 0x80;
		}
    }
}
//-----------------------------------------------------------------------------------------------------//
// Rx Output <Amplitude> Control Byte170-173
// Value only config by Apply_DataPathInit Byte143 or Apply_Immediate Byte144
//-----------------------------------------------------------------------------------------------------//
void RX_AMP_OutSwing_Control(uint8_t DSP_Control)
{
    uint8_t AMP_C_RX1,AMP_C_RX2,AMP_C_RX3,AMP_C_RX4 ;
	uint8_t AMP_C_RX5,AMP_C_RX6,AMP_C_RX7,AMP_C_RX8 ;
	// Device 3 level Control 00 / 01 / 02
	// MSA PAGE10 Byte 170 RX12 AMP CONTROL
	// MSA PAGE10 Byte 171 RX34 AMP CONTROL
	// MSA PAGE10 Byte 172 RX56 AMP CONTROL
	// MSA PAGE10 Byte 173 RX78 AMP CONTROL
	//Get Rx Out AMP Channel Data
	AMP_C_RX1 = Get_CH_Data(QSFPDD_P10[42],0);
	AMP_C_RX2 = Get_CH_Data(QSFPDD_P10[42],1);
	AMP_C_RX3 = Get_CH_Data(QSFPDD_P10[43],2);
	AMP_C_RX4 = Get_CH_Data(QSFPDD_P10[43],3);
	AMP_C_RX5 = Get_CH_Data(QSFPDD_P10[44],4);
	AMP_C_RX6 = Get_CH_Data(QSFPDD_P10[44],5);
	AMP_C_RX7 = Get_CH_Data(QSFPDD_P10[45],6);
	AMP_C_RX8 = Get_CH_Data(QSFPDD_P10[45],7);
	//------------------------------------------------------------------------------------------//
	// RX1-RX4 Control                                                                         //
	//----------------------------------------------------------------------------------------//
	// RX1 Output AMP setting//
	if((Config_Error_flag&0x01)==0)
    {
        // Get PAM4 AMP Value
		if(Signal_Status==PAM4_to_PAM4)
            DSP_System_Side_PHY0_MEMORY_MAP.System_Side_Main_CH0 = ((uint16_t)PAM4_AMP_Table[AMP_C_RX1]<<8);
        // Get NRZ AMP Value
        else
            DSP_System_Side_PHY0_MEMORY_MAP.System_Side_Main_CH0 = ((uint16_t)NRZ_AMP_Table[AMP_C_RX1]<<8);

        if(DSP_Control)
		{
			Configuration_Error_code_Feedback(0,Config_In_Process);
			System_Side_Tab_Load |= 0x01;
		}
    } 
	// RX2 Output Swing setting//
	if((Config_Error_flag&0x02)==0)
    {
        // Get PAM4 AMP Value
		if(Signal_Status==PAM4_to_PAM4)
            DSP_System_Side_PHY0_MEMORY_MAP.System_Side_Main_CH1 = ((uint16_t)PAM4_AMP_Table[AMP_C_RX2]<<8);
        // Get NRZ AMP Value
        else
            DSP_System_Side_PHY0_MEMORY_MAP.System_Side_Main_CH1 = ((uint16_t)NRZ_AMP_Table[AMP_C_RX2]<<8);
        
        if(DSP_Control)
		{
			Configuration_Error_code_Feedback(1,Config_In_Process);
			System_Side_Tab_Load |= 0x02;
		}
    }
	// RX3 Output Swing setting
	if((Config_Error_flag&0x04)==0)
    {
        // Get PAM4 AMP Value
		if(Signal_Status==PAM4_to_PAM4)
            DSP_System_Side_PHY0_MEMORY_MAP.System_Side_Main_CH2 = ((uint16_t)PAM4_AMP_Table[AMP_C_RX3]<<8);
        // Get NRZ AMP Value
        else
            DSP_System_Side_PHY0_MEMORY_MAP.System_Side_Main_CH2 = ((uint16_t)NRZ_AMP_Table[AMP_C_RX3]<<8);
        
        if(DSP_Control)
		{
			Configuration_Error_code_Feedback(2,Config_In_Process);
			System_Side_Tab_Load |= 0x04;
		}
    }
	// RX4 Output Swing setting
	if((Config_Error_flag&0x08)==0)
    {
        // Get PAM4 AMP Value
		if(Signal_Status==PAM4_to_PAM4)
            DSP_System_Side_PHY0_MEMORY_MAP.System_Side_Main_CH3 = ((uint16_t)PAM4_AMP_Table[AMP_C_RX4]<<8 );
        // Get NRZ AMP Value
        else
            DSP_System_Side_PHY0_MEMORY_MAP.System_Side_Main_CH3 = ((uint16_t)NRZ_AMP_Table[AMP_C_RX4]<<8 );
        
        if(DSP_Control)
		{
			Configuration_Error_code_Feedback(3,Config_In_Process);
			System_Side_Tab_Load |= 0x08;
		}
    }
    //------------------------------------------------------------------------------------------//
	// RX5-RX8 Control                                                                         //
	//----------------------------------------------------------------------------------------//
	// RX5 Output AMP setting//'
	if((Config_Error_flag&0x10)==0)
    {
        // Get PAM4 AMP Value
		if(Signal_Status==PAM4_to_PAM4)
            DSP_System_Side_PHY1_MEMORY_MAP.System_Side_Main_CH0 = ((uint16_t)PAM4_AMP_Table[AMP_C_RX5]<<8 );
        // Get NRZ AMP Value
        else
            DSP_System_Side_PHY1_MEMORY_MAP.System_Side_Main_CH0 = ((uint16_t)NRZ_AMP_Table[AMP_C_RX5]<<8 );
        
        if(DSP_Control)
		{
			Configuration_Error_code_Feedback(4,Config_In_Process);
			System_Side_Tab_Load |= 0x10;
		}
    }
	// RX6 Output Swing setting//
	if((Config_Error_flag&0x20)==0)
    {
        // Get PAM4 AMP Value
		if(Signal_Status==PAM4_to_PAM4)
            DSP_System_Side_PHY1_MEMORY_MAP.System_Side_Main_CH1 = ((uint16_t)PAM4_AMP_Table[AMP_C_RX6]<<8 );
        // Get NRZ AMP Value
        else
            DSP_System_Side_PHY1_MEMORY_MAP.System_Side_Main_CH1 = ((uint16_t)NRZ_AMP_Table[AMP_C_RX6]<<8 );
        
        if(DSP_Control)
		{
			Configuration_Error_code_Feedback(5,Config_In_Process);
			System_Side_Tab_Load |= 0x20;
		}
    }
	// RX7 Output Swing setting
	if((Config_Error_flag&0x40)==0)
    {
        // Get PAM4 AMP Value
		if(Signal_Status==PAM4_to_PAM4)
            DSP_System_Side_PHY1_MEMORY_MAP.System_Side_Main_CH2 = ((uint16_t)PAM4_AMP_Table[AMP_C_RX7]<<8 );
        // Get NRZ AMP Value
        else
            DSP_System_Side_PHY1_MEMORY_MAP.System_Side_Main_CH2 = ((uint16_t)NRZ_AMP_Table[AMP_C_RX7]<<8 );
        
        if(DSP_Control)
		{
			Configuration_Error_code_Feedback(6,Config_In_Process);
			System_Side_Tab_Load |= 0x40;
		}
    }
	// RX8 Output Swing setting
	if((Config_Error_flag&0x80)==0)
    {
        // Get PAM4 AMP Value
		if(Signal_Status==PAM4_to_PAM4)
            DSP_System_Side_PHY1_MEMORY_MAP.System_Side_Main_CH3 = ((uint16_t)PAM4_AMP_Table[AMP_C_RX8]<<8 );
        // Get NRZ AMP Value
        else
            DSP_System_Side_PHY1_MEMORY_MAP.System_Side_Main_CH3 = ((uint16_t)NRZ_AMP_Table[AMP_C_RX8]<<8 );
        
        if(DSP_Control)
		{
			Configuration_Error_code_Feedback(7,Config_In_Process);
			System_Side_Tab_Load |= 0x80;
		}
    }
}
//------------------------------------------------------------------------------------------//
// DSP System Side Tab Load Control                                                        //
//----------------------------------------------------------------------------------------//
void QDD_DSP_Tap_ReLoader()
{
	//RX1
	if(System_Side_Tab_Load&0x01)
	{
		DSP_System_Side_TX_FIR_SET(0);
		System_Side_Tab_Load &= ~0x01 ;
		Configuration_Error_code_Feedback( 0 , Accepted );
	}
	//RX2
	if(System_Side_Tab_Load&0x02)
	{
		DSP_System_Side_TX_FIR_SET(1);
		System_Side_Tab_Load &= ~0x02 ;
		Configuration_Error_code_Feedback( 1 , Accepted );
	}
	//RX3
	if(System_Side_Tab_Load&0x04)
	{
		DSP_System_Side_TX_FIR_SET(2);
		System_Side_Tab_Load &= ~0x04 ;
		Configuration_Error_code_Feedback( 2 , Accepted );
	}
	//RX4
	if(System_Side_Tab_Load&0x08)
	{
		DSP_System_Side_TX_FIR_SET(3);
		System_Side_Tab_Load &= ~0x08 ;
		Configuration_Error_code_Feedback( 3 , Accepted );
	}
	//RX5
	if(System_Side_Tab_Load&0x10)
	{
		DSP_System_Side_TX_FIR_SET(4);
		System_Side_Tab_Load &= ~0x10 ;
		Configuration_Error_code_Feedback( 4 , Accepted );
	}
	//RX6
	if(System_Side_Tab_Load&0x20)
	{
		DSP_System_Side_TX_FIR_SET(5);
		System_Side_Tab_Load &= ~0x20 ;
		Configuration_Error_code_Feedback( 5 , Accepted );
	}
	//RX7
	if(System_Side_Tab_Load&0x40)
	{
		DSP_System_Side_TX_FIR_SET(6);
		System_Side_Tab_Load &= ~0x40 ;
		Configuration_Error_code_Feedback( 6 , Accepted );
	}
	//RX8
	if(System_Side_Tab_Load&0x80)
	{
		DSP_System_Side_TX_FIR_SET(7);
		System_Side_Tab_Load &= ~0x80 ;
		Configuration_Error_code_Feedback( 7 , Accepted );
	}
}

void Return_Config_In_Process()
{
    for(uint8_t Lane=0;Lane<8;Lane++)
    {
        Configuration_Error_code_Feedback( Lane , Config_In_Process );
    }
}

uint8_t Apply_Triggers()
{
    uint8_t Trigger=0;
    if((QSFPDD_P10[15]>0x00)||(QSFPDD_P10[16]>0x00))
    {
        Trigger=1;
        Control_Set=0;
    }
    if((QSFPDD_P10[50]>0x00)||(QSFPDD_P10[51]>0x00))
    {
        Trigger=1;
        Control_Set=1;
    }
    return Trigger;
}
//-----------------------------------------------------------------------------------------------------//
// Staged Control Set 0, Apply Controls
//-----------------------------------------------------------------------------------------------------//
void Apply_DataPathInit_Immediate(uint8_t Module_State)
{
	if(Module_State==MODULE_LOW_PWR)
    {
		// Apply_DataPathInit and Apply_Immediate to config value and return error code
		if(Apply_Triggers())
		{
			Return_Config_In_Process();
			if(AppSel_Control_Feedback())
			{
				if(Signal_Integrity_Control_Feedback())
				{
					// Update control value to sram for dsp init
					RX_Pre_Control(DSP_Write_Disable);
					RX_Post_Control(DSP_Write_Disable);
					RX_AMP_OutSwing_Control(DSP_Write_Disable);
				}
			}
			// Read these register shall return 0
            // Staged Control Set 0,Apply Triggers
			QSFPDD_P10[15]=0x00;
			QSFPDD_P10[16]=0x00;
            // Staged Control Set 1,Apply Triggers
			QSFPDD_P10[50]=0x00;
			QSFPDD_P10[51]=0x00;
		}
    }
    // Force to config mode and SI Control
    else
    {
		// Apply_DataPathInit and Apply_Immediate to config value and return error code
		if(Apply_Triggers())
		{
			Return_Config_In_Process();
			// AppSel_Control_Feedback and force to config chip mode
			if(AppSel_Control_Feedback())
			{
				
				if(Bef_DSP_MODE_SET!=DSP_MODE_SET)
				{
					// LDD Disable
//					TxDIS_Power_flag=1;
//					Tx_Disable(0xFF);
					//DSP_Init();
					Bef_DSP_MODE_SET=DSP_MODE_SET;
					// LDD Enable
//					TxDIS_Power_flag=1;
//					Tx_Disable(0x00);
//					MSA_DataPathState(DataPathInitialized);
				}
				// Signal_Integrity_Control_Feedback and Force to config SI control
				if(Signal_Integrity_Control_Feedback())
				{
					
					RX_Pre_Control(DSP_Write_Enable);
					RX_Post_Control(DSP_Write_Enable);
					RX_AMP_OutSwing_Control(DSP_Write_Enable);
          QDD_DSP_Tap_ReLoader();
				}
				// For DSP_Tx_Los_Check check again to change data path state by tx lol
				Tx_Los_Enable_Flag=1;
			}
			// Read these register shall return 0
			// Staged Control Set 0,Apply Triggers
			QSFPDD_P10[15]=0x00;
			QSFPDD_P10[16]=0x00;
			// Staged Control Set 1,Apply Triggers
			QSFPDD_P10[50]=0x00;
			QSFPDD_P10[51]=0x00;
		}
    }
}

void CMIS_PAGE10h_Control()
{
//    Tx_Disable(QSFPDD_P10[2]);
    Rx_Output_Disable(QSFPDD_P10[10]);
    if(Get_DataPathReDeinitS()==0)
        Auto_Squelch_Tx_Output();
    Auto_Squelch_Rx_Output();
}