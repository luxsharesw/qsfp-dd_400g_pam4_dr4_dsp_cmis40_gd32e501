#include "gd32e501.h"
#include "core_cm33.h"
#include "CMIS_MSA.h"
#include "Calibration_Struct.h"
#include "GD_FlahMap.h"
#include "string.h"
//--------------------------------------------------------//
// Chip ICs .h                                            //
//--------------------------------------------------------//
#include "DSP.h"
#include "DSP_Line_Side.h"
#include "DSP_System_Side.h"
//------------------------------------------------------------------------------------------//
// Page13h Flags
//------------------------------------------------------------------------------------------//
uint8_t P13_B144_Bef = 0;
uint8_t P13_B152_Bef = 0;
uint8_t P13_B180_Bef = 0;
uint8_t P13_B181_Bef = 0;
uint8_t P13_B182_Bef = 0;
uint8_t P13_B183_Bef = 0;
uint8_t P13_B155_Bef = 0;
uint8_t P13_B147_Bef = 0;
//-----------------------------------------------------------------------------------------------------//
// Functions
//-----------------------------------------------------------------------------------------------------//
uint8_t Get_SystemSide_PRBS_Pattern(uint8_t Data)
{
    uint8_t PRBS_Pattern = 0x00;
    
    if(Signal_Status==PAM4_to_PAM4)
    {
        if(Data==0x00)
            PRBS_Pattern=PRBS31Q_SystemSide;
        else if(Data==0x02)
            PRBS_Pattern=PRBS23Q_SystemSide;
        else if(Data==0x04)
            PRBS_Pattern=PRBS15Q_SystemSide;
        else if(Data==0x06)
            PRBS_Pattern=PRBS13Q_SystemSide;
        else if(Data==0x08)
            PRBS_Pattern=PRBS9Q_SystemSide;
        else if(Data==0x0A)
            PRBS_Pattern=PRBS7Q_SystemSide;
        else if(Data==0x0C)
            PRBS_Pattern=SSPRQ_SystemSide;
    }
    else
    {
        if(Data==0x01)
            PRBS_Pattern=PRBS31Q_SystemSide;
        else if(Data==0x03)
            PRBS_Pattern=PRBS23Q_SystemSide;
        else if(Data==0x05)
            PRBS_Pattern=PRBS15Q_SystemSide;
        else if(Data==0x07)
            PRBS_Pattern=PRBS13Q_SystemSide;
        else if(Data==0x09)
            PRBS_Pattern=PRBS9Q_SystemSide;
        else if(Data==0x0B)
            PRBS_Pattern=PRBS7Q_SystemSide;
        else if(Data==0x0C)
            PRBS_Pattern=SSPRQ_SystemSide;
    }
    
    return PRBS_Pattern;
}

uint8_t Get_LineSide_PRBS_Pattern(uint8_t Data)
{
    uint8_t PRBS_Pattern = 0x00;
    
    if(Signal_Status==PAM4_to_PAM4)
    {
        if(Data==0x00)
            PRBS_Pattern=PRBS31Q_LineSide;
        else if(Data==0x02)
            PRBS_Pattern=PRBS23Q_LineSide;
        else if(Data==0x04)
            PRBS_Pattern=PRBS15Q_LineSide;
        else if(Data==0x06)
            PRBS_Pattern=PRBS13Q_LineSide;
        else if(Data==0x08)
            PRBS_Pattern=PRBS9Q_LineSide;
        else if(Data==0x0A)
            PRBS_Pattern=PRBS7Q_LineSide;
        else if(Data==0x0C)
            PRBS_Pattern=SSPRQ_LineSide;
    }
    else
    {
        if(Data==0x01)
            PRBS_Pattern=PRBS31Q_LineSide;
        else if(Data==0x03)
            PRBS_Pattern=PRBS23Q_LineSide;
        else if(Data==0x05)
            PRBS_Pattern=PRBS15Q_LineSide;
        else if(Data==0x07)
            PRBS_Pattern=PRBS13Q_LineSide;
        else if(Data==0x09)
            PRBS_Pattern=PRBS9Q_LineSide;
        else if(Data==0x0B)
            PRBS_Pattern=PRBS7Q_LineSide;
        else if(Data==0x0C)
            PRBS_Pattern=SSPRQ_LineSide;
    }
    
    return PRBS_Pattern;
}
//-----------------------------------------------------------------------------------------------------//
// Page13h Controls                                                                                    //
//-----------------------------------------------------------------------------------------------------//
//-----------------------------------------------------------------------------------------------------//
// Host Side Pattern Generator Controls Byte144
//-----------------------------------------------------------------------------------------------------//
void Host_Side_Pattern_Generator_Control()
{
    //Host Side Generator Enable
    if(P13_B144_Bef!=QSFPDD_P13[16])
    {
		//Lane0-Lane3
        if(QSFPDD_P13[16] & 0x01)
            DSP_System_Side_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[20],0)), 0 , 1);
        else
            DSP_System_Side_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[20],0)), 0 , 0);
        
        if(QSFPDD_P13[16] & 0x02)
            DSP_System_Side_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[20],1)), 1 , 1);
        else
            DSP_System_Side_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[20],1)), 1 , 0);
        
        if(QSFPDD_P13[16] & 0x04)
            DSP_System_Side_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[21],2)), 2 , 1);
        else
            DSP_System_Side_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[21],2)), 2 , 0);
        
        if(QSFPDD_P13[16] & 0x08)
            DSP_System_Side_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[21],3)), 3 , 1);
        else
            DSP_System_Side_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[21],3)), 3 , 0);

		//Lane4-Lane7
        if(QSFPDD_P13[16] & 0x10)
            DSP_System_Side_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[22],0)), 4 , 1);
        else
            DSP_System_Side_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[22],0)), 4 , 0);
        
        if(QSFPDD_P13[16] & 0x20)
            DSP_System_Side_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[22],1)), 5 , 1);
        else
            DSP_System_Side_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[22],1)), 5 , 0);
        
        if(QSFPDD_P13[16] & 0x40)
            DSP_System_Side_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[23],2)), 6 , 1);
        else
            DSP_System_Side_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[23],2)), 6 , 0);
        
        if(QSFPDD_P13[16] & 0x80)
            DSP_System_Side_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[23],3)), 7 , 1);
        else
            DSP_System_Side_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[23],3)), 7 , 0);
    }

    P13_B144_Bef = QSFPDD_P13[16];
}
//-----------------------------------------------------------------------------------------------------//
// Media Side Pattern Generator Controls Byte152
//-----------------------------------------------------------------------------------------------------//
void Media_Side_Pattern_Generator_Control()
{
    //Media Side Generator Enable
    if(P13_B152_Bef!=QSFPDD_P13[24])
    {
		//Lane0-Lane3
        if(QSFPDD_P13[24] & 0x01)
            DSP_Line_Side_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[28],0)), 0 , 1);
        else
            DSP_Line_Side_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[28],0)), 0 , 0);
        
        if(QSFPDD_P13[24] & 0x02)
            DSP_Line_Side_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[28],1)), 1 , 1);
        else
            DSP_Line_Side_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[28],1)), 1 , 0);
        
        if(QSFPDD_P13[24] & 0x04)
            DSP_Line_Side_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[29],2)), 2 , 1);
        else
            DSP_Line_Side_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[29],2)), 2 , 0);
        
        if(QSFPDD_P13[24] & 0x08)
            DSP_Line_Side_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[29],3)), 3 , 1);
        else
            DSP_Line_Side_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[29],3)), 3 , 0);
		
        //Lane4-Lane7
        if(QSFPDD_P13[24] & 0x10)
            DSP_Line_Side_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[30],0)), 4 , 1);
        else
            DSP_Line_Side_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[30],0)), 4 , 0);
        
        if(QSFPDD_P13[24] & 0x20)
            DSP_Line_Side_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[30],1)), 5 , 1);
        else
            DSP_Line_Side_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[30],1)), 5 , 0);
        
        if(QSFPDD_P13[24] & 0x40)
            DSP_Line_Side_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[31],2)), 6 , 1);
        else
            DSP_Line_Side_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[31],2)), 6 , 0);
        
        if(QSFPDD_P13[24] & 0x80)
            DSP_Line_Side_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[31],3)), 7 , 1);
        else
            DSP_Line_Side_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[31],3)), 7 , 0);
    }
    P13_B152_Bef = QSFPDD_P13[24];
}
//-----------------------------------------------------------------------------------------------------//
// Loopback Controls - Media Byte180-181
//-----------------------------------------------------------------------------------------------------//
void Media_Side_LoopBack_Control()
{
	// Page13h 180 / 52 Media Output ( Line Side Digital Loopback )
	// Page13h 181 / 53 Media Input  ( Line Side Remote Loopback )
	//---------------------------------------------------------------------------//
	// Media Output  ( Line Side Digital Loopback )
	//---------------------------------------------------------------------------//
	if(P13_B180_Bef!=QSFPDD_P13[52])
	{
        //Lane0-Lane3
		if(QSFPDD_P13[52] & 0x01)
            DSP_Line_Side_Digital_Loopback_SET(0,1);
        else
            DSP_Line_Side_Digital_Loopback_SET(0,0);
        
		if(QSFPDD_P13[52] & 0x02)
            DSP_Line_Side_Digital_Loopback_SET(1,1);
        else
            DSP_Line_Side_Digital_Loopback_SET(1,0);
        
		if(QSFPDD_P13[52] & 0x04)
            DSP_Line_Side_Digital_Loopback_SET(2,1);
        else
            DSP_Line_Side_Digital_Loopback_SET(2,0);
        
		if(QSFPDD_P13[52] & 0x08)
            DSP_Line_Side_Digital_Loopback_SET(3,1);
        else
            DSP_Line_Side_Digital_Loopback_SET(3,0);
        
        //Lane4-Lane7
		if(QSFPDD_P13[52] & 0x10)
            DSP_Line_Side_Digital_Loopback_SET(4,1);
        else
            DSP_Line_Side_Digital_Loopback_SET(4,0);
        
		if(QSFPDD_P13[52] & 0x20)
            DSP_Line_Side_Digital_Loopback_SET(5,1);
        else
            DSP_Line_Side_Digital_Loopback_SET(5,0);
        
		if(QSFPDD_P13[52] & 0x40)
            DSP_Line_Side_Digital_Loopback_SET(6,1);
        else
            DSP_Line_Side_Digital_Loopback_SET(6,0);
        
		if(QSFPDD_P13[52] & 0x80)
            DSP_Line_Side_Digital_Loopback_SET(7,1);
        else
            DSP_Line_Side_Digital_Loopback_SET(7,0);
    }
	//---------------------------------------------------------------------------//
	// Media Input  ( Line Side Remote Loopback )
	//---------------------------------------------------------------------------//
	if(P13_B181_Bef!=QSFPDD_P13[53])
    {
        //Lane0-Lane3
		if(QSFPDD_P13[53] & 0x01)
            DSP_Line_Side_Remote_Loopback_SET(0,1);
        else
            DSP_Line_Side_Remote_Loopback_SET(0,0);
        
		if(QSFPDD_P13[53] & 0x02)
            DSP_Line_Side_Remote_Loopback_SET(1,1);
        else
            DSP_Line_Side_Remote_Loopback_SET(1,0);
        
		if(QSFPDD_P13[53] & 0x04)
            DSP_Line_Side_Remote_Loopback_SET(2,1);
        else
            DSP_Line_Side_Remote_Loopback_SET(2,0);
        
		if(QSFPDD_P13[53] & 0x08)
            DSP_Line_Side_Remote_Loopback_SET(3,1);
        else
            DSP_Line_Side_Remote_Loopback_SET(3,0);
        
        //Lane4-Lane7
		if(QSFPDD_P13[53] & 0x10)
            DSP_Line_Side_Remote_Loopback_SET(4,1);
        else
            DSP_Line_Side_Remote_Loopback_SET(4,0);
        
		if(QSFPDD_P13[53] & 0x20)
            DSP_Line_Side_Remote_Loopback_SET(5,1);
        else
            DSP_Line_Side_Remote_Loopback_SET(5,0);
        
		if(QSFPDD_P13[53] & 0x40)
            DSP_Line_Side_Remote_Loopback_SET(6,1);
        else
            DSP_Line_Side_Remote_Loopback_SET(6,0);
        
		if(QSFPDD_P13[53] & 0x80)
            DSP_Line_Side_Remote_Loopback_SET(7,1);
        else
            DSP_Line_Side_Remote_Loopback_SET(7,0);
	}
	P13_B180_Bef = QSFPDD_P13[52];
	P13_B181_Bef = QSFPDD_P13[53];
}

//-----------------------------------------------------------------------------------------------------//
// Loopback Controls - Host Byte182-183
//-----------------------------------------------------------------------------------------------------//
void Host_Side_LoopBack_Control()
{
	// Page13h 182 / 54 Host Output ( System Side Digital loopback )
	// Page13h 183 / 55 Host Input  ( System Side Remote loopback )
	//---------------------------------------------------------------------------//
	// Host Output ( System Side Digital Loopback )
	//---------------------------------------------------------------------------//
	if(P13_B182_Bef!=QSFPDD_P13[54])
    {
        //Lane0-Lane3
		if(QSFPDD_P13[54] & 0x01)
            DSP_System_Side_Digital_Loopback_SET(0,1);
        else
            DSP_System_Side_Digital_Loopback_SET(0,0);
        
		if(QSFPDD_P13[54] & 0x02)
            DSP_System_Side_Digital_Loopback_SET(1,1);
        else
            DSP_System_Side_Digital_Loopback_SET(1,0);
        
		if(QSFPDD_P13[54] & 0x04)
            DSP_System_Side_Digital_Loopback_SET(2,1);
        else
            DSP_System_Side_Digital_Loopback_SET(2,0);
        
		if(QSFPDD_P13[54] & 0x08)
            DSP_System_Side_Digital_Loopback_SET(3,1);
        else
            DSP_System_Side_Digital_Loopback_SET(3,0);
        
        //Lane4-Lane7
		if(QSFPDD_P13[54] & 0x10)
            DSP_System_Side_Digital_Loopback_SET(4,1);
        else
            DSP_System_Side_Digital_Loopback_SET(4,0);
        
		if(QSFPDD_P13[54] & 0x20)
            DSP_System_Side_Digital_Loopback_SET(5,1);
        else
            DSP_System_Side_Digital_Loopback_SET(5,0);
        
		if(QSFPDD_P13[54] & 0x40)
            DSP_System_Side_Digital_Loopback_SET(6,1);
        else
            DSP_System_Side_Digital_Loopback_SET(6,0);
        
		if(QSFPDD_P13[54] & 0x80)
            DSP_System_Side_Digital_Loopback_SET(7,1);
        else
            DSP_System_Side_Digital_Loopback_SET(7,0);
	}

	//---------------------------------------------------------------------------//
	// Host Input ( System Side Remote Loopback )
	//---------------------------------------------------------------------------//
	if(P13_B183_Bef!=QSFPDD_P13[55])
    {
        //Lane0-Lane3
		if(QSFPDD_P13[55] & 0x01)
            DSP_System_Side_Remote_Loopback_SET(0,1);
        else
            DSP_System_Side_Remote_Loopback_SET(0,0);
        
		if(QSFPDD_P13[55] & 0x02)
            DSP_System_Side_Remote_Loopback_SET(1,1);
        else
            DSP_System_Side_Remote_Loopback_SET(1,0);
        
		if(QSFPDD_P13[55] & 0x04)
            DSP_System_Side_Remote_Loopback_SET(2,1);
        else
            DSP_System_Side_Remote_Loopback_SET(2,0);
        
		if(QSFPDD_P13[55] & 0x08)
            DSP_System_Side_Remote_Loopback_SET(3,1);
        else
            DSP_System_Side_Remote_Loopback_SET(3,0);
        
        //Lane4-Lane7
		if(QSFPDD_P13[55] & 0x10)
            DSP_System_Side_Remote_Loopback_SET(4,1);
        else
            DSP_System_Side_Remote_Loopback_SET(4,0);
        
		if(QSFPDD_P13[55] & 0x20)
            DSP_System_Side_Remote_Loopback_SET(5,1);
        else
            DSP_System_Side_Remote_Loopback_SET(5,0);
        
		if(QSFPDD_P13[55] & 0x40)
            DSP_System_Side_Remote_Loopback_SET(6,1);
        else
            DSP_System_Side_Remote_Loopback_SET(6,0);
        
		if(QSFPDD_P13[55] & 0x80)
            DSP_System_Side_Remote_Loopback_SET(7,1);
        else
            DSP_System_Side_Remote_Loopback_SET(7,0);
	}
	P13_B182_Bef = QSFPDD_P13[54];
	P13_B183_Bef = QSFPDD_P13[55];
}

void CMIS_PAGE13h_Diagnostics_Function()
{
	Host_Side_Pattern_Generator_Control();
	Media_Side_Pattern_Generator_Control();
	Media_Side_LoopBack_Control();
	Host_Side_LoopBack_Control();
}
