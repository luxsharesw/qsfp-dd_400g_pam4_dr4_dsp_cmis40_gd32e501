
#include "gd32e501.h"
#include "core_cm33.h"
#include "CMIS_MSA.h"
#include "stdint.h"
#include "Calibration_Struct.h"
#include "Calibration_Struct_2.h"
#include "GD32E501_GPIO_Customize_Define.h"
//--------------------------------------------------------//
// Chip ICs .h                                            //
//--------------------------------------------------------//
#include "DSP.h"
#include "TIA_Control.h"
#include "LDD_Control.h"
#include "TEC_APC_Control.h"

#define ADC_SM            0
#define RXP_CH0_CH1       1
#define RXP_CH2_CH3       2
#define TXB_TXP_CH01_SM   3
#define TXB_TXP_CH23_SM   4
#define TEC_CH12          5
#define TEC_CH34          6
#define APC_CONTROL       7
#define OP_SM             8
#define TRXLOLLOS         9
#define Other_DDMI        10

#define SHOW_RSSI_Enable   1
#define SHOW_RSSI_Disable  0

uint8_t SHOW_RSSI_CH0 = 1 ;
uint8_t SHOW_RSSI_CH1 = 1 ;
uint8_t SHOW_RSSI_CH2 = 1 ;
uint8_t SHOW_RSSI_CH3 = 1 ;
uint8_t SHOW_RSSI_CH4 = 1 ;
uint8_t SHOW_RSSI_CH5 = 1 ;
uint8_t SHOW_RSSI_CH6 = 1 ;
uint8_t SHOW_RSSI_CH7 = 1 ;
int8_t Temperature_Index = 0 ;
uint16_t Bias_Current_CH0,Bias_Current_CH1,Bias_Current_CH2,Bias_Current_CH3;
uint16_t Bias_Current_CH4,Bias_Current_CH5,Bias_Current_CH6,Bias_Current_CH7;

//------------------------------------------------------------------------------------------//
// DDMI Flags
//------------------------------------------------------------------------------------------//
uint8_t DDMI_Update_C = 0 ;
uint8_t PowerON_AWEN_flag = 0 ;
uint8_t AW_DDMI_Count = 0 ;
uint16_t DDMI_DataCount = 0 ;

int16_t Temp_CAL( int16_t Vaule , uint8_t Slop , uint8_t Shift , int16_t Offset)
{
	uint32_t Buffer_temp ;

	Buffer_temp = (uint32_t)Vaule * Slop ;
	Buffer_temp = (uint32_t)Buffer_temp >> Shift ;

	Vaule = (uint32_t)Buffer_temp + Offset * 256 ;

    return Vaule ;
}

uint16_t CAL_Function( uint16_t Vaule , uint8_t Slop , uint8_t Shift , int16_t Offset )
{
	uint32_t Buffer_temp ;

	Buffer_temp = (uint32_t)Vaule * Slop ;
	Buffer_temp = (uint32_t)Buffer_temp >> Shift ;

	Vaule = (uint32_t)Buffer_temp + Offset;

	return Vaule;
}

void Temperature_Monitor(int16_t tempsensor_value)
{
	int16_t Temp_Buffer;
	int8_t  Indx_Temp ;

	Temp_Buffer = tempsensor_value ;

	Temp_Buffer = Temp_CAL(tempsensor_value,CALIB_MEMORY_MAP.TEMP_SCALE1M,CALIB_MEMORY_MAP.TEMP_SCALE1L,CALIB_MEMORY_MAP.TEMP_OFFSET1);

	QSFPDD_A0[14] = Temp_Buffer >> 8 ;
	QSFPDD_A0[15] = Temp_Buffer ;

	Indx_Temp = QSFPDD_A0[14];
    
    if(Indx_Temp<-40)
        Indx_Temp = -40 ;

    if(Indx_Temp>85)
        Indx_Temp = 86 ;
    
    Indx_Temp = ( Indx_Temp + 40 );

	Temperature_Index = (Indx_Temp>>1) ;
       
	if(Temperature_Index > 64) 
        Temperature_Index = 64;
    
    CALIB_MEMORY_MAP.Temp_Index = Temperature_Index;
}

void VCC_Monitor( uint16_t VCC_Vaule)
{
    int16_t  offset_Buffer_VCC = 0 ;
    offset_Buffer_VCC = (CALIB_MEMORY_MAP.VCC_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.VCC_OFFSET_LSB );
    
	VCC_Vaule = CAL_Function( VCC_Vaule , CALIB_MEMORY_MAP.VCC_SCALEM , CALIB_MEMORY_MAP.VCC_SCALEL , offset_Buffer_VCC ) ;

	QSFPDD_A0[16] = VCC_Vaule >> 8 ;
	QSFPDD_A0[17] = VCC_Vaule ;
}

void BIAS_TxPower_Monitor( uint8_t Channel)
{
	uint16_t MOD_Temp_Buffer = 0;
	uint16_t TXP_BUFFER = 0 ;  
    int16_t  offset_Buffer_B = 0 ;
    int16_t  offset_Buffer_P = 0 ;
    uint16_t  Biase_buffer = 0 ;

	switch(Channel)
	{
		case 0:
        Biase_buffer =LDD_Control_MEMORY_MAP.MP5940_IDAC_MSB_CH1;//((LDD_Control_MEMORY_MAP.MP5940_IDAC_MSB_CH1 << 2) + (LDD_Control_MEMORY_MAP.MP5940_IDAC_LSB_CH1 & 0x03));
				Bias_Current_CH0 = ( (Biase_buffer<<2)+(LDD_Control_MEMORY_MAP.MP5940_IDAC_LSB_CH1 & 0x03))*122;
                
        TXP_BUFFER = GET_ADC_RowData(TX1MPDANODE);
        
        offset_Buffer_B = (CALIB_MEMORY_MAP.IBias0_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.IBias0_OFFSET_LSB );
        offset_Buffer_P = (CALIB_MEMORY_MAP.TXP0_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.TXP0_OFFSET_LSB );
        
				Bias_Current_CH0 = CAL_Function( Bias_Current_CH0 , CALIB_MEMORY_MAP.IBias0_SCALEM , CALIB_MEMORY_MAP.IBias0_SCALE1L , offset_Buffer_B ) ;
				TXP_BUFFER = CAL_Function( TXP_BUFFER , CALIB_MEMORY_MAP.TXP0_SCALEM , CALIB_MEMORY_MAP.TXP0_SCALEL , offset_Buffer_P ) ;

				if((QSFPDD_P10[2] & 0x01) || (Tx_Disable_Flag & 0x01))
				{
					QSFPDD_P11[42] = 0 ;
					QSFPDD_P11[43] = 0 ;
					// CH0  Tx Power
					QSFPDD_P11[26] = 0 ;
					QSFPDD_P11[27] = 1 ;
                    // Not support datapath do not return flag status
					if(Get_Support_DataPathState(Channel))
					{
						// Tx Power Low Alarm Warning Trigger
						QSFPDD_P11[12] |= 0x01 ;
						QSFPDD_P11[14] |= 0x01 ;
						// Tx Bias Low Alarm Warning Trigger
						QSFPDD_P11[16] |= 0x01 ;
						QSFPDD_P11[18] |= 0x01 ;
					}
				}
				else
				{
					QSFPDD_P11[42] = Bias_Current_CH0 >> 8 ;
					QSFPDD_P11[43] = Bias_Current_CH0 ;
					// CH0 TxPower
					QSFPDD_P11[26] = TXP_BUFFER >> 8 ;
					QSFPDD_P11[27] = TXP_BUFFER ;
				}
				break;
		case 1:
        Biase_buffer =LDD_Control_MEMORY_MAP.MP5940_IDAC_MSB_CH1;//(LDD_Control_MEMORY_MAP.MP5940_IDAC_MSB_CH1 << 2 | (LDD_Control_MEMORY_MAP.MP5940_IDAC_LSB_CH1 & 0x03));
				Bias_Current_CH1 = ( (Biase_buffer<<2)+(LDD_Control_MEMORY_MAP.MP5940_IDAC_LSB_CH1 & 0x03))*122;
        
        TXP_BUFFER = GET_ADC_RowData(TX2MPDANODE);
                        
        offset_Buffer_B = (CALIB_MEMORY_MAP.IBias1_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.IBias1_OFFSET_LSB );
        offset_Buffer_P = (CALIB_MEMORY_MAP.TXP1_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.TXP1_OFFSET_LSB );

				Bias_Current_CH1 = CAL_Function( Bias_Current_CH1 , CALIB_MEMORY_MAP.IBias1_SCALEM , CALIB_MEMORY_MAP.IBias1_SCALE1L , offset_Buffer_B ) ;
				TXP_BUFFER = CAL_Function( TXP_BUFFER , CALIB_MEMORY_MAP.TXP1_SCALEM , CALIB_MEMORY_MAP.TXP1_SCALEL , offset_Buffer_P ) ;

				if((QSFPDD_P10[2] & 0x02) || (Tx_Disable_Flag & 0x02))
				{
					QSFPDD_P11[44] = 0 ;
					QSFPDD_P11[45] = 0 ;
					// CH1  Tx Power
					QSFPDD_P11[28] = 0 ;
					QSFPDD_P11[29] = 1 ;
                    // Not support datapath do not return flag status
					if(Get_Support_DataPathState(Channel))
					{
						// Tx Power Low Alarm Warning Trigger
						QSFPDD_P11[12] |= 0x02 ;
						QSFPDD_P11[14] |= 0x02 ;
						// Tx Bias Low Alarm Warning Trigger
						QSFPDD_P11[16] |= 0x02 ;
						QSFPDD_P11[18] |= 0x02 ;
					}
				}
				else
				{
					QSFPDD_P11[44] = Bias_Current_CH1 >> 8 ;
					QSFPDD_P11[45] = Bias_Current_CH1 ;
					// CH1  Tx Power
					QSFPDD_P11[28] = TXP_BUFFER >> 8 ;
					QSFPDD_P11[29] = TXP_BUFFER ;
				}
				break;
		case 2:
        Biase_buffer =LDD_Control_MEMORY_MAP.MP5940_IDAC_MSB_CH3;//(LDD_Control_MEMORY_MAP.MP5940_IDAC_MSB_CH3 << 2 | (LDD_Control_MEMORY_MAP.MP5940_IDAC_LSB_CH3 & 0x03));
        Bias_Current_CH2 = ( (Biase_buffer<<2)+(LDD_Control_MEMORY_MAP.MP5940_IDAC_LSB_CH3 & 0x03))*122;
        
        TXP_BUFFER = GET_ADC_RowData(TX3MPDANODE);
                                
        offset_Buffer_B = (CALIB_MEMORY_MAP.IBias2_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.IBias2_OFFSET_LSB );
        offset_Buffer_P = (CALIB_MEMORY_MAP.TXP2_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.TXP2_OFFSET_LSB );

				Bias_Current_CH2 = CAL_Function( Bias_Current_CH2 , CALIB_MEMORY_MAP.IBias2_SCALEM , CALIB_MEMORY_MAP.IBias2_SCALE1L , offset_Buffer_B ) ;
				TXP_BUFFER = CAL_Function( TXP_BUFFER , CALIB_MEMORY_MAP.TXP2_SCALEM , CALIB_MEMORY_MAP.TXP2_SCALEL , offset_Buffer_P ) ;

				if((QSFPDD_P10[2] & 0x04) || (Tx_Disable_Flag & 0x04))
				{
					QSFPDD_P11[46] = 0 ;
					QSFPDD_P11[47] = 0 ;
					// CH2  Tx Power
					QSFPDD_P11[30] = 0 ;
					QSFPDD_P11[31] = 1 ;
					// Not support datapath do not return flag status
					if(Get_Support_DataPathState(Channel))
					{
						// Tx Power Low Alarm Warning Trigger
						QSFPDD_P11[12] |= 0x04 ;
						QSFPDD_P11[14] |= 0x04 ;
						// Tx Bias Low Alarm Warning Trigger
						QSFPDD_P11[16] |= 0x04 ;
						QSFPDD_P11[18] |= 0x04 ;
					}
				}
				else
				{
					QSFPDD_P11[46] = Bias_Current_CH2 >> 8 ;
					QSFPDD_P11[47] = Bias_Current_CH2 ;
					// CH2  Tx Power
					QSFPDD_P11[30] = TXP_BUFFER >> 8 ;
					QSFPDD_P11[31] = TXP_BUFFER ;
				}
			    break;
		case 3:
				Biase_buffer =LDD_Control_MEMORY_MAP.MP5940_IDAC_MSB_CH3;//(LDD_Control_MEMORY_MAP.MP5940_IDAC_MSB_CH3 << 2 | (LDD_Control_MEMORY_MAP.MP5940_IDAC_LSB_CH3 & 0x03));
        Bias_Current_CH3 = ( (Biase_buffer<<2)+(LDD_Control_MEMORY_MAP.MP5940_IDAC_LSB_CH3 & 0x03))*122;
        TXP_BUFFER = GET_ADC_RowData(TX4MPDANODE);
        
        offset_Buffer_B = (CALIB_MEMORY_MAP.IBias3_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.IBias3_OFFSET_LSB );
        offset_Buffer_P = (CALIB_MEMORY_MAP.TXP3_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.TXP3_OFFSET_LSB );

				Bias_Current_CH3 = CAL_Function( Bias_Current_CH3 , CALIB_MEMORY_MAP.IBias3_SCALEM , CALIB_MEMORY_MAP.IBias3_SCALE1L , offset_Buffer_B ) ;
				TXP_BUFFER = CAL_Function( TXP_BUFFER , CALIB_MEMORY_MAP.TXP3_SCALEM , CALIB_MEMORY_MAP.TXP3_SCALEL , offset_Buffer_P ) ;

				if((QSFPDD_P10[2] & 0x08) || (Tx_Disable_Flag & 0x08))
				{
					QSFPDD_P11[48] = 0 ;
					QSFPDD_P11[49] = 0 ;
					// CH3  Tx Power
					QSFPDD_P11[32] = 0 ;
					QSFPDD_P11[33] = 1 ;
					// Not support datapath do not return flag status
					if(Get_Support_DataPathState(Channel))
					{
						// Tx Power Low Alarm Warning Trigger
						QSFPDD_P11[12] |= 0x08 ;
						QSFPDD_P11[14] |= 0x08 ;
						// Tx Bias Low Alarm Warning Trigger
						QSFPDD_P11[16] |= 0x08 ;
						QSFPDD_P11[18] |= 0x08 ;
					}
				}
				else
				{
					QSFPDD_P11[48] = Bias_Current_CH3 >> 8 ;
					QSFPDD_P11[49] = Bias_Current_CH3 ;
					// CH3  Tx Power
					QSFPDD_P11[32] = TXP_BUFFER >> 8 ;
					QSFPDD_P11[33] = TXP_BUFFER ;
				}
				break;
		default:
				break;
	}

}

void RX_POWER_M_CH0_CH3(uint8_t Rx_CH)
{
	uint16_t RX_POWER_CH0,RX_POWER_CH1,RX_POWER_CH2,RX_POWER_CH3;
	uint16_t RX_POWER_CH4,RX_POWER_CH5,RX_POWER_CH6,RX_POWER_CH7;
    
    int16_t  offset_Buffer_RXP = 0 ;
    uint16_t RXLOS_Assert_Buffer = 0 ;
    uint16_t RXLOS_DeAssert_Buffer = 0 ;

	switch(Rx_CH)
	{
		case 0:
			    RX_POWER_CH0 = TIA_RX1_RX4_RSSI(0);
        
                RXLOS_Assert_Buffer = (CALIB_MEMORY_MAP.Rx0_LOS_Assret_MSB << 8 | CALIB_MEMORY_MAP.Rx0_LOS_Assret_LSB );
                RXLOS_DeAssert_Buffer = (CALIB_MEMORY_MAP.Rx0_LOS_DeAssret_MSB << 8 | CALIB_MEMORY_MAP.Rx0_LOS_DeAssret_LSB );
        
                if(RX_POWER_CH0>400)
                {
                    offset_Buffer_RXP = (CALIB_MEMORY_MAP.RX0_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.RX0_OFFSET_LSB );
                    RX_POWER_CH0 = CAL_Function( RX_POWER_CH0 , CALIB_MEMORY_MAP.RX0_SCALEM , CALIB_MEMORY_MAP.RX0_SCALEL , offset_Buffer_RXP );
			    }
                else
                    RX_POWER_CH0 = 0;
     
                
                if(RX_POWER_CH0>0xF000)
			    	RX_POWER_CH0 = 0;
				// RX RSSI CH 0
				if( RX_POWER_CH0 < RXLOS_Assert_Buffer )
				{
//					if((QSFPDD_A0[3]&0x06)==0x06)
//						QSFPDD_P11[19]=QSFPDD_P11[20]=Rx_LOS_Buffer |= 0x01;
					SHOW_RSSI_CH0 = SHOW_RSSI_Disable ;
				}
					

				if ( RX_POWER_CH0 >= RXLOS_DeAssert_Buffer )
				{
//					QSFPDD_P11[19]=QSFPDD_P11[20]=Rx_LOS_Buffer &= ~0x01;
					SHOW_RSSI_CH0 = SHOW_RSSI_Enable ;
				}
					

				if( SHOW_RSSI_CH0 == SHOW_RSSI_Enable )
				{
					QSFPDD_P11[58] = RX_POWER_CH0 >> 8 ;
					QSFPDD_P11[59] = RX_POWER_CH0 ;
                    
				}
				else
				{
					QSFPDD_P11[58] = 0 ;
					QSFPDD_P11[59] = 1 ;
				}

				break;
		case 1:
			    RX_POWER_CH1 = TIA_RX1_RX4_RSSI(1);
        
                RXLOS_Assert_Buffer = (CALIB_MEMORY_MAP.Rx1_LOS_Assret_MSB << 8 | CALIB_MEMORY_MAP.Rx1_LOS_Assret_LSB );
                RXLOS_DeAssert_Buffer = (CALIB_MEMORY_MAP.Rx1_LOS_DeAssret_MSB << 8 | CALIB_MEMORY_MAP.Rx1_LOS_DeAssret_LSB );     
        
                if(RX_POWER_CH1>400)
                {
                    offset_Buffer_RXP = (CALIB_MEMORY_MAP.RX1_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.RX1_OFFSET_LSB );
                    RX_POWER_CH1 = CAL_Function( RX_POWER_CH1 , CALIB_MEMORY_MAP.RX1_SCALEM , CALIB_MEMORY_MAP.RX1_SCALEL , offset_Buffer_RXP );
			    }
                else
                    RX_POWER_CH1 = 0;
                
                if(RX_POWER_CH1>0xF000)
			    	RX_POWER_CH1 = 0;
				// RX RSSI CH 1
				if( RX_POWER_CH1 < RXLOS_Assert_Buffer )
				{
//					if((QSFPDD_A0[3]&0x06)==0x06)
//					  QSFPDD_P11[19]=QSFPDD_P11[20]=Rx_LOS_Buffer |= 0x02;
					SHOW_RSSI_CH1 = SHOW_RSSI_Disable;
				}
					

				if( RX_POWER_CH1 >= RXLOS_DeAssert_Buffer )
				{
//					QSFPDD_P11[19]=QSFPDD_P11[20]=Rx_LOS_Buffer &= ~0x02;
					SHOW_RSSI_CH1 = SHOW_RSSI_Enable;
				}
					

				if( SHOW_RSSI_CH1 == SHOW_RSSI_Enable )
				{
					QSFPDD_P11[60] = RX_POWER_CH1 >> 8 ;
					QSFPDD_P11[61] = RX_POWER_CH1 ;
				}
				else
				{
					QSFPDD_P11[60] = 0;
					QSFPDD_P11[61] = 1 ;
				}

				break;
		case 2:
				RX_POWER_CH2 = TIA_RX1_RX4_RSSI(2);
                RXLOS_Assert_Buffer = (CALIB_MEMORY_MAP.Rx2_LOS_Assret_MSB << 8 | CALIB_MEMORY_MAP.Rx2_LOS_Assret_LSB );
                RXLOS_DeAssert_Buffer = (CALIB_MEMORY_MAP.Rx2_LOS_DeAssret_MSB << 8 | CALIB_MEMORY_MAP.Rx2_LOS_DeAssret_LSB );                 
                
                if(RX_POWER_CH2>400)
                {
                    offset_Buffer_RXP = (CALIB_MEMORY_MAP.RX2_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.RX2_OFFSET_LSB );
                    RX_POWER_CH2 = CAL_Function( RX_POWER_CH2 , CALIB_MEMORY_MAP.RX2_SCALEM , CALIB_MEMORY_MAP.RX2_SCALEL , offset_Buffer_RXP );
				}
                else
                    RX_POWER_CH2 = 0;                   
                
                if(RX_POWER_CH2>0xF000)
					RX_POWER_CH2 = 0;
				// RX RSSI CH 2
				if( RX_POWER_CH2 < RXLOS_Assert_Buffer )
				{
//					if((QSFPDD_A0[3]&0x06)==0x06)
//						QSFPDD_P11[19]=QSFPDD_P11[20]=Rx_LOS_Buffer |= 0x04;
					SHOW_RSSI_CH2 = SHOW_RSSI_Disable;
				}
					

				if( RX_POWER_CH2 >= RXLOS_DeAssert_Buffer )
				{
//					QSFPDD_P11[19]=QSFPDD_P11[20]=Rx_LOS_Buffer &= ~0x04;
					SHOW_RSSI_CH2 = SHOW_RSSI_Enable;
				}
					

				if(SHOW_RSSI_CH2 == SHOW_RSSI_Enable)
				{
					QSFPDD_P11[62] = RX_POWER_CH2 >> 8 ;
					QSFPDD_P11[63] = RX_POWER_CH2 ;
                    
				}
				else
				{
					QSFPDD_P11[62] = 0;
					QSFPDD_P11[63] = 1 ;
				}

				break;
		case 3:
				RX_POWER_CH3 = TIA_RX1_RX4_RSSI(3);
                
                RXLOS_Assert_Buffer = (CALIB_MEMORY_MAP.Rx3_LOS_Assret_MSB << 8 | CALIB_MEMORY_MAP.Rx3_LOS_Assret_LSB  );
                RXLOS_DeAssert_Buffer = (CALIB_MEMORY_MAP.Rx3_LOS_DeAssret_MSB << 8 | CALIB_MEMORY_MAP.Rx3_LOS_DeAssret_LSB  );  
                
                if(RX_POWER_CH3>400)
                {
                    offset_Buffer_RXP = (CALIB_MEMORY_MAP.RX3_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.RX3_OFFSET_LSB );
                    RX_POWER_CH3 = CAL_Function( RX_POWER_CH3 , CALIB_MEMORY_MAP.RX3_SCALEM , CALIB_MEMORY_MAP.RX3_SCALEL , offset_Buffer_RXP );
				}
                else
                    RX_POWER_CH3 = 0;
                
                if(RX_POWER_CH3>0xF000)
					RX_POWER_CH3 = 0;
				// RX RSSI CH 3
				if( RX_POWER_CH3 < RXLOS_Assert_Buffer )
				{
//					if((QSFPDD_A0[3]&0x06)==0x06)
//					  QSFPDD_P11[19]=QSFPDD_P11[20]=Rx_LOS_Buffer |= 0x08;
					SHOW_RSSI_CH3 = SHOW_RSSI_Disable;
				}
					

				if( RX_POWER_CH3 >= RXLOS_DeAssert_Buffer )
				{
//					QSFPDD_P11[19]=QSFPDD_P11[20]=Rx_LOS_Buffer &= ~0x08;
					SHOW_RSSI_CH3 = SHOW_RSSI_Enable;
				}
					

				if(SHOW_RSSI_CH3==SHOW_RSSI_Enable)
				{
					QSFPDD_P11[64] = RX_POWER_CH3 >> 8 ;
					QSFPDD_P11[65] = RX_POWER_CH3 ;
                    
				}
				else
				{
					QSFPDD_P11[64] = 0;
					QSFPDD_P11[65] = 1 ;
				}
				break;

		default:
			    break;
	}
}

void QSFPDD_DDMI_StateMachine(uint8_t StateMachine )
{
	switch(StateMachine)
	{
		case ADC_SM:
					Temperature_Monitor(GET_GD_Temperature());
					VCC_Monitor( GET_ADC_Value_Data( P3V3_RX_Mon )*2 );
					break;
        
		case RXP_CH0_CH1:
					RX_POWER_M_CH0_CH3(0);
					RX_POWER_M_CH0_CH3(1);
					break;
        
        case RXP_CH2_CH3:
					RX_POWER_M_CH0_CH3(2);
					RX_POWER_M_CH0_CH3(3);
					break;
        
		case TXB_TXP_CH01_SM:
                    BIAS_TxPower_Monitor(0);
                    BIAS_TxPower_Monitor(1);
					break;     
        
		case TXB_TXP_CH23_SM:                   
                    BIAS_TxPower_Monitor(2);
                    BIAS_TxPower_Monitor(3);
					break;    
        
        case TEC_CH12:
                    if(CALIB_MEMORY_MAP.TEC_Enable==1)
                    {
                        TEC_Control(0);
                        TEC_Control(1);
                    }
                    break;
                    
        case TEC_CH34:
                    if(CALIB_MEMORY_MAP.TEC_Enable==1)
                    {
                        TEC_Control(2);
                        TEC_Control(3);
                    }
                    break;   
                    
        case APC_CONTROL:      
            
                    if(CALIB_MEMORY_MAP.LUT_Mode_EN) 
                        Bias_MOD_Updata_Current_Temperature_vary();                           
                    else
                    {
                        if(CALIB_MEMORY_MAP.APC_Enable==1)
                        {
                            APC_Control(0);
                            APC_Control(2);  
                        }
                    }
                            
		case OP_SM:
					if(PowerON_AWEN_flag==1)
						DDMI_AW_Monitor();
					break;
                    
		case TRXLOLLOS:
                    if(DSP_INIT_Flag)
                    {
                        //CMIS40_TXLOS_LOL_AW();
                        CMIS40_RXLOS_LOL_AW();
                    }
			        break;

		case Other_DDMI:
					Get_Module_Power_Monitor();
					break;

		default:
				break;
	}
}

void Initialize_TRx_DDMI()
{
    QSFPDD_DDMI_StateMachine(TXB_TXP_CH01_SM);
    QSFPDD_DDMI_StateMachine(TXB_TXP_CH23_SM);
    QSFPDD_DDMI_StateMachine(RXP_CH0_CH1);
    QSFPDD_DDMI_StateMachine(RXP_CH2_CH3);
}

void MSA_DDMI_Function()
{
	// DDMI Function update
	if( DDMI_DataCount > 50 )
	{
		// Module in Low Power State
		if(( Module_Status == MODULE_LOW_PWR )||( CALIB_MEMORY_MAP.DDMI_DISABLE == 1))
        {
            QSFPDD_DDMI_ONLY_ADC_GET();
            Initialize_TRx_DDMI();
        }
        // Module in Ready State
		else
		{
			// Status Machine Loop
			QSFPDD_DDMI_StateMachine( DDMI_Update_C );
			DDMI_Update_C++;
			//Status Machine flag
			if( DDMI_Update_C >= 11 )
			{
				DDMI_Update_C = 0;
				if(PowerON_AWEN_flag==0)
				{
					AW_DDMI_Count ++ ;
					if(AW_DDMI_Count>2)
						PowerON_AWEN_flag = 1;
				}
			}
		}
		DDMI_DataCount = 0;
	}
	DDMI_DataCount++;
}

void QSFPDD_DDMI_ONLY_ADC_GET()
{
    Temperature_Monitor(GET_GD_Temperature());
    QSFPDD_A0[16] = 0x80;
	QSFPDD_A0[17] = 0x5C;
    //VCC_Monitor( GET_ADC_Value_Data( P3V3_RX_Mon )*2 );
    //Get_module_Power_Monitor();
}