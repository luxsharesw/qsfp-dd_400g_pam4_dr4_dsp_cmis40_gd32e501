#include "gd32e501.h"
#include "core_cm33.h"
#include "systick.h"
#include <stdint.h>
#include "CMIS_MSA.h"
#include "Calibration_Struct.h"
#include "Calibration_Struct_2.h"
#include "GD_FlahMap.h"
#include "GD32E501_M_I2C1_PB3_PB4.h"
#include "GD32E501_GPIO_Customize_Define.h"
//--------------------------------------------------------//
// Chip ICs .h                                            //
//--------------------------------------------------------//
#include "DSP.h"
#include "DSP_Line_Side.h"
#include "DSP_System_Side.h"
#include "TEC_APC_Control.h"
#include "LDD_Control.h"
#include "TIA_Control.h"
//------------------------------------------------------------------------------------------//
// State Machine Flags
//------------------------------------------------------------------------------------------//
// Update from CALIB_MEMORY_MAP.QDD_SoftInitM_EN, Use on Modeule_MGMT_INIT()
uint8_t SoftWare_Init_flag = 0;
uint8_t Line_Side_Squelch_Flag=0;
//PAM4 or NRZ Mode Select
uint8_t Signal_Status=PAM4_to_PAM4;
uint8_t Module_Status = 0;
uint8_t DSP_MODE_SET = 0x00;
uint8_t Bef_DSP_MODE_SET=0xFF;
// For Control DSP Bandwidth
uint8_t Bandwidth_Flag=0;
uint8_t Bandwidth_Count = 0 ;
uint8_t DDMI_Trigger_Flag=0;
//-----------------------------------------------------------------------------------------------------//
// Functions
//-----------------------------------------------------------------------------------------------------//
void Get_Start_AppSelCode()
{
    uint8_t i;
	// Get Start Mode From AppSelCode1
	// 1 Port 8 Lanes Host to 4 Lanes Media
	if(QSFPDD_A0[89]==0x01)
	{
		for(i=0;i<8;i++)
		{
			QSFPDD_P10[17+i]=0x10;
		}
	}
	// 4Port 2 Lanes Host to 1 Lanes Media
	else if(QSFPDD_A0[89]==0x55)
	{
		for(i=0;i<8;i++)
		{
			// Lane0-1
			if(i<2)
				QSFPDD_P10[17+i]=0x10;
			// Lane2-3
			else if(i<4)
				QSFPDD_P10[17+i]=0x14;
			// Lane4-5
			else if(i<6)
				QSFPDD_P10[17+i]=0x18;
			// Lane6-7
			else if(i<8)
				QSFPDD_P10[17+i]=0x1C;
		}
	}
    // AppSelCode1 not define use default
    else
    {
        DSP_MODE_SET = Mode1_CHIP_MODE_2X53G_KP4PAM_TO_1X106G_KP4PAM ;
		// PAM4 MODE
        Signal_Status = PAM4_to_PAM4;
    }
}

void Get_Staged_Control_Set_0()
{
    uint8_t i;
	// Get Start Mode From AppSelCode1
	// 1 Port 8 Lanes
	if(QSFPDD_A0[89]==0x01)
	{
		for(i=0;i<8;i++)
			QSFPDD_P10[17+i]=0x10;
	}
	// 2 Port 4Lanes
	else if(QSFPDD_A0[89]==0x11)
	{
		for(i=0;i<8;i++)
		{
			// Lane0-3
			if(i<4)
				QSFPDD_P10[17+i]=0x10;
			// Lane4-7
			else
				QSFPDD_P10[17+i]=0x18;
		}
	}
	// 4 Port 2Lanes
	else if(QSFPDD_A0[89]==0x55)
	{
		for(i=0;i<8;i++)
		{
			// Lane0-1
			if(i<=2)
				QSFPDD_P10[17+i]=0x10;
			// Lane2-3
			else if(i<=4)
				QSFPDD_P10[17+i]=0x14;
			// Lane4-5
			else if(i<=6)
				QSFPDD_P10[17+i]=0x18;
			// Lane6-7
			else if(i<=8)
				QSFPDD_P10[17+i]=0x1C;
		}
	}
	// 8Port 1Lane
	else if(QSFPDD_A0[89]==0xFF)
	{
		for(i=0;i<8;i++)
			QSFPDD_P10[17+i]=(0x10+(i*2));
	}
    // AppSelCode1 not define use default
    else
    {
        for(i=0;i<8;i++)
            QSFPDD_P10[17+i]=0x10;
    }
    // Update Active AppSelCodep[]
	for(i=0;i<8;i++)
        Active_AppSelCode[i]=QSFPDD_P10[17+i];
}
uint8_t Get_Staged_Control_Set_1()
{
    uint8_t i,Supported=1;
	// Get Start Mode From AppSelCode9
	// 1 Port 8 Lanes
	if(QSFPDD_P1[98]==0x01)
	{
		for(i=0;i<8;i++)
			QSFPDD_P10[52+i]=0x90;
	}
	// 2 Port 4Lanes
	else if(QSFPDD_P1[98]==0x11)
	{
		for(i=0;i<8;i++)
		{
			// Lane0-3
			if(i<4)
				QSFPDD_P10[52+i]=0x90;
			// Lane4-7
			else
				QSFPDD_P10[52+i]=0x98;
		}
	}
	// 4 Port 2Lanes
	else if(QSFPDD_P1[98]==0x55)
	{
		for(i=0;i<8;i++)
		{
			// Lane0-1
			if(i<=2)
				QSFPDD_P10[52+i]=0x90;
			// Lane2-3
			else if(i<=4)
				QSFPDD_P10[52+i]=0x94;
			// Lane4-5
			else if(i<=6)
				QSFPDD_P10[52+i]=0x98;
			// Lane6-7
			else if(i<=8)
				QSFPDD_P10[52+i]=0x9C;
		}
	}
	// 8Port 1Lane
	else if(QSFPDD_P1[98]==0xFF)
	{
		for(i=0;i<8;i++)
			QSFPDD_P10[52+i]=(0x90+(i*2));
	}
    // Not Define
    else if(QSFPDD_P1[98]==0x00)
    {
        for(i=0;i<8;i++)
            QSFPDD_P10[52+i]=0x00;
        Supported=0;
    }
    return Supported;
}
void Copy_Staged_TRx_Controls()
{
    // Move Staged Control Set 0 TRx Controls to Staged Control Set 1
    // TRx Controls
    for(uint8_t i=0;i<21;i++)
    {
        QSFPDD_P10[60+i]=QSFPDD_P10[25+i];
    }
}

void PowerUp_Device_Control()
{
    uint8_t TEC_count = 0;
    LPMODE_POWER_ON();
    // LDD Initiialize  
    MP5490_Write_Control_Data();
    Acacia_LDD_Write_ALL();
    DAC_TO_TOPS_SETTING();
    // TIA Initilaize     
    Acacia_TIA_Write_ALL();
    
    for(TEC_count=0;TEC_count<50;TEC_count++)
    {
        TEC_Control(0);
        TEC_Control(1);
        TEC_Control(2);
        TEC_Control(3);
    }
}
//-----------------------------------------------------------------------------------------------------//
// CMIS State Machine
//-----------------------------------------------------------------------------------------------------//
//------------------------------------------------------//
// Modeule_MGMT_INIT
//------------------------------------------------------//
void Modeule_MGMT_INIT()
{
	Get_Staged_Control_Set_0();
	if(Get_Staged_Control_Set_1())
	{
		Copy_Staged_TRx_Controls();
	}
	
	Module_Status=MODULE_LOW_PWR;
	Module_State(Module_Status);
	IntL_G_Low();
	MSA_DataPathState(DataPathDeactivated);
//    uint8_t i;
//	// Enable DSP Low power mode
//	LPMODE_DSP_High();

//	// Start HW INIT or SW INIT by LowPwrS() State
//	if( CALIB_MEMORY_MAP.QDD_SoftInitM_EN == 0x01 )
//		SoftWare_Init_flag = 0 ;
//    //Force to Hardware Initialization
//	else
//		SoftWare_Init_flag = 1 ;

//    // HW INIT and SW INIT Bit4 Force LowPwr = 0 , Bit6 LowPwr = 1
//	QSFPDD_A0[26] = 0x40 ;
//	// DataPathDeinit=00h
//	QSFPDD_P10[0] = 0x00;
//    Get_Start_AppSelCode();
//	Module_Status = MODULE_LOW_PWR;
//    Module_State(Module_Status);
//	IntL_G_Low();
//	MSA_DataPathState(DataPathDeactivated);
    // Trigger to show config status
	QSFPDD_P10[15]=0xFF;
}

//------------------------------------------------------//
// QSFPDD_MSA_StateMachine
//------------------------------------------------------//
void QSFPDD_MSA_StateMachine()
{
	uint8_t i = 0;

	switch( Module_Status )
	{
		// Transient State
		case MODULE_MGMT_INIT:
			 Modeule_MGMT_INIT();
			 break;

		// Steady State
		case MODULE_LOW_PWR:

			 // SW and HW Init Mode can use LowPwrS to check it power up or not
			 if((LowPwrS()==0)||(SoftWare_Init_flag))
			 {
				 Module_Status = MODULE_PWR_UP ;
				 Module_State(Module_Status);
				 // For DSP_Tx_Los_Check check again to change data path state by tx lol
				 Tx_Los_Enable_Flag=1;
                 // Trigger to config dsp bandwidth
                 Bef_Rx_LOL_LATCH_R=0xFF;
			 }
			 // ResetS T or F Function
			 ResetS();
			 // MSA Host Request function
			 Apply_DataPathInit_Immediate(MODULE_LOW_PWR);
			 if(DDMI_Trigger_Flag)
			 {
				 Initialize_TRx_DDMI();
				 for(i=0;i<18;i++)
					Clear_Flag(i+7);
			 }
			 break;

		// Transient State
		case MODULE_PWR_UP:

//             PowerUp_Device_Control();
             // DSP to High Power Mode
             LPMODE_DSP_Low();
             delay_1ms(200);
		   if(Bef_DSP_MODE_SET !=DSP_MODE_SET)
			 {
				 DSP_MODE_SET=Mode1_CHIP_MODE_2X53G_KP4PAM_TO_1X106G_KP4PAM;
				 DSP_Init();
         Bef_DSP_MODE_SET=DSP_MODE_SET;
			 }
//			 MSA_DataPathState(DataPathInitialized);
//       LDD_Trun_on();
			 if(LowPwrS())
			 {
				 Module_Status = MODULE_PWR_DN ;
				 Module_State(Module_Status);
			 }
			 else
			 {
				 Module_Status = MODULE_READY ;
				 Module_State(Module_Status);
				 IntL_G_Low();
			 }

			 break;

		// Transient State
		case MODULE_PWR_DN:
             // Enable DSP Low power mode
             LPMODE_DSP_High();
             // Disable all of devices
			 LPMDOE_POWER_OFF();
			 // DDMI TxP and Tx bias to show no value
			 Tx_Disable_Flag=0x0F;
			 Module_Status = MODULE_LOW_PWR ;
			 Module_State(Module_Status);
			 IntL_G_Low();
			 // Clear all off interrupt flags exclude temp and vcc
			 for(i=0;i<18;i++)
				 Clear_Flag(i+7);
             // Disable TRx CDR LOS LOL
             DSP_INIT_Flag=0;
//             // Rx output will squelch after power up
//             Bef_Rx_Los=0xFF;
			 break;

		// Steady State
		case MODULE_READY:
			// LowPwrS for Arista to force to low pow mode
			if((LowPwrExS())||(LowPwrS()))
			{
				 Module_Status = MODULE_PWR_DN ;
				 Module_State(Module_Status);
				 // DataPathDeinitS will triggered, next stop state is low pow
//				 MSA_DataPathState(DataPathDeactivated);
			}
			else
			{
                // CMIS Controls
                CMIS_PAGE10h_Control();
                CMIS_PAGE13h_Diagnostics_Function();
                CMIS_PAGE14h_Diagnostics_Function();
                // ResetS T or F Function
                ResetS();
                // MSA Host Request function
                Apply_DataPathInit_Immediate(MODULE_READY);
			}
			break;

		case MODULE_FAULT:
			 break;
	}
	MSA_DDMI_Function();
}