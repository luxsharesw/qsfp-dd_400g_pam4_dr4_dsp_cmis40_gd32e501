/*
 * Calibration_Struct.h
 *
 *  Created on: 2017/3/8
 *      Author: Ivan_Lin
 */

#ifndef CALIBRATION_STRUCT_H_
#define CALIBRATION_STRUCT_H_

struct CALIB_MEMORY
{
    uint8_t  VCC_SCALEM;           //Address = 0x80
    uint8_t  VCC_SCALEL;           //Address = 0x81
    uint8_t  VCC_OFFSET_MSB;       //Address = 0x82
    uint8_t  VCC_OFFSET_LSB;       //Address = 0x83

    uint8_t  TEMP_SCALE1M;         //Address = 0x84
    uint8_t  TEMP_SCALE1L;         //Address = 0x85
    uint8_t  TEMP_OFFSET1;         //Address = 0x86

    uint8_t  IBias0_SCALEM;        //Address = 0x87
    uint8_t  IBias0_SCALE1L;       //Address = 0x88
    uint8_t  IBias0_OFFSET_MSB;    //Address = 0x89
    uint8_t  IBias0_OFFSET_LSB;    //Address = 0x8A

    uint8_t  IBias1_SCALEM;        //Address = 0x8B
    uint8_t  IBias1_SCALE1L;       //Address = 0x8C
    uint8_t  IBias1_OFFSET_MSB;    //Address = 0x8D
    uint8_t  IBias1_OFFSET_LSB;    //Address = 0x8E

    uint8_t  IBias2_SCALEM;        //Address = 0x8F
    uint8_t  IBias2_SCALE1L;       //Address = 0x90
    uint8_t  IBias2_OFFSET_MSB;    //Address = 0x91
    uint8_t  IBias2_OFFSET_LSB;    //Address = 0x92

    uint8_t  IBias3_SCALEM;        //Address = 0x93
    uint8_t  IBias3_SCALE1L;       //Address = 0x94
    uint8_t  IBias3_OFFSET_MSB;    //Address = 0x95
    uint8_t  IBias3_OFFSET_LSB;    //Address = 0x96

    uint8_t  TXP0_SCALEM;          //Address = 0x97
    uint8_t  TXP0_SCALEL;          //Address = 0x98
    uint8_t  TXP0_OFFSET_MSB;      //Address = 0x99
    uint8_t  TXP0_OFFSET_LSB;      //Address = 0x9A
    
    uint8_t  TXP1_SCALEM;          //Address = 0x9B
    uint8_t  TXP1_SCALEL;          //Address = 0x9C
    uint8_t  TXP1_OFFSET_MSB;      //Address = 0x9D
    uint8_t  TXP1_OFFSET_LSB;      //Address = 0x9E
    
    uint8_t  TXP2_SCALEM;          //Address = 0x9F
    uint8_t  TXP2_SCALEL;          //Address = 0xA0
    uint8_t  TXP2_OFFSET_MSB;      //Address = 0xA1
    uint8_t  TXP2_OFFSET_LSB;      //Address = 0xA2

    uint8_t  TXP3_SCALEM;          //Address = 0xA3
    uint8_t  TXP3_SCALEL;          //Address = 0xA4
    uint8_t  TXP3_OFFSET_MSB;      //Address = 0xA5
    uint8_t  TXP3_OFFSET_LSB;      //Address = 0xA6

    uint8_t  RX0_SCALEM;           //Address = 0xA7
    uint8_t  RX0_SCALEL;           //Address = 0xA8
    uint8_t  RX0_OFFSET_MSB;       //Address = 0xA9
    uint8_t  RX0_OFFSET_LSB;       //Address = 0xAA

    uint8_t  RX1_SCALEM;           //Address = 0xAB
    uint8_t  RX1_SCALEL;           //Address = 0xAC
    uint8_t  RX1_OFFSET_MSB;       //Address = 0xAD
    uint8_t  RX1_OFFSET_LSB;       //Address = 0xAE
    
    uint8_t  RX2_SCALEM;           //Address = 0xAF
    uint8_t  RX2_SCALEL;           //Address = 0xB0
    uint8_t  RX2_OFFSET_MSB;       //Address = 0xB1
    uint8_t  RX2_OFFSET_LSB;       //Address = 0xB2

    uint8_t  RX3_SCALEM;           //Address = 0xB3
    uint8_t  RX3_SCALEL;           //Address = 0xB4
    uint8_t  RX3_OFFSET_MSB;       //Address = 0xB5
    uint8_t  RX3_OFFSET_LSB;       //Address = 0xB6

    uint8_t  Rx0_LOS_Assret_MSB;   //Address = 0xB7
    uint8_t  Rx0_LOS_Assret_LSB;   //Address = 0xB8    
    uint8_t  Rx0_LOS_DeAssret_MSB; //Address = 0xB9
    uint8_t  Rx0_LOS_DeAssret_LSB; //Address = 0xBA                
    uint8_t  Rx1_LOS_Assret_MSB;   //Address = 0xBB
    uint8_t  Rx1_LOS_Assret_LSB;   //Address = 0xBC     
    uint8_t  Rx1_LOS_DeAssret_MSB; //Address = 0xBD
    uint8_t  Rx1_LOS_DeAssret_LSB; //Address = 0xBE        
    uint8_t  Rx2_LOS_Assret_MSB;   //Address = 0xBF
    uint8_t  Rx2_LOS_Assret_LSB;   //Address = 0xC0    
    uint8_t  Rx2_LOS_DeAssret_MSB; //Address = 0xC1
    uint8_t  Rx2_LOS_DeAssret_LSB; //Address = 0xC2    
    uint8_t  Rx3_LOS_Assret_MSB;  //Address = 0xC3 
    uint8_t  Rx3_LOS_Assret_LSB;  //Address = 0xC4    
    uint8_t  Rx3_LOS_DeAssret_MSB;//Address = 0xC5
    uint8_t  Rx3_LOS_DeAssret_LSB;//Address = 0xC6
        
    uint8_t  LUT_High_Temp;        //Address = 0xC7
    uint8_t  LUT_Low_Temp;         //Address = 0xC8
    uint8_t  TEC_Enable;           //Address = 0xC9
    uint8_t  APC_Enable;           //Address = 0xCA
    uint8_t  SOF_MODSEL;           //Address = 0xCB
    uint8_t  LUT_Mode_EN;          //Address = 0xCC
    uint8_t  QDD_SoftInitM_EN;     //Address = 0xCD

    uint8_t  SKIP_DSP_Tx_Los_Check;//Address = 0xCE
    uint8_t  Reserved_2;           //Address = 0xCF

    uint8_t  DDMI_DISABLE;         //Address = 0xD0

    uint8_t  R12_Swining_Level;    //Address = 0xD1
    uint8_t  R34_Swining_Level;    //Address = 0xD2
    uint8_t  R56_Swining_Level;    //Address = 0xD3
    uint8_t  R78_Swining_Level;    //Address = 0xD4

    uint8_t  R12_Swining_Level_2;  //Address = 0xD5
    uint8_t  R34_Swining_Level_2;  //Address = 0xD6
    uint8_t  R56_Swining_Level_2;  //Address = 0xD7
    uint8_t  R78_Swining_Level_2;  //Address = 0xD8 

    uint8_t  R12_Pre_Level;        //Address = 0xD9
    uint8_t  R34_Pre_Level;        //Address = 0xDA
    uint8_t  R56_Pre_Level;        //Address = 0xDB
    uint8_t  R78_Pre_Level;        //Address = 0xDC

    uint8_t  R12_Post_Level;       //Address = 0xDD
    uint8_t  R34_Post_Level;       //Address = 0xDE
    uint8_t  R56_Post_Level;       //Address = 0xDF
    uint8_t  R78_Post_Level;       //Address = 0xE0

    uint8_t  MSA_Optional_Flag;    //Address = 0xE1
    uint8_t  COB_OR_Nomal_Mode;    //Address = 0xE2
    
    uint8_t  CAL_Buffer[4];        //Address = 0xE3 - 0xE6
    uint8_t  Temp_Index;           //Address = 0xE7    
    uint8_t  CAL_Buffer_2[13];     //Address = 0xE8 - 0xF4
    
    uint8_t  CheckSum_EN;          //Address = 0xF5
    uint8_t  Error_Flag;           //Address = 0xF6
    uint8_t  DEBUG_TEMP1;          //Address = 0xF7
    uint16_t CHECKSUM_V;           //Address = 0xF8 - 0xF9
    uint16_t FW_VERSION;           //Address = 0xFA - 0xFB
    uint16_t DEBUG_TEMP2;          //Address = 0xFC - 0xFD
    uint16_t DEBUG_TEMP3;          //Address = 0xFE - 0xFF
};

extern struct CALIB_MEMORY CALIB_MEMORY_MAP;

#endif /* CALIBRATION_STRUCT_H_ */
