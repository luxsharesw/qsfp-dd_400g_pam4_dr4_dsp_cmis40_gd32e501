

void Master_I2C1_ByteWrite_PB34( uint8_t SADR , uint8_t Mem_REG , uint8_t Write_DATA);
uint8_t Master_I2C1_ByteREAD_PB34(uint8_t SADR ,uint8_t Mem_REG );
void Delay_I2C1(uint32_t i);
uint8_t Master_I2C1_TwoByte_PB34(uint8_t driver_Addr, uint8_t start_Addr , uint8_t DATA_0 ,uint8_t DATA_1);

uint16_t MI2C1_WordADR_R_PB34(uint8_t SADR ,uint16_t MemADR );
uint8_t MI2C1_WordADR_W_PB34(uint8_t SADR, uint16_t MemADR , uint16_t DATA_Value);

uint8_t MI2C1_WordADR_HLBW_PB34(uint8_t SADR, uint16_t MemADR , uint8_t MSB ,uint8_t LSB);
