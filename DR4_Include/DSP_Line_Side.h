/*
 * BRCM_DSP58281_Optional.h
 *
 *  Created on: 2019�~8��28��
 *      Author: Ivan_Lin
 */
#ifndef INC_DSP_LINE_SIDE_H_
#define INC_DSP_LINE_SIDE_H_
#define LW_NRZ_MODE   0
#define LW_EPAM2_MODE 1
#define LW_PAM4_MODE  2
#define LW_EPAM4_mode 3
#define COMMAND_ID_SET_LANE_CTRL_INFO 15
#define DIR_INGRESS  0
#define LANE_SUSPEND 7
#define LANE_RESUME  8
#define FLT_MAX      3.402823466e+38F        /* max value */

extern uint16_t DSP_LineSide_SNR;
extern uint16_t DSP_LineSide_LTP;
//------------------------------------------------//
//---------------- API----------------------------//
//------------------------------------------------//
void Line_Side_ALL_CH1_CH4_Control_P86();
uint8_t DSP_Line_Side_LOS();
uint8_t DSP_Line_Side_LOL();
void DSP_Line_Side_Digital_Loopback_SET(uint8_t Lane_CH,uint8_t Enable);
void DSP_Line_Side_Remote_Loopback_SET(uint8_t Lane_CH,uint8_t Enable);
void DSP_Line_Side_PRBS_SET(uint8_t Pattern ,uint8_t Lane_CH ,uint8_t Enable);
void DSP_LineSide_SNR_LTP_GET(uint8_t Lane_CH);
uint16_t DEC_EQUAL_TO_HEX(uint16_t Data_Value);
void DSP_LineSide_TRX_Squelch_SET(uint8_t Lane_CH ,uint8_t TRX_Side,uint8_t Enable);
void DSP_LineSide_TRX_Polarity_SET(uint8_t Lane_CH , uint8_t TRX_Side_SEL);
void DSP_LineSide_TX_Squelch_SET(uint8_t Lane_CH ,uint8_t Enable);
void DSP_LineSide_RX_Squelch_SET(uint8_t Lane_CH ,uint8_t Enable);
#endif  /*INC_DSP_LINE_SIDE_H_*/
