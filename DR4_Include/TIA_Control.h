
void Acacia_TIA_Write_ALL();
void Acacia_TIA_Read_ALL();
void Acaaia_TIA_Direct_Control();
uint16_t TIA_RX1_RX4_RSSI(uint8_t Channel);
void Acacia_TIA_GetSP();

struct TIA_Control_MEMORY
{
    // Acacia Tia Control 
    uint8_t  RSVD0;                        //        SRAM Address 80
    uint8_t  RSVD1;                        //        SRAM Address 81
    uint8_t  GLBL_CHIPID_TIA1_MSB;         // 0x4000 SRAM Address 82
    uint8_t  GLBL_CHIPID_TIA1_LSB;         // 0x4000 SRAM Address 83    
    uint8_t  GLBL_BIASGEN_TIA1_CTRL_MSB;   // 0x400C SRAM Address 84
    uint8_t  GLBL_BIASGEN_TIA1_CTRL_LSB;   // 0x400C SRAM Address 85
    uint8_t  GLBL_CHIPID_TIA2_MSB;         // 0x4000 SRAM Address 86
    uint8_t  GLBL_CHIPID_TIA2_LSB;         // 0x4000 SRAM Address 87    
    uint8_t  GLBL_BIASGEN_TIA2_CTRL_MSB;   // 0x400C SRAM Address 88
    uint8_t  GLBL_BIASGEN_TIA2_CTRL_LSB;   // 0x400C SRAM Address 89    
    uint8_t  GLBL_Data[6];                 //        SRAM Address 8A-8F
    //TIA1 CH1
    uint8_t  XI_IBIAS10_VGA_CH1_MSB;       // 0x0000 SRAM Address 90
    uint8_t  XI_IBIAS10_VGA_CH1_LSB;       // 0x0000 SRAM Address 91
    uint8_t  XI_AGC1_CH1_MSB;              // 0x0002 SRAM Address 92
    uint8_t  XI_AGC1_CH1_LSB;              // 0x0002 SRAM Address 93
    uint8_t  XI_IBIAS32_DRIVER_CH1_MSB;    // 0x0006 SRAM Address 94
    uint8_t  XI_IBIAS32_DRIVER_CH1_LSB;    // 0x0006 SRAM Address 95
    uint8_t  XI_RF1_TIA_CH1_MSB;           // 0x000A SRAM Address 96
    uint8_t  XI_RF1_TIA_CH1_LSB;           // 0x000A SRAM Address 97
    uint8_t  XI_RF3_PREDRV_CH1_MSB;        // 0x000E SRAM Address 98
    uint8_t  XI_RF3_PREDRV_CH1_LSB;        // 0x000E SRAM Address 99
    uint8_t  XI_RF4_DRV_CH1_MSB;           // 0x0010 SRAM Address 9A
    uint8_t  XI_RF4_DRV_CH1_LSB;           // 0x0010 SRAM Address 9B
    uint8_t  XI_RF6_CH1_MSB;               // 0x0014 SRAM Address 9C
    uint8_t  XI_RF6_CH1_LSB;               // 0x0014 SRAM Address 9D
    uint8_t  DataBCH1_MSB;                 //        SRAM Address 9E
    uint8_t  DataBCH1_LSB;                 //        SRAM Address 9F
    //TIA1 CH2    
    uint8_t  XI_IBIAS10_VGA_CH2_MSB;       // 0x1000 SRAM Address A0
    uint8_t  XI_IBIAS10_VGA_CH2_LSB;       // 0x1000 SRAM Address A1
    uint8_t  XI_AGC1_CH2_MSB;              // 0x1002 SRAM Address A2
    uint8_t  XI_AGC1_CH2_LSB;              // 0x1002 SRAM Address A3
    uint8_t  XI_IBIAS32_DRIVER_CH2_MSB;    // 0x1006 SRAM Address A4
    uint8_t  XI_IBIAS32_DRIVER_CH2_LSB;    // 0x1006 SRAM Address A5
    uint8_t  XI_RF1_TIA_CH2_MSB;           // 0x100A SRAM Address A6
    uint8_t  XI_RF1_TIA_CH2_LSB;           // 0x100A SRAM Address A7
    uint8_t  XI_RF3_PREDRV_CH2_MSB;        // 0x100E SRAM Address A8
    uint8_t  XI_RF3_PREDRV_CH2_LSB;        // 0x100E SRAM Address A9
    uint8_t  XI_RF4_DRV_CH2_MSB;           // 0x1010 SRAM Address AA
    uint8_t  XI_RF4_DRV_CH2_LSB;           // 0x1010 SRAM Address AB
    uint8_t  XI_RF6_CH2_MSB;               // 0x1014 SRAM Address AC
    uint8_t  XI_RF6_CH2_LSB;               // 0x1014 SRAM Address AD
    uint8_t  DataBCH2_MSB;                 //        SRAM Address AE
    uint8_t  DataBCH2_LSB;                 //        SRAM Address AF
    //TIA2 CH3    
    uint8_t  XI_IBIAS10_VGA_CH3_MSB;       // 0x2000 SRAM Address B0
    uint8_t  XI_IBIAS10_VGA_CH3_LSB;       // 0x2000 SRAM Address B1
    uint8_t  XI_AGC1_CH3_MSB;              // 0x2002 SRAM Address B2
    uint8_t  XI_AGC1_CH3_LSB;              // 0x2002 SRAM Address B3
    uint8_t  XI_IBIAS32_DRIVER_CH3_MSB;    // 0x2006 SRAM Address B4
    uint8_t  XI_IBIAS32_DRIVER_CH3_LSB;    // 0x2006 SRAM Address B5
    uint8_t  XI_RF1_TIA_CH3_MSB;           // 0x200A SRAM Address B6
    uint8_t  XI_RF1_TIA_CH3_LSB;           // 0x200A SRAM Address B7
    uint8_t  XI_RF3_PREDRV_CH3_MSB;        // 0x200E SRAM Address B8
    uint8_t  XI_RF3_PREDRV_CH3_LSB;        // 0x200E SRAM Address B9
    uint8_t  XI_RF4_DRV_CH3_MSB;           // 0x2010 SRAM Address BA
    uint8_t  XI_RF4_DRV_CH3_LSB;           // 0x2010 SRAM Address BB
    uint8_t  XI_RF6_CH3_MSB;               // 0x2014 SRAM Address BC
    uint8_t  XI_RF6_CH3_LSB;               // 0x2014 SRAM Address BD
    uint8_t  DataBCH3_MSB;                 //        SRAM Address BE
    uint8_t  DataBCH3_LSB;                 //        SRAM Address BF
    //TIA2 CH4     
    uint8_t  XI_IBIAS10_VGA_CH4_MSB;       // 0x3000 SRAM Address C0
    uint8_t  XI_IBIAS10_VGA_CH4_LSB;       // 0x3000 SRAM Address C1
    uint8_t  XI_AGC1_CH4_MSB;              // 0x3002 SRAM Address C2
    uint8_t  XI_AGC1_CH4_LSB;              // 0x3002 SRAM Address C3
    uint8_t  XI_IBIAS32_DRIVER_CH4_MSB;    // 0x3006 SRAM Address C4
    uint8_t  XI_IBIAS32_DRIVER_CH4_LSB;    // 0x3006 SRAM Address C5
    uint8_t  XI_RF1_TIA_CH4_MSB;           // 0x300A SRAM Address C6
    uint8_t  XI_RF1_TIA_CH4_LSB;           // 0x300A SRAM Address C7
    uint8_t  XI_RF3_PREDRV_CH4_MSB;        // 0x300E SRAM Address C8
    uint8_t  XI_RF3_PREDRV_CH4_LSB;        // 0x300E SRAM Address C9
    uint8_t  XI_RF4_DRV_CH4_MSB;           // 0x3010 SRAM Address CA
    uint8_t  XI_RF4_DRV_CH4_LSB;           // 0x3010 SRAM Address CB
    uint8_t  XI_RF6_CH4_MSB;               // 0x3014 SRAM Address CC
    uint8_t  XI_RF6_CH4_LSB;               // 0x3014 SRAM Address CD
    uint8_t  DataBCH4_MSB;                 //        SRAM Address CE
    uint8_t  DataBCH4_LSB;                 //        SRAM Address CF
    
    uint8_t  TIA_VLDO_TIA_VREF_MSB_CH1;    // 0x000C SRAM Address D0
    uint8_t  TIA_VLDO_TIA_VREF_LSB_CH1;    // 0x000C SRAM Address D1
    uint8_t  TIA_VLDO_TIA_VREF_MSB_CH2;    // 0x000C SRAM Address D2
    uint8_t  TIA_VLDO_TIA_VREF_LSB_CH2;    // 0x000C SRAM Address D3
    uint8_t  TIA_VLDO_TIA_VREF_MSB_CH3;    // 0x000C SRAM Address D4
    uint8_t  TIA_VLDO_TIA_VREF_LSB_CH3;    // 0x000C SRAM Address D5
    uint8_t  TIA_VLDO_TIA_VREF_MSB_CH4;    // 0x000C SRAM Address D6
    uint8_t  TIA_VLDO_TIA_VREF_LSB_CH4;    // 0x000C SRAM Address D7
    //----------------------------------------------------------------//
    //Sense Point Get adc voltage
    //---------------------------------------------------------------//
    uint8_t  TIA_SP_PDO_REF_CH1;           // SP 1   SRAM Address D8
    uint8_t  TIA_SP_PDO_P_CH1;             // SP 2   SRAM Address D9
    uint8_t  TIA_SP_PDO_N_CH1;             // SP 3   SRAM Address DA
    uint8_t  TIA_SP_OA_REF_CH1;            // SP 4   SRAM Address DB
    uint8_t  TIA_SP_GCAMP_CH1;             // SP 5   SRAM Address DC
    uint8_t  TIA_SP_PKD_CH1;               // SP 6   SRAM Address DD  
    
    uint8_t  TIA_SP_PDO_REF_CH2;           // SP 1   SRAM Address DE
    uint8_t  TIA_SP_PDO_P_CH2;             // SP 2   SRAM Address DF
    uint8_t  TIA_SP_PDO_N_CH2;             // SP 3   SRAM Address E0
    uint8_t  TIA_SP_OA_REF_CH2;            // SP 4   SRAM Address E1
    uint8_t  TIA_SP_GCAMP_CH2;             // SP 5   SRAM Address E2
    uint8_t  TIA_SP_PKD_CH2;               // SP 6   SRAM Address E3   
    uint8_t  TIA_SP_PDO_REF_CH3;           // SP 1   SRAM Address E4
    uint8_t  TIA_SP_PDO_P_CH3;             // SP 2   SRAM Address E5
    uint8_t  TIA_SP_PDO_N_CH3;             // SP 3   SRAM Address E6
    uint8_t  TIA_SP_OA_REF_CH3;            // SP 4   SRAM Address E7
    uint8_t  TIA_SP_GCAMP_CH3;             // SP 5   SRAM Address E8
    uint8_t  TIA_SP_PKD_CH3;               // SP 6   SRAM Address E9   
    uint8_t  TIA_SP_PDO_REF_CH4;           // SP 1   SRAM Address EA
    uint8_t  TIA_SP_PDO_P_CH4;             // SP 2   SRAM Address EB
    uint8_t  TIA_SP_PDO_N_CH4;             // SP 3   SRAM Address EC
    uint8_t  TIA_SP_OA_REF_CH4;            // SP 4   SRAM Address ED
    uint8_t  TIA_SP_GCAMP_CH4;             // SP 5   SRAM Address EE
    uint8_t  TIA_SP_PKD_CH4;               // SP 6   SRAM Address EF
      
    uint16_t Direct_Satrt_T12;             //        SRAM Address F0 - F1
    uint16_t Driect_MEMADR_T12;            //        SRAM Address F2 - F3
    uint16_t Driect_WBUFF_T12;             //        SRAM Address F4 - F5
    uint16_t Driect_RBUFF_T12;             //        SRAM Address F6 - F7
    uint16_t Direct_Satrt_T34;             //        SRAM Address F8 - F9
    uint16_t Driect_MEMADR_T34;            //        SRAM Address FA - FB
    uint16_t Driect_WBUFF_T34;             //        SRAM Address FC - FD
    uint16_t Driect_RBUFF_T34;             //        SRAM Address FE - FF
};

extern struct TIA_Control_MEMORY TIA_Control_MEMORY_MAP;
