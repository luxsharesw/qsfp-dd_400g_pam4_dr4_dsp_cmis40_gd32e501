
#include "gd32e501.h"

#define I2C0_SLAVE_ADDRESS7 0xA0

/* function declarations */
/* handle I2C0 event interrupt request */
void I2C0_EventIRQ_Handler(void);
/* handle I2C0 error interrupt request */
void I2C0_ErrorIRQ_Handler(void);

