
#define MATA_03819_AD   0x20

void MATA03819_SET_PAGE( uint8_t PAGE );
void MATA03819_P8A_Write();
void MATA03819_Write_ALL();
void MATA03819_P8A_READ();
void MATA03819_READ_ALL();
void MATA03819_Init();
uint16_t GET_RSSI_CH0_CH3(uint8_t RSSI_CH);

struct MATA03819_TIA_MEMORY
{
	uint8_t CHIP_ID ;                 // reg. address 00  RW  SRAM Address 80
	uint8_t REVID ;                   // reg. address 01  RW  SRAM Address 81
	uint8_t I2C_ADR_MODE ;            // reg. address 03  RW  SRAM Address 82
	// CDR Page 20 CH0
	uint8_t OUTPUT_EQ_CH0 ;           // reg. address 00  RW  SRAM Address 83
	uint8_t REFERNCE_SWING_CH0 ;      // reg. address 02  RW  SRAM Address 84
	uint8_t Manual_VGA0_Filter_CH0 ;  // reg. address 03  RW  SRAM Address 85
	uint8_t Manual_VGA1_Filter_CH0 ;  // reg. address 04  RW  SRAM Address 86
	uint8_t Manual_VGA2_Filter_CH0 ;  // reg. address 05  RW  SRAM Address 87
	uint8_t Tune_REF_SEL_LOW_CH0 ;    // reg. address 0A  RW  SRAM Address 88
	uint8_t TIA_Filter_Swing_CH0 ;    // reg. address 0D  RW  SRAM Address 89
	uint8_t LOS_THRESH_HST_CH0 ;      // reg. address 19  RW  SRAM Address 8A
	uint8_t LOS_THRESHOLD_CH0 ;       // reg. address 1A  RW  SRAM Address 8B
	uint8_t AGC_LSB_CH0 ;             // reg. address C0  RW  SRAM Address 8C
	uint8_t AGC_MSB_CH0 ;             // reg. address C1  RW  SRAM Address 8D
	uint8_t AGC_AD_MODE_CH0 ;         // reg. address C2  RW  SRAM Address 8E
	uint8_t Bias_LSB_CH0 ;            // reg. address E0  RW  SRAM Address 8F
	uint8_t Bias_MSB_CH0 ;            // reg. address E1  RW  SRAM Address 90
	uint8_t B_AD_MODE_CH0 ;           // reg. address E2  RW  SRAM Address 91
	uint8_t DCD_LSB_CH0 ;             // reg. address D0  RW  SRAM Address 92
	uint8_t DCD_MSB_CH0 ;             // reg. address D1  RW  SRAM Address 93
	uint8_t DCD_AD_MODE_CH0 ;         // reg. address D2  RW  SRAM Address 94
	uint8_t TIA_VGA_REGULATOR_CH0 ;   // reg. address 01  RW  SRAM Address 95
	uint8_t TIA_VGA_SWEEP_CH0 ;  	  // reg. address 0C  RW  SRAM Address 96
	uint8_t RSVD_2_CH0 ; 			  //                  RW  SRAM Address 97
	uint8_t RSVD_3_CH0 ;  			  //                  RW  SRAM Address 98
	// CDR Page 21 CH1
	uint8_t OUTPUT_EQ_CH1 ;           // reg. address 00  RW  SRAM Address 99
	uint8_t REFERNCE_SWING_CH1 ;      // reg. address 02  RW  SRAM Address 9A
	uint8_t Manual_VGA0_Filter_CH1 ;  // reg. address 03  RW  SRAM Address 9B
	uint8_t Manual_VGA1_Filter_CH1 ;  // reg. address 04  RW  SRAM Address 9C
	uint8_t Manual_VGA2_Filter_CH1 ;  // reg. address 05  RW  SRAM Address 9D
	uint8_t Tune_REF_SEL_LOW_CH1 ;    // reg. address 0A  RW  SRAM Address 9E
	uint8_t TIA_Filter_Swing_CH1 ;    // reg. address 0D  RW  SRAM Address 9F
	uint8_t LOS_THRESH_HST_CH1 ;      // reg. address 19  RW  SRAM Address A0
	uint8_t LOS_THRESHOLD_CH1 ;       // reg. address 1A  RW  SRAM Address A1
	uint8_t AGC_LSB_CH1 ;             // reg. address C0  RW  SRAM Address A2
	uint8_t AGC_MSB_CH1 ;             // reg. address C1  RW  SRAM Address A3
	uint8_t AGC_AD_MODE_CH1 ;         // reg. address C2  RW  SRAM Address A4
	uint8_t Bias_LSB_CH1 ;            // reg. address E0  RW  SRAM Address A5
	uint8_t Bias_MSB_CH1 ;            // reg. address E1  RW  SRAM Address A6
	uint8_t B_AD_MODE_CH1 ;           // reg. address E2  RW  SRAM Address A7
	uint8_t DCD_LSB_CH1 ;             // reg. address D0  RW  SRAM Address A8
	uint8_t DCD_MSB_CH1 ;             // reg. address D1  RW  SRAM Address A9
	uint8_t DCD_AD_MODE_CH1 ;         // reg. address D2  RW  SRAM Address AA
	uint8_t TIA_VGA_REGULATOR_CH1 ;   // reg. address 01  RW  SRAM Address AB
	uint8_t TIA_VGA_SWEEP_CH1 ;  	  // reg. address 0C  RW  SRAM Address AC
	uint8_t RSVD_2_CH1 ; 			  //                  RW  SRAM Address AD
	uint8_t RSVD_3_CH1 ;  			  //                  RW  SRAM Address AE
	// CDR Page 22 CH2
	uint8_t OUTPUT_EQ_CH2 ;           // reg. address 00  RW  SRAM Address AF
	uint8_t REFERNCE_SWING_CH2 ;      // reg. address 02  RW  SRAM Address B0
	uint8_t Manual_VGA0_Filter_CH2 ;  // reg. address 03  RW  SRAM Address B1
	uint8_t Manual_VGA1_Filter_CH2 ;  // reg. address 04  RW  SRAM Address B2
	uint8_t Manual_VGA2_Filter_CH2 ;  // reg. address 05  RW  SRAM Address B3
	uint8_t Tune_REF_SEL_LOW_CH2 ;    // reg. address 0A  RW  SRAM Address B4
	uint8_t TIA_Filter_Swing_CH2 ;    // reg. address 0D  RW  SRAM Address B5
	uint8_t LOS_THRESH_HST_CH2 ;      // reg. address 19  RW  SRAM Address B6
	uint8_t LOS_THRESHOLD_CH2 ;       // reg. address 1A  RW  SRAM Address B7
	uint8_t AGC_LSB_CH2 ;             // reg. address C0  RW  SRAM Address B8
	uint8_t AGC_MSB_CH2 ;             // reg. address C1  RW  SRAM Address B9
	uint8_t AGC_AD_MODE_CH2 ;         // reg. address C2  RW  SRAM Address BA
	uint8_t Bias_LSB_CH2 ;            // reg. address E0  RW  SRAM Address BB
	uint8_t Bias_MSB_CH2 ;            // reg. address E1  RW  SRAM Address BC
	uint8_t B_AD_MODE_CH2 ;           // reg. address E2  RW  SRAM Address BD
	uint8_t DCD_LSB_CH2 ;             // reg. address D0  RW  SRAM Address BE
	uint8_t DCD_MSB_CH2 ;             // reg. address D1  RW  SRAM Address BF
	uint8_t DCD_AD_MODE_CH2 ;         // reg. address D2  RW  SRAM Address C0
	uint8_t TIA_VGA_REGULATOR_CH2 ;   // reg. address 01  RW  SRAM Address C1
	uint8_t TIA_VGA_SWEEP_CH2 ;  	  // reg. address 0C  RW  SRAM Address C2
	uint8_t RSVD_2_CH2 ; 			  //                  RW  SRAM Address C3
	uint8_t RSVD_3_CH2 ;  			  //                  RW  SRAM Address C4
	// CDR Page 23 CH3
	uint8_t OUTPUT_EQ_CH3 ;           // reg. address 00  RW  SRAM Address C5
	uint8_t REFERNCE_SWING_CH3 ;      // reg. address 02  RW  SRAM Address C6
	uint8_t Manual_VGA0_Filter_CH3 ;  // reg. address 03  RW  SRAM Address C7
	uint8_t Manual_VGA1_Filter_CH3 ;  // reg. address 04  RW  SRAM Address C8
	uint8_t Manual_VGA2_Filter_CH3 ;  // reg. address 05  RW  SRAM Address C9
	uint8_t Tune_REF_SEL_LOW_CH3 ;    // reg. address 0A  RW  SRAM Address CA
	uint8_t TIA_Filter_Swing_CH3 ;    // reg. address 0D  RW  SRAM Address CB
	uint8_t LOS_THRESH_HST_CH3 ;      // reg. address 19  RW  SRAM Address CC
	uint8_t LOS_THRESHOLD_CH3 ;       // reg. address 1A  RW  SRAM Address CD
	uint8_t AGC_LSB_CH3 ;             // reg. address C0  RW  SRAM Address CE
	uint8_t AGC_MSB_CH3 ;             // reg. address C1  RW  SRAM Address CF
	uint8_t AGC_AD_MODE_CH3 ;         // reg. address C2  RW  SRAM Address D0
	uint8_t Bias_LSB_CH3 ;            // reg. address E0  RW  SRAM Address D1
	uint8_t Bias_MSB_CH3 ;            // reg. address E1  RW  SRAM Address D2
	uint8_t B_AD_MODE_CH3 ;           // reg. address E2  RW  SRAM Address D3
	uint8_t DCD_LSB_CH3 ;             // reg. address D0  RW  SRAM Address D4
	uint8_t DCD_MSB_CH3 ;             // reg. address D1  RW  SRAM Address D5
	uint8_t DCD_AD_MODE_CH3 ;         // reg. address D2  RW  SRAM Address D6
	uint8_t TIA_VGA_REGULATOR_CH3 ;   // reg. address 01  RW  SRAM Address D7
	uint8_t TIA_VGA_SWEEP_CH3 ;  	  // reg. address 0C  RW  SRAM Address D8
	uint8_t RSVD_2_CH3 ; 			  //                  RW  SRAM Address D9
	uint8_t RSVD_3_CH3 ;  			  //                  RW  SRAM Address DA

//	uint8_t intermal_1V_regulator_Level ; // reg. address 04 RW  SRAM Address DB
	uint8_t AGC_Sequence_normal;      //reg. address F8   RW  SRAM Address DB
	uint8_t DAC67_SET_normal;         //reg. address F0   RW  SRAM Address DC
	uint8_t DAC6;                     //reg. address 3A   RW  SRAM Address DD
	uint8_t DAC7;                     //reg. address 3B   RW  SRAM Address DE

	uint8_t DAC1415_SET_normal;       //reg. address F1   RW  SRAM Address DF
	uint8_t DAC14;                    //reg. address 42   RW  SRAM Address E0
	uint8_t DAC15;                    //reg. address 43   RW  SRAM Address E1

	uint8_t DAC3435_SET_normal;       //reg. address F4   RW  SRAM Address E2
	uint8_t DAC34;                    //reg. address 56   RW  SRAM Address E3
	uint8_t DAC35;                    //reg. address 57   RW  SRAM Address E4

	uint8_t Control_TC_C5;            //reg. address C5   RW  SRAM Address E5
	uint8_t Control_TC_C6;            //reg. address C6   RW  SRAM Address E6
	uint8_t SET_Average_C7;           //reg. address C7   RW  SRAM Address E7

	uint8_t Control_TC_D5;            //reg. address D5   RW  SRAM Address E8
	uint8_t Control_TC_D6;            //reg. address D6   RW  SRAM Address E9
	uint8_t SET_Average_D7;           //reg. address D7   RW  SRAM Address EA

	uint8_t Control_TC_E5;            //reg. address E5   RW  SRAM Address EB
	uint8_t Control_TC_E6;            //reg. address E6   RW  SRAM Address EC
	uint8_t SET_Average_E7;           //reg. address E7   RW  SRAM Address ED
	uint8_t Bias_Offset_17;           //reg. address 17   RW  SRAM Address EE

	uint8_t RSVD_Buffer[9] ;         //                  RW  SRAM Address EF - F7
	// Direct_Control
	uint8_t Direct_AD;                // Direct_Control Address RW  SRAM Address 0xF8
	uint8_t Direct_Data[5];           // Direct_Control Data    RW  SRAM Address 0xF9 - 0xFD
	uint8_t Direct_RW;                // Direct_Control RW      RW  SRAM Address 0xFE
	uint8_t Direct_EN;                // Direct_Control RW      RW  SRAM Address 0xFF
};

extern struct MATA03819_TIA_MEMORY MATA03819_TIA_MEMORY_MAP;

