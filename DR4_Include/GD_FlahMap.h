

// Gigadevice flash block size 0x400
// 1024 byte one block erase
#define MCU_Code_End    0x08038000

#define FS_QSFPDD_A0    0x08038000
#define FS_QSFPDD_P0    0x08038400
#define FS_QSFPDD_P1    0x08038800
#define FS_QSFPDD_P2    0x08038C00
#define FS_QSFPDD_P3    0x08039000

#define FS_PMIC_P81     0x0803A800
#define FS_VCSEL_P82    0x0803AC00
#define FS_TIA_P8A      0x0803B400
#define FS_DSP_LS_P86   0x0803BC00
#define FS_DSP_SS_P87   0x0803C000
#define FS_DSP_SS_P8E   0x0803C800

#define FS_Ibias01_PA0  0x0803CC00
#define FS_Ibias23_PA1  0x0803D000

#define FS_Cal0_P90     0x0803EC00
#define FS_MSA_O_P91    0x0803F000
#define FS_Cal1_P92     0x0803F400
#define FS_ID_INFO_PB0  0x0803F800 

extern uint16_t MCU_FW_VERSION ; 