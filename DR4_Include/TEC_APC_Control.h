
void MPD_ANODE_ADC_GET();
void TEC_MPD_ADC_GET();
void RFPD_BIAS_ADC_GET();
void DAC_TO_TOPS_SETTING();
void Acacia_Tx_Monitor_ADC();
void TEC_Control(uint8_t Channel);
void APC_Control(uint8_t Channel);


struct TEC_APC_Control_MEMORY
{ 
    uint8_t  RSVD0;                        // SRAM Address 80
    uint8_t  RSVD1;                        // SRAM Address 81
    uint16_t MPD_ANODE_ADC_TX1;            // SRAM Address 82-83
    uint16_t MPD_ANODE_ADC_TX2;            // SRAM Address 84-85
    uint16_t MPD_ANODE_ADC_TX3;            // SRAM Address 86-87
    uint16_t MPD_ANODE_ADC_TX4;            // SRAM Address 88-89
    uint16_t TEC_MPD_ADC_TX1;              // SRAM Address 8A-8B
    uint16_t TEC_MPD_ADC_TX2;              // SRAM Address 8C-8D
    uint16_t TEC_MPD_ADC_TX3;              // SRAM Address 8E-8F
    uint16_t TEC_MPD_ADC_TX4;              // SRAM Address 90-91
    uint16_t RFPD_BIAS_TX1;                // SRAM Address 92-93
    uint16_t RFPD_BIAS_TX2;                // SRAM Address 94-95
    uint16_t RFPD_BIAS_TX3;                // SRAM Address 96-97
    uint16_t RFPD_BIAS_TX4;                // SRAM Address 98-99
    uint16_t TEMP_BUFFER[3];               // SRAM Address 9A-9F
    
    uint16_t DAC_TO_TOPS_TX1;              // SRAM Address A0-A1
    uint16_t DAC_TO_TOPS_TX2;              // SRAM Address A2-A3
    uint16_t DAC_TO_TOPS_TX3;              // SRAM Address A4-A5
    uint16_t DAC_TO_TOPS_TX4;              // SRAM Address A6-A7
    uint16_t DAC_TEMP_BUFFER[4];           // SRAM Address A8-AF
    
    uint8_t  TEC_SumError_CH1;             // SRAM Address B0
    uint8_t  TEC_SumError_CH2;             // SRAM Address B1
    uint8_t  TEC_SumError_CH3;             // SRAM Address B2
    uint8_t  TEC_SumError_CH4;             // SRAM Address B3
    
    uint16_t TIA_RSSI_CH1;                 // SRAM Address B4-B5
    uint16_t TIA_RSSI_CH2;                 // SRAM Address B6-B7
    uint16_t TIA_RSSI_CH3;                 // SRAM Address B8-B9
    uint16_t TIA_RSSI_CH4;                 // SRAM Address BA-BB
    
    uint16_t DATA_BUFFER[34];              // SRAM Address BC-FF
};

extern struct TEC_APC_Control_MEMORY TEC_APC_Control_MEMORY_MAP;