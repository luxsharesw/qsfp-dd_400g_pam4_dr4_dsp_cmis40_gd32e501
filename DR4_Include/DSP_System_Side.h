/*
 * BRCM_DSP_D87540.h
 *
 *  Created on: 2018Ǿ11ū13ũ
 *      Author: Ivan_Lin
 */

#ifndef INC_DSP_SYSTEM_SIDE_H_
#define INC_DSP_SYSTEM_SIDE_H_
//------------------------------------------------//
//---------------- API----------------------------//
//------------------------------------------------//
void DSP_System_Side_TX_FIR_SET( uint8_t Lane_CH);
void System_Side_ALL_CH1_CH4_Control_P87();
void System_Side_ALL_CH1_CH4_Control_P8E();
void DSP_System_Side_TX_Squelch_SET(uint8_t Channel,uint8_t Control_Data);
void DSP_System_Side_RX_Squelch_SET(uint8_t Channel,uint8_t Control_Data);
uint8_t DSP_System_Side_LOL();
uint8_t DSP_System_Side_LOS();
void DSP_System_Side_Digital_Loopback_SET(uint8_t Lane_CH,uint8_t Enable);
void DSP_System_Side_Remote_Loopback_SET(uint8_t Lane_CH,uint8_t Enable);
void DSP_System_Side_PRBS_SET(uint8_t Pattern ,uint8_t Lane_CH ,uint8_t Enable);
void DSP_SystemSide_SNR_LTP_GET(uint8_t Lane_CH);
extern uint16_t DSP_SystemSide_SNR;
extern uint16_t DSP_SystemSide_LTP;
#endif  /*INC_DSP_SYSTEM_SIDE_H_*/