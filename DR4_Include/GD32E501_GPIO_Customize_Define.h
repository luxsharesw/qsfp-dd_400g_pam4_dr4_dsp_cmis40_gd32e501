//----------------------------------------------------//
// ADC Analog Define (Color Pink)
//----------------------------------------------------//
//----------------------------------------------------//
//ADC 2.5/4096 = 0.00061  6.1 ( 100mV)
//ADC 3.3/4096 = 0.00080  8
//----------------------------------------------------//
// Analog define 
#define ADC_CONV_Value_mV   6.1   

#define TX1DMPD         ADC_CHANNEL_0       //PA0	
#define TX2DMPD         ADC_CHANNEL_1       //PA1	
#define TX3DMPD         ADC_CHANNEL_2       //PA2	
#define TX4DMPD         ADC_CHANNEL_3       //PA3	
#define P3V3_RX_Mon     ADC_CHANNEL_8       //PB0	
#define REF_D3_BIAS     ADC_CHANNEL_9       //PB1
#define REF_D4_BIAS     ADC_CHANNEL_4       //PB2
#define P1V8_Mon        ADC_CHANNEL_7       //PB5
#define TX1MPDANODE     ADC_CHANNEL_10      //PC0
#define TX2MPDANODE     ADC_CHANNEL_11      //PC1
#define TX3MPDANODE     ADC_CHANNEL_12      //PC2
#define TX4MPDANODE     ADC_CHANNEL_13      //PC3
#define REF_D1_BIAS     ADC_CHANNEL_14      //PF4
#define REF_D2_BIAS     ADC_CHANNEL_15      //PF5
#define GD_Temp_Sensor  ADC_CHANNEL_16
//----------------------------------------------------//
// GPIO Digital Golden Finger Define (Color Yellow)
//----------------------------------------------------//
#define MDC_DSP             GPIO_PIN_4      // PC4  Input OP
#define MDIO_DSP            GPIO_PIN_5      // PC5  Input OP
#define IntL_GPIOC          GPIO_PIN_9      // PC9  OUTPUT_OP
#define ModselL_G_GPIOC     GPIO_PIN_15     // PC15 Input OP
#define RESETL_G_GPIOF      GPIO_PIN_1      // PF1  Input OP
#define LPMODE_G_GPIOF      GPIO_PIN_0      // PF0  Input OP
//----------------------------------------------------//
// GPIO Digital Internal Control Define (Color Orange)
//----------------------------------------------------//
#define P3V3_DSP_EN_GPIOA     GPIO_PIN_11     // PA11 OUTPUT_PP
#define P3V3_RX_EN_GPIOA      GPIO_PIN_12     // PA12 OUTPUT_PP
#define P3V3_TX_EN_GPIOA      GPIO_PIN_15     // PA15 OUTPUT_PP
#define LOS_To_DSP            GPIO_PIN_10     // PB10 OUTPUT_PP - DSP Active High
#define MODSELB_DSP_GPIOB     GPIO_PIN_12     // PB12 OUTPUT_PP
#define RESET_L_DSP_GPIOB     GPIO_PIN_13     // PB13 OUTPUT_PP
#define INTR_N_DSP_GPIOB      GPIO_PIN_14     // PB14 INPUT_PP  - HW Name is LASI_DSP
#define LPMODE_DSP_GPIOB      GPIO_PIN_15     // PB15 OUTPUT_PP - HW Name is INITMODE_DSP
#define OSC_EN_GPIOC          GPIO_PIN_6      // PC6  OUTPUT_PP

#define TIA_CH12_RESET_GPIOA  GPIO_PIN_8      // PA8  OUTPUT_PP
#define TIA_CH34_RESET_GPIOA  GPIO_PIN_9      // PA9  OUTPUT_PP
#define DRV_RESET_GPIOA       GPIO_PIN_10     // PA10 OUTPUT_PP
#define P3V3_OERF_EN_GPIOB    GPIO_PIN_11     // PB11 OUTPUT_PP 
#define P3V6_REBIAS_EN_GPIOC  GPIO_PIN_10     // PC10 OUTPUT_PP
#define P3V6_REMODE_EN_GPIOC  GPIO_PIN_11     // PC11 OUTPUT_PP
#define MP5490_LDEN_GPIOC     GPIO_PIN_12     // PC12 OUTPUT_PP
#define MP5490_RESET_GPIOC    GPIO_PIN_13     // PC13 OUTPUT_PP
#define P3V6_DRV_EN_GPIOF     GPIO_PIN_6      // PF6  OUTPUT_PP
#define P1V8_LB_EN_GPIOF      GPIO_PIN_7      // PF7  OUTPUT_PP
//------------------------------------------------------------------//
// GPIO OUTPUT Define
// DSP Control
//------------------------------------------------------------------//
#define P3V3_DSP_PowerUp()     gpio_bit_set(GPIOA, P3V3_DSP_EN_GPIOA)    
#define P3V3_RX_PowerUp()      gpio_bit_set(GPIOA, P3V3_RX_EN_GPIOA) 
#define P3V3_TX_PowerUp()      gpio_bit_set(GPIOA, P3V3_TX_EN_GPIOA) 
#define MODSELB_DSP_High()     gpio_bit_set(GPIOB, MODSELB_DSP_GPIOB) 
#define RESET_L_DSP_High()     gpio_bit_set(GPIOB, RESET_L_DSP_GPIOB)
#define LPMODE_DSP_High()      gpio_bit_set(GPIOB, LPMODE_DSP_GPIOB)
#define OSCEN_Pin_High()       gpio_bit_set(GPIOC, OSC_EN_GPIOC)

#define P3V3_DSP_PowerDown()   gpio_bit_reset(GPIOA, P3V3_DSP_EN_GPIOA)
#define P3V3_RX_PowerDown()    gpio_bit_reset(GPIOA, P3V3_RX_EN_GPIOA)
#define P3V3_TX_PowerDown()    gpio_bit_reset(GPIOA, P3V3_TX_EN_GPIOA)
#define MODSELB_DSP_Low()      gpio_bit_reset(GPIOB, MODSELB_DSP_GPIOB) 
#define RESET_L_DSP_Low()      gpio_bit_reset(GPIOB, RESET_L_DSP_GPIOB)
#define LPMODE_DSP_Low()       gpio_bit_reset(GPIOB, LPMODE_DSP_GPIOB)
#define OSCEN_Pin_Low()        gpio_bit_reset(GPIOC, OSC_EN_GPIOC)
//------------------------------------------------------------------//
// LDD and TIA Control
//------------------------------------------------------------------//
#define TIA_CH12_RESET_High()  gpio_bit_set(GPIOA, TIA_CH12_RESET_GPIOA)
#define TIA_CH34_RESET_High()  gpio_bit_set(GPIOA, TIA_CH34_RESET_GPIOA)
#define DRV_RESET_High()       gpio_bit_set(GPIOA, DRV_RESET_GPIOA)
#define P3V3_OERF_EN_High()    gpio_bit_set(GPIOB, P3V3_OERF_EN_GPIOB)
#define P3V6_REBIAS_EN_High()  gpio_bit_set(GPIOC, P3V6_REBIAS_EN_GPIOC)
#define P3V6_REMODE_EN_High()  gpio_bit_set(GPIOC, P3V6_REMODE_EN_GPIOC)
#define MP5490_LDEN_High()     gpio_bit_set(GPIOC, MP5490_LDEN_GPIOC)
#define MP5490_RESET_High()    gpio_bit_set(GPIOC, MP5490_RESET_GPIOC)
#define P3V6_DRV_EN_High()     gpio_bit_set(GPIOF, P3V6_DRV_EN_GPIOF)
#define P1V8_LB_EN_High()      gpio_bit_set(GPIOF, P1V8_LB_EN_GPIOF)

#define TIA_CH12_RESET_Low()   gpio_bit_reset(GPIOA, TIA_CH12_RESET_GPIOA)
#define TIA_CH34_RESET_Low()   gpio_bit_reset(GPIOA, TIA_CH34_RESET_GPIOA)
#define DRV_RESET_Low()        gpio_bit_reset(GPIOA, DRV_RESET_GPIOA)
#define P3V3_OERF_EN_Low()     gpio_bit_reset(GPIOB, P3V3_OERF_EN_GPIOB)
#define P3V6_REBIAS_EN_Low()   gpio_bit_reset(GPIOC, P3V6_REBIAS_EN_GPIOC)
#define P3V6_REMODE_EN_Low()   gpio_bit_reset(GPIOC, P3V6_REMODE_EN_GPIOC)
#define MP5490_LDEN_Low()      gpio_bit_reset(GPIOC, MP5490_LDEN_GPIOC)
#define MP5490_RESET_Low()     gpio_bit_reset(GPIOC, MP5490_RESET_GPIOC)
#define P3V6_DRV_EN_Low()      gpio_bit_reset(GPIOF, P3V6_DRV_EN_GPIOF)
#define P1V8_LB_EN_Low()       gpio_bit_reset(GPIOF, P1V8_LB_EN_GPIOF)
//------------------------------------------------------------------//
// MSA HW Control
//------------------------------------------------------------------//
#define IntL_G_High()            gpio_bit_set(GPIOC, IntL_GPIOC) 
#define IntL_G_Low()             gpio_bit_reset(GPIOC, IntL_GPIOC) 