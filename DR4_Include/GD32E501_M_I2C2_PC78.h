

void Master_I2C2_ByteWrite_PC78( uint8_t SADR , uint8_t Mem_REG , uint8_t Write_DATA);
uint8_t Master_I2C2_ByteREAD_PC78(uint8_t SADR ,uint8_t Mem_REG );
void Delay_I2C2(uint32_t i);   
void Master_I2C2_TwoByte_PC78( uint8_t SADR , uint8_t Mem_REG , uint8_t WD_MSB , uint8_t WD_LSB);
uint16_t MI2C2_WordADR_R_PC78(uint8_t SADR ,uint16_t MemADR );
void MI2C2_WordADR_W_PC78( uint8_t SADR , uint16_t MemADR , uint16_t DATA_Value );
void MI2C2_WordADR_HLBW_PC78(uint8_t SADR,uint16_t MemADR,uint8_t MSB,uint8_t LSB);