// Bias current control
void MP5490_Write_Control_Data();
void MP5490_Read_AllData();
void MP5490_Read_all_REG();
void MP5490_LDD_Interface_test();
void MP5490_Direct_Control();
void Bias_MOD_Updata_Current_Temperature_vary();
void Update_Bias_Current(uint8_t Channel,uint16_t update_current);
// Acacia sip control
void Acaaia_LDD_Direct_Control();
void Acacia_LDD_Read_ALL();
void Acacia_LDD_Write_ALL();
void Acacia_LDD_Interface_test();
void Update_dia_temp();
void update_Sensor_Value();
void GetDebug_SP_data(uint8_t Channel);
void RESETControl();
void Device_UnReset();
void Device_RESET();
void GetSP_LawData(uint8_t Channel,uint8_t HL_Data);
void LDD_Trun_on();
void LDD_TX1_TX8_Tx_Disable(uint8_t Lane,uint8_t Value);

#define MP5490_ADR       0xC0


extern uint8_t MP5490_Read_Buffer[128];

struct LDD_Control_MEMORY
{
    // MP5490 IDAC Control 
    uint8_t MP5940_ID;                // REG 0x3F SRAM Address 80
    uint8_t MP5940_REV;               // REG 0x3E SRAM Address 81
    uint8_t MP5940_ADDR;              // REG 0x3D SRAM Address 82
    uint8_t RSVD;                     //          SRAM Address 83
    uint8_t MP5940_Statu_1;           // REG 0x2E SRAM Address 84
    uint8_t MP5940_Statu_2;           // REG 0x2F SRAM Address 85
    uint8_t MP5940_Statu_3;           // REG 0x30 SRAM Address 86
    uint8_t MP5940_Warn_1;            // REG 0x32 SRAM Address 87
    uint8_t MP5940_IDACM_MSB_CH1;     // REG 0x48 SRAM Address 88
    uint8_t MP5940_IDACM_LSB_CH1;     // REG 0x49 SRAM Address 89
    uint8_t MP5940_IDACM_MSB_CH2;     // REG 0x4A SRAM Address 8A
    uint8_t MP5940_IDACM_LSB_CH2;     // REG 0x4B SRAM Address 8B
    uint8_t MP5940_IDACM_MSB_CH3;     // REG 0x4C SRAM Address 8C
    uint8_t MP5940_IDACM_LSB_CH3;     // REG 0x4D SRAM Address 8D
    uint8_t MP5940_IDACM_MSB_CH4;     // REG 0x4E SRAM Address 8E
    uint8_t MP5940_IDACM_LSB_CH4;     // REG 0x4F SRAM Address 8F   
    uint8_t MP5940_CTL0;              // REG 0x00 SRAM Address 90
    uint8_t MP5940_CTL1;              // REG 0x01 SRAM Address 91
    uint8_t MP5940_CTL2;              // REG 0x02 SRAM Address 92
    uint8_t MP5940_CTL3;              // REG 0x03 SRAM Address 93
    uint8_t MP5940_CTL4;              // REG 0x04 SRAM Address 94
    uint8_t MP5940_IDAC_MSB_CH1;      // REG 0x07 SRAM Address 95        
    uint8_t MP5940_IDAC_LSB_CH1;      // REG 0x08 SRAM Address 96
    uint8_t MP5940_IDAC_MSB_CH2;      // REG 0x09 SRAM Address 97  
    uint8_t MP5940_IDAC_LSB_CH2;      // REG 0x0A SRAM Address 98
    uint8_t MP5940_IDAC_MSB_CH3;      // REG 0x0B SRAM Address 99  
    uint8_t MP5940_IDAC_LSB_CH3;      // REG 0x0C SRAM Address 9A    
    uint8_t MP5940_IDAC_MSB_CH4;      // REG 0x0D SRAM Address 9B         
    uint8_t MP5940_IDAC_LSB_CH4;      // REG 0x0E SRAM Address 9C
    uint8_t MP5940_EML_THR;           // REG 0x1B SRAM Address 9D
    uint8_t MP5940_IDAC_THR;          // REG 0x1C SRAM Address 9E
    uint8_t MP5940_Protection;        // REG 0x29 SRAM Address 9F    
    uint8_t MP5940_VIDACM_E;          // REG 0x2A SRAM Address A0
    uint8_t MP5940_CONFIG;            // REG 0x2D SRAM Address A1       
    uint8_t MP5940_MASK;              // REG 0x34 SRAM Address A2
    uint8_t MP5940_Data_temp;         //          SRAM Address A3 
    uint8_t DAC_TOPS_MSB_CH1;         //          SRAM Address A4
    uint8_t DAC_TOPS_LSB_CH1;         //          SRAM Address A5
    uint8_t DAC_TOPS_MSB_CH2;         //          SRAM Address A6
    uint8_t DAC_TOPS_LSB_CH2;         //          SRAM Address A7
    uint8_t DAC_TOPS_MSB_CH3;         //          SRAM Address A8
    uint8_t DAC_TOPS_LSB_CH3;         //          SRAM Address A9
    uint8_t DAC_TOPS_MSB_CH4;         //          SRAM Address AA
    uint8_t DAC_TOPS_LSB_CH4;         //          SRAM Address AB     
    uint8_t MP5940_DS;                //          SRAM Address AC
    uint8_t MP5940_DM;                //          SRAM Address AD
    uint8_t MP5940_WB;                //          SRAM Address AE
    uint8_t MP5940_RB;                //          SRAM Address AF   
    // Acacia LDD GLBL Control 
    uint8_t GLBL_Acacia_LDD_CHID_MSB; // 0x4000 SRAM Address B0
    uint8_t GLBL_Acacia_LDD_CHID_LSB; // 0x4000 SRAM Address B1
    uint8_t GLBL_BIAS_GEN_CTRL_MSB;   // 0x400C SRAM Address B2
    uint8_t GLBL_BIAS_GEN_CTRL_LSB;   // 0x400C SRAM Address B3
    uint8_t GLBL_Data[12];            //        SRAM Address B4-BF     
    // Acacia LDD CH1 Control     
    uint8_t XI_AGC1_CH1_MSB;          // 0x0002 SRAM Address C0
    uint8_t XI_AGC1_CH1_LSB;          // 0x0002 SRAM Address C1
    uint8_t XI_RF2_VGA_CH1_MSB;       // 0x000C SRAM Address C2
    uint8_t XI_RF2_VGA_CH1_LSB;       // 0x000C SRAM Address C3
    uint8_t XI_RF4_DRV_CH1_MSB;       // 0x0010 SRAM Address C4
    uint8_t XI_RF4_DRV_CH1_LSB;       // 0x0010 SRAM Address C5
    uint8_t XI_RF6_CH1_MSB;           // 0x0014 SRAM Address C6
    uint8_t XI_RF6_CH1_LSB;           // 0x0014 SRAM Address C7
    // Acacia LDD CH2 Control     
    uint8_t XI_AGC1_CH2_MSB;          // 0x1002 SRAM Address C8
    uint8_t XI_AGC1_CH2_LSB;          // 0x1002 SRAM Address C9
    uint8_t XI_RF2_VGA_CH2_MSB;       // 0x100C SRAM Address CA
    uint8_t XI_RF2_VGA_CH2_LSB;       // 0x100C SRAM Address CB
    uint8_t XI_RF4_DRV_CH2_MSB;       // 0x1010 SRAM Address CC
    uint8_t XI_RF4_DRV_CH2_LSB;       // 0x1010 SRAM Address CD
    uint8_t XI_RF6_CH2_MSB;           // 0x1014 SRAM Address CE
    uint8_t XI_RF6_CH2_LSB;           // 0x1014 SRAM Address CF
    // Acacia LDD CH3 Control         
    uint8_t XI_AGC1_CH3_MSB;          // 0x2002 SRAM Address D0
    uint8_t XI_AGC1_CH3_LSB;          // 0x2002 SRAM Address D1
    uint8_t XI_RF2_VGA_CH3_MSB;       // 0x200C SRAM Address D2
    uint8_t XI_RF2_VGA_CH3_LSB;       // 0x200C SRAM Address D3
    uint8_t XI_RF4_DRV_CH3_MSB;       // 0x2010 SRAM Address D4
    uint8_t XI_RF4_DRV_CH3_LSB;       // 0x2010 SRAM Address D5
    uint8_t XI_RF6_CH3_MSB;           // 0x2014 SRAM Address D6
    uint8_t XI_RF6_CH3_LSB;           // 0x2014 SRAM Address D7
    // Acacia LDD CH4 Control         
    uint8_t XI_AGC1_CH4_MSB;          // 0x2002 SRAM Address D8
    uint8_t XI_AGC1_CH4_LSB;          // 0x2002 SRAM Address D9
    uint8_t XI_RF2_VGA_CH4_MSB;       // 0x200C SRAM Address DA
    uint8_t XI_RF2_VGA_CH4_LSB;       // 0x200C SRAM Address DB
    uint8_t XI_RF4_DRV_CH4_MSB;       // 0x2010 SRAM Address DC
    uint8_t XI_RF4_DRV_CH4_LSB;       // 0x2010 SRAM Address DD
    uint8_t XI_RF6_CH4_MSB;           // 0x2014 SRAM Address DE
    uint8_t XI_RF6_CH4_LSB;           // 0x2014 SRAM Address DF
    
    uint8_t Power_Control;            //             Address E0
    uint8_t RESET_Control;            //             Address E1
    
    uint8_t TEC_Target_value_MSB_CH1; //             Address E2
    uint8_t TEC_Target_value_LSB_CH1; //             Address E3
    uint8_t TEC_Target_value_MSB_CH2; //             Address E4
    uint8_t TEC_Target_value_LSB_CH2; //             Address E5
    uint8_t TEC_Target_value_MSB_CH3; //             Address E6
    uint8_t TEC_Target_value_LSB_CH3; //             Address E7
    uint8_t TEC_Target_value_MSB_CH4; //             Address E8
    uint8_t TEC_Target_value_LSB_CH4; //             Address E9
    
    uint8_t Dia_temp_CH1;             //             Address EA
    uint8_t Dia_temp_CH2;             //             Address EB
    uint8_t Dia_temp_CH3;             //             Address EC
    uint8_t Dia_temp_CH4;             //             Address ED
    
    uint8_t APC_Target_value_MSB_CH1; //             Address EE
    uint8_t APC_Target_value_LSB_CH1; //             Address EF
    uint8_t APC_Target_value_MSB_CH2; //             Address F0
    uint8_t APC_Target_value_LSB_CH2; //             Address F1
    uint8_t APC_Target_value_MSB_CH3; //             Address F2
    uint8_t APC_Target_value_LSB_CH3; //             Address F3
    uint8_t APC_Target_value_MSB_CH4; //             Address F4
    uint8_t APC_Target_value_LSB_CH4; //             Address F5
    
    uint8_t LDD_Buffer[2];            // SRAM Address F6 - F7
    
    uint16_t Direct_Satrt;            // SRAM Address F8 - F9
    uint16_t Driect_MEMADR;           // SRAM Address FA - FB
    uint16_t Driect_WBUFF;            // SRAM Address FC - FD
    uint16_t Driect_RBUFF;            // SRAM Address FE - FF
};

extern struct LDD_Control_MEMORY LDD_Control_MEMORY_MAP;

