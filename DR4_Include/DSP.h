#ifndef INC_DSP_H_
#define INC_DSP_H_

//----------------------------------------------------------------------------------------//
// DSP 87400 I2C Command
//----------------------------------------------------------------------------------------//
//#define BRCM_I2C_SAD    0xA0        // BSC Slave Address
#define BRCM_PAGE_SELECT      0x7F    // QSFP Page Select Register
#define BRCM_IND_ADDR0        0x80    // Indirect Address register 0
#define BRCM_IND_ADDR1        0x81    // Indirect Address register 1
#define BRCM_IND_ADDR2        0x82    // Indirect Address register 2
#define BRCM_IND_ADDR3        0x83    // Indirect Address register 3
// BRCM Read/Write Data length
#define BRCM_IND_LEN0         0x84    // Indirect Length register 0
#define BRCM_IND_LEN1         0x85    // Indirect Length register 1
// R/W Command Address
// 0x03 Write Command
// 0x01 Read  Command
#define BRCM_IND_CTRL         0x86    // Indirect Read/Write Control register
#define BRCM_WFIFO            0x87    // Write FIFO register
#define BRCM_RFIFO            0x90    // Read  FIFO register

//DSP Chip Modes
#define Mode1_CHIP_MODE_2X53G_KP4PAM_TO_1X106G_KP4PAM      0xA0
#define Mode2_CHIP_MODE_2X51G_KR4PAM_TO_1X103G_KR4PAM      0xA1
#define Mode3_CHIP_MODE_4X25G_KR4NRZ_TO_1X103G_KP4PAM      0xA2
#define Mode4_CHIP_MODE_4x25G_KR4NRZ_TO_4x25G_KR4NRZ       0xA3
#define Mode5_CHIP_MODE_4x25G_NRZ_TO_1X106G_KP4PAM         0xA4
#define Mode6_CHIP_MODE_4x53G_KP4PAM_TO_2X106G_KP4PAM      0xA5
#define Mode7_CHIP_MODE_8x53G_KP4PAM_TO_4X106G_KP4PAM      0xA6
#define Mode8_CHIP_MODE_2x53G_KP4PAM_TO_1X112G_KP4PAM      0xA7
#define Mode9_CHIP_MODE_4x53G_KP4PAM_TO_2X112G_KP4PAM      0xA8
#define Mode10_CHIP_MODE_8x53G_KP4PAM_TO_4X112G_KP4PAM     0xA9
#define Mode11_CHIP_MODE_4x26G_NRZ_TO_1X106G_PAM           0xAA
#define Mode12_CHIP_MODE_1X53G_KP4PAM_TO_1X53G_KP4PAM      0xAB
#define Mode13_CHIP_MODE_2X53G_KP4PAM_TO_2X53G_KP4PAM      0xAC
#define Mode14_CHIP_MODE_4X53G_KP4PAM_TO_4X53G_KP4PAM      0xAD
#define Mode15_CHIP_MODE_2X25G_NRZ_TO_1X51G_PAM            0xAE

// BSC Slave Address
#define DSP_Slave_Address                 0xA0

#define BRCM_Default_Length             0x0004
#define COMMAND_Base_ADR            0x00047000
#define Remote_Loopback_mode                 0
#define Digital_LoopBack_mode                1
#define DSP_PAM4_FIR_MAX                   168
#define DSP_NRZ_FIR_MAX                    128
#define TX_Side                              1
#define RX_Side                              0
#define Squelch                              1
#define Unsquelch                            0
//DSP Pattern setting value
#define PRBS7Q_SystemSide      0x00
#define PRBS9Q_SystemSide      0x01
#define PRBS13Q_SystemSide     0x0A
#define PRBS15Q_SystemSide     0x03
#define PRBS23Q_SystemSide     0x04
#define PRBS31Q_SystemSide     0x05
#define SSPRQ_SystemSide       0xBB
#define PRBS7Q_LineSide        0x07
#define PRBS9Q_LineSide        0x09
#define PRBS13Q_LineSide       0x0D
#define PRBS15Q_LineSide       0x0F
#define PRBS23Q_LineSide       0x17
#define PRBS31Q_LineSide       0x1F
#define SSPRQ_LineSide         0xAA
// PLL Bandwidth Parameter
#define Default_Bandwidth   0
#define Normal_Bandwidth    1
#define Maximun_Bandwidth   2

extern struct DSP_Direct_Control_MEMORY DSP_Direct_Control_MEMORY_MAP;
extern struct DSP_Line_Side_PHY0_MEMORY DSP_Line_Side_PHY0_MEMORY_MAP;
extern struct DSP_System_Side_PHY0_MEMORY DSP_System_Side_PHY0_MEMORY_MAP;
extern struct DSP_Line_Side_PHY1_MEMORY DSP_Line_Side_PHY1_MEMORY_MAP;
extern struct DSP_System_Side_PHY1_MEMORY DSP_System_Side_PHY1_MEMORY_MAP;
extern uint32_t COMMAND_4700_DATA[18];
extern uint8_t REV_ID;
extern uint8_t DSP_INIT_Flag;

uint32_t BRCM_Control_READ_Data( uint32_t BRCM_ADDR);
void BRCM_Control_WRITE_Data( uint32_t BRCM_ADDR , uint32_t Data_Value , uint16_t Data_Length);

void BRCM_READ_Data( uint8_t *BRCM_ADDR , uint8_t *DataBuffer , uint8_t Data_Length);
void BRCM_WRITE_Data( uint8_t *BRCM_ADDR , uint8_t *DataBuffer , uint8_t Data_Length);

void DSP87400_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(uint32_t Read_ADDRESS);
void DSP87400_CAPI_CW_CMD(uint32_t INTF_ADR0,uint32_t INTF_DATA0,uint32_t INTF_ADR1,uint32_t INTF_DATA1);
void DSP87400_CAPI_Command_response(uint32_t CHK_ADR0,uint32_t CHK_ADR1);
void DSP_PLL_BANDWIDTH_CONFIG(uint8_t Bandwidth);

//------------------------------------------------//
//---------------- API----------------------------//
//------------------------------------------------//
void TEST_DSP_Command_Direct_Control();
uint16_t GET_DSP_Temperature();
void DSP_Init();
void Get_DSP_Chip_Mode();
void SET_DSP_CHIP_MODE();
void Trigger_CMD_Update_DSP_REG();
void Get_NRZ_Default_System_Side();
void DSP_SW_RESET();
void DSP_RESET_BRING_UP();

struct DSP_Direct_Control_MEMORY
{
    uint8_t BRCM_REG_ADDR_0 ;         // RW  SRAM Address 80
    uint8_t BRCM_REG_ADDR_1 ;         // RW  SRAM Address 81
    uint8_t BRCM_REG_ADDR_2 ;         // RW  SRAM Address 82
    uint8_t BRCM_REG_ADDR_3 ;         // RW  SRAM Address 83
    uint8_t WRITE_BUFFER_0 ;          // RW  SRAM Address 84
    uint8_t WRITE_BUFFER_1 ;          // RW  SRAM Address 85
    uint8_t WRITE_BUFFER_2 ;          // RW  SRAM Address 86
    uint8_t WRITE_BUFFER_3 ;          // RW  SRAM Address 87
    uint8_t READ_BUFFER_0 ;           // RW  SRAM Address 88
    uint8_t READ_BUFFER_1 ;           // RW  SRAM Address 89
    uint8_t READ_BUFFER_2 ;           // RW  SRAM Address 8A
    uint8_t READ_BUFFER_3 ;           // RW  SRAM Address 8B
    uint8_t LENGITH_MSB ;             // RW  SRAM Address 8C
	uint8_t LENGITH_LSB ;             // RW  SRAM Address 8D
	uint8_t Direct_Control ;          // RW  SRAM Address 8E
	uint8_t RW_EN ;                   // RW  SRAM Address 8F
	uint8_t CHIP_INFO_LSB[4];         // RW  SRAM Address 90 - 93
	uint8_t CHIP_INFO_MSB[4];         // RW  SRAM Address 94 - 97
	uint8_t Buffer[8];                // RW  SRAM Address 98 - 9F
	uint8_t Phy_ID;                   // RW  SRAM Address A0
	uint8_t BRCM_ADDR_DATA;           // RW  SRAM Address A1
	uint8_t Debug_flag;			      // RW  SRAM Address A2
    uint8_t Ready_Byte_MSB;           // RW  SRAM Address A3
    uint8_t Ready_Byte_LSB;           // RW  SRAM Address A4   
    uint8_t BRCM_CHID_MSB;            // RW  SRAM Address A5
    uint8_t BRCM_CHID_LSB;            // RW  SRAM Address A6
	uint8_t CMD_CON;                  // RW  SRAM Address A7
	uint8_t Trigger_CMD;              // RW  SRAM Address A8
	uint8_t CHIP_MODE_VALUE;          // RW  SRAM Address A9 
    uint8_t DSP_Delay_Count_MSB;      // RW  SRAM Address AA
    uint8_t DSP_Delay_Count_LSB;      // RW  SRAM Address AB
	uint8_t Temp_Buffer[84];          // RW  SRAM Address AC - FF
};

struct DSP_Line_Side_PHY0_MEMORY
{
	uint16_t DSP_CHIP_MODE ;              // RW DSP 5200C8C7  SRAM Address 80 - 81
	uint16_t Trigger_MODE ;               // RW DSP 5200C884  SRAM Address 82 - 83

	uint16_t Line_Side_PRE1_CH0 ;         // RW DSP 580034D0  SRAM Address 84 - 85
	uint16_t Line_Side_PRE1_CH1 ;         // RW DSP 580037D0  SRAM Address 86 - 87
	uint16_t Line_Side_PRE1_CH2 ;         // RW DSP 58003BD0  SRAM Address 88 - 89
	uint16_t Line_Side_PRE1_CH3 ;         // RW DSP 58003FD0  SRAM Address 8A - 8B

	uint16_t Line_Side_Main_CH0 ;         // RW DSP 580034D4  SRAM Address 8C - 8D
	uint16_t Line_Side_Main_CH1 ;         // RW DSP 580037D4  SRAM Address 8E - 8F
	uint16_t Line_Side_Main_CH2 ;         // RW DSP 58003BD4  SRAM Address 90 - 91
	uint16_t Line_Side_Main_CH3 ;         // RW DSP 58003FD4  SRAM Address 92 - 93

	uint16_t Line_Side_POST1_CH0 ;        // RW DSP 580034D8  SRAM Address 94 - 95
	uint16_t Line_Side_POST1_CH1 ;        // RW DSP 580037D8  SRAM Address 96 - 97
	uint16_t Line_Side_POST1_CH2 ;        // RW DSP 58003BD8  SRAM Address 98 - 99
	uint16_t Line_Side_POST1_CH3 ;        // RW DSP 58003FD8  SRAM Address 9A - 9B

	uint16_t Line_Side_POST2_CH0 ;        // RW DSP 580034DC  SRAM Address 9C - 9D
	uint16_t Line_Side_POST2_CH1 ;        // RW DSP 580037DC  SRAM Address 9E - 9F
	uint16_t Line_Side_POST2_CH2 ;        // RW DSP 58003BDC  SRAM Address A0 - A1
	uint16_t Line_Side_POST2_CH3 ;        // RW DSP 58003FDC  SRAM Address A2 - A3

	uint16_t Line_Side_POST3_CH0 ;        // RW DSP 580034E0  SRAM Address A4 - A5
	uint16_t Line_Side_POST3_CH1 ;        // RW DSP 580037E0  SRAM Address A6 - A7
	uint16_t Line_Side_POST3_CH2 ;        // RW DSP 58003BE0  SRAM Address A8 - A9
	uint16_t Line_Side_POST3_CH3 ;        // RW DSP 58003FE0  SRAM Address AA - AB

	uint16_t Line_Side_PRE2_CH0 ;         // RW DSP 580034CC  SRAM Address AC - AD
	uint16_t Line_Side_PRE2_CH1 ;         // RW DSP 580037CC  SRAM Address AE - AF
	uint16_t Line_Side_PRE2_CH2 ;         // RW DSP 58003BCC  SRAM Address B0 - B1
	uint16_t Line_Side_PRE2_CH3 ;         // RW DSP 58003FCC  SRAM Address B2 - B3

	uint16_t Line_Side_TX_Polarity_CH0 ;  // RW DSP 580035CC  SRAM Address B4 - B5
	uint16_t Line_Side_TX_Polarity_CH1 ;  // RW DSP 580075CC  SRAM Address B6 - B7
	uint16_t Line_Side_TX_Polarity_CH2 ;  // RW DSP 5800B5CC  SRAM Address B8 - B9
	uint16_t Line_Side_TX_Polarity_CH3 ;  // RW DSP 5800F5CC  SRAM Address BA - BB

	uint16_t Line_Side_RX_Polarity_CH0 ;  // RW DSP 5800110C  SRAM Address BC - BD
	uint16_t Line_Side_RX_Polarity_CH1 ;  // RW DSP 5800510C  SRAM Address BE - BF
	uint16_t Line_Side_RX_Polarity_CH2 ;  // RW DSP 5800910C  SRAM Address C0 - C1
	uint16_t Line_Side_RX_Polarity_CH3 ;  // RW DSP 5800D10C  SRAM Address C2 - C3
    
    uint8_t Line_side_LShift_00_CH0;      // RW DSP 58001828  SRAM Address C4
    uint8_t Line_side_LShift_01_CH0;      // RW DSP 58001828  SRAM Address C5
    uint8_t Line_side_LShift_10_CH0;      // RW DSP 58001828  SRAM Address C6
    uint8_t Line_side_LShift_11_CH0;      // RW DSP 58001828  SRAM Address C7
    
    uint8_t Line_side_LShift_00_CH1;      // RW DSP 58001828  SRAM Address C8
    uint8_t Line_side_LShift_01_CH1;      // RW DSP 58001828  SRAM Address C9
    uint8_t Line_side_LShift_10_CH1;      // RW DSP 58001828  SRAM Address CA
    uint8_t Line_side_LShift_11_CH1;      // RW DSP 58001828  SRAM Address CB
    
    uint8_t Line_side_LShift_00_CH2;      // RW DSP 58001828  SRAM Address CC
    uint8_t Line_side_LShift_01_CH2;      // RW DSP 58001828  SRAM Address CD
    uint8_t Line_side_LShift_10_CH2;      // RW DSP 58001828  SRAM Address CE
    uint8_t Line_side_LShift_11_CH2;      // RW DSP 58001828  SRAM Address CF
    
    uint8_t Line_side_LShift_00_CH3;      // RW DSP 58001828  SRAM Address D0
    uint8_t Line_side_LShift_01_CH3;      // RW DSP 58001828  SRAM Address D1
    uint8_t Line_side_LShift_10_CH3;      // RW DSP 58001828  SRAM Address D2
    uint8_t Line_side_LShift_11_CH3;      // RW DSP 58001828  SRAM Address D3

    uint8_t Line_side_TXFIR_TAPS_CH0;     // RW DSP           SRAM Address D4
    uint8_t Line_side_TXFIR_TAPS_CH1;     // RW DSP           SRAM Address D5
    uint8_t Line_side_TXFIR_TAPS_CH2;     // RW DSP           SRAM Address D6
    uint8_t Line_side_TXFIR_TAPS_CH3;     // RW DSP           SRAM Address D7
    
    uint8_t Line_side_SNR_MSB_CH0;        // RW DSP           SRAM Address D8
    uint8_t Line_side_SNR_LSB_CH0;        // RW DSP           SRAM Address D9
    uint8_t Line_side_SNR_MSB_CH1;        // RW DSP           SRAM Address DA
    uint8_t Line_side_SNR_LSB_CH1;        // RW DSP           SRAM Address DB
    uint8_t Line_side_SNR_MSB_CH2;        // RW DSP           SRAM Address DC
    uint8_t Line_side_SNR_LSB_CH2;        // RW DSP           SRAM Address DD
    uint8_t Line_side_SNR_MSB_CH3;        // RW DSP           SRAM Address DE
    uint8_t Line_side_SNR_LSB_CH3;        // RW DSP           SRAM Address DF
    
	uint16_t Line_Side_TAP6_CH0 ;         // RW DSP           SRAM Address E0 - E1
	uint16_t Line_Side_TAP6_CH1 ;         // RW DSP           SRAM Address E2 - E3
	uint16_t Line_Side_TAP6_CH2 ;         // RW DSP           SRAM Address E4 - E5
	uint16_t Line_Side_TAP6_CH3 ;         // RW DSP           SRAM Address E6 - E7
    
	uint8_t Line_Side_Buffer_1[20] ;        //  SRAM Address E8 - FB
    
    uint16_t DSP_0V75;                    // RW DSP XXXXXXXX  SRAM Address FC - FD
    uint16_t DSP_0V90;                    // RW DSP XXXXXXXX  SRAM Address FE - FF
};

struct DSP_System_Side_PHY0_MEMORY
{
	uint16_t System_Side_PRE1_CH0 ;        // RW DSP 500344D0  SRAM Address 80 - 81
	uint16_t System_Side_PRE1_CH1 ;        // RW DSP 501344D0  SRAM Address 82 - 83
	uint16_t System_Side_PRE1_CH2 ;        // RW DSP 502344D0  SRAM Address 84 - 85
	uint16_t System_Side_PRE1_CH3 ;        // RW DSP 503344D0  SRAM Address 86 - 87

	uint16_t System_Side_Main_CH0 ;        // RW DSP 500344D4  SRAM Address 88 - 89
	uint16_t System_Side_Main_CH1 ;        // RW DSP 501344D4  SRAM Address 8A - 8B
	uint16_t System_Side_Main_CH2 ;        // RW DSP 502344D4  SRAM Address 8C - 8D
	uint16_t System_Side_Main_CH3 ;        // RW DSP 503344D4  SRAM Address 8E - 8F

	uint16_t System_Side_POST1_CH0 ;       // RW DSP 500344D8  SRAM Address 90 - 91
	uint16_t System_Side_POST1_CH1 ;       // RW DSP 501344D8  SRAM Address 92 - 93
	uint16_t System_Side_POST1_CH2 ;       // RW DSP 502344D8  SRAM Address 94 - 95
	uint16_t System_Side_POST1_CH3 ;       // RW DSP 503344D8  SRAM Address 96 - 97

	uint16_t System_Side_POST2_CH0 ;       // RW DSP 500344DC  SRAM Address 98 - 99
	uint16_t System_Side_POST2_CH1 ;       // RW DSP 501344DC  SRAM Address 9A - 9B
	uint16_t System_Side_POST2_CH2 ;       // RW DSP 502344DC  SRAM Address 9C - 9D
	uint16_t System_Side_POST2_CH3 ;       // RW DSP 503344DC  SRAM Address 9E - 9F

	uint16_t System_Side_POST3_CH0 ;       // RW DSP 500344E0  SRAM Address A0 - A1
	uint16_t System_Side_POST3_CH1 ;       // RW DSP 501344E0  SRAM Address A2 - A3
	uint16_t System_Side_POST3_CH2 ;       // RW DSP 502344E0  SRAM Address A4 - A5
	uint16_t System_Side_POST3_CH3 ;       // RW DSP 503344E0  SRAM Address A6 - A7

	uint16_t System_Side_PRE2_CH0 ;        // RW DSP 500344CC  SRAM Address A8 - A9
	uint16_t System_Side_PRE2_CH1 ;        // RW DSP 501344CC  SRAM Address AA - AB
	uint16_t System_Side_PRE2_CH2 ;        // RW DSP 502344CC  SRAM Address AC - AD
	uint16_t System_Side_PRE2_CH3 ;        // RW DSP 503344CC  SRAM Address AE - AF

	uint16_t System_Side_TX_Polarity_CH0 ; // RW DSP 500345CC  SRAM Address B0 - B1
	uint16_t System_Side_TX_Polarity_CH1 ; // RW DSP 501345CC  SRAM Address B2 - B3
	uint16_t System_Side_TX_Polarity_CH2 ; // RW DSP 502345CC  SRAM Address B4 - B5
	uint16_t System_Side_TX_Polarity_CH3 ; // RW DSP 503345CC  SRAM Address B6 - B7

	uint16_t System_Side_RX_Polarity_CH0 ; // RW DSP 5003458C  SRAM Address B8 - B9
	uint16_t System_Side_RX_Polarity_CH1 ; // RW DSP 5013458C  SRAM Address BA - BB
	uint16_t System_Side_RX_Polarity_CH2 ; // RW DSP 5023458C  SRAM Address BC - BD
	uint16_t System_Side_RX_Polarity_CH3 ; // RW DSP 5033458C  SRAM Address BE - BF
    
    uint8_t System_side_SNR_MSB_CH0;        // RW DSP           SRAM Address C0
    uint8_t System_side_SNR_LSB_CH0;        // RW DSP           SRAM Address C1
    uint8_t System_side_SNR_MSB_CH1;        // RW DSP           SRAM Address C2
    uint8_t System_side_SNR_LSB_CH1;        // RW DSP           SRAM Address C3
    uint8_t System_side_SNR_MSB_CH2;        // RW DSP           SRAM Address C4
    uint8_t System_side_SNR_LSB_CH2;        // RW DSP           SRAM Address C5
    uint8_t System_side_SNR_MSB_CH3;        // RW DSP           SRAM Address C6
    uint8_t System_side_SNR_LSB_CH3;        // RW DSP           SRAM Address C7
    
	uint8_t System_Side_Buffer[56];        // SRAM Address C8 - FF
};

struct DSP_Line_Side_PHY1_MEMORY
{
	uint16_t Reserved_80_81 ;             // RW DSP 5200C8C7  SRAM Address 80 - 81
	uint16_t Reserved_82_83 ;             // RW DSP 5200C884  SRAM Address 82 - 83

	uint16_t Line_Side_PRE1_CH0 ;         // RW DSP 580034D0  SRAM Address 84 - 85
	uint16_t Line_Side_PRE1_CH1 ;         // RW DSP 580037D0  SRAM Address 86 - 87
	uint16_t Line_Side_PRE1_CH2 ;         // RW DSP 58003BD0  SRAM Address 88 - 89
	uint16_t Line_Side_PRE1_CH3 ;         // RW DSP 58003FD0  SRAM Address 8A - 8B

	uint16_t Line_Side_Main_CH0 ;         // RW DSP 580034D4  SRAM Address 8C - 8D
	uint16_t Line_Side_Main_CH1 ;         // RW DSP 580037D4  SRAM Address 8E - 8F
	uint16_t Line_Side_Main_CH2 ;         // RW DSP 58003BD4  SRAM Address 90 - 91
	uint16_t Line_Side_Main_CH3 ;         // RW DSP 58003FD4  SRAM Address 92 - 93

	uint16_t Line_Side_POST1_CH0 ;        // RW DSP 580034D8  SRAM Address 94 - 95
	uint16_t Line_Side_POST1_CH1 ;        // RW DSP 580037D8  SRAM Address 96 - 97
	uint16_t Line_Side_POST1_CH2 ;        // RW DSP 58003BD8  SRAM Address 98 - 99
	uint16_t Line_Side_POST1_CH3 ;        // RW DSP 58003FD8  SRAM Address 9A - 9B

	uint16_t Line_Side_POST2_CH0 ;        // RW DSP 580034DC  SRAM Address 9C - 9D
	uint16_t Line_Side_POST2_CH1 ;        // RW DSP 580037DC  SRAM Address 9E - 9F
	uint16_t Line_Side_POST2_CH2 ;        // RW DSP 58003BDC  SRAM Address A0 - A1
	uint16_t Line_Side_POST2_CH3 ;        // RW DSP 58003FDC  SRAM Address A2 - A3

	uint16_t Line_Side_POST3_CH0 ;        // RW DSP 580034E0  SRAM Address A4 - A5
	uint16_t Line_Side_POST3_CH1 ;        // RW DSP 580037E0  SRAM Address A6 - A7
	uint16_t Line_Side_POST3_CH2 ;        // RW DSP 58003BE0  SRAM Address A8 - A9
	uint16_t Line_Side_POST3_CH3 ;        // RW DSP 58003FE0  SRAM Address AA - AB

	uint16_t Line_Side_PRE2_CH0 ;         // RW DSP 580034CC  SRAM Address AC - AD
	uint16_t Line_Side_PRE2_CH1 ;         // RW DSP 580037CC  SRAM Address AE - AF
	uint16_t Line_Side_PRE2_CH2 ;         // RW DSP 58003BCC  SRAM Address B0 - B1
	uint16_t Line_Side_PRE2_CH3 ;         // RW DSP 58003FCC  SRAM Address B2 - B3

	uint16_t Line_Side_TX_Polarity_CH0 ;  // RW DSP 580035CC  SRAM Address B4 - B5
	uint16_t Line_Side_TX_Polarity_CH1 ;  // RW DSP 580075CC  SRAM Address B6 - B7
	uint16_t Line_Side_TX_Polarity_CH2 ;  // RW DSP 5800B5CC  SRAM Address B8 - B9
	uint16_t Line_Side_TX_Polarity_CH3 ;  // RW DSP 5800F5CC  SRAM Address BA - BB

	uint16_t Line_Side_RX_Polarity_CH0 ;  // RW DSP 5800110C  SRAM Address BC - BD
	uint16_t Line_Side_RX_Polarity_CH1 ;  // RW DSP 5800510C  SRAM Address BE - BF
	uint16_t Line_Side_RX_Polarity_CH2 ;  // RW DSP 5800910C  SRAM Address C0 - C1
	uint16_t Line_Side_RX_Polarity_CH3 ;  // RW DSP 5800D10C  SRAM Address C2 - C3
    
    uint8_t Line_side_LShift_00_CH0;      // RW DSP 58001828  SRAM Address C4
    uint8_t Line_side_LShift_01_CH0;      // RW DSP 58001828  SRAM Address C5
    uint8_t Line_side_LShift_10_CH0;      // RW DSP 58001828  SRAM Address C6
    uint8_t Line_side_LShift_11_CH0;      // RW DSP 58001828  SRAM Address C7
    
    uint8_t Line_side_LShift_00_CH1;      // RW DSP 58001828  SRAM Address C8
    uint8_t Line_side_LShift_01_CH1;      // RW DSP 58001828  SRAM Address C9
    uint8_t Line_side_LShift_10_CH1;      // RW DSP 58001828  SRAM Address CA
    uint8_t Line_side_LShift_11_CH1;      // RW DSP 58001828  SRAM Address CB
    
    uint8_t Line_side_LShift_00_CH2;      // RW DSP 58001828  SRAM Address CC
    uint8_t Line_side_LShift_01_CH2;      // RW DSP 58001828  SRAM Address CD
    uint8_t Line_side_LShift_10_CH2;      // RW DSP 58001828  SRAM Address CE
    uint8_t Line_side_LShift_11_CH2;      // RW DSP 58001828  SRAM Address CF
    
    uint8_t Line_side_LShift_00_CH3;      // RW DSP 58001828  SRAM Address D0
    uint8_t Line_side_LShift_01_CH3;      // RW DSP 58001828  SRAM Address D1
    uint8_t Line_side_LShift_10_CH3;      // RW DSP 58001828  SRAM Address D2
    uint8_t Line_side_LShift_11_CH3;      // RW DSP 58001828  SRAM Address D3

	uint8_t Line_Side_Buffer[44] ;        //  SRAM Address D4 - FF
};

struct DSP_System_Side_PHY1_MEMORY
{
	uint16_t System_Side_PRE1_CH0 ;        // RW DSP 500344D0  SRAM Address 80 - 81
	uint16_t System_Side_PRE1_CH1 ;        // RW DSP 501344D0  SRAM Address 82 - 83
	uint16_t System_Side_PRE1_CH2 ;        // RW DSP 502344D0  SRAM Address 84 - 85
	uint16_t System_Side_PRE1_CH3 ;        // RW DSP 503344D0  SRAM Address 86 - 87

	uint16_t System_Side_Main_CH0 ;        // RW DSP 500344D4  SRAM Address 88 - 89
	uint16_t System_Side_Main_CH1 ;        // RW DSP 501344D4  SRAM Address 8A - 8B
	uint16_t System_Side_Main_CH2 ;        // RW DSP 502344D4  SRAM Address 8C - 8D
	uint16_t System_Side_Main_CH3 ;        // RW DSP 503344D4  SRAM Address 8E - 8F

	uint16_t System_Side_POST1_CH0 ;       // RW DSP 500344D8  SRAM Address 90 - 91
	uint16_t System_Side_POST1_CH1 ;       // RW DSP 501344D8  SRAM Address 92 - 93
	uint16_t System_Side_POST1_CH2 ;       // RW DSP 502344D8  SRAM Address 94 - 95
	uint16_t System_Side_POST1_CH3 ;       // RW DSP 503344D8  SRAM Address 96 - 97

	uint16_t System_Side_POST2_CH0 ;       // RW DSP 500344DC  SRAM Address 98 - 99
	uint16_t System_Side_POST2_CH1 ;       // RW DSP 501344DC  SRAM Address 9A - 9B
	uint16_t System_Side_POST2_CH2 ;       // RW DSP 502344DC  SRAM Address 9C - 9D
	uint16_t System_Side_POST2_CH3 ;       // RW DSP 503344DC  SRAM Address 9E - 9F

	uint16_t System_Side_POST3_CH0 ;       // RW DSP 500344E0  SRAM Address A0 - A1
	uint16_t System_Side_POST3_CH1 ;       // RW DSP 501344E0  SRAM Address A2 - A3
	uint16_t System_Side_POST3_CH2 ;       // RW DSP 502344E0  SRAM Address A4 - A5
	uint16_t System_Side_POST3_CH3 ;       // RW DSP 503344E0  SRAM Address A6 - A7

	uint16_t System_Side_PRE2_CH0 ;        // RW DSP 500344CC  SRAM Address A8 - A9
	uint16_t System_Side_PRE2_CH1 ;        // RW DSP 501344CC  SRAM Address AA - AB
	uint16_t System_Side_PRE2_CH2 ;        // RW DSP 502344CC  SRAM Address AC - AD
	uint16_t System_Side_PRE2_CH3 ;        // RW DSP 503344CC  SRAM Address AE - AF

	uint16_t System_Side_TX_Polarity_CH0 ; // RW DSP 500345CC  SRAM Address B0 - B1
	uint16_t System_Side_TX_Polarity_CH1 ; // RW DSP 501345CC  SRAM Address B2 - B3
	uint16_t System_Side_TX_Polarity_CH2 ; // RW DSP 502345CC  SRAM Address B4 - B5
	uint16_t System_Side_TX_Polarity_CH3 ; // RW DSP 503345CC  SRAM Address B6 - B7

	uint16_t System_Side_RX_Polarity_CH0 ; // RW DSP 5003458C  SRAM Address B8 - B9
	uint16_t System_Side_RX_Polarity_CH1 ; // RW DSP 5013458C  SRAM Address BA - BB
	uint16_t System_Side_RX_Polarity_CH2 ; // RW DSP 5023458C  SRAM Address BC - BD
	uint16_t System_Side_RX_Polarity_CH3 ; // RW DSP 5033458C  SRAM Address BE - BF

    uint8_t System_side_SNR_MSB_CH0;        // RW DSP           SRAM Address C0
    uint8_t System_side_SNR_LSB_CH0;        // RW DSP           SRAM Address C1
    uint8_t System_side_SNR_MSB_CH1;        // RW DSP           SRAM Address C2
    uint8_t System_side_SNR_LSB_CH1;        // RW DSP           SRAM Address C3
    uint8_t System_side_SNR_MSB_CH2;        // RW DSP           SRAM Address C4
    uint8_t System_side_SNR_LSB_CH2;        // RW DSP           SRAM Address C5
    uint8_t System_side_SNR_MSB_CH3;        // RW DSP           SRAM Address C6
    uint8_t System_side_SNR_LSB_CH3;        // RW DSP           SRAM Address C7
    
	uint8_t System_Side_Buffer[56];        // SRAM Address C8 - FF
};

#endif  /*INC_H_*/
