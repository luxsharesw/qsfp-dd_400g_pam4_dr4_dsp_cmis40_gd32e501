#include "gd32e501.h"
#include "core_cm33.h"
#include "systick.h"
#include "string.h"
#include "CMIS_MSA.h"
#include "GD32E501_M_I2C2_PC78.h"
#include "Calibration_Struct.h"
#include "DSP.h"
#include "DSP_Line_Side.h"
#include "math.h"

uint32_t COMMAND_4700_DATA[18]={0};
uint16_t DSP_LineSide_SNR=0x0000;
uint16_t DSP_LineSide_LTP=0x0000;

uint16_t DEC_EQUAL_TO_HEX(uint16_t Data_Value)
{
    uint16_t Data_Buffer = 0;
    uint16_t Hex_Value = 0;
    // Data_Value Ex:1745 dec will convert to 1745 hex
    // Get Bit12-Bit15 Hex Data
    Data_Buffer = Data_Value/1000 ;
    Hex_Value |= 0x1000*Data_Buffer; 
    
    // Get Bit8-Bit11 Hex Data
    Data_Buffer = 0 ;
    // Remove Thousand value
    Data_Buffer = Data_Value%1000;
    Data_Buffer = Data_Buffer/100;
    Hex_Value |= 0x100*Data_Buffer; 
    
    //Get Bit4-Bit7 Hex Data
    Data_Buffer = 0 ;
    // Remove Hundred value
    Data_Buffer = Data_Value%100;
    Data_Buffer = Data_Buffer/10;
    Hex_Value |= 0x10*Data_Buffer; 
    
    //Get Bit0-Bit3 Hex Data
    Data_Buffer = 0 ;
    // Remove Ten value
    Data_Buffer = Data_Value%10;
    
    Hex_Value|=Data_Buffer;
    
    return Hex_Value;
}

//-----------------------------------------------------------------------------------------------------//
// DSP Line Side TX FIR Setting
//-----------------------------------------------------------------------------------------------------//
void DSP_LineSide_TX_FIR_SET( uint8_t Lane_CH)
{
    uint8_t Level_Shift_Enable=1;
    
    //If Line side optical is NRZ, do not set level shift
    if(Signal_Status==NRZ_to_NRZ)
        Level_Shift_Enable=0;
    
    if(Lane_CH==0)
    {
        if(REV_ID==0xA0)
            COMMAND_4700_DATA[0]  = 0x00000040 ;
        // REV A1
        else
            COMMAND_4700_DATA[0]  = 0x00000044 ;
        
        COMMAND_4700_DATA[1]  = 0x00000002 ;
        COMMAND_4700_DATA[2]  = 0x000000FF | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_TXFIR_TAPS_CH0 << 16;
        //Update Pre2  bit0-15
        //Update Pre1  bit16-31
        COMMAND_4700_DATA[3]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_PRE2_CH0)
                                           | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_PRE1_CH0) << 16;
                                           
        //Update Main  bit0-15
        //Update Post1 bit16-31
        COMMAND_4700_DATA[4]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_Main_CH0) 
                                           | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_POST1_CH0) << 16;
        //Update Post2 bit0-15
        //Update Post3 bit16-31
        COMMAND_4700_DATA[5]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_POST2_CH0)
                                           | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_POST3_CH0) << 16;
        //Update Level Shift Value
        if(Level_Shift_Enable==1)
        {
            if(REV_ID==0xA0)
            {
                COMMAND_4700_DATA[6]  = 0x00000000 | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_00_CH0
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_01_CH0 << 8
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_10_CH0 << 16
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_11_CH0 << 24;
                COMMAND_4700_DATA[7]  = 0x00000101 ;
            }
            // REV A1
            else
            {
                COMMAND_4700_DATA[6]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_TAP6_CH0)
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_00_CH0 << 16
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_01_CH0 << 24 ;
                COMMAND_4700_DATA[7]  = 0x01010000 | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_10_CH0
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_11_CH0 << 8 ;
            }
        }
        //NRZ Mode no change level shift
        else
        {
            COMMAND_4700_DATA[6]  = 0x00000000 ;
            if(REV_ID==0xA0)
                COMMAND_4700_DATA[7]  = 0x00000101 ;
            // REV A1
            else
                COMMAND_4700_DATA[7]  = 0x01010000 ;
        }
        COMMAND_4700_DATA[8]  = 0x00000000 ;
        COMMAND_4700_DATA[9]  = 0x00000000 ;
        COMMAND_4700_DATA[10] = 0x00000000 ;
        COMMAND_4700_DATA[11] = 0x00000000 ;
        COMMAND_4700_DATA[12] = 0x00000000 ;
        COMMAND_4700_DATA[13] = 0x00000000 ;
        COMMAND_4700_DATA[14] = 0x00000000 ;
        COMMAND_4700_DATA[15] = 0x00000000 ;
        COMMAND_4700_DATA[16] = 0x00000000 ;
        COMMAND_4700_DATA[17] = 0x00000000 ;
        
        DSP87400_CAPI_CW_CMD( 0x5201CA98 , 0x00000001 , 0x5201CA94 , 0x00008020 );
    }
    else if(Lane_CH==1)
    {      
        if(REV_ID==0xA0)
            COMMAND_4700_DATA[0]  = 0x00010040 ;
        // REV A1
        else
            COMMAND_4700_DATA[0]  = 0x00010044 ;
        
        COMMAND_4700_DATA[1]  = 0x00000002 ;
        COMMAND_4700_DATA[2]  = 0x000000FF | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_TXFIR_TAPS_CH1 << 16;
        //Update Pre2  bit0-15
        //Update Pre1  bit16-31
        COMMAND_4700_DATA[3]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_PRE2_CH1)
                                           | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_PRE1_CH1) << 16;
                                           
        //Update Main  bit0-15
        //Update Post1 bit16-31
        COMMAND_4700_DATA[4]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_Main_CH1) 
                                           | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_POST1_CH1) << 16;
        //Update Post2 bit0-15
        //Update Post3 bit16-31
        COMMAND_4700_DATA[5]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_POST2_CH1)
                                           | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_POST3_CH1) << 16;
        //Update Level Shift Value
        if(Level_Shift_Enable==1)
        {
            if(REV_ID==0xA0)
            {
                COMMAND_4700_DATA[6]  = 0x00000000 | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_00_CH1
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_01_CH1 << 8
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_10_CH1 << 16
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_11_CH1 << 24;
                COMMAND_4700_DATA[7]  = 0x00000101 ;
            }
            // REV A1
            else
            {
                COMMAND_4700_DATA[6]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_TAP6_CH1)
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_00_CH1 << 16
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_01_CH1 << 24 ;
                COMMAND_4700_DATA[7]  = 0x01010000 | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_10_CH1
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_11_CH1 << 8 ;
            }
        }
        //NRZ Mode no change level shift
        else
        {
            COMMAND_4700_DATA[6]  = 0x00000000 ;
            if(REV_ID==0xA0)
                COMMAND_4700_DATA[7]  = 0x00000101 ;
            // REV A1
            else
                COMMAND_4700_DATA[7]  = 0x01010000 ;
        }
        COMMAND_4700_DATA[8]  = 0x00000000 ;
        COMMAND_4700_DATA[9]  = 0x00000000 ;
        COMMAND_4700_DATA[10] = 0x00000000 ;
        COMMAND_4700_DATA[11] = 0x00000000 ;
        COMMAND_4700_DATA[12] = 0x00000000 ;
        COMMAND_4700_DATA[13] = 0x00000000 ;
        COMMAND_4700_DATA[14] = 0x00000000 ;
        COMMAND_4700_DATA[15] = 0x00000000 ;
        COMMAND_4700_DATA[16] = 0x00000000 ;
        COMMAND_4700_DATA[17] = 0x00000000 ;
        
        DSP87400_CAPI_CW_CMD( 0x5201CA98 , 0x00000002 , 0x5201CA94 , 0x00008020 );
    }
    else if(Lane_CH==2)
    {      
        if(REV_ID==0xA0)
            COMMAND_4700_DATA[0]  = 0x00020040 ;
        // REV A1
        else
            COMMAND_4700_DATA[0]  = 0x00020044 ;
        
        COMMAND_4700_DATA[1]  = 0x00000002 ;
        COMMAND_4700_DATA[2]  = 0x000000FF | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_TXFIR_TAPS_CH2 << 16;
        //Update Pre2  bit0-15
        //Update Pre1  bit16-31
        COMMAND_4700_DATA[3]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_PRE2_CH2)
                                           | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_PRE1_CH2) << 16;
                                           
        //Update Main  bit0-15
        //Update Post1 bit16-31
        COMMAND_4700_DATA[4]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_Main_CH2) 
                                           | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_POST1_CH2) << 16;
        //Update Post2 bit0-15
        //Update Post3 bit16-31
        COMMAND_4700_DATA[5]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_POST2_CH2)
                                           | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_POST3_CH2) << 16;
        //Update Level Shift Value
        if(Level_Shift_Enable==1)
        {
            if(REV_ID==0xA0)
            {
                COMMAND_4700_DATA[6]  = 0x00000000 | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_00_CH2
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_01_CH2 << 8
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_10_CH2 << 16
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_11_CH2 << 24;
                COMMAND_4700_DATA[7]  = 0x00000101 ;
            }
            // REV A1
            else
            {
                COMMAND_4700_DATA[6]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_TAP6_CH2)
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_00_CH2 << 16
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_01_CH2 << 24 ;
                COMMAND_4700_DATA[7]  = 0x01010000 | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_10_CH2
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_11_CH2 << 8 ;
            }
        }
        //NRZ Mode no change level shift
        else
        {
            COMMAND_4700_DATA[6]  = 0x00000000 ;
            if(REV_ID==0xA0)
                COMMAND_4700_DATA[7]  = 0x00000101 ;
            // REV A1
            else
                COMMAND_4700_DATA[7]  = 0x01010000 ;
        }
        COMMAND_4700_DATA[8]  = 0x00000000 ;
        COMMAND_4700_DATA[9]  = 0x00000000 ;
        COMMAND_4700_DATA[10] = 0x00000000 ;
        COMMAND_4700_DATA[11] = 0x00000000 ;
        COMMAND_4700_DATA[12] = 0x00000000 ;
        COMMAND_4700_DATA[13] = 0x00000000 ;
        COMMAND_4700_DATA[14] = 0x00000000 ;
        COMMAND_4700_DATA[15] = 0x00000000 ;
        COMMAND_4700_DATA[16] = 0x00000000 ;
        COMMAND_4700_DATA[17] = 0x00000000 ;
        
        DSP87400_CAPI_CW_CMD( 0x5201CA98 , 0x00000004 , 0x5201CA94 , 0x00008020 );
    }
    else if(Lane_CH==3)
    {      
        if(REV_ID==0xA0)
            COMMAND_4700_DATA[0]  = 0x00030040 ;
        // REV A1
        else
            COMMAND_4700_DATA[0]  = 0x00030044 ;
        
        COMMAND_4700_DATA[1]  = 0x00000002 ;
        COMMAND_4700_DATA[2]  = 0x000000FF | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_TXFIR_TAPS_CH3 << 16;
        //Update Pre2  bit0-15
        //Update Pre1  bit16-31
        COMMAND_4700_DATA[3]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_PRE2_CH3)
                                           | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_PRE1_CH3) << 16;
                                           
        //Update Main  bit0-15
        //Update Post1 bit16-31
        COMMAND_4700_DATA[4]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_Main_CH3) 
                                           | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_POST1_CH3) << 16;
        //Update Post2 bit0-15
        //Update Post3 bit16-31
        COMMAND_4700_DATA[5]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_POST2_CH3)
                                           | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_POST3_CH3) << 16;
        //Update Level Shift Value
        if(Level_Shift_Enable==1)
        {
            if(REV_ID==0xA0)
            {
                COMMAND_4700_DATA[6]  = 0x00000000 | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_00_CH3
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_01_CH3 << 8
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_10_CH3 << 16
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_11_CH3 << 24;
                COMMAND_4700_DATA[7]  = 0x00000101 ;
            }
            // REV A1
            else
            {
                COMMAND_4700_DATA[6]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_TAP6_CH3)
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_00_CH3 << 16
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_01_CH3 << 24 ;
                COMMAND_4700_DATA[7]  = 0x01010000 | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_10_CH3
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_11_CH3 << 8 ;
            }
        }
        //NRZ Mode no change level shift
        else
        {
            COMMAND_4700_DATA[6]  = 0x00000000 ;
            if(REV_ID==0xA0)
                COMMAND_4700_DATA[7]  = 0x00000101 ;
            // REV A1
            else
                COMMAND_4700_DATA[7]  = 0x01010000 ;
        }
        COMMAND_4700_DATA[8]  = 0x00000000 ;
        COMMAND_4700_DATA[9]  = 0x00000000 ;
        COMMAND_4700_DATA[10] = 0x00000000 ;
        COMMAND_4700_DATA[11] = 0x00000000 ;
        COMMAND_4700_DATA[12] = 0x00000000 ;
        COMMAND_4700_DATA[13] = 0x00000000 ;
        COMMAND_4700_DATA[14] = 0x00000000 ;
        COMMAND_4700_DATA[15] = 0x00000000 ;
        COMMAND_4700_DATA[16] = 0x00000000 ;
        COMMAND_4700_DATA[17] = 0x00000000 ;
        
        DSP87400_CAPI_CW_CMD( 0x5201CA98 , 0x00000008 , 0x5201CA94 , 0x00008020 );
    }
    DSP87400_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA94);
    DSP87400_CAPI_Command_response(0x5201CA9C,0x5201CAA0);    
}

//-----------------------------------------------------------------------------------------------------//
// DSP Line Side TRX Polarity
// TRX_Side_SEL = 1  System side Tx
// TRX_Side_SEL = 0  System side Rx
//-----------------------------------------------------------------------------------------------------//
void DSP_LineSide_TRX_Polarity_SET(uint8_t Lane_CH , uint8_t TRX_Side_SEL)
{
    if(Lane_CH==0)
    {
        COMMAND_4700_DATA[0] = 0x00000002;
        
         //Tx Polarity
        if(TRX_Side_SEL)
            COMMAND_4700_DATA[1] = 0xCCCC0001 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_TX_Polarity_CH0);
        //Rx Polarity
        else
            COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_RX_Polarity_CH0);
        
        DSP87400_CAPI_CW_CMD( 0x5201CA98 , 0x00000001 , 0x5201CA94 , 0x00008006 );
    }
    else if(Lane_CH==1)
    {
        COMMAND_4700_DATA[0] = 0x00010002;
        
         //Tx Polarity
        if(TRX_Side_SEL)
            COMMAND_4700_DATA[1] = 0xCCCC0001 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_TX_Polarity_CH1);
        //Rx Polarity
        else
            COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_RX_Polarity_CH1);
        
        DSP87400_CAPI_CW_CMD( 0x5201CA98 , 0x00000002 , 0x5201CA94 , 0x00008006 );       
    }
    else if(Lane_CH==2)
    {
        COMMAND_4700_DATA[0] = 0x00020002;
        
         //Tx Polarity
        if(TRX_Side_SEL)
            COMMAND_4700_DATA[1] = 0xCCCC0001 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_TX_Polarity_CH2);
        //Rx Polarity
        else
            COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_RX_Polarity_CH2);
        
        DSP87400_CAPI_CW_CMD( 0x5201CA98 , 0x00000004 , 0x5201CA94 , 0x00008006 );    
    }
    else if(Lane_CH==3)
    {
        COMMAND_4700_DATA[0] = 0x00030002;
        
         //Tx Polarity
        if(TRX_Side_SEL)
            COMMAND_4700_DATA[1] = 0xCCCC0001 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_TX_Polarity_CH3);
        //Rx Polarity
        else
            COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_RX_Polarity_CH3);
        
        DSP87400_CAPI_CW_CMD( 0x5201CA98 , 0x00000008 , 0x5201CA94 , 0x00008006 );  
    }  
    DSP87400_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA94);
    DSP87400_CAPI_Command_response(0x5201CA9C,0x5201CAA0);
}

//-----------------------------------------------------------------------------------------------------//
// DSP LineSide TRX Squelch
// TRX_Side = 1  TX Squelch ( Moudle Rx ouput )
// TRX_Side = 0  RX Squelch
//-----------------------------------------------------------------------------------------------------//
void DSP_LineSide_TRX_Squelch_SET(uint8_t Lane_CH ,uint8_t TRX_Side,uint8_t Enable)
{  
    if(Lane_CH==0)    
    {
        COMMAND_4700_DATA[0] = 0x00000003 ;
        DSP87400_CAPI_CW_CMD( 0x5201CA98 , 0x00000001 , 0x5201CA94 , 0x0000800F );
    }
    else if(Lane_CH==1)    
    {
        COMMAND_4700_DATA[0] = 0x00010003 ;
        DSP87400_CAPI_CW_CMD( 0x5201CA98 , 0x00000002 , 0x5201CA94 , 0x0000800F );
    }
    else if(Lane_CH==2)   
    {
        COMMAND_4700_DATA[0] = 0x00020003 ;        
        DSP87400_CAPI_CW_CMD( 0x5201CA98 , 0x00000004 , 0x5201CA94 , 0x0000800F );
    }
    else if(Lane_CH==3)   
    {
        COMMAND_4700_DATA[0] = 0x00030003 ;           
        DSP87400_CAPI_CW_CMD( 0x5201CA98 , 0x00000008 , 0x5201CA94 , 0x0000800F );
    }
    else if(Lane_CH==All_CH)   
    {
        COMMAND_4700_DATA[0] = 0x00040003 ;           
        DSP87400_CAPI_CW_CMD( 0x5201CA98 , 0x0000000F , 0x5201CA94 , 0x0000800F );
    }
    //Tx Squelch
    if(TRX_Side)
    {
        if(Enable)
            COMMAND_4700_DATA[1] = 0xCC000101 ;
        else
            COMMAND_4700_DATA[1] = 0xCC000201 ;
    }
    //Rx Squelch
    else
    {
        if(Enable)
            COMMAND_4700_DATA[1] = 0xCC000100 ;
        else
            COMMAND_4700_DATA[1] = 0xCC000200 ;
    }  
    
    DSP87400_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA94);
    DSP87400_CAPI_Command_response(0x5201CA9C,0x5201CAA0);
}

//-----------------------------------------------------------------------------------------------------//
// DSP Line Digital/Remote Loopback function
// LoopBack_mode = 1  Digital Loopback
// LoopBack_mode = 0  Remote Loopbcak
//-----------------------------------------------------------------------------------------------------//
void DSP_LineSide_Loopback_SET(uint8_t LoopBack_mode,uint8_t Lane_CH,uint8_t Enable)
{
    if(Lane_CH==0)
    {
        COMMAND_4700_DATA[0]  = 0x00000008 ;
        DSP87400_CAPI_CW_CMD( 0x5201CA98 , 0x00000001 , 0x5201CA94 , 0x00008014 );
    }
    else if(Lane_CH==1)
    {
        COMMAND_4700_DATA[0]  = 0x00010008 ;
        DSP87400_CAPI_CW_CMD( 0x5201CA98 , 0x00000002 , 0x5201CA94 , 0x00008014 );
    }
    else if(Lane_CH==2)
    {
        COMMAND_4700_DATA[0]  = 0x00020008 ;
        DSP87400_CAPI_CW_CMD( 0x5201CA98 , 0x00000004 , 0x5201CA94 , 0x00008014 );
    }
    else if(Lane_CH==3)
    {
        COMMAND_4700_DATA[0]  = 0x00030008 ;
        DSP87400_CAPI_CW_CMD( 0x5201CA98 , 0x00000008 , 0x5201CA94 , 0x00008014 );
    }
    //Digital Loopback
    if(LoopBack_mode)
    {
        COMMAND_4700_DATA[1] = 0x00000001;
        if(Enable)
            COMMAND_4700_DATA[2]  = 0x00000001 ;
        else
            COMMAND_4700_DATA[2]  = 0x00000000 ;
    }
    //Remote Loopback
    else
    {
        COMMAND_4700_DATA[1] = 0x00000003;
        if(Enable)
            COMMAND_4700_DATA[2]  = 0x00000001 ;
        else
            COMMAND_4700_DATA[2]  = 0x00000000 ;
    }
    COMMAND_4700_DATA[3] = 0x00000000;
    DSP87400_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA94);
    DSP87400_CAPI_Command_response(0x5201CA9C,0x5201CAA0);
}

//-----------------------------------------------------------------------------------------------------//
// DSP Line Side PRBS Info
//-----------------------------------------------------------------------------------------------------//
uint16_t DSP_LineSide_PRBS_Info_GET(uint8_t Lane_CH)
{
    uint16_t Iconnt = 0;
    uint16_t For_count = 7;
    uint32_t Base_Offset = 0;
    uint32_t Lock_Status = 0x00000000;
    uint32_t Lane_Buffer = 0x00000000;
    
    Lane_Buffer = Lane_CH<<16;
    BRCM_Control_READ_Data(0x5201CA94) ;
    
    COMMAND_4700_DATA[0] = 0x00000018 | Lane_Buffer;
    COMMAND_4700_DATA[1] = 0x00000000 ;
    COMMAND_4700_DATA[2] = 0xCCCCCCCC ;
    COMMAND_4700_DATA[3] = 0xCCCCCCCC ;
    COMMAND_4700_DATA[4] = 0xCCCCCCCC ;
    COMMAND_4700_DATA[5] = 0xCCCCCCCC ;
    COMMAND_4700_DATA[6] = 0xCCCCCCCC ;
    
    for(Iconnt=0;Iconnt<=For_count;Iconnt++)
    {
        Base_Offset = ( Iconnt+1 ) ;
        BRCM_Control_WRITE_Data( COMMAND_Base_ADR + ( Base_Offset*4 ) , COMMAND_4700_DATA[Base_Offset] , BRCM_Default_Length);
    }
    //Write Set Lane
    if(Lane_CH==0)
        BRCM_Control_WRITE_Data( 0x5201CA98 , 0x00000001 , BRCM_Default_Length);
    else if(Lane_CH==1)
        BRCM_Control_WRITE_Data( 0x5201CA98 , 0x00000002 , BRCM_Default_Length);
    else if(Lane_CH==2)
        BRCM_Control_WRITE_Data( 0x5201CA98 , 0x00000004 , BRCM_Default_Length);
    else if(Lane_CH==3)
        BRCM_Control_WRITE_Data( 0x5201CA98 , 0x00000008 , BRCM_Default_Length);
    //Write Command ID
    BRCM_Control_WRITE_Data( 0x5201CA94 , 0x0000801D , BRCM_Default_Length);
    //cmd_response_address
    BRCM_Control_READ_Data(0x5201CA9C) ;
    //rsp_lane_mask_address
    BRCM_Control_READ_Data(0x5201CAA0) ;
    //Read Response Data
    BRCM_Control_READ_Data(0x00047400) ;
    BRCM_Control_READ_Data(0x00047404) ;
    Lock_Status = BRCM_Control_READ_Data(0x00047408) ;
    BRCM_Control_READ_Data(0x0004740C) ;
    BRCM_Control_READ_Data(0x00047410) ;
    BRCM_Control_READ_Data(0x00047414) ;
    BRCM_Control_READ_Data(0x00047418) ;
    //intf_capi2fw_command_response
    BRCM_Control_WRITE_Data( 0x5201CA84 , 0x00000000 , BRCM_Default_Length);
    BRCM_Control_WRITE_Data( 0x5201CA94 , 0x00000000 , BRCM_Default_Length);
    BRCM_Control_WRITE_Data( 0x5201CA74 , 0x00000000 , BRCM_Default_Length);
    BRCM_Control_WRITE_Data( 0x5201CA9C , 0x00000000 , BRCM_Default_Length);
    BRCM_Control_WRITE_Data( 0x5201CAA0 , 0x00000000 , BRCM_Default_Length);
    
    return Lock_Status;
}
//----------------------------------------------------------------------------------//
// DSP Line_Side_ALL_CH14_Control_P86
//----------------------------------------------------------------------------------//
void Line_Side_ALL_CH1_CH4_Control_P86()
{
    
    DSP_LineSide_TRX_Polarity_SET( 0 , TX_Side);
    DSP_LineSide_TRX_Polarity_SET( 1 , TX_Side);
    DSP_LineSide_TRX_Polarity_SET( 2 , TX_Side);
    DSP_LineSide_TRX_Polarity_SET( 3 , TX_Side);
    
    DSP_LineSide_TX_FIR_SET(0);
    DSP_LineSide_TX_FIR_SET(1);
    DSP_LineSide_TX_FIR_SET(2);
    DSP_LineSide_TX_FIR_SET(3);
    
    DSP_LineSide_TRX_Polarity_SET( 0 , TX_Side);
    DSP_LineSide_TRX_Polarity_SET( 1 , TX_Side);
    DSP_LineSide_TRX_Polarity_SET( 2 , TX_Side);
    DSP_LineSide_TRX_Polarity_SET( 3 , TX_Side);
    
    DSP_LineSide_TRX_Polarity_SET( 0 , RX_Side);
    DSP_LineSide_TRX_Polarity_SET( 1 , RX_Side);
    DSP_LineSide_TRX_Polarity_SET( 2 , RX_Side);
    DSP_LineSide_TRX_Polarity_SET( 3 , RX_Side);
}

void LineSide_ALL_CH_SQ(uint8_t EN_C)
{
    DSP_LineSide_TRX_Squelch_SET( 0 , RX_Side , EN_C );
    DSP_LineSide_TRX_Squelch_SET( 1 , RX_Side , EN_C );
    DSP_LineSide_TRX_Squelch_SET( 2 , RX_Side , EN_C );
    DSP_LineSide_TRX_Squelch_SET( 3 , RX_Side , EN_C );
}

//----------------------------------------------------------------------------------//
// DSP Line_Side_LOS
//----------------------------------------------------------------------------------//
uint8_t DSP_Line_Side_LOS()
{
    uint16_t Data_Buffer=0x0000;
    uint8_t LOS_STATUS;
    // LOL bit0 - bit7 , Lock:0,nolock:1
    // LOS bit8 - bit15, Lock:0,nolock:1
    // System Side Input
    Data_Buffer=BRCM_Control_READ_Data(0x5201CABC);
    LOS_STATUS=((Data_Buffer&0x0F00)>>8);
    Data_Buffer=BRCM_Control_READ_Data(0x5201CAB4);
    if(Data_Buffer!=0x0F0F)
    {
        BRCM_Control_WRITE_Data( 0x5201CAEC , Data_Buffer , BRCM_Default_Length);
        BRCM_Control_READ_Data(0x5201CABC);
        BRCM_Control_READ_Data(0x5201CAB4);
        BRCM_Control_WRITE_Data( 0x5201CAEC , 0x00000000 , BRCM_Default_Length);
    }
    return LOS_STATUS;
}

//----------------------------------------------------------------------------------//
// DSP Line_Side_LOL
//----------------------------------------------------------------------------------//
uint8_t DSP_Line_Side_LOL()
{
    uint16_t Data_Buffer=0x0000;
    uint8_t LOL_STATUS;

    // CDR Lock bit0 - bit7 , Lock:1,nolock:0
    LOL_STATUS=~(BRCM_Control_READ_Data(0x5201CAB4)&0x0000000F);
    LOL_STATUS&=0x0F;
    return LOL_STATUS;
}

//----------------------------------------------------------------------------------//
// DSP Line_Side_Loopback_Control
//----------------------------------------------------------------------------------//
void DSP_Line_Side_Digital_Loopback_SET(uint8_t Lane_CH,uint8_t Enable)
{   
    //Lane0 to Lane3 Control
    DSP_LineSide_Loopback_SET( Digital_LoopBack_mode , Lane_CH , Enable );
}

void DSP_Line_Side_Remote_Loopback_SET(uint8_t Lane_CH,uint8_t Enable)
{
    //Lane0 to Lane3 Control
    DSP_LineSide_Loopback_SET( Remote_Loopback_mode , Lane_CH , Enable );
}
//----------------------------------------------------------------------------------//
// DSP Line_Side_PRBS_Control
//----------------------------------------------------------------------------------//
void DSP_Line_Side_PRBS_SET(uint8_t Pattern ,uint8_t Lane_CH ,uint8_t Enable)
{
    //Lane Select
    if(Lane_CH==0)
    {
        COMMAND_4700_DATA[0]  = 0x00000030 ;
        DSP87400_CAPI_CW_CMD( 0x5201CA98 , 0x00000001 , 0x5201CA94 , 0x00008016 );
    }
    else if(Lane_CH==1)
    {
        COMMAND_4700_DATA[0]  = 0x00010030 ;
        DSP87400_CAPI_CW_CMD( 0x5201CA98 , 0x00000002 , 0x5201CA94 , 0x00008016 );
    }
    else if(Lane_CH==2)
    {
        COMMAND_4700_DATA[0]  = 0x000200030 ;
        DSP87400_CAPI_CW_CMD( 0x5201CA98 , 0x00000004 , 0x5201CA94 , 0x00008016 );
    }
    else if(Lane_CH==3)
    {
        COMMAND_4700_DATA[0]  = 0x00030030 ;
        DSP87400_CAPI_CW_CMD( 0x5201CA98 , 0x00000008 , 0x5201CA94 , 0x00008016 );
    }
    
    //PRBS Generator
    if(Pattern!=SSPRQ_LineSide)
    {
        COMMAND_4700_DATA[1]  = 0x00000000;
        COMMAND_4700_DATA[2]  = 0x00000001;
        //PRBS Enable
        if(Enable)
            COMMAND_4700_DATA[3]  = 0x00000001;
        else
            COMMAND_4700_DATA[3]  = 0x00000000;
        //PRBS Pattern
        COMMAND_4700_DATA[4] = 0x00000000 | (Pattern<<16);
    }
    //SSPRQ Generator
    else
    {
        COMMAND_4700_DATA[1]  = 0x00000006;
        COMMAND_4700_DATA[2]  = 0x00000001;
        //PRBS Enable
        if(Enable)
            COMMAND_4700_DATA[3]  = 0x00000001;
        else
            COMMAND_4700_DATA[3]  = 0x00000000;
    }
    
    COMMAND_4700_DATA[5] = 0x00000000;
    COMMAND_4700_DATA[6] = 0x00000000;
    COMMAND_4700_DATA[7] = 0x00000000;
    COMMAND_4700_DATA[8] = 0x00000000;
    COMMAND_4700_DATA[9] = 0x00000000;
    COMMAND_4700_DATA[10] = 0x00000000;
    COMMAND_4700_DATA[11] = 0x00000000;
    COMMAND_4700_DATA[12] = 0x00000000;
    
    //Lane0-Lane3
    DSP87400_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA94);
    DSP87400_CAPI_Command_response(0x5201CA9C,0x5201CAA0);
}

void DSP_LineSide_TX_Squelch_SET(uint8_t Lane_CH ,uint8_t Enable)
{
    // CH0-3
    DSP_LineSide_TRX_Squelch_SET(All_CH ,TX_Side, Enable);
}

void DSP_LineSide_RX_Squelch_SET(uint8_t Lane_CH ,uint8_t Enable)
{
    // CH0-3
    DSP_LineSide_TRX_Squelch_SET(All_CH ,RX_Side, Enable);
}

//----------------------------------------------------------------------------------//
// IEEE754 U32 to Float
//----------------------------------------------------------------------------------//
float Binary32ToDouble(uint32_t value)
{
  uint32_t exponent;
  float fraction, result, minus = -1;

  if((value&0x80000000) == 0)
      minus = 1;
  exponent = ((value&0x7F800000)>>23) - 127;
  fraction = (value&0x7FFFFF) + 0x800000;
  fraction = (fraction / 0x800000);
  result = minus * fraction * pow(2, exponent);
  return(result);
}

//----------------------------------------------------------------------------------//
// DSP Line_Side_SNR
//----------------------------------------------------------------------------------//
void DSP_LineSide_SNR_LTP_GET(uint8_t Lane_CH)
{
    uint8_t i, level, v_valid=1;
    uint32_t Address=0x00047004, Data_Buffer, slicer_p_value[4], slicer_v_value[3];
    uint32_t slicer_threshold[3], slicer_target[4], slicer_p_location[4], slicer_v_location[3];
    float slicer_mean_value[4], slicer_sigma_sqr_value[4];
    float snr_lvl[3]={0}, ltp_lvl[3]={0}, snr_min=0, ltp_min = FLT_MAX;
    
    BRCM_Control_READ_Data(0x5201CA94);
    // Write Data size
    BRCM_Control_WRITE_Data( 0x00047000 , (0x0000005C | Lane_CH<<16), BRCM_Default_Length);
    // Set value start 1 to 23
    for(i=0;i<23;i++)
    {
        BRCM_Control_WRITE_Data( Address , 0xCCCCCCCC , BRCM_Default_Length);
        Address+=4;
    }
    // Set Lane
    BRCM_Control_WRITE_Data( 0x5201CA98 , (0x00000001<<Lane_CH) , BRCM_Default_Length);
    // Set Command
    BRCM_Control_WRITE_Data( 0x5201CA94 , 0x00008050 , BRCM_Default_Length);
    // Check feedback status about take 1350ms
    for(i=0;i<100;i++)
    {
        Data_Buffer=BRCM_Control_READ_Data(0x5201CA9C);
        if(Data_Buffer&0x8000)
            break;
        else
            delay_1ms(20);
    }
    // Read Select Lane
    BRCM_Control_READ_Data(0x5201CAA0);
    // Read Command
    BRCM_Control_READ_Data(0x00047400);
    // Get slicer_threshold[0] and slicer_threshold[1] Data
    Data_Buffer=BRCM_Control_READ_Data(0x00047404);
    slicer_threshold[0]=Data_Buffer&0x0000FFFF;
    slicer_threshold[1]=(Data_Buffer&0xFFFF0000)>>16;
    // Get slicer_threshold[2] and slicer_target[0] Data
    Data_Buffer=BRCM_Control_READ_Data(0x00047408);
    slicer_threshold[2]=Data_Buffer&0x0000FFFF;
    slicer_target[0]=(Data_Buffer&0xFFFF0000)>>16;
    // Get data between slicer target[1] and slicer target[3]
    Data_Buffer=BRCM_Control_READ_Data(0x0004740C);
    slicer_target[1]=Data_Buffer&0x0000FFFF;
    slicer_target[2]=(Data_Buffer&0xFFFF0000)>>16;
    Data_Buffer=BRCM_Control_READ_Data(0x00047410);
    slicer_target[3]=Data_Buffer&0x0000FFFF;
    // Get data between slicer_mean_value[0] and slicer_mean_value[3]
    slicer_mean_value[0]=(Binary32ToDouble(BRCM_Control_READ_Data(0x00047414)));
    slicer_mean_value[1]=(Binary32ToDouble(BRCM_Control_READ_Data(0x00047418)));
    slicer_mean_value[2]=(Binary32ToDouble(BRCM_Control_READ_Data(0x0004741C)));
    slicer_mean_value[3]=(Binary32ToDouble(BRCM_Control_READ_Data(0x00047420)));
    // Get data between slicer_sigma_sqr_value[0] and slicer_sigma_sqr_value[3]
    slicer_sigma_sqr_value[0]=(float)sqrt(Binary32ToDouble(BRCM_Control_READ_Data(0x00047424)));
    slicer_sigma_sqr_value[1]=(float)sqrt(Binary32ToDouble(BRCM_Control_READ_Data(0x00047428)));
    slicer_sigma_sqr_value[2]=(float)sqrt(Binary32ToDouble(BRCM_Control_READ_Data(0x0004742C)));
    slicer_sigma_sqr_value[3]=(float)sqrt(Binary32ToDouble(BRCM_Control_READ_Data(0x00047430)));
    // Get data between slicer_p_value[0] and slicer_p_value[3]
    slicer_p_value[0]=BRCM_Control_READ_Data(0x00047434);
    slicer_p_value[1]=BRCM_Control_READ_Data(0x00047438);
    slicer_p_value[2]=BRCM_Control_READ_Data(0x0004743C);
    slicer_p_value[3]=BRCM_Control_READ_Data(0x00047440);
    // Get slicer_p_location[0] and slicer_p_location[1] Data
    Data_Buffer=BRCM_Control_READ_Data(0x00047444);
    slicer_p_location[0]=Data_Buffer&0x0000FFFF;
    slicer_p_location[1]=(Data_Buffer&0xFFFF0000)>>16;
    // Get slicer_p_location[2] and slicer_p_location[3] Data
    Data_Buffer=BRCM_Control_READ_Data(0x00047448);
    slicer_p_location[2]=Data_Buffer&0x0000FFFF;
    slicer_p_location[3]=(Data_Buffer&0xFFFF0000)>>16;
    // Get data between slicer_v_value[0] and slicer_v_value[2]
    slicer_v_value[0]=BRCM_Control_READ_Data(0x0004744C);
    slicer_v_value[1]=BRCM_Control_READ_Data(0x00047050);
    slicer_v_value[2]=BRCM_Control_READ_Data(0x00047054);
    // Get data between slicer_v_location[0] and slicer_v_location[2]
    Data_Buffer=BRCM_Control_READ_Data(0x00047458);
    slicer_v_location[0]=Data_Buffer&0x0000FFFF;
    slicer_v_location[1]=(Data_Buffer&0xFFFF0000)>>16;
    Data_Buffer=BRCM_Control_READ_Data(0x0004745C);
    slicer_v_location[2]=Data_Buffer&0x0000FFFF;
    // Clear
    BRCM_Control_WRITE_Data( 0x5201CA84 , 0x00000000 , BRCM_Default_Length);
    BRCM_Control_WRITE_Data( 0x5201CA94 , 0x00000000 , BRCM_Default_Length);
    BRCM_Control_WRITE_Data( 0x5201CA74 , 0x00000000 , BRCM_Default_Length);
    BRCM_Control_WRITE_Data( 0x5201CAA4 , 0x00000000 , BRCM_Default_Length);
    BRCM_Control_WRITE_Data( 0x5201CA9C , 0x00000000 , BRCM_Default_Length);
    BRCM_Control_WRITE_Data( 0x5201CAA0 , 0x00000000 , BRCM_Default_Length);
    /* SNR = 10* log10(min{SNR0, SNR1, SNR2 }) where SNRi= (mean(i+1)- mean(i))/( sigma(i+1) +sigma(i)), expressed in 1/256 dB units
       LTP = 10* log10(min{LTP0, LTP1, LTP2 }) where LTPi= (P(i+1) + P(i))/( 2V(i)), expressed in 1/256 dB units*/
    for(level = 0; level < 3; level++)
	{
         if((slicer_sigma_sqr_value[level]+slicer_sigma_sqr_value[level+1])>0)
		 {
            snr_lvl[level] = (float)((slicer_mean_value[level+1]-slicer_mean_value[level])/
                                     (slicer_sigma_sqr_value[level+1]+slicer_sigma_sqr_value[level]));
            snr_min = (level==0)?snr_lvl[0]:((snr_lvl[level]<snr_min)?snr_lvl[level]:snr_min);
         }
         /*
         ltp_lvl[level] = 0xFFFF;
         if(slicer_v_value[level])
         {
             ltp_lvl[level] = (float)((slicer_p_value[level+1]+slicer_p_value[level])/2/slicer_v_value[level]);
             ltp_min = (level==0)?ltp_lvl[0]:((ltp_lvl[level]<ltp_min)?ltp_lvl[level]:ltp_min);
         }
		 else
             v_valid&=0;
         */
    }
    if (snr_min <= 0)
        snr_min = (float)1.0;
    DSP_LineSide_SNR = (float)((log10(snr_min)*2560)+0.5);
    //DSP_LineSide_LTP = (v_valid==0)?0xFFFF:((float)((log10(ltp_min)*2560)+0.5));
    DSP_LineSide_SNR=DEC_EQUAL_TO_HEX(DSP_LineSide_SNR);
}