#include "gd32e501.h"
#include "core_cm33.h"
#include "systick.h"
#include "string.h"
#include "CMIS_MSA.h"
#include "GD32E501_M_I2C2_PC78.h"
#include "DSP.h"
#include "DSP_Line_Side.h"
#include "DSP_System_Side.h"
#include "Calibration_Struct.h"

uint32_t INTF_CAPI_SW_CMD_ADR0    = 0x5201CA78;
uint32_t INTF_CAPI_SW_CMD_DATA0   = 0x0000800F;
uint32_t INTF_CAPI_SW_CMD_ADR1    = 0x5201CA74;
uint32_t INTF_CAPI_SW_CMD_DATA1   = 0x00008008;

struct DSP_Direct_Control_MEMORY DSP_Direct_Control_MEMORY_MAP;
struct DSP_Line_Side_PHY0_MEMORY DSP_Line_Side_PHY0_MEMORY_MAP;
struct DSP_System_Side_PHY0_MEMORY DSP_System_Side_PHY0_MEMORY_MAP;
struct DSP_Line_Side_PHY1_MEMORY DSP_Line_Side_PHY1_MEMORY_MAP;
struct DSP_System_Side_PHY1_MEMORY DSP_System_Side_PHY1_MEMORY_MAP;

uint32_t First_Init_Flag=0;
uint8_t REV_ID=0;
uint8_t DSP_INIT_Flag=0;

// Default Voltage Value
uint16_t Bef_0V75 = 0x02EE;
uint16_t Bef_0V90 = 0x0384;
uint8_t DSP_VCC_Flag = 0;

void BRCM_Control_WRITE_Data( uint32_t BRCM_ADDR , uint32_t Data_Value , uint16_t Data_Length)
{
	uint8_t ADDR_Byte3,ADDR_Byte2,ADDR_Byte1,ADDR_Byte0;
	uint8_t Write_data_buffer_0,Write_data_buffer_1,Write_data_buffer_2,Write_data_buffer_3;
	uint8_t LEN_LSB,LEN_MSB ;
    
	ADDR_Byte0 = BRCM_ADDR ;
	ADDR_Byte1 = BRCM_ADDR>>8 ;
	ADDR_Byte2 = BRCM_ADDR>>16 ;
	ADDR_Byte3 = BRCM_ADDR>>24 ;

	LEN_LSB = ( Data_Length & 0xFF ) ;
	LEN_MSB = ( ( Data_Length >> 8 ) & 0xFF ) ;

	Write_data_buffer_0 = Data_Value ;
	Write_data_buffer_1 = Data_Value >> 8 ;
    Write_data_buffer_2 = Data_Value >> 16 ;
    Write_data_buffer_3 = Data_Value >> 24 ;
    
	// Step1 : Write 0xFF to QSFP PAGE SELECT Control register
	Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_PAGE_SELECT , 0xFF );
	// Step2 : Write INDADDR0 - INDADDR3
	Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_IND_ADDR0 , ADDR_Byte0 );
	Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_IND_ADDR1 , ADDR_Byte1 );
	Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_IND_ADDR2 , ADDR_Byte2 );
	Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_IND_ADDR3 , ADDR_Byte3 );
	// Step3 : Write Data Length
	Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_IND_LEN0 , LEN_LSB );
	Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_IND_LEN1 , LEN_MSB );
	// Step4 : Write Data to WFIFO 0x03 write command
	Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_IND_CTRL , 0x03 );
	// Step5 : Write Data
	Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_WFIFO , Write_data_buffer_0 );
	Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_WFIFO , Write_data_buffer_1 );
	Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_WFIFO , Write_data_buffer_2 );
	Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_WFIFO , Write_data_buffer_3 );   
}

uint32_t BRCM_Control_READ_Data( uint32_t BRCM_ADDR)
{
	uint8_t ADDR_Byte3,ADDR_Byte2,ADDR_Byte1,ADDR_Byte0;
	uint8_t IIcount = 0;
	uint8_t Read_data_buffer_0,Read_data_buffer_1,Read_data_buffer_2,Read_data_buffer_3;
	uint32_t  return_DATA = 0x00000000 ;
	uint8_t TEMP_BUUFFER;
    
	ADDR_Byte0 = BRCM_ADDR ;
	ADDR_Byte1 = BRCM_ADDR>>8 ;
	ADDR_Byte2 = BRCM_ADDR>>16 ;
	ADDR_Byte3 = BRCM_ADDR>>24 ;
    
	// Step1 : Write 0xFF to QSFP PAGE SELECT Control register
	Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_PAGE_SELECT , 0xFF );
	// Step2 : Write INDADDR0 - INDADDR3
	Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_IND_ADDR0 , ADDR_Byte0 );
	Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_IND_ADDR1 , ADDR_Byte1 );
	Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_IND_ADDR2 , ADDR_Byte2 );
	Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_IND_ADDR3 , ADDR_Byte3 );
	// Step3 : Write Data Length
	Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_IND_LEN0 , 4 );
	Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_IND_LEN1 , 0 );
	// Step4 : Write Data to WFIFO 0x03 write command
	Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_IND_CTRL , 0x01 );
	// Step5 : Write Data
	Read_data_buffer_0 = Master_I2C2_ByteREAD_PC78( DSP_Slave_Address , BRCM_RFIFO );
	Read_data_buffer_1 = Master_I2C2_ByteREAD_PC78( DSP_Slave_Address , BRCM_RFIFO );
	Read_data_buffer_2 = Master_I2C2_ByteREAD_PC78( DSP_Slave_Address , BRCM_RFIFO );
	Read_data_buffer_3 = Master_I2C2_ByteREAD_PC78( DSP_Slave_Address , BRCM_RFIFO );
    
    return_DATA |= Read_data_buffer_0 ;
    return_DATA |= Read_data_buffer_1 <<8 ;
    return_DATA |= Read_data_buffer_2 <<16 ;
    return_DATA |= Read_data_buffer_3 <<24 ;
    
	return return_DATA;
}

void BRCM_WRITE_Data( uint8_t *BRCM_ADDR , uint8_t *DataBuffer , uint8_t Data_Length)
{
	uint8_t ADDR_Byte3,ADDR_Byte2,ADDR_Byte1,ADDR_Byte0;
	uint8_t LEN_LSB,LEN_MSB ;
	uint8_t IIcount ;
    
    // GD MCU MSB & LSB Swap
	ADDR_Byte3 = *BRCM_ADDR++ ; 
	ADDR_Byte2 = *BRCM_ADDR++ ; 
	ADDR_Byte1 = *BRCM_ADDR++ ; 
	ADDR_Byte0 = *BRCM_ADDR++ ; 
    
    LEN_LSB = Data_Length ;
    LEN_MSB = 0x00 ;
	//LEN_LSB = ( Data_Length & 0xFF ) ;
	//LEN_MSB = ( ( Data_Length >> 8 ) & 0xFF ) ;

	// Step1 : Write 0xFF to QSFP PAGE SELECT Control register
	Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_PAGE_SELECT , 0xFF );
	// Step2 : Write INDADDR0 - INDADDR3
	Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_IND_ADDR0 , ADDR_Byte0 );
	Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_IND_ADDR1 , ADDR_Byte1 );
	Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_IND_ADDR2 , ADDR_Byte2 );
	Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_IND_ADDR3 , ADDR_Byte3 );
	// Step3 : Write Data Length
	Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_IND_LEN0 , LEN_LSB );
	Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_IND_LEN1 , LEN_MSB );
	// Step4 : Write Data to WFIFO 0x03 write command
	Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_IND_CTRL , 0x03 );
    
	for(IIcount=0;IIcount<LEN_LSB;IIcount++)
        Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_WFIFO , *DataBuffer++ );
}
 
void BRCM_READ_Data( uint8_t *BRCM_ADDR , uint8_t *DataBuffer , uint8_t Data_Length)
{
	uint8_t ADDR_Byte3,ADDR_Byte2,ADDR_Byte1,ADDR_Byte0;
	uint8_t LEN_LSB,LEN_MSB ;
	uint8_t IIcount = 0 , Count = 0 ;
    
    // GD MCU MSB & LSB Swap
	ADDR_Byte3 = *BRCM_ADDR++ ; 
	ADDR_Byte2 = *BRCM_ADDR++ ; 
	ADDR_Byte1 = *BRCM_ADDR++ ; 
	ADDR_Byte0 = *BRCM_ADDR++ ; 

    LEN_LSB = Data_Length ;
    LEN_MSB = 0x00 ;
	//LEN_LSB = ( Data_Length & 0xFF ) ;
	//LEN_MSB = ( ( Data_Length >> 8 ) & 0xFF ) ;

	// Step1 : Write 0xFF to QSFP PAGE SELECT Control register
	Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_PAGE_SELECT , 0xFF );
	// Step2 : Write INDADDR0 - INDADDR3
	Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_IND_ADDR0 , ADDR_Byte0 );
	Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_IND_ADDR1 , ADDR_Byte1 );
	Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_IND_ADDR2 , ADDR_Byte2 );
	Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_IND_ADDR3 , ADDR_Byte3 );
	// Step3 : Write Data Length
	Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_IND_LEN0 , LEN_LSB );
	Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_IND_LEN1 , LEN_MSB );
	// Step4 : Write Data to WFIFO 0x01 READ command
	Master_I2C2_ByteWrite_PC78( DSP_Slave_Address , BRCM_IND_CTRL , 0x01 );
        
	for(IIcount=0;IIcount<LEN_LSB;IIcount++)
       *DataBuffer++ = Master_I2C2_ByteREAD_PC78( DSP_Slave_Address , BRCM_RFIFO );      
}

//--------------------------------Chip Function--------------------------------------------------------//
//-----------------------------------------------------------------------------------------------------//
// DSP87400 Chip Status uc Ready/Avs Done/SPI_Dev_ready
//-----------------------------------------------------------------------------------------------------//

uint32_t DSP87400_EEPROM_AVS_UcReady_CHECK()
{
	uint16_t  JJ_Count = 0 ;
	uint16_t  II_Count = 0 ;
    uint32_t  DataBuffer = 0 ,Result=0;

    while(1)
    {
        DataBuffer = BRCM_Control_READ_Data(0x5201D470);
        // AVS & EEPROM Download & uMIC Ready value is 0x0F
        if( DataBuffer == 0x0F )
        {
            Result = 1;
            break;
        }
        else
        {
            JJ_Count++;
            delay_1ms(10);

            if( JJ_Count >= 300 )
            {
                JJ_Count = 0 ;
                break;
            }
        }
    }
    return Result;
}

//-----------------------------------------------------------------------------------------------------//
// DSP87400_CAPI_CW_CMD
//-----------------------------------------------------------------------------------------------------//
void DSP87400_CAPI_CW_CMD(uint32_t INTF_ADR0,uint32_t INTF_DATA0,uint32_t INTF_ADR1,uint32_t INTF_DATA1)
{
    INTF_CAPI_SW_CMD_ADR0 = INTF_ADR0 ;
    INTF_CAPI_SW_CMD_DATA0 = INTF_DATA0 ;
    INTF_CAPI_SW_CMD_ADR1 = INTF_ADR1 ;
    INTF_CAPI_SW_CMD_DATA1 = INTF_DATA1 ;
}

void DSP87400_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(uint32_t Read_ADDRESS)
{
    uint32_t Read_Buffer = 0;
    uint16_t Iconnt = 0;
    uint16_t For_count = 0;
    uint32_t Base_Offset = 0;
    uint16_t DataSize_Buffer =0;
    
    Read_Buffer = BRCM_Control_READ_Data(Read_ADDRESS);
    
    DataSize_Buffer = COMMAND_4700_DATA[0] ;
    For_count = DataSize_Buffer/4 ;
    
    if(For_count>=1)
        For_count = For_count - 1;
    // Step 1 : set data size and command
    //data size                      Bit0-15 : capi_config_info_t
    //intf_util_assign_tocken Bit16-31: This utility incrementally generate a new token
    BRCM_Control_WRITE_Data( COMMAND_Base_ADR , COMMAND_4700_DATA[0] , BRCM_Default_Length);
    //ref_clk CAPI_REF_CLK_FRQ_156_25_MHZ_ETHERNET
    //func_mode Bit16-31: CAPI_MODE_200G     
    for(Iconnt=0;Iconnt<=For_count;Iconnt++)
    {
        Base_Offset = ( Iconnt+1 ) ;
        BRCM_Control_WRITE_Data( COMMAND_Base_ADR + ( Base_Offset*4 ) , COMMAND_4700_DATA[Base_Offset] , BRCM_Default_Length);
    }
    //INTF_CAPI2FW_CW_CMD_LANE_MASK_GPREG
    BRCM_Control_WRITE_Data( INTF_CAPI_SW_CMD_ADR0 , INTF_CAPI_SW_CMD_DATA0 , BRCM_Default_Length);
    //INTF_CAPI2FW_CW_CMD_REQUEST_GPREG
    BRCM_Control_WRITE_Data( INTF_CAPI_SW_CMD_ADR1 , INTF_CAPI_SW_CMD_DATA1 , BRCM_Default_Length);
}

//-----------------------------------------------------------------------------------------------------//
// DSP87400 intf_capi2fw_command_response
//-----------------------------------------------------------------------------------------------------//
void DSP87400_CAPI_Command_response(uint32_t CHK_ADR0,uint32_t CHK_ADR1)
{
    uint32_t Read_Buffer;
    uint32_t IIcount = 0 ;
    uint32_t Time_out_count = 20;
    // Check whether the FW has processed the command 
    // INTF_CAPI2FW_CW_CMD_RESPONSE_GPREG = 0x00008000
    while(1)
    {
        Read_Buffer = BRCM_Control_READ_Data(CHK_ADR0);      
        if(Read_Buffer==0x00008000)
            break;
        else
        {
            for(IIcount=0;IIcount<10000;IIcount++);
            
            if((Time_out_count--) == 0)
                break;
        }       
    }    
    // This is optional since the tocken verification is already completed 
    // INTF_CAPI2FW_CW_RSP_LANE_MASK_GPREG = 0x0000800F
    Read_Buffer = BRCM_Control_READ_Data(CHK_ADR1);
    
    // clear register data 
    BRCM_Control_WRITE_Data( 0x5201CA84 , 0x00000000 , BRCM_Default_Length);
    BRCM_Control_WRITE_Data( 0x5201CA94 , 0x00000000 , BRCM_Default_Length);
    BRCM_Control_WRITE_Data( 0x5201CA74 , 0x00000000 , BRCM_Default_Length);
   	BRCM_Control_WRITE_Data( 0x5201CAA4 , 0x00000000 , BRCM_Default_Length);
    BRCM_Control_WRITE_Data( CHK_ADR0   , 0x00000000 , BRCM_Default_Length);
    BRCM_Control_WRITE_Data( CHK_ADR1   , 0x00000000 , BRCM_Default_Length); 
}

//-----------------------------------------------------------------------------------------------------//
// DSP87400 line_lane_cfg_done
//-----------------------------------------------------------------------------------------------------//
void DSP87400_line_lane_cfg_done()
{   
    if(DSP_MODE_SET==Mode1_CHIP_MODE_2X53G_KP4PAM_TO_1X106G_KP4PAM)
    {
        BRCM_Control_READ_Data(0x5201CD68);    
        BRCM_Control_READ_Data(0x5201CB30);
        BRCM_Control_READ_Data(0x5201CB34);
        BRCM_Control_READ_Data(0x5201CB38);
        BRCM_Control_READ_Data(0x5201CB3C);    
    }
}
//-----------------------------------------------------------------------------------------------------//
// DSP87400 host_lane_cfg_done
//-----------------------------------------------------------------------------------------------------//
void DSP87400_host_lane_cfg_done()
{
    BRCM_Control_READ_Data(0x5201CB60);
    BRCM_Control_READ_Data(0x5201CB64);
    BRCM_Control_READ_Data(0x5201CB68);
    BRCM_Control_READ_Data(0x5201CB6C);  
    BRCM_Control_READ_Data(0x5201CB70);
    BRCM_Control_READ_Data(0x5201CB74);
    BRCM_Control_READ_Data(0x5201CB78);
    BRCM_Control_READ_Data(0x5201CB7C);
}
//-----------------------------------------------------------------------------------------------------//
// DSP87400 line_cfg_st_lane_cw_cfg_rdy
//-----------------------------------------------------------------------------------------------------//
void DSP87400_line_cfg_st_lane_cw_cfg_rdy()
{
    BRCM_Control_READ_Data(0x5201D464);
    BRCM_Control_READ_Data(0x5201D464);
    BRCM_Control_READ_Data(0x5201D464);
    BRCM_Control_READ_Data(0x5201D464);
}
//-----------------------------------------------------------------------------------------------------//
// DSP87400 host_cfg_st_lane_cw_cfg_rdy
//-----------------------------------------------------------------------------------------------------//
void DSP87400_host_cfg_st_lane_cw_cfg_rdy()
{
    BRCM_Control_READ_Data(0x5201D464);
    BRCM_Control_READ_Data(0x5201D464);
    BRCM_Control_READ_Data(0x5201D464);
    BRCM_Control_READ_Data(0x5201D464);
    BRCM_Control_READ_Data(0x5201D464);
    BRCM_Control_READ_Data(0x5201D464);
    BRCM_Control_READ_Data(0x5201D464);
    BRCM_Control_READ_Data(0x5201D464);
}

//-----------------------------------------------------------------------------------------------------//
// DSP Internal Voltage Control
//-----------------------------------------------------------------------------------------------------//
void DSP_SET_0V75()
{
    uint32_t Read_Buffer = 0;
    uint16_t Data_Buffer = 0x0000;
    
    Data_Buffer=Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.DSP_0V75);
    if(Bef_0V75!=Data_Buffer)
    {
        //Default Voltage
        if(Data_Buffer==0)
            Data_Buffer=750;
        else if(Data_Buffer>1000)
            Data_Buffer=750;
        //calulate voltage
        Data_Buffer=((((Data_Buffer*1000)-500000)/3125)&0xFF)<<8;
            
        //* Do a read modify write to regC *///
        BRCM_Control_WRITE_Data( 0x4000B000 , 0X00000286 , BRCM_Default_Length );
        BRCM_Control_WRITE_Data( 0x4000B004 , 0x61320000 , BRCM_Default_Length );
        Read_Buffer=BRCM_Control_READ_Data(0x4000B000);
        Read_Buffer=BRCM_Control_READ_Data(0x4000B000);
        Read_Buffer=BRCM_Control_READ_Data(0x4000B004);
        
        BRCM_Control_WRITE_Data( 0x4000B000 , 0X00000286 , BRCM_Default_Length );
        BRCM_Control_WRITE_Data( 0x4000B004 ,(0x51320000|Data_Buffer), BRCM_Default_Length );
        Read_Buffer=BRCM_Control_READ_Data(0x4000B000);
        Read_Buffer=BRCM_Control_READ_Data(0x4000B000);
        
        //* Toggle by writing 1 to reg0[1] and then 0 to complete the transaction *//
        BRCM_Control_WRITE_Data( 0x4000B000 , 0X00000286 , BRCM_Default_Length );
        BRCM_Control_WRITE_Data( 0x4000B004 , 0x61020000 , BRCM_Default_Length );
        Read_Buffer=BRCM_Control_READ_Data(0x4000B000);
        Read_Buffer=BRCM_Control_READ_Data(0x4000B000);
        Read_Buffer=BRCM_Control_READ_Data(0x4000B004)|0x00000002;
        
        BRCM_Control_WRITE_Data( 0x4000B000 , 0X00000286 , BRCM_Default_Length );
        BRCM_Control_WRITE_Data( 0x4000B004 ,(0x51020000|Read_Buffer), BRCM_Default_Length );
        Read_Buffer=BRCM_Control_READ_Data(0x4000B000);
        Read_Buffer=BRCM_Control_READ_Data(0x4000B000);
        
        BRCM_Control_WRITE_Data( 0x4000B000 , 0X00000286 , BRCM_Default_Length );
        BRCM_Control_WRITE_Data( 0x4000B004 , 0x61020000 , BRCM_Default_Length );
        Read_Buffer=BRCM_Control_READ_Data(0x4000B000);
        Read_Buffer=BRCM_Control_READ_Data(0x4000B000);
        Read_Buffer=BRCM_Control_READ_Data(0x4000B004)&0x0000FFFD;
        
        BRCM_Control_WRITE_Data( 0x4000B000 , 0X00000286 , BRCM_Default_Length );
        BRCM_Control_WRITE_Data( 0x4000B004 ,(0x51020000|Read_Buffer), BRCM_Default_Length );
        Read_Buffer=BRCM_Control_READ_Data(0x4000B000);
        Read_Buffer=BRCM_Control_READ_Data(0x4000B000);
        
        BRCM_Control_WRITE_Data( 0x4000B000 , 0X00000286 , BRCM_Default_Length );
        BRCM_Control_WRITE_Data( 0x4000B004 , 0x61320000 , BRCM_Default_Length );
        Read_Buffer=BRCM_Control_READ_Data(0x4000B000);
        Read_Buffer=BRCM_Control_READ_Data(0x4000B000);
        Read_Buffer=BRCM_Control_READ_Data(0x4000B004);
        Bef_0V75=Data_Buffer;
    }
}

void DSP_SET_0V90()
{
    uint32_t Read_Buffer = 0;
    uint16_t Data_Buffer = 0x0000;
    Data_Buffer=Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.DSP_0V90);
    if(Bef_0V90!=Data_Buffer)
    {
        //Default Voltage
        if(Data_Buffer==0)
            Data_Buffer=900;
        else if(Data_Buffer>1100)
            Data_Buffer=900;
        
        //calulate voltage
        Data_Buffer=((((Data_Buffer*1000)-500000)/3125)&0xFF)<<8;
        
        //* Do a read modify write to regC *///
        BRCM_Control_WRITE_Data( 0x4000B000 , 0X00000286 , BRCM_Default_Length );
        BRCM_Control_WRITE_Data( 0x4000B004 , 0x62320000 , BRCM_Default_Length );
        Read_Buffer=BRCM_Control_READ_Data(0x4000B000);
        Read_Buffer=BRCM_Control_READ_Data(0x4000B000);
        Read_Buffer=BRCM_Control_READ_Data(0x4000B004);
        
        BRCM_Control_WRITE_Data( 0x4000B000 , 0X00000286 , BRCM_Default_Length );
        BRCM_Control_WRITE_Data( 0x4000B004 ,(0x52320000|Data_Buffer), BRCM_Default_Length );
        Read_Buffer=BRCM_Control_READ_Data(0x4000B000);
        Read_Buffer=BRCM_Control_READ_Data(0x4000B000);
        
        //* Toggle by writing 1 to reg0[1] and then 0 to complete the transaction *//
        BRCM_Control_WRITE_Data( 0x4000B000 , 0X00000286 , BRCM_Default_Length );
        BRCM_Control_WRITE_Data( 0x4000B004 , 0x62020000 , BRCM_Default_Length );
        Read_Buffer=BRCM_Control_READ_Data(0x4000B000);
        Read_Buffer=BRCM_Control_READ_Data(0x4000B000);
        Read_Buffer=BRCM_Control_READ_Data(0x4000B004)|0x00000002;
        
        BRCM_Control_WRITE_Data( 0x4000B000 , 0X00000286 , BRCM_Default_Length );
        BRCM_Control_WRITE_Data( 0x4000B004 ,(0x52020000|Read_Buffer), BRCM_Default_Length );
        Read_Buffer=BRCM_Control_READ_Data(0x4000B000);
        Read_Buffer=BRCM_Control_READ_Data(0x4000B000);
        
        BRCM_Control_WRITE_Data( 0x4000B000 , 0X00000286 , BRCM_Default_Length );
        BRCM_Control_WRITE_Data( 0x4000B004 , 0x62020000 , BRCM_Default_Length );
        Read_Buffer=BRCM_Control_READ_Data(0x4000B000);
        Read_Buffer=BRCM_Control_READ_Data(0x4000B000);
        Read_Buffer=BRCM_Control_READ_Data(0x4000B004)&0x0000FFFD;
        
        BRCM_Control_WRITE_Data( 0x4000B000 , 0X00000286 , BRCM_Default_Length );
        BRCM_Control_WRITE_Data( 0x4000B004 ,(0x52020000|Read_Buffer), BRCM_Default_Length );
        Read_Buffer=BRCM_Control_READ_Data(0x4000B000);
        Read_Buffer=BRCM_Control_READ_Data(0x4000B000);
        
        BRCM_Control_WRITE_Data( 0x4000B000 , 0X00000286 , BRCM_Default_Length );
        BRCM_Control_WRITE_Data( 0x4000B004 , 0x62320000 , BRCM_Default_Length );
        Read_Buffer=BRCM_Control_READ_Data(0x4000B000);
        Read_Buffer=BRCM_Control_READ_Data(0x4000B000);
        Read_Buffer=BRCM_Control_READ_Data(0x4000B004);
        Bef_0V90=Data_Buffer;
    }
}

//-----------------------------------------------------------------------------------------------------------------//
//------------------------------------------------- API------------------------------------------------------------//
//-----------------------------------------------------------------------------------------------------------------//

//----------------------------------------------------------------------------------//
// Trigger_CMD_Update_DSP_REG
//----------------------------------------------------------------------------------//
void Trigger_CMD_Update_DSP_REG()
{
	Line_Side_ALL_CH1_CH4_Control_P86();
	System_Side_ALL_CH1_CH4_Control_P87();
	System_Side_ALL_CH1_CH4_Control_P8E();
    DSP_SET_0V75();
    DSP_SET_0V90();
    DSP_PLL_BANDWIDTH_CONFIG(Normal_Bandwidth);
}

void TEST_DSP_Command_Direct_Control()
{
    uint32_t Read_temp_buffer ;
	if( DSP_Direct_Control_MEMORY_MAP.RW_EN == 0x01 )
	{
		if( DSP_Direct_Control_MEMORY_MAP.Direct_Control == 0x00 )
            BRCM_WRITE_Data( &DSP_Direct_Control_MEMORY_MAP.BRCM_REG_ADDR_0 , &DSP_Direct_Control_MEMORY_MAP.WRITE_BUFFER_0 , DSP_Direct_Control_MEMORY_MAP.LENGITH_LSB);
		else if( DSP_Direct_Control_MEMORY_MAP.Direct_Control == 0x01 )
            BRCM_READ_Data( &DSP_Direct_Control_MEMORY_MAP.BRCM_REG_ADDR_0 , &DSP_Direct_Control_MEMORY_MAP.READ_BUFFER_0 , DSP_Direct_Control_MEMORY_MAP.LENGITH_LSB);

		DSP_Direct_Control_MEMORY_MAP.RW_EN = 0x00 ;
	}
}

uint32_t DSP_GET_AVS_STATUS()
{
    uint32_t Data_Buffer,Result=0,i;
    
    for(i=0;i<100;i++)
    {
        Data_Buffer=BRCM_Control_READ_Data(0x5201D408);
        // 0x2000 mean init success, 0x0001 mean avs enable
        if(Data_Buffer&0x2001)
        {
            Result=1;
            break;
        }
        else
            delay_1ms(1);
    }
    return Result;
}

uint32_t DSP_GET_DOWNLOAD_STATUS()
{
    uint32_t Data_Buffer,Result=0,i;
    
    for(i=0;i<100;i++)
    {
        // Get FW Version 0xD01C
        Data_Buffer=BRCM_Control_READ_Data(0x5201C810);
        if(Data_Buffer>=0xD107)
            Result|=0x01;
        // Get FW Version Minor 0x0000
        BRCM_Control_READ_Data(0x5201C814);
        if(Data_Buffer==0x0000)
            Result|=0x02;
        // First Download Status must equal 0x600D
        BRCM_Control_READ_Data(0x5201CDB8);
        if(Data_Buffer==0x600D)
            Result|=0x04;
        // Get CRC32 0x600D
        BRCM_Control_READ_Data(0x5201CAF4);
        if(Data_Buffer==0x600D)
            Result|=0x08;
        if(Result==0x0F)
        {
            Result=1;
            break;
        }
        else
            delay_1ms(1);
    }
    return Result;
}
void DSP_GET_REV_ID()
{
    uint32_t i;
    
    for(i=0;i<50;i++)
    {
        REV_ID=BRCM_Control_READ_Data(0x5201D004)>>4;
        if(REV_ID>=0xA0)
            break;
        else
            delay_1ms(1);
    }
}
//----------------------------------------------------------------------------------//
// DSP87400 SW RESET 
//----------------------------------------------------------------------------------//
void DSP_SW_RESET()
{
    uint32_t Data_Buffer=0x00000000;
    uint16_t Iconnt = 0;
    uint16_t For_count = 128;
    
    // REG_RDB_BOOT_EN_CTRL
    BRCM_Control_READ_Data(0x5201DB04);
    BRCM_Control_WRITE_Data( 0x5201DB04 , 0xFFFFFFFD , BRCM_Default_Length);
    BRCM_Control_READ_Data(0x5201DB04);
    BRCM_Control_WRITE_Data( 0x5201DB04 , 0xFFFFFFFF , BRCM_Default_Length);
    
    BRCM_Control_WRITE_Data( 0x40000000 , 0x00001803 , BRCM_Default_Length);
    // QUAD_CORE_CONFIG_RDB_CFGVAL_0_RDB
    Data_Buffer=0x5201D400;
    for(Iconnt=0;Iconnt<For_count;Iconnt++)
    {
        BRCM_Control_WRITE_Data( Data_Buffer , 0x00000000 , BRCM_Default_Length);
        Data_Buffer=Data_Buffer+0x00000004;
    }
    // QUAD_CORE_EGRMGT_RDB_QC_CHIP_SW_RST_RDB
    Data_Buffer=BRCM_Control_READ_Data(0x5201C000);
    BRCM_Control_WRITE_Data( 0x5201C000 , Data_Buffer|0x00000001 , BRCM_Default_Length);
    delay_1ms(1);
    BRCM_Control_WRITE_Data( 0x40000000 , 0x00001803 , BRCM_Default_Length);
    // disable remap
    BRCM_Control_WRITE_Data( 0x40001080 , 0x00000000 , BRCM_Default_Length);
    BRCM_Control_WRITE_Data( 0x40000000 , 0x00001802 , BRCM_Default_Length);
}
//----------------------------------------------------------------------------------//
// DSP87400 RESET BRING UP
//----------------------------------------------------------------------------------//
void DSP_RESET_BRING_UP()
{
    uint32_t Data_Buffer=0x00000000;
    // host_download_mem_reset
    BRCM_Control_WRITE_Data( 0x5003C02C , 0x00000010 , BRCM_Default_Length);
    delay_1ms(100);
    BRCM_Control_WRITE_Data( 0x5003C02C , 0x00000000 , BRCM_Default_Length);
    BRCM_Control_WRITE_Data( 0x5201C190 , 0x00000010 , BRCM_Default_Length);
    delay_1ms(100);
    BRCM_Control_WRITE_Data( 0x5201C190 , 0x00000000 , BRCM_Default_Length);
    // QUAD_CORE_EGRMGT_RDB_QC_CHIP_SW_RST_RDB
    Data_Buffer=BRCM_Control_READ_Data(0x5201C000);
    BRCM_Control_WRITE_Data( 0x5201C000 , Data_Buffer|0x00000001 , BRCM_Default_Length);
}

//----------------------------------------------------------------------------------//
// DSP87400 PLL Bandwidth Config
//----------------------------------------------------------------------------------//
void DSP_PLL_BANDWIDTH_CONFIG(uint8_t Bandwidth)
{
    COMMAND_4700_DATA[0]  = 0x00000014 ;
    COMMAND_4700_DATA[1]  = 0x80000000 ;
    // Set Bandwidth Value
    COMMAND_4700_DATA[2]  = 0x00000000 | Bandwidth;
    COMMAND_4700_DATA[3]  = 0x00000000 ;
    COMMAND_4700_DATA[4]  = 0x00000000 ;
    COMMAND_4700_DATA[5]  = 0x00000000 ;
    // Set Lane and Command
    DSP87400_CAPI_CW_CMD( 0x5201CA98 , 0x0000000F , 0x5201CA94 , 0x00008054 );
    // Start Write Process
    DSP87400_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA94);
    DSP87400_CAPI_Command_response(0x5201CA9C,0x5201CAA0);   
}

//-----------------------------------------------------------------------------------------------------//
// DSP 87400 CHIP MODE
//-----------------------------------------------------------------------------------------------------//
//Mode1
void SET_CHIP_MODE_2X53G_KP4PAM_TO_1X106G_KP4PAM()
{
    // Step1 : DSP87540_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW
    // Step2 : DSP87540_CAPI_Command_response
    // Step3 : Delay 20 ms
    // Step4 : is_chip_mode_config_in_progress >= 200
    // Step5 : port_mode_cfg_log if port_mode_cfg_log != 0xF =>FW report the CHIP MODE CONFIG fail
    // Step6 : line_lane_cfg_done
    // Step7 : host_lane_cfg_done
    // COMMAND Vaule definition
    // 4700 Data Size
    // 4704 Function mode & refernce CLK
    // 4708 Mux_type & FEC Term
    // 470C Host FEC type & Line FEC Type
    // 4710 LW_BR & BH_BR
    // 4714 Line_Lane_Mask & Line_Lane_modulation
    // 4718 Line_Lane_Mask & Line_Lane_modulation
    // 471C Power Down & Status
    COMMAND_4700_DATA[0] = 0x0000001C;
    COMMAND_4700_DATA[1] = 0x00030000;
    COMMAND_4700_DATA[2] = 0x00000000;
    COMMAND_4700_DATA[3] = 0x00000000;
    COMMAND_4700_DATA[4] = 0x00000000;
    COMMAND_4700_DATA[5] = 0x000F0001;
    COMMAND_4700_DATA[6] = 0x00FF0001;
    COMMAND_4700_DATA[7] = 0x00000002;  
    DSP87400_CAPI_CW_CMD( 0x5201CA78 , 0x0000800F , 0x5201CA74 , 0x00008011 );
    DSP87400_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA74);
    DSP87400_CAPI_Command_response(0x5201CA7C,0x5201CA80);
    delay_1ms(100);
    BRCM_Control_READ_Data(0x5201D47C);
    BRCM_Control_READ_Data(0x5201D450);
    BRCM_Control_READ_Data(0x5201D450);
    DSP87400_line_lane_cfg_done();
    DSP87400_host_lane_cfg_done();
    DSP87400_line_cfg_st_lane_cw_cfg_rdy();
    DSP87400_host_cfg_st_lane_cw_cfg_rdy();
}

void SET_DSP_CHIP_MODE()
{
	uint16_t Read_Buffer;
	uint16_t Break_count = 0 ;
	uint16_t ChipMode_CHECK;
    //mode1(Default Mode)
	if( DSP_MODE_SET == Mode1_CHIP_MODE_2X53G_KP4PAM_TO_1X106G_KP4PAM )
	{
        //Set Chip Mode
        SET_CHIP_MODE_2X53G_KP4PAM_TO_1X106G_KP4PAM();
        DSP_GET_AVS_STATUS();
        // System/Line side FIR Setting 
        Line_Side_ALL_CH1_CH4_Control_P86();
        System_Side_ALL_CH1_CH4_Control_P87();
        System_Side_ALL_CH1_CH4_Control_P8E();
  }
}

void Get_DSP_Chip_Mode()
{
    uint8_t Data_Buffer;
    //Get DSP Chip Mode LSB Data
    Data_Buffer = DSP_Line_Side_PHY0_MEMORY_MAP.DSP_CHIP_MODE>>8;

    //Mode 1 CHIP_MODE_2X53G_KP4PAM_TO_1X106G_KP4PAM DSP Default Mode
   	if(Data_Buffer==0x00)
    {
        DSP_MODE_SET=Mode1_CHIP_MODE_2X53G_KP4PAM_TO_1X106G_KP4PAM;
        //PAM4 to PAM4 Mode
        //Signal_Status=PAM4_to_PAM4;
    }
}

//-----------------------------------------------------------------------------------------------------//
// Get DSP87400 temperature 
//-----------------------------------------------------------------------------------------------------/
uint16_t GET_DSP_Temperature()
{
    uint32_t ReadDSP_Buffer ;    
    uint16_t GET_Temperature = 0 ;
    
    ReadDSP_Buffer = BRCM_Control_READ_Data(0x5201C800);
    // Check bit15 = 1 Temperature data is correct
    // Alibaba Byte70-71 DSP dia temperature
    GET_Temperature = ReadDSP_Buffer ;
    
    return GET_Temperature;
}

void Get_NRZ_Default_System_Side()
{
    DSP_System_Side_PHY0_MEMORY_MAP.System_Side_PRE1_CH0=0xF4FF;
    DSP_System_Side_PHY0_MEMORY_MAP.System_Side_PRE1_CH1=0xF4FF;
    DSP_System_Side_PHY0_MEMORY_MAP.System_Side_PRE1_CH2=0xF4FF;
    DSP_System_Side_PHY0_MEMORY_MAP.System_Side_PRE1_CH3=0xF4FF;
    
    DSP_System_Side_PHY1_MEMORY_MAP.System_Side_PRE1_CH0=0xF4FF;
    DSP_System_Side_PHY1_MEMORY_MAP.System_Side_PRE1_CH1=0xF4FF;
    DSP_System_Side_PHY1_MEMORY_MAP.System_Side_PRE1_CH2=0xF4FF;
    DSP_System_Side_PHY1_MEMORY_MAP.System_Side_PRE1_CH3=0xF4FF;
    
    
    DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST1_CH0 = 0x0000;
    DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST1_CH1 = 0x0000;
    DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST1_CH2 = 0x0000;
    DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST1_CH3 = 0x0000;
    
    DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST1_CH0 = 0x0000;
    DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST1_CH1 = 0x0000;
    DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST1_CH2 = 0x0000;
    DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST1_CH3 = 0x0000;
    
    
    DSP_System_Side_PHY0_MEMORY_MAP.System_Side_Main_CH0 = 0x7000;
    DSP_System_Side_PHY0_MEMORY_MAP.System_Side_Main_CH1 = 0x7000;
    DSP_System_Side_PHY0_MEMORY_MAP.System_Side_Main_CH2 = 0x7000;
    DSP_System_Side_PHY0_MEMORY_MAP.System_Side_Main_CH3 = 0x7000;
    
    DSP_System_Side_PHY1_MEMORY_MAP.System_Side_Main_CH0 = 0x7000;
    DSP_System_Side_PHY1_MEMORY_MAP.System_Side_Main_CH1 = 0x7000;
    DSP_System_Side_PHY1_MEMORY_MAP.System_Side_Main_CH2 = 0x7000;
    DSP_System_Side_PHY1_MEMORY_MAP.System_Side_Main_CH3 = 0x7000;
}
//----------------------------------------------------------------------------------//
// DSP87400 Initialize 
//----------------------------------------------------------------------------------//
void DSP_Init()
{ 
    uint32_t Data_Buffer,DSP_READY_FLAG=0;
    // Get DSP Rev ID for select new and old line side system side function
    DSP_GET_REV_ID();
    
    if(REV_ID==0xA0)
        DSP_READY_FLAG=DSP87400_EEPROM_AVS_UcReady_CHECK();
    // REV A1
    else if(First_Init_Flag==0)
    {
        Data_Buffer=DSP87400_EEPROM_AVS_UcReady_CHECK();
        if(Data_Buffer)
            DSP_READY_FLAG|=0x01;
        Data_Buffer=DSP_GET_AVS_STATUS();
       if(Data_Buffer)
            DSP_READY_FLAG|=0x02;
        Data_Buffer=DSP_GET_DOWNLOAD_STATUS();
        if(Data_Buffer)
            DSP_READY_FLAG|=0x04;
        if(DSP_READY_FLAG==0x07)
            DSP_READY_FLAG=1;
    }
    if(DSP_READY_FLAG)
    {
        // Delay for Line Side Polarity Lane0 Config After DSP87400_EEPROM_AVS_UcReady_CHECK()
        delay_1ms(100);
        // After dsp initialize trigger do send fw cmd all lanes on line side
        DSP_LineSide_TRX_Polarity_SET( 0 , TX_Side);
        DSP_LineSide_TRX_Polarity_SET( 1 , TX_Side);
        DSP_LineSide_TRX_Polarity_SET( 2 , TX_Side);
        DSP_LineSide_TRX_Polarity_SET( 3 , TX_Side);
        DSP_System_Side_RX_Squelch_SET(All_CH,Squelch);
        DSP_System_Side_TX_Squelch_SET(All_CH,Squelch);
        //Default Mode on chip
        if(DSP_MODE_SET==Mode1_CHIP_MODE_2X53G_KP4PAM_TO_1X106G_KP4PAM)
        {
//					  CALIB_MEMORY_MAP.CAL_Buffer[0]=5;
            // Lane0 to Lane3 Initialize from sram
            Line_Side_ALL_CH1_CH4_Control_P86();
            System_Side_ALL_CH1_CH4_Control_P87();
            // Lane4 to Lane7 Initialize from sram
            System_Side_ALL_CH1_CH4_Control_P8E();
        }
        else
            SET_DSP_CHIP_MODE();
        //Config 0V75 and 0V90
        if(DSP_VCC_Flag==0)
        {
            //DSP_SET_0V75();
            //DSP_SET_0V90();
            DSP_VCC_Flag=1;
        }
        DSP_System_Side_RX_Squelch_SET(All_CH,Unsquelch);
				DSP_System_Side_TX_Squelch_SET(All_CH,Unsquelch);
				DSP_LineSide_TX_Squelch_SET(All_CH,Unsquelch);
        DSP_INIT_Flag=1;
    }
}