#include "gd32e501.h"
#include "core_cm33.h"
#include "systick.h"
#include "string.h"
#include "CMIS_MSA.h"
#include "GD32E501_M_I2C2_PC78.h"
#include "DSP.h"
#include "DSP_Line_Side.h"
#include "math.h"

uint8_t LOL_Clear_Flag = 0;
uint16_t DSP_SystemSide_SNR=0x0000;
uint16_t DSP_SystemSide_LTP=0x0000;
//-----------------------------------------------------------------------------------------------------//
// DSP System Side TRX Polarity
// TRX_Side_SEL = 1  System side Tx
// TRX_Side_SEL = 0  System side Rx
//-----------------------------------------------------------------------------------------------------//
void DSP_SystemSide_TRX_Polarity_SET(uint8_t Lane_CH , uint8_t TRX_Side_SEL)
{
    if(Lane_CH==0)
    {
        COMMAND_4700_DATA[0] = 0x00000002;
        
        //Tx Polarity
        if(TRX_Side_SEL)
            COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_TX_Polarity_CH0);
        //Rx Polarity
        else
            COMMAND_4700_DATA[1] = 0xCCCC0001 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_RX_Polarity_CH0);
        
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000001 , 0x5201CA84 , 0x00008006 );
    }
    else if(Lane_CH==1)
    {
        COMMAND_4700_DATA[0] = 0x00010002;
        
        //Tx Polarity
        if(TRX_Side_SEL)
            COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_TX_Polarity_CH1);
        //Rx Polarity
        else
            COMMAND_4700_DATA[1] = 0xCCCC0001 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_RX_Polarity_CH1);
        
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000002 , 0x5201CA84 , 0x00008006 );        
    }
    else if(Lane_CH==2)
    {
        COMMAND_4700_DATA[0] = 0x00020002;
        
        //Tx Polarity
        if(TRX_Side_SEL)
            COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_TX_Polarity_CH2);
        //Rx Polarity
        else
            COMMAND_4700_DATA[1] = 0xCCCC0001 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_RX_Polarity_CH2);
        
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000004 , 0x5201CA84 , 0x00008006 );      
    }
    else if(Lane_CH==3)
    {
        COMMAND_4700_DATA[0] = 0x00030002;
        
        //Tx Polarity
        if(TRX_Side_SEL)
            COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_TX_Polarity_CH3);
        //Rx Polarity
        else
            COMMAND_4700_DATA[1] = 0xCCCC0001 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_RX_Polarity_CH3);
        
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000008 , 0x5201CA84 , 0x00008006 );  
    }
    else if(Lane_CH==4)
    {
        COMMAND_4700_DATA[0] = 0x00040002;
        
        //Tx Polarity
        if(TRX_Side_SEL)
            COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_TX_Polarity_CH0);
        //Rx Polarity
        else
            COMMAND_4700_DATA[1] = 0xCCCC0001 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_RX_Polarity_CH0);
        
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000010 , 0x5201CA84 , 0x00008006 );  
    }
    else if(Lane_CH==5)
    {
        COMMAND_4700_DATA[0] = 0x00050002;
        
        //Tx Polarity
        if(TRX_Side_SEL)
            COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_TX_Polarity_CH1);
        //Rx Polarity
        else
            COMMAND_4700_DATA[1] = 0xCCCC0001 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_RX_Polarity_CH1);
        
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000020 , 0x5201CA84 , 0x00008006 );  
    }
    else if(Lane_CH==6)
    {
        COMMAND_4700_DATA[0] = 0x00060002;
        
        //Tx Polarity
        if(TRX_Side_SEL)
            COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_TX_Polarity_CH2);
        //Rx Polarity
        else
            COMMAND_4700_DATA[1] = 0xCCCC0001 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_RX_Polarity_CH2);
        
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000040 , 0x5201CA84 , 0x00008006 );  
    }
    else if(Lane_CH==7)
    {
        COMMAND_4700_DATA[0] = 0x00070002;
        
        //Tx Polarity
        if(TRX_Side_SEL)
            COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_TX_Polarity_CH3);
        //Rx Polarity
        else
            COMMAND_4700_DATA[1] = 0xCCCC0001 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_RX_Polarity_CH3);
        
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000080 , 0x5201CA84 , 0x00008006 );  
    }
    DSP87400_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA84);
    DSP87400_CAPI_Command_response(0x5201CA8C,0x5201CA90);
}

//-----------------------------------------------------------------------------------------------------//
// DSP SystemSide TRX Squelch
// TRX_Side = 1  TX Squelch ( Moudle Rx ouput )
// TRX_Side = 0  RX Squelch
//-----------------------------------------------------------------------------------------------------//
void DSP_SystemSide_TRX_Squelch_SET(uint8_t Lane_CH ,uint8_t TRX_Side,uint8_t Enable)
{  
    if(Lane_CH==0)    
    {
        COMMAND_4700_DATA[0] = 0x00000003 ;
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000001 , 0x5201CA84 , 0x0000800F );
    }
    else if(Lane_CH==1)    
    {
        COMMAND_4700_DATA[0] = 0x00010003 ;
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000002 , 0x5201CA84 , 0x0000800F );
    }
    else if(Lane_CH==2)   
    {
        COMMAND_4700_DATA[0] = 0x00020003 ;        
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000004 , 0x5201CA84 , 0x0000800F );
    }
    else if(Lane_CH==3)   
    {
        COMMAND_4700_DATA[0] = 0x00030003 ;           
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000008 , 0x5201CA84 , 0x0000800F );
    }
    else if(Lane_CH==4)    
    {
        COMMAND_4700_DATA[0] = 0x00040003 ;
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000010 , 0x5201CA84 , 0x0000800F );
    }
    else if(Lane_CH==5)   
    {
        COMMAND_4700_DATA[0] = 0x00050003 ;        
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000020 , 0x5201CA84 , 0x0000800F );
    }
    else if(Lane_CH==6)   
    {
        COMMAND_4700_DATA[0] = 0x00060003 ;           
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000040 , 0x5201CA84 , 0x0000800F );
    }
    else if(Lane_CH==7)   
    {
        COMMAND_4700_DATA[0] = 0x00070003 ;           
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000080 , 0x5201CA84 , 0x0000800F );
    }
    else if(Lane_CH==All_CH)
    {
        COMMAND_4700_DATA[0] = 0x00080003 ;           
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x000000FF , 0x5201CA84 , 0x0000800F );
    }        
    //Tx Squelch
    if(TRX_Side)
    {
        if(Enable)
            COMMAND_4700_DATA[1] = 0xCC000100 ;
        else
            COMMAND_4700_DATA[1] = 0xCC000200 ;
    }
    //Rx Squelch
    else
    {
        if(Enable)
            COMMAND_4700_DATA[1] = 0xCC010101 ;
        else
            COMMAND_4700_DATA[1] = 0xCC010201 ;
    }  
    
    DSP87400_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA84);
    DSP87400_CAPI_Command_response(0x5201CA8C,0x5201CA90);
}

//-----------------------------------------------------------------------------------------------------//
// DSP SystemSide Digital/Remote Loopback function
// LoopBack_mode = 1  Digital Loopback
// LoopBack_mode = 0  remote Loopbcak
//-----------------------------------------------------------------------------------------------------//
void DSP_SystemSide_Loopback_SET(uint8_t LoopBack_mode,uint8_t Lane_CH,uint8_t Enable)
{
    if(Lane_CH==0)
    {
        COMMAND_4700_DATA[0]  = 0x00000008 ;
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000001 , 0x5201CA84 , 0x00008014 );
    }
    else if(Lane_CH==1)
    {
        COMMAND_4700_DATA[0]  = 0x00010008 ;
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000002 , 0x5201CA84 , 0x00008014 );
    }
    else if(Lane_CH==2)
    {
        COMMAND_4700_DATA[0]  = 0x00020008 ;
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000004 , 0x5201CA84 , 0x00008014 );
    }
    else if(Lane_CH==3)
    {
        COMMAND_4700_DATA[0]  = 0x00030008 ;
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000008 , 0x5201CA84 , 0x00008014 );
    }
    else if(Lane_CH==4)
    {
        COMMAND_4700_DATA[0]  = 0x00040008 ;
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000010 , 0x5201CA84 , 0x00008014 );
    }
    else if(Lane_CH==5)
    {
        COMMAND_4700_DATA[0]  = 0x00050008 ;
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000020 , 0x5201CA84 , 0x00008014 );
    }
    else if(Lane_CH==6)
    {
        COMMAND_4700_DATA[0]  = 0x00060008 ;
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000040 , 0x5201CA84 , 0x00008014 );
    }
    else if(Lane_CH==7)
    {
        COMMAND_4700_DATA[0]  = 0x00070008 ;
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000080 , 0x5201CA84 , 0x00008014 );
    }
    else if(Lane_CH==All_CH)
    {
        COMMAND_4700_DATA[0]  = 0x00080008 ;
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x000000FF , 0x5201CA84 , 0x00008014 );
    }
    //Digital Loopback
    if(LoopBack_mode)
    {
        COMMAND_4700_DATA[1] = 0x00000006 ;
        if(Enable)
            COMMAND_4700_DATA[2]  = 0x00000001 ;
        else
            COMMAND_4700_DATA[2]  = 0x00000000 ;
    }
    //Remote Loopback
    else
    {
        COMMAND_4700_DATA[1] = 0x00000008 ;
        if(Enable)
            COMMAND_4700_DATA[2]  = 0x00000001 ;
        else
            COMMAND_4700_DATA[2]  = 0x00000000 ;
    }
           
    DSP87400_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA84);
    DSP87400_CAPI_Command_response(0x5201CA8C,0x5201CA90);
}
//-----------------------------------------------------------------------------------------------------------------//
//------------------------------------------------- API------------------------------------------------------------//
//-----------------------------------------------------------------------------------------------------------------//

//-----------------------------------------------------------------------------------------------------//
// DSP System Side TX FIR Setting
//-----------------------------------------------------------------------------------------------------//
void DSP_System_Side_TX_FIR_SET( uint8_t Lane_CH)
{
    if(Lane_CH==0)
    {
        if(REV_ID==0xA0)
            COMMAND_4700_DATA[0]  = 0x00000040 ;
        // REV A1
        else
            COMMAND_4700_DATA[0]  = 0x00000044 ;
        
        COMMAND_4700_DATA[1]  = 0x00000002 ;
        COMMAND_4700_DATA[2]  = 0x000300FF ;
        //Update Pre2 Bit0-15 , Pre1 Bit16-31
        COMMAND_4700_DATA[3]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_PRE2_CH0)
                                           | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_PRE1_CH0)<< 16;
        //Update Main Bit0-15 , Post1 Bit16-31
        COMMAND_4700_DATA[4]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_Main_CH0)
                                           | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST1_CH0)<< 16;
        //Update Post2 Bit0-15, Post3 Bit16-31
        COMMAND_4700_DATA[5]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST2_CH0)
                                           | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST3_CH0)<< 16;
        COMMAND_4700_DATA[6]  = 0x00000000 ;
        COMMAND_4700_DATA[7]  = 0x00000000 ;
        COMMAND_4700_DATA[8]  = 0x00000000 ;
        COMMAND_4700_DATA[9]  = 0x00000000 ;
        COMMAND_4700_DATA[10] = 0x00000000 ;
        COMMAND_4700_DATA[11] = 0x00000000 ;
        COMMAND_4700_DATA[12] = 0x00000000 ;
        COMMAND_4700_DATA[13] = 0x00000000 ;
        COMMAND_4700_DATA[14] = 0x00000000 ;
        COMMAND_4700_DATA[15] = 0x00000000 ;
        COMMAND_4700_DATA[16] = 0x00000000 ;
        COMMAND_4700_DATA[17] = 0x00000000 ;
        
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000001 , 0x5201CA84 , 0x00008020 );
    }
    else if(Lane_CH==1)
    {
        if(REV_ID==0xA0)
            COMMAND_4700_DATA[0]  = 0x00010040 ;
        // REV A1
        else
            COMMAND_4700_DATA[0]  = 0x00010044 ;
        
        COMMAND_4700_DATA[1]  = 0x00000002 ;
        COMMAND_4700_DATA[2]  = 0x000300FF ;
        //Update Pre2 Bit0-15 , Pre1 Bit16-31
        COMMAND_4700_DATA[3]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_PRE2_CH1)
                                           | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_PRE1_CH1)<< 16;
        //Update Main Bit0-15 , Post1 Bit16-31
        COMMAND_4700_DATA[4]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_Main_CH1)
                                           | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST1_CH1)<< 16;
        //Update Post2 Bit0-15, Post3 Bit16-31
        COMMAND_4700_DATA[5]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST2_CH1)
                                           | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST3_CH1)<< 16;
        COMMAND_4700_DATA[6]  = 0x00000000 ;
        COMMAND_4700_DATA[7]  = 0x00000000 ;
        COMMAND_4700_DATA[8]  = 0x00000000 ;
        COMMAND_4700_DATA[9]  = 0x00000000 ;
        COMMAND_4700_DATA[10] = 0x00000000 ;
        COMMAND_4700_DATA[11] = 0x00000000 ;
        COMMAND_4700_DATA[12] = 0x00000000 ;
        COMMAND_4700_DATA[13] = 0x00000000 ;
        COMMAND_4700_DATA[14] = 0x00000000 ;
        COMMAND_4700_DATA[15] = 0x00000000 ;
        COMMAND_4700_DATA[16] = 0x00000000 ;
        COMMAND_4700_DATA[17] = 0x00000000 ;
        
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000002 , 0x5201CA84 , 0x00008020 );
    }
    else if(Lane_CH==2)
    {
        if(REV_ID==0xA0)
            COMMAND_4700_DATA[0]  = 0x00020040 ;
        // REV A1
        else
            COMMAND_4700_DATA[0]  = 0x00020044 ;
        
        COMMAND_4700_DATA[1]  = 0x00000002 ;
        COMMAND_4700_DATA[2]  = 0x000300FF ;
        //Update Pre2 Bit0-15 , Pre1 Bit16-31
        COMMAND_4700_DATA[3]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_PRE2_CH2)
                                           | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_PRE1_CH2)<< 16;
        //Update Main Bit0-15 , Post1 Bit16-31
        COMMAND_4700_DATA[4]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_Main_CH2)
                                           | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST1_CH2)<< 16;
        //Update Post2 Bit0-15, Post3 Bit16-31
        COMMAND_4700_DATA[5]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST2_CH2)
                                           | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST3_CH2)<< 16;
        COMMAND_4700_DATA[6]  = 0x00000000 ;
        COMMAND_4700_DATA[7]  = 0x00000000 ;
        COMMAND_4700_DATA[8]  = 0x00000000 ;
        COMMAND_4700_DATA[9]  = 0x00000000 ;
        COMMAND_4700_DATA[10] = 0x00000000 ;
        COMMAND_4700_DATA[11] = 0x00000000 ;
        COMMAND_4700_DATA[12] = 0x00000000 ;
        COMMAND_4700_DATA[13] = 0x00000000 ;
        COMMAND_4700_DATA[14] = 0x00000000 ;
        COMMAND_4700_DATA[15] = 0x00000000 ;
        COMMAND_4700_DATA[16] = 0x00000000 ;
        COMMAND_4700_DATA[17] = 0x00000000 ;
        
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000004 , 0x5201CA84 , 0x00008020 );
    }
    else if(Lane_CH==3)
    {
        if(REV_ID==0xA0)
            COMMAND_4700_DATA[0]  = 0x00030040 ;
        // REV A1
        else
            COMMAND_4700_DATA[0]  = 0x00030044 ;
        
        COMMAND_4700_DATA[1]  = 0x00000002 ;
        COMMAND_4700_DATA[2]  = 0x000300FF ;
        //Update Pre2 Bit0-15 , Pre1 Bit16-31
        COMMAND_4700_DATA[3]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_PRE2_CH3)
                                           | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_PRE1_CH3)<< 16;
        //Update Main Bit0-15 , Post1 Bit16-31
        COMMAND_4700_DATA[4]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_Main_CH3)
                                           | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST1_CH3)<< 16;
        //Update Post2 Bit0-15, Post3 Bit16-31
        COMMAND_4700_DATA[5]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST2_CH3)
                                           | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST3_CH3)<< 16;
        COMMAND_4700_DATA[6]  = 0x00000000 ;
        COMMAND_4700_DATA[7]  = 0x00000000 ;
        COMMAND_4700_DATA[8]  = 0x00000000 ;
        COMMAND_4700_DATA[9]  = 0x00000000 ;
        COMMAND_4700_DATA[10] = 0x00000000 ;
        COMMAND_4700_DATA[11] = 0x00000000 ;
        COMMAND_4700_DATA[12] = 0x00000000 ;
        COMMAND_4700_DATA[13] = 0x00000000 ;
        COMMAND_4700_DATA[14] = 0x00000000 ;
        COMMAND_4700_DATA[15] = 0x00000000 ;
        COMMAND_4700_DATA[16] = 0x00000000 ;
        COMMAND_4700_DATA[17] = 0x00000000 ;
        
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000008 , 0x5201CA84 , 0x00008020 );
    }
    else if(Lane_CH==4)
    {
        if(REV_ID==0xA0)
            COMMAND_4700_DATA[0]  = 0x00040040 ;
        // REV A1
        else
            COMMAND_4700_DATA[0]  = 0x00040044 ;
        
        COMMAND_4700_DATA[1]  = 0x00000002 ;
        COMMAND_4700_DATA[2]  = 0x000300FF ;
        //Update Pre2 Bit0-15 , Pre1 Bit16-31
        COMMAND_4700_DATA[3]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_PRE2_CH0)
                                           | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_PRE1_CH0)<< 16;
        //Update Main Bit0-15 , Post1 Bit16-31
        COMMAND_4700_DATA[4]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_Main_CH0)
                                           | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST1_CH0)<< 16;
        //Update Post2 Bit0-15, Post3 Bit16-31
        COMMAND_4700_DATA[5]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST2_CH0)
                                           | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST3_CH0)<< 16;
        COMMAND_4700_DATA[6]  = 0x00000000 ;
        COMMAND_4700_DATA[7]  = 0x00000000 ;
        COMMAND_4700_DATA[8]  = 0x00000000 ;
        COMMAND_4700_DATA[9]  = 0x00000000 ;
        COMMAND_4700_DATA[10] = 0x00000000 ;
        COMMAND_4700_DATA[11] = 0x00000000 ;
        COMMAND_4700_DATA[12] = 0x00000000 ;
        COMMAND_4700_DATA[13] = 0x00000000 ;
        COMMAND_4700_DATA[14] = 0x00000000 ;
        COMMAND_4700_DATA[15] = 0x00000000 ;
        COMMAND_4700_DATA[16] = 0x00000000 ;
        COMMAND_4700_DATA[17] = 0x00000000 ;
        
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000010 , 0x5201CA84 , 0x00008020 );
    }
    else if(Lane_CH==5)
    {
        if(REV_ID==0xA0)
            COMMAND_4700_DATA[0]  = 0x00050040 ;
        // REV A1
        else
            COMMAND_4700_DATA[0]  = 0x00050044 ;
        
        COMMAND_4700_DATA[1]  = 0x00000002 ;
        COMMAND_4700_DATA[2]  = 0x000300FF ;
        //Update Pre2 Bit0-15 , Pre1 Bit16-31
        COMMAND_4700_DATA[3]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_PRE2_CH1)
                                           | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_PRE1_CH1)<< 16;
        //Update Main Bit0-15 , Post1 Bit16-31
        COMMAND_4700_DATA[4]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_Main_CH1)
                                           | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST1_CH1)<< 16;
        //Update Post2 Bit0-15, Post3 Bit16-31
        COMMAND_4700_DATA[5]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST2_CH1)
                                           | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST3_CH1)<< 16;
        COMMAND_4700_DATA[6]  = 0x00000000 ;
        COMMAND_4700_DATA[7]  = 0x00000000 ;
        COMMAND_4700_DATA[8]  = 0x00000000 ;
        COMMAND_4700_DATA[9]  = 0x00000000 ;
        COMMAND_4700_DATA[10] = 0x00000000 ;
        COMMAND_4700_DATA[11] = 0x00000000 ;
        COMMAND_4700_DATA[12] = 0x00000000 ;
        COMMAND_4700_DATA[13] = 0x00000000 ;
        COMMAND_4700_DATA[14] = 0x00000000 ;
        COMMAND_4700_DATA[15] = 0x00000000 ;
        COMMAND_4700_DATA[16] = 0x00000000 ;
        COMMAND_4700_DATA[17] = 0x00000000 ;
        
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000020 , 0x5201CA84 , 0x00008020 );
    }
    else if(Lane_CH==6)
    {
        if(REV_ID==0xA0)
            COMMAND_4700_DATA[0]  = 0x00060040 ;
        // REV A1
        else
            COMMAND_4700_DATA[0]  = 0x00060044 ;
        
        COMMAND_4700_DATA[1]  = 0x00000002 ;
        COMMAND_4700_DATA[2]  = 0x000300FF ;
        //Update Pre2 Bit0-15 , Pre1 Bit16-31
        COMMAND_4700_DATA[3]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_PRE2_CH2)
                                           | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_PRE1_CH2)<< 16;
        //Update Main Bit0-15 , Post1 Bit16-31
        COMMAND_4700_DATA[4]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_Main_CH2)
                                           | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST1_CH2)<< 16;
        //Update Post2 Bit0-15, Post3 Bit16-31
        COMMAND_4700_DATA[5]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST2_CH2)
                                           | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST3_CH2)<< 16;
        COMMAND_4700_DATA[6]  = 0x00000000 ;
        COMMAND_4700_DATA[7]  = 0x00000000 ;
        COMMAND_4700_DATA[8]  = 0x00000000 ;
        COMMAND_4700_DATA[9]  = 0x00000000 ;
        COMMAND_4700_DATA[10] = 0x00000000 ;
        COMMAND_4700_DATA[11] = 0x00000000 ;
        COMMAND_4700_DATA[12] = 0x00000000 ;
        COMMAND_4700_DATA[13] = 0x00000000 ;
        COMMAND_4700_DATA[14] = 0x00000000 ;
        COMMAND_4700_DATA[15] = 0x00000000 ;
        COMMAND_4700_DATA[16] = 0x00000000 ;
        COMMAND_4700_DATA[17] = 0x00000000 ;
        
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000040 , 0x5201CA84 , 0x00008020 );
    }
    else if(Lane_CH==7)
    {
        if(REV_ID==0xA0)
            COMMAND_4700_DATA[0]  = 0x00070040 ;
        // REV A1
        else
            COMMAND_4700_DATA[0]  = 0x00070044 ;
        
        COMMAND_4700_DATA[1]  = 0x00000002 ;
        COMMAND_4700_DATA[2]  = 0x000300FF ;
        //Update Pre2 Bit0-15 , Pre1 Bit16-31
        COMMAND_4700_DATA[3]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_PRE2_CH3)
                                           | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_PRE1_CH3)<< 16;
        //Update Main Bit0-15 , Post1 Bit16-31
        COMMAND_4700_DATA[4]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_Main_CH3)
                                           | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST1_CH3)<< 16;
        //Update Post2 Bit0-15, Post3 Bit16-31
        COMMAND_4700_DATA[5]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST2_CH3)
                                           | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST3_CH3)<< 16;
        COMMAND_4700_DATA[6]  = 0x00000000 ;
        COMMAND_4700_DATA[7]  = 0x00000000 ;
        COMMAND_4700_DATA[8]  = 0x00000000 ;
        COMMAND_4700_DATA[9]  = 0x00000000 ;
        COMMAND_4700_DATA[10] = 0x00000000 ;
        COMMAND_4700_DATA[11] = 0x00000000 ;
        COMMAND_4700_DATA[12] = 0x00000000 ;
        COMMAND_4700_DATA[13] = 0x00000000 ;
        COMMAND_4700_DATA[14] = 0x00000000 ;
        COMMAND_4700_DATA[15] = 0x00000000 ;
        COMMAND_4700_DATA[16] = 0x00000000 ;
        COMMAND_4700_DATA[17] = 0x00000000 ;
        
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000080 , 0x5201CA84 , 0x00008020 );
    }
    DSP87400_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA84);
    DSP87400_CAPI_Command_response(0x5201CA8C,0x5201CA90);
}
//-----------------------------------------------------------------------------------------------------//
// DSP System Side PRBS Gen Clear
//-----------------------------------------------------------------------------------------------------//
void DSP_SystemSide_PRBS_Gen_Clear()
{
    COMMAND_4700_DATA[0] = 0x00000004 ;
    COMMAND_4700_DATA[1] = 0xCCCCCCCC ;
    
    DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x000000FF , 0x5201CA84 , 0x0000801C );
    // Start Write Process
    DSP87400_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA84);
    DSP87400_CAPI_Command_response(0x5201CA8C,0x5201CA90);
}
//-----------------------------------------------------------------------------------------------------//
// DSP System Side PRBS Info
//-----------------------------------------------------------------------------------------------------//
uint16_t DSP_SystemSide_PRBS_Info_GET(uint8_t Lane_CH)
{
    uint32_t Lock_Status = 0x00000000;
    uint32_t Lane_Buffer = 0x00000000;
    
    Lane_Buffer = Lane_CH;
    Lane_Buffer = Lane_Buffer<<16;
    BRCM_Control_READ_Data(0x5201CA84) ;
    
    BRCM_Control_WRITE_Data( 0x00047000 , 0x00000018 | Lane_Buffer, BRCM_Default_Length);
    BRCM_Control_WRITE_Data( 0x00047004 , 0x00000001 , BRCM_Default_Length);
    BRCM_Control_WRITE_Data( 0x00047008 , 0xCCCCCCCC , BRCM_Default_Length);
    BRCM_Control_WRITE_Data( 0x0004700C , 0xCCCCCCCC , BRCM_Default_Length);
    BRCM_Control_WRITE_Data( 0x00047010 , 0xCCCCCCCC , BRCM_Default_Length);
    BRCM_Control_WRITE_Data( 0x00047014 , 0xCCCCCCCC , BRCM_Default_Length);
    BRCM_Control_WRITE_Data( 0x00047018 , 0xCCCCCCCC , BRCM_Default_Length);
    
    //Write Set Lane
    BRCM_Control_WRITE_Data( 0x5201CA88 , 0x00000001 << Lane_CH, BRCM_Default_Length);
    //Write Command ID
    BRCM_Control_WRITE_Data( 0x5201CA84 , 0x0000801D , BRCM_Default_Length);
    //cmd_response_address
    BRCM_Control_READ_Data(0x5201CA8C) ;
    //rsp_lane_mask_address
    BRCM_Control_READ_Data(0x5201CA90) ;
    //Read Response Data
    BRCM_Control_READ_Data(0x00047400) ;
    BRCM_Control_READ_Data(0x00047404) ;
    Lock_Status = BRCM_Control_READ_Data(0x00047408) ;
    BRCM_Control_READ_Data(0x0004740C) ;
    BRCM_Control_READ_Data(0x00047410) ;
    BRCM_Control_READ_Data(0x00047414) ;
    BRCM_Control_READ_Data(0x00047418) ;
    //intf_capi2fw_command_response
    BRCM_Control_WRITE_Data( 0x5201CA84 , 0x00000000 , BRCM_Default_Length);
    BRCM_Control_WRITE_Data( 0x5201CA94 , 0x00000000 , BRCM_Default_Length);
    BRCM_Control_WRITE_Data( 0x5201CA74 , 0x00000000 , BRCM_Default_Length);
    BRCM_Control_WRITE_Data( 0x5201CA8C , 0x00000000 , BRCM_Default_Length);
    BRCM_Control_WRITE_Data( 0x5201CA90 , 0x00000000 , BRCM_Default_Length);
    
    return Lock_Status;
}

//----------------------------------------------------------------------------------//
// DSP System_Side_ALL_CH1_CH4_Control_P87
//----------------------------------------------------------------------------------//
void System_Side_ALL_CH1_CH4_Control_P87()
{
    DSP_System_Side_TX_FIR_SET( 0);
    DSP_System_Side_TX_FIR_SET( 1);
    DSP_System_Side_TX_FIR_SET( 2);
    DSP_System_Side_TX_FIR_SET( 3);

    DSP_SystemSide_TRX_Polarity_SET( 0 , TX_Side);   
    DSP_SystemSide_TRX_Polarity_SET( 1 , TX_Side);
    DSP_SystemSide_TRX_Polarity_SET( 2 , TX_Side);
    DSP_SystemSide_TRX_Polarity_SET( 3 , TX_Side);
    
    DSP_SystemSide_TRX_Polarity_SET( 0 , RX_Side);   
    DSP_SystemSide_TRX_Polarity_SET( 1 , RX_Side);
    DSP_SystemSide_TRX_Polarity_SET( 2 , RX_Side);
    DSP_SystemSide_TRX_Polarity_SET( 3 , RX_Side);
}

//----------------------------------------------------------------------------------//
// DSP System_Side_ALL_CH5_CH8_Control_P8E
//----------------------------------------------------------------------------------//
void System_Side_ALL_CH1_CH4_Control_P8E()
{
    DSP_System_Side_TX_FIR_SET( 4);
    DSP_System_Side_TX_FIR_SET( 5);
    DSP_System_Side_TX_FIR_SET( 6);
    DSP_System_Side_TX_FIR_SET( 7);

    DSP_SystemSide_TRX_Polarity_SET( 4 , TX_Side);   
    DSP_SystemSide_TRX_Polarity_SET( 5 , TX_Side);
    DSP_SystemSide_TRX_Polarity_SET( 6 , TX_Side);
    DSP_SystemSide_TRX_Polarity_SET( 7 , TX_Side);
    
    DSP_SystemSide_TRX_Polarity_SET( 4 , RX_Side);   
    DSP_SystemSide_TRX_Polarity_SET( 5 , RX_Side);
    DSP_SystemSide_TRX_Polarity_SET( 6 , RX_Side);
    DSP_SystemSide_TRX_Polarity_SET( 7 , RX_Side);
}

//----------------------------------------------------------------------------------//
// DSP System_Side_TRX_Squelch_Control
//----------------------------------------------------------------------------------//
void DSP_System_Side_TX_Squelch_SET(uint8_t Channel,uint8_t Control_Data)
{
    //Lane0 to Lane7 Control
    DSP_SystemSide_TRX_Squelch_SET( Channel , TX_Side , Control_Data);
}

void DSP_System_Side_RX_Squelch_SET(uint8_t Channel,uint8_t Control_Data)
{
    //Lane0 to Lane7 Control
    DSP_SystemSide_TRX_Squelch_SET( Channel , RX_Side , Control_Data);
}

//----------------------------------------------------------------------------------//
// DSP System_Side_LOS
//----------------------------------------------------------------------------------//
uint8_t DSP_System_Side_LOS()
{
    uint16_t Data_Buffer=0x0000;
    uint8_t LOS_STATUS;

    // LOL bit0 - bit7 , Lock:0,nolock:1
    // LOS bit8 - bit15, Lock:0,nolock:1
    // System Side Input
    Data_Buffer=BRCM_Control_READ_Data(0x5201CAC0);
    LOS_STATUS=((Data_Buffer&0xFF00)>>8);
    Data_Buffer=BRCM_Control_READ_Data(0x5201CAB8);
    if(Data_Buffer!=0xFFFF)
    {
        BRCM_Control_WRITE_Data( 0x5201CAF0 , Data_Buffer , BRCM_Default_Length);
        BRCM_Control_READ_Data(0x5201CAC0);
        BRCM_Control_READ_Data(0x5201CAB8);
        BRCM_Control_WRITE_Data( 0x5201CAF0 , 0x00000000 , BRCM_Default_Length);
    }
    return LOS_STATUS;
}

//----------------------------------------------------------------------------------//
// DSP System_Side_LOL
//----------------------------------------------------------------------------------//
uint8_t DSP_System_Side_LOL()
{
    uint16_t Data_Buffer=0x0000;
    uint8_t LOL_STATUS;
    
    // CDR Lock bit0 - bit7 , Lock:1,nolock:0
    LOL_STATUS=~(BRCM_Control_READ_Data(0x5201CAB8)&0x000000FF);
    
    return LOL_STATUS;
}

//----------------------------------------------------------------------------------//
// DSP System_Side_Loopback_Control
//----------------------------------------------------------------------------------//
void DSP_System_Side_Digital_Loopback_SET(uint8_t Lane_CH,uint8_t Enable)
{
    //Lane0 to Lane7 Control
    DSP_SystemSide_Loopback_SET( Digital_LoopBack_mode , Lane_CH , Enable);
}

void DSP_System_Side_Remote_Loopback_SET(uint8_t Lane_CH,uint8_t Enable)
{
    //Lane0 to Lane7 Control
    DSP_SystemSide_Loopback_SET( Remote_Loopback_mode , Lane_CH , Enable);
}
//----------------------------------------------------------------------------------//
// DSP System_Side_PRBS_Control
//----------------------------------------------------------------------------------//
void DSP_System_Side_PRBS_SET(uint8_t Pattern ,uint8_t Lane_CH ,uint8_t Enable)
{
    //Lane Select
    if(Lane_CH==0)
    {
        COMMAND_4700_DATA[0]  = 0x00000030 ;
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000001 , 0x5201CA84 , 0x00008016 );
    }
    else if(Lane_CH==1)
    {
        COMMAND_4700_DATA[0]  = 0x00010030 ;
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000002 , 0x5201CA84 , 0x00008016 );
    }
    else if(Lane_CH==2)
    {
        COMMAND_4700_DATA[0]  = 0x000200030 ;
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000004 , 0x5201CA84 , 0x00008016 );
    }
    else if(Lane_CH==3)
    {
        COMMAND_4700_DATA[0]  = 0x00030030 ;
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000008 , 0x5201CA84 , 0x00008016 );
    }
    else if(Lane_CH==4)
    {
        COMMAND_4700_DATA[0]  = 0x00040030 ;
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000010 , 0x5201CA84 , 0x00008016 );
    }
    else if(Lane_CH==5)
    {
        COMMAND_4700_DATA[0]  = 0x00050030 ;
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000020 , 0x5201CA84 , 0x00008016 );
    }
    else if(Lane_CH==6)
    {
        COMMAND_4700_DATA[0]  = 0x000600030 ;
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000040 , 0x5201CA84 , 0x00008016 );
    }
    else if(Lane_CH==7)
    {
        COMMAND_4700_DATA[0]  = 0x00070030 ;
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x00000080 , 0x5201CA84 , 0x00008016 );
    }
    else if(Lane_CH==All_CH)
    {
        COMMAND_4700_DATA[0]  = 0x00080030 ;
        DSP87400_CAPI_CW_CMD( 0x5201CA88 , 0x000000FF , 0x5201CA84 , 0x00008016 );
    }
    //PRBS Generator
    COMMAND_4700_DATA[1]  = 0x00000001;
    COMMAND_4700_DATA[2]  = 0x00000000;
    //PRBS Enable
    if(Enable)
        COMMAND_4700_DATA[3]  = 0x00000001;
    else
        COMMAND_4700_DATA[3]  = 0x00000000;
    //PRBS Pattern
    COMMAND_4700_DATA[4] = Pattern;

    COMMAND_4700_DATA[5] = 0x00000000;
    COMMAND_4700_DATA[6] = 0x00000000;
    COMMAND_4700_DATA[7] = 0x00000000;
    COMMAND_4700_DATA[8] = 0x00000000;
    COMMAND_4700_DATA[9] = 0x00000000;
    COMMAND_4700_DATA[10] = 0x00000000;
    COMMAND_4700_DATA[11] = 0x00000000;
    COMMAND_4700_DATA[12] = 0x00000000;
    
    //Lane0-Lane3
    DSP87400_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA84);
    DSP87400_CAPI_Command_response(0x5201CA8C,0x5201CA90);   
}
//----------------------------------------------------------------------------------//
// DSP System_Side_SNR LTP
//----------------------------------------------------------------------------------//
void DSP_SystemSide_SNR_LTP_GET(uint8_t Lane_CH)
{
    uint8_t i;
    uint32_t Address=0x00047004,Data_Buffer;
    uint16_t mode,cmis_cap = 0xFFFE, cmis_out;
    double eye_u, eye_d, p1_lvl, sigma, p1_lvl_3rd, e_hlf;

    BRCM_Control_READ_Data(0x5201CA84);
    // Write Data size
    BRCM_Control_WRITE_Data( 0x00047000 , (0x0000005C | Lane_CH<<16), BRCM_Default_Length);
    // Set value start 1 to 23
    for(i=0;i<23;i++)
    {
        BRCM_Control_WRITE_Data( Address , 0xCCCCCCCC , BRCM_Default_Length);
        Address+=4;
    }
    // Set Lane
    BRCM_Control_WRITE_Data( 0x5201CA88 , (0x00000001<<Lane_CH) , BRCM_Default_Length);
    // Set Command
    BRCM_Control_WRITE_Data( 0x5201CA84 , 0x00008050 , BRCM_Default_Length);
    // Check feedback status
    for(i=0;i<50;i++)
    {
        Data_Buffer=BRCM_Control_READ_Data(0x5201CA8C);
        if(Data_Buffer&0x8000)
            break;
        else
            delay_1ms(20);
    }
    // Read Select Lane
    BRCM_Control_READ_Data(0x5201CA90);
    // Read Command
    BRCM_Control_READ_Data(0x00047400);
    // Get eye_u and eye_d Data
    Data_Buffer=BRCM_Control_READ_Data(0x00047404);
    eye_u=Data_Buffer&0x0000FFFF;
    eye_d=(Data_Buffer&0xFFFF0000)>>16;
    // Get p1_lvl and mode Data
    Data_Buffer=BRCM_Control_READ_Data(0x00047408);
    p1_lvl=Data_Buffer&0x0000FFFF;
    mode=(Data_Buffer&0xFFFF0000)>>16;
    // Read rest register
    Address=0x0004740C;
    for(i=0;i<21;i++)
    {
        BRCM_Control_READ_Data( Address );
        Address+=4;
    }
    // Clear
    BRCM_Control_WRITE_Data( 0x5201CA84 , 0x00000000 , BRCM_Default_Length);
    BRCM_Control_WRITE_Data( 0x5201CA94 , 0x00000000 , BRCM_Default_Length);
    BRCM_Control_WRITE_Data( 0x5201CA74 , 0x00000000 , BRCM_Default_Length);
    BRCM_Control_WRITE_Data( 0x5201CAA4 , 0x00000000 , BRCM_Default_Length);
    BRCM_Control_WRITE_Data( 0x5201CA8C , 0x00000000 , BRCM_Default_Length);
    BRCM_Control_WRITE_Data( 0x5201CA90 , 0x00000000 , BRCM_Default_Length);
    
    // calulate snr and ltp
    e_hlf = (eye_u+eye_d)/2;
    p1_lvl_3rd = p1_lvl/3;
    if(mode)
    {
        sigma = (p1_lvl_3rd - e_hlf)/3.09;
        if (sigma <= 0)
            DSP_SystemSide_SNR = 0xFFFF;
        else
        {
            cmis_out = (uint32_t)(20*log10(p1_lvl_3rd/sigma)*256);
            DSP_SystemSide_SNR = (cmis_out > cmis_cap)? cmis_cap : cmis_out;
        }
    } 
    else 
    {
        sigma = (p1_lvl - e_hlf)/4.26488;
        if (sigma <= 0)
            DSP_SystemSide_SNR = 0xFFFF;
        else
        {
            cmis_out = (uint32_t)(20*log10(p1_lvl/sigma)*256);
            DSP_SystemSide_SNR = (cmis_out > cmis_cap)? cmis_cap : cmis_out;
        }
    }
    if (sigma <= 0)
        DSP_SystemSide_LTP = 0xFFFF;
    else
    {
        sigma /= p1_lvl_3rd;
        cmis_out = (uint32_t)(10*log10(1/exp(-1./(2*(sigma*sigma))))*256);
        DSP_SystemSide_LTP = (cmis_out > cmis_cap) ? cmis_cap : cmis_out;
    }
    // Convert to dec equal hex
    DSP_SystemSide_SNR=DEC_EQUAL_TO_HEX(DSP_SystemSide_SNR);
    DSP_SystemSide_LTP=DEC_EQUAL_TO_HEX(DSP_SystemSide_LTP);
}