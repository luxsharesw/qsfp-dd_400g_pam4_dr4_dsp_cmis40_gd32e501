#include "gd32e501.h"
#include "systick.h"
#include "GD32E501_GPIO_Customize_Define.h"
#include "CMIS_MSA.h"
#include "TEC_APC_Control.h"
#include "LDD_Control.h"


struct TEC_APC_Control_MEMORY TEC_APC_Control_MEMORY_MAP;

void  MPD_ANODE_ADC_GET()
{
    // Tx MPD Power voltage
    //TEC_APC_Control_MEMORY_MAP.MPD_ANODE_ADC_TX1 = Swap_Bytes(GET_ADC_Value_Data(TX1MPDANODE));
    //TEC_APC_Control_MEMORY_MAP.MPD_ANODE_ADC_TX2 = Swap_Bytes(GET_ADC_Value_Data(TX2MPDANODE));
    //TEC_APC_Control_MEMORY_MAP.MPD_ANODE_ADC_TX3 = Swap_Bytes(GET_ADC_Value_Data(TX3MPDANODE));
    //TEC_APC_Control_MEMORY_MAP.MPD_ANODE_ADC_TX4 = Swap_Bytes(GET_ADC_Value_Data(TX4MPDANODE));    
    
    
    TEC_APC_Control_MEMORY_MAP.MPD_ANODE_ADC_TX1 = Swap_Bytes(GET_ADC_RowData(TX1MPDANODE));
    TEC_APC_Control_MEMORY_MAP.MPD_ANODE_ADC_TX2 = Swap_Bytes(GET_ADC_RowData(TX2MPDANODE));
    TEC_APC_Control_MEMORY_MAP.MPD_ANODE_ADC_TX3 = Swap_Bytes(GET_ADC_RowData(TX3MPDANODE));
    TEC_APC_Control_MEMORY_MAP.MPD_ANODE_ADC_TX4 = Swap_Bytes(GET_ADC_RowData(TX4MPDANODE));   
}

void TEC_MPD_ADC_GET()
{
    // TEC Feedback voltage
    TEC_APC_Control_MEMORY_MAP.TEC_MPD_ADC_TX1 = Swap_Bytes(GET_ADC_Value_Data(TX1DMPD));
    TEC_APC_Control_MEMORY_MAP.TEC_MPD_ADC_TX2 = Swap_Bytes(GET_ADC_Value_Data(TX2DMPD));
    TEC_APC_Control_MEMORY_MAP.TEC_MPD_ADC_TX3 = Swap_Bytes(GET_ADC_Value_Data(TX3DMPD));
    TEC_APC_Control_MEMORY_MAP.TEC_MPD_ADC_TX4 = Swap_Bytes(GET_ADC_Value_Data(TX4DMPD));
/*    TEC_APC_Control_MEMORY_MAP.TEC_MPD_ADC_TX1 = Swap_Bytes(GET_ADC_RowData(TX1DMPD));
    TEC_APC_Control_MEMORY_MAP.TEC_MPD_ADC_TX2 = Swap_Bytes(GET_ADC_RowData(TX2DMPD));
    TEC_APC_Control_MEMORY_MAP.TEC_MPD_ADC_TX3 = Swap_Bytes(GET_ADC_RowData(TX3DMPD));
    TEC_APC_Control_MEMORY_MAP.TEC_MPD_ADC_TX4 = Swap_Bytes(GET_ADC_RowData(TX4DMPD));*/
}

void RFPD_BIAS_ADC_GET()
{
    // RSSI voltage
    TEC_APC_Control_MEMORY_MAP.RFPD_BIAS_TX1 = Swap_Bytes(GET_ADC_Value_Data(REF_D1_BIAS));
    TEC_APC_Control_MEMORY_MAP.RFPD_BIAS_TX2 = Swap_Bytes(GET_ADC_Value_Data(REF_D2_BIAS));
    TEC_APC_Control_MEMORY_MAP.RFPD_BIAS_TX3 = Swap_Bytes(GET_ADC_Value_Data(REF_D3_BIAS));
    TEC_APC_Control_MEMORY_MAP.RFPD_BIAS_TX4 = Swap_Bytes(GET_ADC_Value_Data(REF_D4_BIAS));  
}

void GD32E501_DAC_Control(uint8_t Channel,uint16_t DAC_Data)
{
    if(Channel==1)
        dac_data_set(DAC0,0,DAC_ALIGN_12B_R,DAC_Data); 
    else if(Channel==2)
        dac_data_set(DAC0,1,DAC_ALIGN_12B_R,DAC_Data); 
    else if(Channel==3)
        dac_data_set(DAC1,0,DAC_ALIGN_12B_R,DAC_Data); 
    else if(Channel==4)
        dac_data_set(DAC1,1,DAC_ALIGN_12B_R,DAC_Data); 
}

void DAC_TO_TOPS_SETTING()
{
    uint16_t DAC_OUT_Value = 0 ;
    
    DAC_OUT_Value = ( LDD_Control_MEMORY_MAP.DAC_TOPS_MSB_CH1<<8 | LDD_Control_MEMORY_MAP.DAC_TOPS_LSB_CH1 );
    GD32E501_DAC_Control(1,DAC_OUT_Value);
    DAC_OUT_Value = ( LDD_Control_MEMORY_MAP.DAC_TOPS_MSB_CH2<<8 | LDD_Control_MEMORY_MAP.DAC_TOPS_LSB_CH2 );
    GD32E501_DAC_Control(2,DAC_OUT_Value);  
    DAC_OUT_Value = ( LDD_Control_MEMORY_MAP.DAC_TOPS_MSB_CH3<<8 | LDD_Control_MEMORY_MAP.DAC_TOPS_LSB_CH3 );
    GD32E501_DAC_Control(3,DAC_OUT_Value); 
    DAC_OUT_Value = ( LDD_Control_MEMORY_MAP.DAC_TOPS_MSB_CH4<<8 | LDD_Control_MEMORY_MAP.DAC_TOPS_LSB_CH4 );
    GD32E501_DAC_Control(4,DAC_OUT_Value);     
}

/*
void DAC_TO_TOPS_SETTING()
{
    GD32E501_DAC_Control(1,Swap_Bytes(TEC_APC_Control_MEMORY_MAP.DAC_TO_TOPS_TX1));
    GD32E501_DAC_Control(2,Swap_Bytes(TEC_APC_Control_MEMORY_MAP.DAC_TO_TOPS_TX2));
    GD32E501_DAC_Control(3,Swap_Bytes(TEC_APC_Control_MEMORY_MAP.DAC_TO_TOPS_TX3));
    GD32E501_DAC_Control(4,Swap_Bytes(TEC_APC_Control_MEMORY_MAP.DAC_TO_TOPS_TX4));   
//    DAC_Svae_Data();
}
*/

void Acacia_Tx_Monitor_ADC()
{
    MPD_ANODE_ADC_GET();
    TEC_MPD_ADC_GET();
    RFPD_BIAS_ADC_GET();
}

void APC_Control(uint8_t Channel)
{
    uint16_t GET_Target_Value1 = 0 , GET_Target_Value2 = 0;
    uint16_t MPD_Temp1 = 0 , MPD_Temp2 = 0;
    uint16_t Updata_DAC_Current = 0 ;
    uint16_t sum_error1 = 0 ,sum_error2 = 0 ; 
    uint16_t Current_Data = 0;
    uint16_t TempBuffer;
    uint16_t error = 0 ;
    uint16_t Update_Current_flag = 0 ;
    float KP = 0.3;
    float KI = 0.05;
    // Power target value
    // TX MPD & Bias current close loop
    // MP5490 Bias lut & Tx MPD control
    // Temperature Index 
    if(Channel<2)
    {
        GET_Target_Value1 = (LDD_Control_MEMORY_MAP.APC_Target_value_MSB_CH1 << 8 | LDD_Control_MEMORY_MAP.APC_Target_value_LSB_CH1);
        GET_Target_Value2 = (LDD_Control_MEMORY_MAP.APC_Target_value_MSB_CH2 << 8 | LDD_Control_MEMORY_MAP.APC_Target_value_LSB_CH2);
        MPD_Temp1 = GET_ADC_RowData(TX1MPDANODE);
        MPD_Temp2 = GET_ADC_RowData(TX2MPDANODE);
        Current_Data = (LDD_Control_MEMORY_MAP.MP5940_IDAC_MSB_CH1 << 8 | LDD_Control_MEMORY_MAP.MP5940_IDAC_LSB_CH1);     
    }
    else 
    {
        GET_Target_Value1 = (LDD_Control_MEMORY_MAP.APC_Target_value_MSB_CH3 << 8 | LDD_Control_MEMORY_MAP.APC_Target_value_LSB_CH3);
        GET_Target_Value2 = (LDD_Control_MEMORY_MAP.APC_Target_value_MSB_CH4 << 8 | LDD_Control_MEMORY_MAP.APC_Target_value_LSB_CH4);
        MPD_Temp1 = GET_ADC_RowData(TX3MPDANODE);
        MPD_Temp2 = GET_ADC_RowData(TX4MPDANODE);
        Current_Data = (LDD_Control_MEMORY_MAP.MP5940_IDAC_MSB_CH3 << 8 | LDD_Control_MEMORY_MAP.MP5940_IDAC_LSB_CH3);
    }
    
    if( (GET_Target_Value1>MPD_Temp1) && (GET_Target_Value2>MPD_Temp2) )
    {
        error = GET_Target_Value1 - MPD_Temp1 ;
        sum_error1 = KP*error + KI*error ;
        error = GET_Target_Value2 - MPD_Temp2 ;
        sum_error2 = KP*error + KI*error ;
        Updata_DAC_Current = ( Current_Data + (sum_error1+sum_error2)/2);
        Update_Current_flag = 1; 
    }
    else if( (GET_Target_Value1<MPD_Temp1) && (GET_Target_Value2<MPD_Temp2) )
    {
        error = MPD_Temp1 - GET_Target_Value1 ;
        sum_error1 = KP*error + KI*error ;     
        error = MPD_Temp2 - GET_Target_Value2 ;   
        sum_error2 = KP*error + KI*error ;      
        Updata_DAC_Current = ( Current_Data - (sum_error1+sum_error2)/2);     
        Update_Current_flag = 1;        
    }   

    if(Update_Current_flag==1)
    {
        if(Channel<2)
            Update_Bias_Current(0,Updata_DAC_Current);
        else 
            Update_Bias_Current(2,Updata_DAC_Current);
    }
}


void TEC_Control(uint8_t Channel)
{
    uint16_t GET_Target_value = 0;
    uint16_t Get_MPD_Vaule = 0;
    uint16_t Updata_DAC_Current = 0 ;
    uint16_t Current_Data = 0;
    uint16_t sum_error = 0 ;
    uint16_t error = 0 ;
    float KP = 0.08;
    float KI = 0.008;
    // PI Control
    // KP Initial value ?
    // KI Initial value ?
    // out = Kp*error + Ki*SumError ;
    // Target value ?
    
    if(Channel==0)
    {
        GET_Target_value = ( LDD_Control_MEMORY_MAP.TEC_Target_value_MSB_CH1<<8 | LDD_Control_MEMORY_MAP.TEC_Target_value_LSB_CH1 );
        Get_MPD_Vaule = GET_ADC_RowData(TX1DMPD);
        Current_Data = ( LDD_Control_MEMORY_MAP.DAC_TOPS_MSB_CH1<<8 | LDD_Control_MEMORY_MAP.DAC_TOPS_LSB_CH1 );
    }
    else if(Channel==1)
    {
        GET_Target_value = ( LDD_Control_MEMORY_MAP.TEC_Target_value_MSB_CH2<<8 | LDD_Control_MEMORY_MAP.TEC_Target_value_LSB_CH2 );
        Get_MPD_Vaule = GET_ADC_RowData(TX2DMPD);
        Current_Data = ( LDD_Control_MEMORY_MAP.DAC_TOPS_MSB_CH2<<8 | LDD_Control_MEMORY_MAP.DAC_TOPS_LSB_CH2 );
    }
    else if(Channel==2)
    {
        GET_Target_value = ( LDD_Control_MEMORY_MAP.TEC_Target_value_MSB_CH3<<8 | LDD_Control_MEMORY_MAP.TEC_Target_value_LSB_CH3 );
        Get_MPD_Vaule = GET_ADC_RowData(TX3DMPD);
        Current_Data = ( LDD_Control_MEMORY_MAP.DAC_TOPS_MSB_CH3<<8 | LDD_Control_MEMORY_MAP.DAC_TOPS_LSB_CH3 );
    }    
    else if(Channel==3)
    {
        GET_Target_value = ( LDD_Control_MEMORY_MAP.TEC_Target_value_MSB_CH4<<8 | LDD_Control_MEMORY_MAP.TEC_Target_value_LSB_CH4 );
        Get_MPD_Vaule = GET_ADC_RowData(TX4DMPD);
        Current_Data = ( LDD_Control_MEMORY_MAP.DAC_TOPS_MSB_CH4<<8 | LDD_Control_MEMORY_MAP.DAC_TOPS_LSB_CH4 );
    } 

    // DAC Voltage++ >> MPD Vaule --
    if(Get_MPD_Vaule>GET_Target_value)
    {
        error = Get_MPD_Vaule - GET_Target_value ;
        sum_error = KP*error + KI*error ;
        Updata_DAC_Current = Current_Data + sum_error;
        
    }
    else
    {
        error = GET_Target_value - Get_MPD_Vaule ;
        sum_error = KP*error + KI*error ;
        if(sum_error>Current_Data)
            Updata_DAC_Current = 100;
        else
            Updata_DAC_Current = Current_Data - sum_error;
    }     
		
		if(Updata_DAC_Current>4000 || Updata_DAC_Current<100)
            Updata_DAC_Current = 2048;
    
/*    if(GET_Target_value>Get_MPD_Vaule)
    {
        error = GET_Target_value - Get_MPD_Vaule ;
        sum_error = KP*error + KI*error ;
        // max can't over 100 count
        if(sum_error>100)
            sum_error = 100;
        Updata_DAC_Current = Current_Data + sum_error;
    }
    else
    {
        error = Get_MPD_Vaule - GET_Target_value ;
        sum_error = KP*error + KI*error ;
        // max can't over 100 count
        if(sum_error>100)
            sum_error = 100;
        Updata_DAC_Current = Current_Data - sum_error;
    }*/
    
    if(Channel==0)    
    {
        GD32E501_DAC_Control(1,Updata_DAC_Current);
        TEC_APC_Control_MEMORY_MAP.TEC_SumError_CH1 = sum_error;
        TEC_APC_Control_MEMORY_MAP.DATA_BUFFER[0] = Swap_Bytes(Updata_DAC_Current);
        
        LDD_Control_MEMORY_MAP.DAC_TOPS_LSB_CH1 = Updata_DAC_Current ;
        LDD_Control_MEMORY_MAP.DAC_TOPS_MSB_CH1 = Updata_DAC_Current >>8 ;
    }
    else if(Channel==1)  
    {
        GD32E501_DAC_Control(2,Updata_DAC_Current);
        TEC_APC_Control_MEMORY_MAP.TEC_SumError_CH2 = sum_error;
        TEC_APC_Control_MEMORY_MAP.DATA_BUFFER[1] = Swap_Bytes(Updata_DAC_Current);
        LDD_Control_MEMORY_MAP.DAC_TOPS_LSB_CH2 = Updata_DAC_Current ;
        LDD_Control_MEMORY_MAP.DAC_TOPS_MSB_CH2 = Updata_DAC_Current >>8 ;
    }
    else if(Channel==2)  
    {
        GD32E501_DAC_Control(3,Updata_DAC_Current);
        TEC_APC_Control_MEMORY_MAP.TEC_SumError_CH3 = sum_error;
        TEC_APC_Control_MEMORY_MAP.DATA_BUFFER[2] = Swap_Bytes(Updata_DAC_Current);
        LDD_Control_MEMORY_MAP.DAC_TOPS_LSB_CH3 = Updata_DAC_Current ;
        LDD_Control_MEMORY_MAP.DAC_TOPS_MSB_CH3 = Updata_DAC_Current >>8 ;
    }
    else if(Channel==3)  
    {
        GD32E501_DAC_Control(4,Updata_DAC_Current);
        TEC_APC_Control_MEMORY_MAP.TEC_SumError_CH4 = sum_error;
        TEC_APC_Control_MEMORY_MAP.DATA_BUFFER[3] = Swap_Bytes(Updata_DAC_Current);
        LDD_Control_MEMORY_MAP.DAC_TOPS_LSB_CH4 = Updata_DAC_Current ;
        LDD_Control_MEMORY_MAP.DAC_TOPS_MSB_CH4 = Updata_DAC_Current >>8 ;
    }
}


void TEC_ALL_SET()
{
    TEC_Control(0);
    TEC_Control(1);
    TEC_Control(2);
    TEC_Control(3);
}


