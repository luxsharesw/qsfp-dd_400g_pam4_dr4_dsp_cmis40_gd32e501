#include "gd32e501.h"
#include "systick.h"
#include "GD32E501_GPIO_Customize_Define.h"
#include "CMIS_MSA.h"
#include "GD32E501_M_I2C2_PC78.h"
#include "LDD_Control.h"
#include "CMIS_MSA.h"

#define Acacia_LDD_SADR       0xE0

uint8_t Get_SPData[128] = {0};
uint16_t Get_SPLawData[50]={0};

void LDD_AGC_Control(uint8_t Channel ,uint8_t Enable)
{
    uint16_t Control_ADR = 0;
    uint16_t Read_buffer = 0;
    
    if(Channel==0)
        Control_ADR = 0x0002;
    else if(Channel==1)
        Control_ADR = 0x1002;
    else if(Channel==2)
        Control_ADR = 0x2002;
    else if(Channel==3)
        Control_ADR = 0x3002;
    
    Read_buffer = MI2C2_WordADR_R_PC78(Acacia_LDD_SADR,Control_ADR);
    
    if(Enable)
        Read_buffer |= 0x0100;
    else
        Read_buffer &=~0x0100;
    
    MI2C2_WordADR_W_PC78(Acacia_LDD_SADR,Control_ADR,Read_buffer);
}

void LDD_Reference_setting(uint8_t Channel ,uint8_t REF_DATA)
{
    uint16_t Control_ADR = 0;
    uint16_t Read_buffer = 0;
    
    if(Channel==0)
        Control_ADR = 0x0014;
    else if(Channel==1)
        Control_ADR = 0x1014;
    else if(Channel==2)
        Control_ADR = 0x2014;
    else if(Channel==3)
        Control_ADR = 0x3014;  

    Read_buffer = MI2C2_WordADR_R_PC78(Acacia_LDD_SADR,Control_ADR);    
    Read_buffer = (( Read_buffer & 0xF0 ) | REF_DATA );
    
    MI2C2_WordADR_W_PC78(Acacia_LDD_SADR,Control_ADR,Read_buffer);
}

void Acacia_LDD_Interface_test()
{
    LDD_Control_MEMORY_MAP.LDD_Buffer[0] = MI2C2_WordADR_R_PC78(Acacia_LDD_SADR,0x4000);
}

void Acaaia_LDD_Direct_Control()
{
    if(LDD_Control_MEMORY_MAP.Direct_Satrt==0xAA)
    {
        MI2C2_WordADR_W_PC78(Acacia_LDD_SADR,LDD_Control_MEMORY_MAP.Driect_MEMADR,LDD_Control_MEMORY_MAP.Driect_WBUFF);
        LDD_Control_MEMORY_MAP.Direct_Satrt = 0x00;
    }
    if(LDD_Control_MEMORY_MAP.Direct_Satrt==0xBB)
    {
        LDD_Control_MEMORY_MAP.Driect_RBUFF = MI2C2_WordADR_R_PC78(Acacia_LDD_SADR,LDD_Control_MEMORY_MAP.Driect_MEMADR);
        LDD_Control_MEMORY_MAP.Direct_Satrt = 0x00;
    }
}

void Acacia_ADC_Pseudocode_Temperature(uint8_t Channel,uint8_t SP_Value)
{
    uint16_t Connect_Channel_ADR = 0 ;
    uint16_t Sensor_Point_ADR = 0;
    if(Channel==0)
    {
        Connect_Channel_ADR = 0x0008;
        Sensor_Point_ADR = 0x0016;
    }
    else if(Channel==1)
    {
        Connect_Channel_ADR = 0x1008;
        Sensor_Point_ADR = 0x1016;
    }
    else if(Channel==2)
    {
        Connect_Channel_ADR = 0x2008;
        Sensor_Point_ADR = 0x2016;
    }
    else if(Channel==3)
    {
        Connect_Channel_ADR = 0x3008;
        Sensor_Point_ADR = 0x3016;
    }
    // Define ADC_Read
    //soft reset ADC-assert reset bits
    MI2C2_WordADR_HLBW_PC78(Acacia_LDD_SADR,0x4006,0x00,0x00); 
    //soft reset ADC-de-assert reset bits
    MI2C2_WordADR_HLBW_PC78(Acacia_LDD_SADR,0x4006,0x00,0x03); 
    //clear ADC measure start bit (bit 0) and ensure PD is 0
    MI2C2_WordADR_HLBW_PC78(Acacia_LDD_SADR,0x4002,0x00,0x00); 
    //connect Global-ATB to the ADC inpu
    MI2C2_WordADR_HLBW_PC78(Acacia_LDD_SADR,0x4008,0x00,0x01);    
    // disconnect Channel-N-ATB to Global-ATB
    MI2C2_WordADR_HLBW_PC78(Acacia_LDD_SADR,0x0016,0x00,0x00);
    MI2C2_WordADR_HLBW_PC78(Acacia_LDD_SADR,0x1016,0x00,0x00);
    MI2C2_WordADR_HLBW_PC78(Acacia_LDD_SADR,0x2016,0x00,0x00);
    MI2C2_WordADR_HLBW_PC78(Acacia_LDD_SADR,0x3016,0x00,0x00);
    //clear ADC measure start bit (bit 0) and ensure PD is 0
    MI2C2_WordADR_HLBW_PC78(Acacia_LDD_SADR,0x4020,0x00,0x00); 
    //connect Channel-N-sense-node to Channel-N-ATB
    MI2C2_WordADR_HLBW_PC78(Acacia_LDD_SADR,Connect_Channel_ADR,0x00,0x01);  
    //connect Global-ATB to the ADC inpu
    MI2C2_WordADR_HLBW_PC78(Acacia_LDD_SADR,Sensor_Point_ADR,0x00,SP_Value);   

}

uint16_t Acacia_LDD_DiaTemp(uint8_t Channel)
{
    uint16_t Get_ADC_Data = 0;
    uint16_t Dia_temperarute = 0;
    Acacia_ADC_Pseudocode_Temperature(Channel,32);
    //start ADC read
    MI2C2_WordADR_HLBW_PC78(Acacia_LDD_SADR,0x4002,0x00,0x01);
    //Get ADC Data
    delay_1ms(1);
    Get_ADC_Data = ( MI2C2_WordADR_R_PC78(Acacia_LDD_SADR,0x4004) & 0x03FF );
    //Cal temperature value
    Dia_temperarute = ((Get_ADC_Data*2.44)-540)/10.2 ;   
    // Clear ADC
    MI2C2_WordADR_HLBW_PC78(Acacia_LDD_SADR,0x4002,0x00,0x00);
    
    return Dia_temperarute;
}

uint16_t Acacia_LDD_LawDataSP_Update(uint8_t Channel,uint8_t SP_Value)
{
    uint16_t Get_ADC_Data = 0;
    uint16_t returnData = 0 ;
    Acacia_ADC_Pseudocode_Temperature(Channel,SP_Value);
    //start ADC read
    MI2C2_WordADR_HLBW_PC78(Acacia_LDD_SADR,0x4002,0x00,0x01);
    //Get ADC Data
    delay_1ms(1);
    Get_ADC_Data = ( MI2C2_WordADR_R_PC78(Acacia_LDD_SADR,0x4004) & 0x03FF );
    
    returnData = Get_ADC_Data;
    // Clear ADC
    MI2C2_WordADR_HLBW_PC78(Acacia_LDD_SADR,0x4002,0x00,0x00);
    
    return returnData;
}

uint16_t Acacia_LDD_SP_Update(uint8_t Channel,uint8_t SP_Value)
{
    uint16_t Get_ADC_Data = 0;
    uint16_t returnData = 0 ;
    Acacia_ADC_Pseudocode_Temperature(Channel,SP_Value);
    //start ADC read
    MI2C2_WordADR_HLBW_PC78(Acacia_LDD_SADR,0x4002,0x00,0x01);
    //Get ADC Data
    delay_1ms(1);
    Get_ADC_Data = ( MI2C2_WordADR_R_PC78(Acacia_LDD_SADR,0x4004) & 0x03FF );
    
    returnData = Get_ADC_Data*2.44 ;
    // Clear ADC
    MI2C2_WordADR_HLBW_PC78(Acacia_LDD_SADR,0x4002,0x00,0x00);
    
    return returnData;
}


void Acacia_LDD_Read_ALL()
{
    uint16_t GET_BUFFER = 0;
    // Acacia reg 4000
    GET_BUFFER = MI2C2_WordADR_R_PC78(Acacia_LDD_SADR,0x4000);
    LDD_Control_MEMORY_MAP.GLBL_Acacia_LDD_CHID_LSB = GET_BUFFER;
    LDD_Control_MEMORY_MAP.GLBL_Acacia_LDD_CHID_MSB = GET_BUFFER>>8;
    // Acacia reg 400C
    GET_BUFFER = MI2C2_WordADR_R_PC78(Acacia_LDD_SADR,0x400C);
    LDD_Control_MEMORY_MAP.GLBL_BIAS_GEN_CTRL_LSB = GET_BUFFER;
    LDD_Control_MEMORY_MAP.GLBL_BIAS_GEN_CTRL_MSB = GET_BUFFER>>8;
    // Acacia reg 0x0002 CH1
    GET_BUFFER = MI2C2_WordADR_R_PC78(Acacia_LDD_SADR,0x0002);
    LDD_Control_MEMORY_MAP.XI_AGC1_CH1_LSB = GET_BUFFER;
    LDD_Control_MEMORY_MAP.XI_AGC1_CH1_MSB = GET_BUFFER>>8;
    // Acacia reg 0x000C CH1
    GET_BUFFER = MI2C2_WordADR_R_PC78(Acacia_LDD_SADR,0x000C);
    LDD_Control_MEMORY_MAP.XI_RF2_VGA_CH1_LSB = GET_BUFFER;
    LDD_Control_MEMORY_MAP.XI_RF2_VGA_CH1_MSB = GET_BUFFER>>8;    
    // Acacia reg 0x0010 CH1
    GET_BUFFER = MI2C2_WordADR_R_PC78(Acacia_LDD_SADR,0x0010);
    LDD_Control_MEMORY_MAP.XI_RF4_DRV_CH1_LSB = GET_BUFFER;
    LDD_Control_MEMORY_MAP.XI_RF4_DRV_CH1_MSB = GET_BUFFER>>8; 
    // Acacia reg 0x0014 CH1
    GET_BUFFER = MI2C2_WordADR_R_PC78(Acacia_LDD_SADR,0x0014);
    LDD_Control_MEMORY_MAP.XI_RF6_CH1_LSB = GET_BUFFER;
    LDD_Control_MEMORY_MAP.XI_RF6_CH1_MSB = GET_BUFFER>>8;  
    // Acacia reg 0x1002 CH1
    GET_BUFFER = MI2C2_WordADR_R_PC78(Acacia_LDD_SADR,0x1002);
    LDD_Control_MEMORY_MAP.XI_AGC1_CH2_LSB = GET_BUFFER;
    LDD_Control_MEMORY_MAP.XI_AGC1_CH2_MSB = GET_BUFFER>>8;
    // Acacia reg 0x100C CH1
    GET_BUFFER = MI2C2_WordADR_R_PC78(Acacia_LDD_SADR,0x100C);
    LDD_Control_MEMORY_MAP.XI_RF2_VGA_CH2_LSB = GET_BUFFER;
    LDD_Control_MEMORY_MAP.XI_RF2_VGA_CH2_MSB = GET_BUFFER>>8;    
    // Acacia reg 0x1010 CH1
    GET_BUFFER = MI2C2_WordADR_R_PC78(Acacia_LDD_SADR,0x1010);
    LDD_Control_MEMORY_MAP.XI_RF4_DRV_CH2_LSB = GET_BUFFER;
    LDD_Control_MEMORY_MAP.XI_RF4_DRV_CH2_MSB = GET_BUFFER>>8; 
    // Acacia reg 0x1014 CH1
    GET_BUFFER = MI2C2_WordADR_R_PC78(Acacia_LDD_SADR,0x1014);
    LDD_Control_MEMORY_MAP.XI_RF6_CH2_LSB = GET_BUFFER;
    LDD_Control_MEMORY_MAP.XI_RF6_CH2_MSB = GET_BUFFER>>8; 
    // Acacia reg 0x2002 CH1
    GET_BUFFER = MI2C2_WordADR_R_PC78(Acacia_LDD_SADR,0x2002);
    LDD_Control_MEMORY_MAP.XI_AGC1_CH3_LSB = GET_BUFFER;
    LDD_Control_MEMORY_MAP.XI_AGC1_CH3_MSB = GET_BUFFER>>8;
    // Acacia reg 0x200C CH1
    GET_BUFFER = MI2C2_WordADR_R_PC78(Acacia_LDD_SADR,0x200C);
    LDD_Control_MEMORY_MAP.XI_RF2_VGA_CH3_LSB = GET_BUFFER;
    LDD_Control_MEMORY_MAP.XI_RF2_VGA_CH3_MSB = GET_BUFFER>>8;    
    // Acacia reg 0x2010 CH1
    GET_BUFFER = MI2C2_WordADR_R_PC78(Acacia_LDD_SADR,0x2010);
    LDD_Control_MEMORY_MAP.XI_RF4_DRV_CH3_LSB = GET_BUFFER;
    LDD_Control_MEMORY_MAP.XI_RF4_DRV_CH3_MSB = GET_BUFFER>>8; 
    // Acacia reg 0x2014 CH1
    GET_BUFFER = MI2C2_WordADR_R_PC78(Acacia_LDD_SADR,0x2014);
    LDD_Control_MEMORY_MAP.XI_RF6_CH3_LSB = GET_BUFFER;
    LDD_Control_MEMORY_MAP.XI_RF6_CH3_MSB = GET_BUFFER>>8; 
    // Acacia reg 0x3002 CH1
    GET_BUFFER = MI2C2_WordADR_R_PC78(Acacia_LDD_SADR,0x3002);
    LDD_Control_MEMORY_MAP.XI_AGC1_CH4_LSB = GET_BUFFER;
    LDD_Control_MEMORY_MAP.XI_AGC1_CH4_MSB = GET_BUFFER>>8;
    // Acacia reg 0x300C CH1
    GET_BUFFER = MI2C2_WordADR_R_PC78(Acacia_LDD_SADR,0x300C);
    LDD_Control_MEMORY_MAP.XI_RF2_VGA_CH4_LSB = GET_BUFFER;
    LDD_Control_MEMORY_MAP.XI_RF2_VGA_CH4_MSB = GET_BUFFER>>8;    
    // Acacia reg 0x3010 CH1
    GET_BUFFER = MI2C2_WordADR_R_PC78(Acacia_LDD_SADR,0x3010);
    LDD_Control_MEMORY_MAP.XI_RF4_DRV_CH4_LSB = GET_BUFFER;
    LDD_Control_MEMORY_MAP.XI_RF4_DRV_CH4_MSB = GET_BUFFER>>8; 
    // Acacia reg 0x3014 CH1
    GET_BUFFER = MI2C2_WordADR_R_PC78(Acacia_LDD_SADR,0x3014);
    LDD_Control_MEMORY_MAP.XI_RF6_CH4_LSB = GET_BUFFER;
    LDD_Control_MEMORY_MAP.XI_RF6_CH4_MSB = GET_BUFFER>>8; 
}

void Acacia_LDD_Write_ALL()
{
    MI2C2_WordADR_HLBW_PC78(Acacia_LDD_SADR,0x0002,LDD_Control_MEMORY_MAP.XI_AGC1_CH1_MSB    ,LDD_Control_MEMORY_MAP.XI_AGC1_CH1_LSB);
    MI2C2_WordADR_HLBW_PC78(Acacia_LDD_SADR,0x000C,LDD_Control_MEMORY_MAP.XI_RF2_VGA_CH1_MSB ,LDD_Control_MEMORY_MAP.XI_RF2_VGA_CH1_LSB);
    MI2C2_WordADR_HLBW_PC78(Acacia_LDD_SADR,0x0010,LDD_Control_MEMORY_MAP.XI_RF4_DRV_CH1_MSB ,LDD_Control_MEMORY_MAP.XI_RF4_DRV_CH1_LSB);    
    MI2C2_WordADR_HLBW_PC78(Acacia_LDD_SADR,0x0014,LDD_Control_MEMORY_MAP.XI_RF6_CH1_MSB     ,LDD_Control_MEMORY_MAP.XI_RF6_CH1_LSB); 

    MI2C2_WordADR_HLBW_PC78(Acacia_LDD_SADR,0x1002,LDD_Control_MEMORY_MAP.XI_AGC1_CH2_MSB    ,LDD_Control_MEMORY_MAP.XI_AGC1_CH2_LSB);
    MI2C2_WordADR_HLBW_PC78(Acacia_LDD_SADR,0x100C,LDD_Control_MEMORY_MAP.XI_RF2_VGA_CH2_MSB ,LDD_Control_MEMORY_MAP.XI_RF2_VGA_CH2_LSB);
    MI2C2_WordADR_HLBW_PC78(Acacia_LDD_SADR,0x1010,LDD_Control_MEMORY_MAP.XI_RF4_DRV_CH2_MSB ,LDD_Control_MEMORY_MAP.XI_RF4_DRV_CH2_LSB);    
    MI2C2_WordADR_HLBW_PC78(Acacia_LDD_SADR,0x1014,LDD_Control_MEMORY_MAP.XI_RF6_CH2_MSB     ,LDD_Control_MEMORY_MAP.XI_RF6_CH2_LSB); 

    MI2C2_WordADR_HLBW_PC78(Acacia_LDD_SADR,0x2002,LDD_Control_MEMORY_MAP.XI_AGC1_CH3_MSB    ,LDD_Control_MEMORY_MAP.XI_AGC1_CH3_LSB);
    MI2C2_WordADR_HLBW_PC78(Acacia_LDD_SADR,0x200C,LDD_Control_MEMORY_MAP.XI_RF2_VGA_CH3_MSB ,LDD_Control_MEMORY_MAP.XI_RF2_VGA_CH3_LSB);
    MI2C2_WordADR_HLBW_PC78(Acacia_LDD_SADR,0x2010,LDD_Control_MEMORY_MAP.XI_RF4_DRV_CH3_MSB ,LDD_Control_MEMORY_MAP.XI_RF4_DRV_CH3_LSB);    
    MI2C2_WordADR_HLBW_PC78(Acacia_LDD_SADR,0x2014,LDD_Control_MEMORY_MAP.XI_RF6_CH3_MSB     ,LDD_Control_MEMORY_MAP.XI_RF6_CH3_LSB);     

    MI2C2_WordADR_HLBW_PC78(Acacia_LDD_SADR,0x3002,LDD_Control_MEMORY_MAP.XI_AGC1_CH4_MSB    ,LDD_Control_MEMORY_MAP.XI_AGC1_CH4_LSB);
    MI2C2_WordADR_HLBW_PC78(Acacia_LDD_SADR,0x300C,LDD_Control_MEMORY_MAP.XI_RF2_VGA_CH4_MSB ,LDD_Control_MEMORY_MAP.XI_RF2_VGA_CH4_LSB);
    MI2C2_WordADR_HLBW_PC78(Acacia_LDD_SADR,0x3010,LDD_Control_MEMORY_MAP.XI_RF4_DRV_CH4_MSB ,LDD_Control_MEMORY_MAP.XI_RF4_DRV_CH4_LSB);    
    MI2C2_WordADR_HLBW_PC78(Acacia_LDD_SADR,0x3014,LDD_Control_MEMORY_MAP.XI_RF6_CH4_MSB     ,LDD_Control_MEMORY_MAP.XI_RF6_CH4_LSB);   
}

void Update_dia_temp()
{  
    uint16_t Get_ADC_Data = 0;   
    Get_ADC_Data = Acacia_LDD_DiaTemp(0); 
    LDD_Control_MEMORY_MAP.Dia_temp_CH1 = Get_ADC_Data;  
    Get_ADC_Data = Acacia_LDD_DiaTemp(1);  
    LDD_Control_MEMORY_MAP.Dia_temp_CH2 = Get_ADC_Data;  
    Get_ADC_Data =Acacia_LDD_DiaTemp(2);  
    LDD_Control_MEMORY_MAP.Dia_temp_CH3 = Get_ADC_Data;  
    Get_ADC_Data = Acacia_LDD_DiaTemp(3);  
    LDD_Control_MEMORY_MAP.Dia_temp_CH4 = Get_ADC_Data;    
}

void GetSP_LawData(uint8_t Channel,uint8_t HL_Data)
{
    uint8_t iicount = 0;
    if(HL_Data==0)
        for(iicount=1;iicount<41;iicount++)
            Get_SPLawData[iicount-1] = Acacia_LDD_LawDataSP_Update(Channel,iicount);
    else
        for(iicount=41;iicount<81;iicount++)
            Get_SPLawData[(iicount-41)] = Acacia_LDD_LawDataSP_Update(Channel,iicount);
}

void GetDebug_SP_data(uint8_t Channel)
{
    uint8_t iicount = 0;
    for(iicount=1;iicount<81;iicount++)
        Get_SPData[iicount] = ( Acacia_LDD_SP_Update(Channel,iicount)/10 );
}

void PowerControl()
{
    if(LDD_Control_MEMORY_MAP.Power_Control == 0xAA)
    {
        // Power Down LDD
        P3V6_DRV_EN_Low();
        LDD_Control_MEMORY_MAP.Power_Control = 0x00;
    }
    if(LDD_Control_MEMORY_MAP.Power_Control == 0x55)
    {
        // Power up LDD       
        P3V6_DRV_EN_High();
        delay_1ms(1);        
        LDD_Control_MEMORY_MAP.Power_Control = 0x00;
    }
}

void Device_RESET()
{
    // RESET IC
    MP5490_RESET_High();
    DRV_RESET_Low();    
    TIA_CH12_RESET_Low();
    TIA_CH34_RESET_Low();
}

void Device_UnReset()
{
    // Normal IC
    MP5490_RESET_Low();
    delay_1ms(1);
    TIA_CH12_RESET_High();
    delay_1ms(1);
    TIA_CH34_RESET_High();
    delay_1ms(1);
    DRV_RESET_High();
    delay_1ms(1);
}

void RESETControl()
{
    if(LDD_Control_MEMORY_MAP.RESET_Control == 0xAA)
    {
        // RESET IC
        MP5490_RESET_High();
        DRV_RESET_Low();    
        TIA_CH12_RESET_Low();
        TIA_CH34_RESET_Low();
    }
    if(LDD_Control_MEMORY_MAP.RESET_Control == 0x55)
    {
        // Normal IC
        MP5490_RESET_Low();
        delay_1ms(1);
        TIA_CH12_RESET_High();
        delay_1ms(1);
        TIA_CH34_RESET_High();
        delay_1ms(1);
        DRV_RESET_High();
        delay_1ms(1);
    }
}


