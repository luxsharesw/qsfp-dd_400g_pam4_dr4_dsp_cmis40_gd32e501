#include "gd32e501.h"
#include "systick.h"
#include "GD32E501_GPIO_Customize_Define.h"
#include "GD32E501_M_I2C1_PB3_PB4.h"
#include "LDD_Control.h"
#include "Calibration_Struct.h"
#include "CMIS_MSA.h"
#include "GD_FlahMap.h"

int8_t bef_Temperature_Indx = 0 ;
uint8_t BiasMode_Setting_flag = 0;
uint8_t MP5490_Read_Buffer[128]={0};
struct LDD_Control_MEMORY LDD_Control_MEMORY_MAP;

void MP5490_Direct_Control()
{
    if(LDD_Control_MEMORY_MAP.MP5940_DS == 0xAA)
    {
        Master_I2C1_ByteWrite_PB34(MP5490_ADR,LDD_Control_MEMORY_MAP.MP5940_DM,LDD_Control_MEMORY_MAP.MP5940_WB);
        LDD_Control_MEMORY_MAP.MP5940_DS = 0x00;
    }
    if(LDD_Control_MEMORY_MAP.MP5940_DS == 0xBB)
    {
        LDD_Control_MEMORY_MAP.MP5940_RB = Master_I2C1_ByteREAD_PB34(MP5490_ADR,LDD_Control_MEMORY_MAP.MP5940_DM);
        LDD_Control_MEMORY_MAP.MP5940_DS = 0x00;
    }
}

void MP5490_Read_AllData()
{
    LDD_Control_MEMORY_MAP.MP5940_ID            = Master_I2C1_ByteREAD_PB34(MP5490_ADR,0x3F);
    LDD_Control_MEMORY_MAP.MP5940_REV           = Master_I2C1_ByteREAD_PB34(MP5490_ADR,0x3E);
    LDD_Control_MEMORY_MAP.MP5940_ADDR          = Master_I2C1_ByteREAD_PB34(MP5490_ADR,0x3D);
    LDD_Control_MEMORY_MAP.MP5940_Statu_1       = Master_I2C1_ByteREAD_PB34(MP5490_ADR,0x2E);
    LDD_Control_MEMORY_MAP.MP5940_Statu_2       = Master_I2C1_ByteREAD_PB34(MP5490_ADR,0x2F);
    LDD_Control_MEMORY_MAP.MP5940_Statu_3       = Master_I2C1_ByteREAD_PB34(MP5490_ADR,0x30);
    LDD_Control_MEMORY_MAP.MP5940_Warn_1        = Master_I2C1_ByteREAD_PB34(MP5490_ADR,0x32);
    LDD_Control_MEMORY_MAP.MP5940_IDACM_MSB_CH1 = Master_I2C1_ByteREAD_PB34(MP5490_ADR,0x48);
    LDD_Control_MEMORY_MAP.MP5940_IDACM_LSB_CH1 = Master_I2C1_ByteREAD_PB34(MP5490_ADR,0x49);
    LDD_Control_MEMORY_MAP.MP5940_IDACM_MSB_CH2 = Master_I2C1_ByteREAD_PB34(MP5490_ADR,0x4A);
    LDD_Control_MEMORY_MAP.MP5940_IDACM_LSB_CH2 = Master_I2C1_ByteREAD_PB34(MP5490_ADR,0x4B);
    LDD_Control_MEMORY_MAP.MP5940_IDACM_MSB_CH3 = Master_I2C1_ByteREAD_PB34(MP5490_ADR,0x4C);
    LDD_Control_MEMORY_MAP.MP5940_IDACM_LSB_CH3 = Master_I2C1_ByteREAD_PB34(MP5490_ADR,0x4D);
    LDD_Control_MEMORY_MAP.MP5940_IDACM_MSB_CH4 = Master_I2C1_ByteREAD_PB34(MP5490_ADR,0x4E);
    LDD_Control_MEMORY_MAP.MP5940_IDACM_LSB_CH4 = Master_I2C1_ByteREAD_PB34(MP5490_ADR,0x4F); 
    LDD_Control_MEMORY_MAP.MP5940_CTL0          = Master_I2C1_ByteREAD_PB34(MP5490_ADR,0x00); 
    LDD_Control_MEMORY_MAP.MP5940_CTL1          = Master_I2C1_ByteREAD_PB34(MP5490_ADR,0x01);
    LDD_Control_MEMORY_MAP.MP5940_CTL2          = Master_I2C1_ByteREAD_PB34(MP5490_ADR,0x02);
    LDD_Control_MEMORY_MAP.MP5940_CTL3          = Master_I2C1_ByteREAD_PB34(MP5490_ADR,0x03);
    LDD_Control_MEMORY_MAP.MP5940_CTL4          = Master_I2C1_ByteREAD_PB34(MP5490_ADR,0x04);   
    LDD_Control_MEMORY_MAP.MP5940_IDAC_MSB_CH1  = Master_I2C1_ByteREAD_PB34(MP5490_ADR,0x07); 
    LDD_Control_MEMORY_MAP.MP5940_IDAC_LSB_CH1  = Master_I2C1_ByteREAD_PB34(MP5490_ADR,0x08);
    LDD_Control_MEMORY_MAP.MP5940_IDAC_MSB_CH2  = Master_I2C1_ByteREAD_PB34(MP5490_ADR,0x09); 
    LDD_Control_MEMORY_MAP.MP5940_IDAC_LSB_CH2  = Master_I2C1_ByteREAD_PB34(MP5490_ADR,0x0A); 
    LDD_Control_MEMORY_MAP.MP5940_IDAC_MSB_CH3  = Master_I2C1_ByteREAD_PB34(MP5490_ADR,0x0B); 
    LDD_Control_MEMORY_MAP.MP5940_IDAC_LSB_CH3  = Master_I2C1_ByteREAD_PB34(MP5490_ADR,0x0C); 
    LDD_Control_MEMORY_MAP.MP5940_IDAC_MSB_CH4  = Master_I2C1_ByteREAD_PB34(MP5490_ADR,0x0D); 
    LDD_Control_MEMORY_MAP.MP5940_IDAC_LSB_CH4  = Master_I2C1_ByteREAD_PB34(MP5490_ADR,0x0E);
    LDD_Control_MEMORY_MAP.MP5940_EML_THR       = Master_I2C1_ByteREAD_PB34(MP5490_ADR,0x1B);
    LDD_Control_MEMORY_MAP.MP5940_IDAC_THR      = Master_I2C1_ByteREAD_PB34(MP5490_ADR,0x1C);
    LDD_Control_MEMORY_MAP.MP5940_Protection    = Master_I2C1_ByteREAD_PB34(MP5490_ADR,0x29);
    LDD_Control_MEMORY_MAP.MP5940_VIDACM_E      = Master_I2C1_ByteREAD_PB34(MP5490_ADR,0x2A);
    LDD_Control_MEMORY_MAP.MP5940_CONFIG        = Master_I2C1_ByteREAD_PB34(MP5490_ADR,0x2D);
    LDD_Control_MEMORY_MAP.MP5940_MASK          = Master_I2C1_ByteREAD_PB34(MP5490_ADR,0x34);
}

void MP5490_Write_Control_Data()
{
	Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x00,LDD_Control_MEMORY_MAP.MP5940_CTL0);
    Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x00,LDD_Control_MEMORY_MAP.MP5940_CTL0);
    Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x01,LDD_Control_MEMORY_MAP.MP5940_CTL1);
    Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x02,LDD_Control_MEMORY_MAP.MP5940_CTL2);
    Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x03,LDD_Control_MEMORY_MAP.MP5940_CTL3);
    Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x04,LDD_Control_MEMORY_MAP.MP5940_CTL4);
    Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x07,LDD_Control_MEMORY_MAP.MP5940_IDAC_MSB_CH1);
    Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x08,LDD_Control_MEMORY_MAP.MP5940_IDAC_LSB_CH1);    
    Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x09,LDD_Control_MEMORY_MAP.MP5940_IDAC_MSB_CH1);
    Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x0A,LDD_Control_MEMORY_MAP.MP5940_IDAC_LSB_CH1);
    Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x0B,LDD_Control_MEMORY_MAP.MP5940_IDAC_MSB_CH3);
    Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x0C,LDD_Control_MEMORY_MAP.MP5940_IDAC_LSB_CH3);
    Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x0D,LDD_Control_MEMORY_MAP.MP5940_IDAC_MSB_CH3);
    Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x0E,LDD_Control_MEMORY_MAP.MP5940_IDAC_LSB_CH3);    
    Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x1B,LDD_Control_MEMORY_MAP.MP5940_EML_THR);
    Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x1C,LDD_Control_MEMORY_MAP.MP5940_IDAC_THR);
    Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x29,LDD_Control_MEMORY_MAP.MP5940_Protection);   
    Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x2A,LDD_Control_MEMORY_MAP.MP5940_VIDACM_E);
    Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x2D,LDD_Control_MEMORY_MAP.MP5940_CONFIG);
    Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x34,LDD_Control_MEMORY_MAP.MP5940_MASK);
}

// debug test user
void MP5490_LDD_Interface_test()
{
    if(CALIB_MEMORY_MAP.CAL_Buffer_2[1])
        MP5490_RESET_High();
    else
        MP5490_RESET_Low();
    
    if(CALIB_MEMORY_MAP.CAL_Buffer_2[2])
        MP5490_LDEN_High();
    else
        MP5490_LDEN_Low();
    
    CALIB_MEMORY_MAP.CAL_Buffer_2[3] = gpio_output_bit_get(GPIOC,MP5490_RESET_GPIOC);
    CALIB_MEMORY_MAP.CAL_Buffer_2[4] = gpio_output_bit_get(GPIOC,MP5490_LDEN_GPIOC);
         
    LDD_Control_MEMORY_MAP.MP5940_ID  = Master_I2C1_ByteREAD_PB34(CALIB_MEMORY_MAP.CAL_Buffer_2[0],0x3F);
    LDD_Control_MEMORY_MAP.MP5940_REV = Master_I2C1_ByteREAD_PB34(CALIB_MEMORY_MAP.CAL_Buffer_2[0],0x3E);
    LDD_Control_MEMORY_MAP.RSVD       = Master_I2C1_ByteREAD_PB34(CALIB_MEMORY_MAP.CAL_Buffer_2[0],0x00);
}

// debug user page 83h
void MP5490_Read_all_REG()
{
    uint16_t iicount = 0;

    for(iicount=0;iicount<0x6c;iicount++)
        MP5490_Read_Buffer[iicount] = Master_I2C1_ByteREAD_PB34(MP5490_ADR,iicount);
}

// APC close loop or LUT open loop
// DR4 two channel bias control
void Bias_MOD_LUT_UpDate_Current()
{
    uint32_t B_Flash_Index = 0 ;
    uint16_t Mod_update = 0;
    
    B_Flash_Index = ( FS_Ibias01_PA0 + Temperature_Index*2);
    LDD_Control_MEMORY_MAP.MP5940_IDAC_MSB_CH1 = GDMCU_FMC_READ_DATA( B_Flash_Index );
    LDD_Control_MEMORY_MAP.MP5940_IDAC_LSB_CH1 = GDMCU_FMC_READ_DATA( B_Flash_Index+1 );        

    Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x07,LDD_Control_MEMORY_MAP.MP5940_IDAC_MSB_CH1);
    Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x08,LDD_Control_MEMORY_MAP.MP5940_IDAC_LSB_CH1); 
    Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x09,LDD_Control_MEMORY_MAP.MP5940_IDAC_MSB_CH1);
    Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x0A,LDD_Control_MEMORY_MAP.MP5940_IDAC_LSB_CH1); 

    B_Flash_Index = ( FS_Ibias23_PA1 + Temperature_Index*2);
    LDD_Control_MEMORY_MAP.MP5940_IDAC_MSB_CH3 = GDMCU_FMC_READ_DATA( B_Flash_Index );
    LDD_Control_MEMORY_MAP.MP5940_IDAC_LSB_CH3 = GDMCU_FMC_READ_DATA( B_Flash_Index+1 );
    
    Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x0B,LDD_Control_MEMORY_MAP.MP5940_IDAC_MSB_CH3);
    Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x0C,LDD_Control_MEMORY_MAP.MP5940_IDAC_LSB_CH3); 
    Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x0D,LDD_Control_MEMORY_MAP.MP5940_IDAC_MSB_CH3);
    Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x0E,LDD_Control_MEMORY_MAP.MP5940_IDAC_LSB_CH3); 
}

void Update_Bias_Current(uint8_t Channel,uint16_t update_current)
{
    if(Channel<2)
    {
        LDD_Control_MEMORY_MAP.MP5940_IDAC_LSB_CH1 = update_current;
        LDD_Control_MEMORY_MAP.MP5940_IDAC_MSB_CH1 = update_current>>8 ;
        Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x07,LDD_Control_MEMORY_MAP.MP5940_IDAC_MSB_CH1);
        Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x08,LDD_Control_MEMORY_MAP.MP5940_IDAC_LSB_CH1);    
        Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x09,LDD_Control_MEMORY_MAP.MP5940_IDAC_MSB_CH1);
        Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x0A,LDD_Control_MEMORY_MAP.MP5940_IDAC_LSB_CH1);
    }
    else
    {
        LDD_Control_MEMORY_MAP.MP5940_IDAC_LSB_CH3 = update_current;
        LDD_Control_MEMORY_MAP.MP5940_IDAC_MSB_CH3 = update_current>>8 ;
        Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x0B,LDD_Control_MEMORY_MAP.MP5940_IDAC_MSB_CH3);
        Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x0C,LDD_Control_MEMORY_MAP.MP5940_IDAC_LSB_CH3);
        Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x0D,LDD_Control_MEMORY_MAP.MP5940_IDAC_MSB_CH3);
        Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x0E,LDD_Control_MEMORY_MAP.MP5940_IDAC_LSB_CH3);  
    }
}


void Bias_MOD_Updata_Current_Temperature_vary()
{
    if(BiasMode_Setting_flag==0)
    {
        if(bef_Temperature_Indx!=Temperature_Index)
            BiasMode_Setting_flag = 1;
        
        bef_Temperature_Indx = Temperature_Index;
    }
    else
    {
        // DR4 only two laser for 4 channel
        BiasMode_Setting_flag = 0;
        Bias_MOD_LUT_UpDate_Current();
    }
}

void LDD_Trun_on()
{
    Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x01,LDD_Control_MEMORY_MAP.MP5940_CTL1|0x0F);
}

void LDD_TX1_TX8_Tx_Disable(uint8_t Lane,uint8_t Value)
{
	if((Lane==0)||(Lane==1))
	{
		if(Value==0)
			LDD_Control_MEMORY_MAP.MP5940_CTL1|=0x0C;
		else
			LDD_Control_MEMORY_MAP.MP5940_CTL1&=~0x0C;
	}
	else if((Lane==2)||(Lane==3))
	{
		if(Value==0)
			LDD_Control_MEMORY_MAP.MP5940_CTL1|=0x03;
		else
			LDD_Control_MEMORY_MAP.MP5940_CTL1&=~0x03;
	}
	Master_I2C1_ByteWrite_PB34(MP5490_ADR,0x01,LDD_Control_MEMORY_MAP.MP5940_CTL1);
}
