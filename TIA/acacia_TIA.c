#include "gd32e501.h"
#include "systick.h"
#include "GD32E501_GPIO_Customize_Define.h"
#include "GD32E501_M_I2C1_PB3_PB4.h"
#include "TIA_Control.h"
#include "CMIS_MSA.h"

#define Acacia_TIA12_SADR  0xE2
#define Acacia_TIA34_SADR  0xE4
#define VR_N_LO              41
#define VR_N_HI              42
#define VR_P_LO              45
#define VR_P_HI              46
#define Connect_ATB        0xA0

struct TIA_Control_MEMORY TIA_Control_MEMORY_MAP;

// ADC Fuction 
uint16_t TIA_RX1_RX4_RSSI(uint8_t Channel)
{
    uint16_t Get_RSSI_Voltage = 0;

    if(Channel==0)
      Get_RSSI_Voltage = GET_ADC_Value_Data(REF_D1_BIAS);
    else if(Channel==1)
      Get_RSSI_Voltage = GET_ADC_Value_Data(REF_D2_BIAS);
    else if(Channel==2)
      Get_RSSI_Voltage = GET_ADC_Value_Data(REF_D3_BIAS);
    else if(Channel==3)
      Get_RSSI_Voltage = GET_ADC_Value_Data(REF_D4_BIAS);

    return Get_RSSI_Voltage;
}

void Acacia_TIA_SEL_SP(uint8_t Channel,uint8_t SensePoint)
{
    uint16_t Connect_Channel_ADR = 0 ;
    uint16_t Sensor_Point_ADR = 0;
    uint16_t SlaveADR = 0;
    if(Channel==0)
    {
        Connect_Channel_ADR = 0x0008;
        Sensor_Point_ADR = 0x0016;
        SlaveADR = Acacia_TIA12_SADR;
    }
    else if(Channel==1)
    {
        Connect_Channel_ADR = 0x1008;
        Sensor_Point_ADR = 0x1016;
        SlaveADR = Acacia_TIA12_SADR;
    }
    else if(Channel==2)
    {
        Connect_Channel_ADR = 0x0008;
        Sensor_Point_ADR = 0x0016;
        SlaveADR = Acacia_TIA34_SADR;
    }
    else if(Channel==3)
    {
        Connect_Channel_ADR = 0x1008;
        Sensor_Point_ADR = 0x1016;
        SlaveADR = Acacia_TIA34_SADR;
    }
    // Define ADC_Read
    //soft reset ADC-assert reset bits
    MI2C1_WordADR_HLBW_PB34(SlaveADR,0x4006,0x00,0x00); 
    //soft reset ADC-de-assert reset bits
    MI2C1_WordADR_HLBW_PB34(SlaveADR,0x4006,0x00,0x03); 
    //clear ADC measure start bit (bit 0) and ensure PD is 0
    MI2C1_WordADR_HLBW_PB34(SlaveADR,0x4002,0x00,0x00); 
    //connect Global-ATB to the ADC inpu
    MI2C1_WordADR_HLBW_PB34(SlaveADR,0x4008,0x00,0x01);    
    // disconnect Channel-N-ATB to Global-ATB
    MI2C1_WordADR_HLBW_PB34(SlaveADR,0x0016,0x00,0x00);
    MI2C1_WordADR_HLBW_PB34(SlaveADR,0x1016,0x00,0x00);
    MI2C1_WordADR_HLBW_PB34(SlaveADR,0x2016,0x00,0x00);
    MI2C1_WordADR_HLBW_PB34(SlaveADR,0x3016,0x00,0x00);
    //clear ADC measure start bit (bit 0) and ensure PD is 0
    MI2C1_WordADR_HLBW_PB34(SlaveADR,0x4020,0x00,0x00); 
    //connect Channel-N-sense-node to Channel-N-ATB
    MI2C1_WordADR_HLBW_PB34(SlaveADR,Connect_Channel_ADR,0x00,0x01);  
    //connect Global-ATB to the ADC inpu
    MI2C1_WordADR_HLBW_PB34(SlaveADR,Sensor_Point_ADR,0x00,SensePoint);   
}

uint16_t Acacia_TIA_SP_GETDATA(uint8_t Channel,uint8_t SP_Value)
{
    uint16_t Get_ADC_Data = 0;
    uint16_t returnData = 0 ;
    uint16_t SlaveADR = 0;
    
    if(SlaveADR<3)
        SlaveADR = Acacia_TIA12_SADR;
    else
        SlaveADR = Acacia_TIA34_SADR;
    
    Acacia_TIA_SEL_SP(Channel,SP_Value);
    //start ADC read
    MI2C1_WordADR_HLBW_PB34(SlaveADR,0x4002,0x00,0x01);
    //Get ADC Data
    delay_1ms(1);
    Get_ADC_Data = ( MI2C1_WordADR_R_PB34(SlaveADR,0x4004) & 0x03FF );
    returnData = Get_ADC_Data*2.44 ;
    // Clear ADC
    MI2C1_WordADR_HLBW_PB34(SlaveADR,0x4002,0x00,0x00);
    
    return returnData;
}


//-------------------------------------------------//
//Sense Point N016
//32 = Temperature
//2D = RSSI
//25 = PDO
//23 = VGC
//45 = VR_P_LO
//46 = VR_P_HI
//41 = VR_N_LO
//42 = VR_N_HI
//-------------------------------------------------//
// Acacia applcation note
/*
uint16_t TIA_RX1_RX4_RSSI(uint8_t Channel)
{
    uint16_t Connect_ATB_ADR = 0 ;
    uint16_t VR_High_Data = 0 ;
    uint16_t VR_Low_Data = 0 ;
    uint16_t RSSI_Data;
    uint16_t Channel_Connect_ADR = 0;
    uint16_t Channel_SensePoint_ADR = 0 ;

    if(Channel==0)
    {
        Channel_Connect_ADR = 0x0008;
        Channel_SensePoint_ADR = 0x0016;
    }
    else if(Channel==1)
    {
        Channel_Connect_ADR = 0x2008;
        Channel_SensePoint_ADR = 0x2016;
    }
    else if(Channel==2)
    {
        Channel_Connect_ADR = 0x0008;
        Channel_SensePoint_ADR = 0x0016;
    }
    else if(Channel==3)
    {
        Channel_Connect_ADR = 0x2008;
        Channel_SensePoint_ADR = 0x2016;
    }

    if(Channel<2)    
    {
        // Setp1: Disaconnect ATB From pads
        // Step2: Connect ATB0 to ATB_North - command is 0xA0
        // Step3: ADC Sens point setting to VR_P_HI - Command is 46
        // Step4: Get ADC data
        // step5: ADC Sens point setting to VR_P_LO - Command is 45
        // Step6: Get ADC data
        // Step7: Restor to default
        Disconnet_ATB_TO_PAD(Channel);
        MI2C1_WordADR_W_PB34(Acacia_TIA12_SADR,Channel_Connect_ADR,Connect_ATB);
        MI2C1_WordADR_W_PB34(Acacia_TIA12_SADR,Channel_SensePoint_ADR,VR_P_HI);
        VR_High_Data = Acacia_Start_adc_TIA12();
        MI2C1_WordADR_W_PB34(Acacia_TIA12_SADR,Channel_SensePoint_ADR,VR_P_LO);
        VR_Low_Data = Acacia_Start_adc_TIA12();
        Restore_ATB_Default(Channel);
    }
    else
    {   
        // Setp1: Disaconnect ATB From pads
        // Step2: Connect ATB0 to ATB_North - command is 0xA0
        // Step3: ADC Sense-Point setting to VR_P_HI - Command is 46
        // Step4: Get ADC data
        // step5: ADC Sense-Point setting to VR_P_LO - Command is 45
        // Step6: Get ADC data
        // Step7: Restor to default
        Disconnet_ATB_TO_PAD(Channel);
        MI2C1_WordADR_W_PB34(Acacia_TIA34_SADR,Channel_Connect_ADR,Connect_ATB);
        MI2C1_WordADR_W_PB34(Acacia_TIA34_SADR,Channel_SensePoint_ADR,VR_P_HI);
        VR_High_Data = Acacia_Start_adc_TIA34();
        MI2C1_WordADR_W_PB34(Acacia_TIA34_SADR,Channel_SensePoint_ADR,VR_P_LO);
        VR_Low_Data = Acacia_Start_adc_TIA34();
        Restore_ATB_Default(Channel);
    }
    
    // return voltage 
    // return Current ( voltage/R )*1000 = mA
    // R is 350 or 700
    RSSI_Data = ( VR_High_Data - VR_Low_Data );  
    
    return RSSI_Data;
}
*/
void Acaaia_TIA_Direct_Control()
{
    if(TIA_Control_MEMORY_MAP.Direct_Satrt_T12==0xAA)
    {
        MI2C1_WordADR_W_PB34(Acacia_TIA12_SADR,TIA_Control_MEMORY_MAP.Driect_MEMADR_T12,TIA_Control_MEMORY_MAP.Driect_WBUFF_T12);
        TIA_Control_MEMORY_MAP.Direct_Satrt_T12 = 0 ;
    }
    if(TIA_Control_MEMORY_MAP.Direct_Satrt_T12==0xBB)
    {
        TIA_Control_MEMORY_MAP.Driect_RBUFF_T12 = MI2C1_WordADR_R_PB34(Acacia_TIA12_SADR,TIA_Control_MEMORY_MAP.Driect_MEMADR_T12);
        TIA_Control_MEMORY_MAP.Direct_Satrt_T12 = 0 ;
    }
    if(TIA_Control_MEMORY_MAP.Direct_Satrt_T34==0xAA)
    {
        MI2C1_WordADR_W_PB34(Acacia_TIA34_SADR,TIA_Control_MEMORY_MAP.Driect_MEMADR_T34,TIA_Control_MEMORY_MAP.Driect_WBUFF_T34);
        TIA_Control_MEMORY_MAP.Direct_Satrt_T34 = 0;
    }
    if(TIA_Control_MEMORY_MAP.Direct_Satrt_T34==0xBB)
    {
        TIA_Control_MEMORY_MAP.Driect_RBUFF_T34 = MI2C1_WordADR_R_PB34(Acacia_TIA34_SADR,TIA_Control_MEMORY_MAP.Driect_MEMADR_T34);
        TIA_Control_MEMORY_MAP.Direct_Satrt_T34 = 0;
    }
}

void Acacia_TIA_Read_ALL()
{
    uint16_t GET_BUFFER = 0;
    //---------------------------------------------------------------------------//
    // GLBL CONTROL
    //---------------------------------------------------------------------------//
    // Acacia reg 4000 T12
    GET_BUFFER = MI2C1_WordADR_R_PB34(Acacia_TIA12_SADR,0x4000);
    TIA_Control_MEMORY_MAP.GLBL_CHIPID_TIA1_LSB = GET_BUFFER ;
    TIA_Control_MEMORY_MAP.GLBL_CHIPID_TIA1_MSB = GET_BUFFER >> 8 ;
    // Acacia reg 400C T12
    GET_BUFFER = MI2C1_WordADR_R_PB34(Acacia_TIA12_SADR,0x400C);
    TIA_Control_MEMORY_MAP.GLBL_BIASGEN_TIA1_CTRL_LSB = GET_BUFFER ;
    TIA_Control_MEMORY_MAP.GLBL_BIASGEN_TIA1_CTRL_MSB = GET_BUFFER >> 8 ;    
    // Acacia reg 4000 T32
    GET_BUFFER = MI2C1_WordADR_R_PB34(Acacia_TIA34_SADR,0x4000);
    TIA_Control_MEMORY_MAP.GLBL_CHIPID_TIA2_LSB = GET_BUFFER ;
    TIA_Control_MEMORY_MAP.GLBL_CHIPID_TIA2_MSB = GET_BUFFER >> 8 ;
    // Acacia reg 400C T32
    GET_BUFFER = MI2C1_WordADR_R_PB34(Acacia_TIA34_SADR,0x400C);
    TIA_Control_MEMORY_MAP.GLBL_BIASGEN_TIA2_CTRL_LSB = GET_BUFFER ;
    TIA_Control_MEMORY_MAP.GLBL_BIASGEN_TIA2_CTRL_MSB = GET_BUFFER >> 8 ;        
    //---------------------------------------------------------------------------//
    // Channel1 CONTROL
    //---------------------------------------------------------------------------//    
    GET_BUFFER = MI2C1_WordADR_R_PB34(Acacia_TIA12_SADR,0x0000);
    TIA_Control_MEMORY_MAP.XI_IBIAS10_VGA_CH1_LSB = GET_BUFFER ;
    TIA_Control_MEMORY_MAP.XI_IBIAS10_VGA_CH1_MSB = GET_BUFFER >> 8 ;
    
    GET_BUFFER = MI2C1_WordADR_R_PB34(Acacia_TIA12_SADR,0x0002);
    TIA_Control_MEMORY_MAP.XI_AGC1_CH1_LSB = GET_BUFFER ;
    TIA_Control_MEMORY_MAP.XI_AGC1_CH1_MSB = GET_BUFFER >> 8 ;   
    
    GET_BUFFER = MI2C1_WordADR_R_PB34(Acacia_TIA12_SADR,0x0006);
    TIA_Control_MEMORY_MAP.XI_IBIAS32_DRIVER_CH1_LSB = GET_BUFFER ;
    TIA_Control_MEMORY_MAP.XI_IBIAS32_DRIVER_CH1_MSB = GET_BUFFER >> 8 ; 
    
    GET_BUFFER = MI2C1_WordADR_R_PB34(Acacia_TIA12_SADR,0x000A);
    TIA_Control_MEMORY_MAP.XI_RF1_TIA_CH1_LSB = GET_BUFFER ;
    TIA_Control_MEMORY_MAP.XI_RF1_TIA_CH1_MSB = GET_BUFFER >> 8 ; 

    GET_BUFFER = MI2C1_WordADR_R_PB34(Acacia_TIA12_SADR,0x000C);
    TIA_Control_MEMORY_MAP.TIA_VLDO_TIA_VREF_LSB_CH1 = GET_BUFFER ;
    TIA_Control_MEMORY_MAP.TIA_VLDO_TIA_VREF_MSB_CH1 = GET_BUFFER >> 8 ;  
    
    GET_BUFFER = MI2C1_WordADR_R_PB34(Acacia_TIA12_SADR,0x000E);
    TIA_Control_MEMORY_MAP.XI_RF3_PREDRV_CH1_LSB = GET_BUFFER ;
    TIA_Control_MEMORY_MAP.XI_RF3_PREDRV_CH1_MSB = GET_BUFFER >> 8 ; 
    
    GET_BUFFER = MI2C1_WordADR_R_PB34(Acacia_TIA12_SADR,0x0010);
    TIA_Control_MEMORY_MAP.XI_RF4_DRV_CH1_LSB = GET_BUFFER ;
    TIA_Control_MEMORY_MAP.XI_RF4_DRV_CH1_MSB = GET_BUFFER >> 8 ; 
    
    GET_BUFFER = MI2C1_WordADR_R_PB34(Acacia_TIA12_SADR,0x0014);
    TIA_Control_MEMORY_MAP.XI_RF6_CH1_LSB = GET_BUFFER ;
    TIA_Control_MEMORY_MAP.XI_RF6_CH1_MSB = GET_BUFFER >> 8 ; 
    //---------------------------------------------------------------------------//
    // Channel2 CONTROL
    //---------------------------------------------------------------------------//    
    GET_BUFFER = MI2C1_WordADR_R_PB34(Acacia_TIA12_SADR,0x1000);
    TIA_Control_MEMORY_MAP.XI_IBIAS10_VGA_CH2_LSB = GET_BUFFER ;
    TIA_Control_MEMORY_MAP.XI_IBIAS10_VGA_CH2_MSB = GET_BUFFER >> 8 ;
    
    GET_BUFFER = MI2C1_WordADR_R_PB34(Acacia_TIA12_SADR,0x1002);
    TIA_Control_MEMORY_MAP.XI_AGC1_CH2_LSB = GET_BUFFER ;
    TIA_Control_MEMORY_MAP.XI_AGC1_CH2_MSB = GET_BUFFER >> 8 ;   
    
    GET_BUFFER = MI2C1_WordADR_R_PB34(Acacia_TIA12_SADR,0x1006);
    TIA_Control_MEMORY_MAP.XI_IBIAS32_DRIVER_CH2_LSB = GET_BUFFER ;
    TIA_Control_MEMORY_MAP.XI_IBIAS32_DRIVER_CH2_MSB = GET_BUFFER >> 8 ; 
    
    GET_BUFFER = MI2C1_WordADR_R_PB34(Acacia_TIA12_SADR,0x100A);
    TIA_Control_MEMORY_MAP.XI_RF1_TIA_CH2_LSB = GET_BUFFER ;
    TIA_Control_MEMORY_MAP.XI_RF1_TIA_CH2_MSB = GET_BUFFER >> 8 ;  
    
    GET_BUFFER = MI2C1_WordADR_R_PB34(Acacia_TIA12_SADR,0x100C);
    TIA_Control_MEMORY_MAP.TIA_VLDO_TIA_VREF_LSB_CH2 = GET_BUFFER ;
    TIA_Control_MEMORY_MAP.TIA_VLDO_TIA_VREF_MSB_CH2 = GET_BUFFER >> 8 ; 
    
    GET_BUFFER = MI2C1_WordADR_R_PB34(Acacia_TIA12_SADR,0x100E);
    TIA_Control_MEMORY_MAP.XI_RF3_PREDRV_CH2_LSB = GET_BUFFER ;
    TIA_Control_MEMORY_MAP.XI_RF3_PREDRV_CH2_MSB = GET_BUFFER >> 8 ; 
    
    GET_BUFFER = MI2C1_WordADR_R_PB34(Acacia_TIA12_SADR,0x1010);
    TIA_Control_MEMORY_MAP.XI_RF4_DRV_CH2_LSB = GET_BUFFER ;
    TIA_Control_MEMORY_MAP.XI_RF4_DRV_CH2_MSB = GET_BUFFER >> 8 ; 
    
    GET_BUFFER = MI2C1_WordADR_R_PB34(Acacia_TIA12_SADR,0x1014);
    TIA_Control_MEMORY_MAP.XI_RF6_CH2_LSB = GET_BUFFER ;
    TIA_Control_MEMORY_MAP.XI_RF6_CH2_MSB = GET_BUFFER >> 8 ; 
    //---------------------------------------------------------------------------//
    // Channel3 CONTROL
    //---------------------------------------------------------------------------//    
    GET_BUFFER = MI2C1_WordADR_R_PB34(Acacia_TIA34_SADR,0x0000);
    TIA_Control_MEMORY_MAP.XI_IBIAS10_VGA_CH3_LSB = GET_BUFFER ;
    TIA_Control_MEMORY_MAP.XI_IBIAS10_VGA_CH3_MSB = GET_BUFFER >> 8 ;
    
    GET_BUFFER = MI2C1_WordADR_R_PB34(Acacia_TIA34_SADR,0x0002);
    TIA_Control_MEMORY_MAP.XI_AGC1_CH3_LSB = GET_BUFFER ;
    TIA_Control_MEMORY_MAP.XI_AGC1_CH3_MSB = GET_BUFFER >> 8 ;   
    
    GET_BUFFER = MI2C1_WordADR_R_PB34(Acacia_TIA34_SADR,0x0006);
    TIA_Control_MEMORY_MAP.XI_IBIAS32_DRIVER_CH3_LSB = GET_BUFFER ;
    TIA_Control_MEMORY_MAP.XI_IBIAS32_DRIVER_CH3_MSB = GET_BUFFER >> 8 ; 
    
    GET_BUFFER = MI2C1_WordADR_R_PB34(Acacia_TIA34_SADR,0x000A);
    TIA_Control_MEMORY_MAP.XI_RF1_TIA_CH3_LSB = GET_BUFFER ;
    TIA_Control_MEMORY_MAP.XI_RF1_TIA_CH3_MSB = GET_BUFFER >> 8 ;  
    
    GET_BUFFER = MI2C1_WordADR_R_PB34(Acacia_TIA34_SADR,0x000C);
    TIA_Control_MEMORY_MAP.TIA_VLDO_TIA_VREF_LSB_CH3 = GET_BUFFER ;
    TIA_Control_MEMORY_MAP.TIA_VLDO_TIA_VREF_MSB_CH3 = GET_BUFFER >> 8 ;
    
    GET_BUFFER = MI2C1_WordADR_R_PB34(Acacia_TIA34_SADR,0x000E);
    TIA_Control_MEMORY_MAP.XI_RF3_PREDRV_CH3_LSB = GET_BUFFER ;
    TIA_Control_MEMORY_MAP.XI_RF3_PREDRV_CH3_MSB = GET_BUFFER >> 8 ; 
    
    GET_BUFFER = MI2C1_WordADR_R_PB34(Acacia_TIA34_SADR,0x0010);
    TIA_Control_MEMORY_MAP.XI_RF4_DRV_CH3_LSB = GET_BUFFER ;
    TIA_Control_MEMORY_MAP.XI_RF4_DRV_CH3_MSB = GET_BUFFER >> 8 ; 
    
    GET_BUFFER = MI2C1_WordADR_R_PB34(Acacia_TIA34_SADR,0x0014);
    TIA_Control_MEMORY_MAP.XI_RF6_CH3_LSB = GET_BUFFER ;
    TIA_Control_MEMORY_MAP.XI_RF6_CH3_MSB = GET_BUFFER >> 8 ; 
    //---------------------------------------------------------------------------//
    // Channel4 CONTROL
    //---------------------------------------------------------------------------//    
    GET_BUFFER = MI2C1_WordADR_R_PB34(Acacia_TIA34_SADR,0x1000);
    TIA_Control_MEMORY_MAP.XI_IBIAS10_VGA_CH4_LSB = GET_BUFFER ;
    TIA_Control_MEMORY_MAP.XI_IBIAS10_VGA_CH4_MSB = GET_BUFFER >> 8 ;
    
    GET_BUFFER = MI2C1_WordADR_R_PB34(Acacia_TIA34_SADR,0x1002);
    TIA_Control_MEMORY_MAP.XI_AGC1_CH4_LSB = GET_BUFFER ;
    TIA_Control_MEMORY_MAP.XI_AGC1_CH4_MSB = GET_BUFFER >> 8 ;   
    
    GET_BUFFER = MI2C1_WordADR_R_PB34(Acacia_TIA34_SADR,0x1006);
    TIA_Control_MEMORY_MAP.XI_IBIAS32_DRIVER_CH4_LSB = GET_BUFFER ;
    TIA_Control_MEMORY_MAP.XI_IBIAS32_DRIVER_CH4_MSB = GET_BUFFER >> 8 ; 
    
    GET_BUFFER = MI2C1_WordADR_R_PB34(Acacia_TIA34_SADR,0x100A);
    TIA_Control_MEMORY_MAP.XI_RF1_TIA_CH4_LSB = GET_BUFFER ;
    TIA_Control_MEMORY_MAP.XI_RF1_TIA_CH4_MSB = GET_BUFFER >> 8 ;  
    
    GET_BUFFER = MI2C1_WordADR_R_PB34(Acacia_TIA34_SADR,0x100C);
    TIA_Control_MEMORY_MAP.TIA_VLDO_TIA_VREF_LSB_CH4 = GET_BUFFER ;
    TIA_Control_MEMORY_MAP.TIA_VLDO_TIA_VREF_MSB_CH4 = GET_BUFFER >> 8 ;
    
    GET_BUFFER = MI2C1_WordADR_R_PB34(Acacia_TIA34_SADR,0x100E);
    TIA_Control_MEMORY_MAP.XI_RF3_PREDRV_CH4_LSB = GET_BUFFER ;
    TIA_Control_MEMORY_MAP.XI_RF3_PREDRV_CH4_MSB = GET_BUFFER >> 8 ; 
    
    GET_BUFFER = MI2C1_WordADR_R_PB34(Acacia_TIA34_SADR,0x1010);
    TIA_Control_MEMORY_MAP.XI_RF4_DRV_CH4_LSB = GET_BUFFER ;
    TIA_Control_MEMORY_MAP.XI_RF4_DRV_CH4_MSB = GET_BUFFER >> 8 ; 
    
    GET_BUFFER = MI2C1_WordADR_R_PB34(Acacia_TIA34_SADR,0x1014);
    TIA_Control_MEMORY_MAP.XI_RF6_CH4_LSB = GET_BUFFER ;
    TIA_Control_MEMORY_MAP.XI_RF6_CH4_MSB = GET_BUFFER >> 8 ;     
  
}

void Acacia_TIA_Write_ALL()
{
    //---------------------------------------------------------------------------//
    // Channel1 CONTROL
    //---------------------------------------------------------------------------//    
    MI2C1_WordADR_HLBW_PB34(Acacia_TIA12_SADR,0x0000,TIA_Control_MEMORY_MAP.XI_IBIAS10_VGA_CH1_MSB    ,TIA_Control_MEMORY_MAP.XI_IBIAS10_VGA_CH1_LSB);
    MI2C1_WordADR_HLBW_PB34(Acacia_TIA12_SADR,0x0002,TIA_Control_MEMORY_MAP.XI_AGC1_CH1_MSB           ,TIA_Control_MEMORY_MAP.XI_AGC1_CH1_LSB);
    MI2C1_WordADR_HLBW_PB34(Acacia_TIA12_SADR,0x0006,TIA_Control_MEMORY_MAP.XI_IBIAS32_DRIVER_CH1_MSB ,TIA_Control_MEMORY_MAP.XI_IBIAS32_DRIVER_CH1_LSB);   
    MI2C1_WordADR_HLBW_PB34(Acacia_TIA12_SADR,0x000A,TIA_Control_MEMORY_MAP.XI_RF1_TIA_CH1_MSB        ,TIA_Control_MEMORY_MAP.XI_RF1_TIA_CH1_LSB);       
    MI2C1_WordADR_HLBW_PB34(Acacia_TIA12_SADR,0x000C,TIA_Control_MEMORY_MAP.TIA_VLDO_TIA_VREF_MSB_CH1 ,TIA_Control_MEMORY_MAP.TIA_VLDO_TIA_VREF_LSB_CH1);   
    MI2C1_WordADR_HLBW_PB34(Acacia_TIA12_SADR,0x000E,TIA_Control_MEMORY_MAP.XI_RF3_PREDRV_CH1_MSB     ,TIA_Control_MEMORY_MAP.XI_RF3_PREDRV_CH1_LSB);  
    MI2C1_WordADR_HLBW_PB34(Acacia_TIA12_SADR,0x0010,TIA_Control_MEMORY_MAP.XI_RF4_DRV_CH1_MSB        ,TIA_Control_MEMORY_MAP.XI_RF4_DRV_CH1_LSB);  
    MI2C1_WordADR_HLBW_PB34(Acacia_TIA12_SADR,0x0014,TIA_Control_MEMORY_MAP.XI_RF6_CH1_MSB            ,TIA_Control_MEMORY_MAP.XI_RF6_CH1_LSB);  
    //---------------------------------------------------------------------------//
    // Channel2 CONTROL
    //---------------------------------------------------------------------------//    
    MI2C1_WordADR_HLBW_PB34(Acacia_TIA12_SADR,0x1000,TIA_Control_MEMORY_MAP.XI_IBIAS10_VGA_CH2_MSB    ,TIA_Control_MEMORY_MAP.XI_IBIAS10_VGA_CH2_LSB);
    MI2C1_WordADR_HLBW_PB34(Acacia_TIA12_SADR,0x1002,TIA_Control_MEMORY_MAP.XI_AGC1_CH2_MSB           ,TIA_Control_MEMORY_MAP.XI_AGC1_CH2_LSB);
    MI2C1_WordADR_HLBW_PB34(Acacia_TIA12_SADR,0x1006,TIA_Control_MEMORY_MAP.XI_IBIAS32_DRIVER_CH2_MSB ,TIA_Control_MEMORY_MAP.XI_IBIAS32_DRIVER_CH2_LSB);   
    MI2C1_WordADR_HLBW_PB34(Acacia_TIA12_SADR,0x100A,TIA_Control_MEMORY_MAP.XI_RF1_TIA_CH2_MSB        ,TIA_Control_MEMORY_MAP.XI_RF1_TIA_CH2_LSB); 
    MI2C1_WordADR_HLBW_PB34(Acacia_TIA12_SADR,0x100C,TIA_Control_MEMORY_MAP.TIA_VLDO_TIA_VREF_MSB_CH2 ,TIA_Control_MEMORY_MAP.TIA_VLDO_TIA_VREF_LSB_CH2);     
    MI2C1_WordADR_HLBW_PB34(Acacia_TIA12_SADR,0x100E,TIA_Control_MEMORY_MAP.XI_RF3_PREDRV_CH2_MSB     ,TIA_Control_MEMORY_MAP.XI_RF3_PREDRV_CH2_LSB);  
    MI2C1_WordADR_HLBW_PB34(Acacia_TIA12_SADR,0x1010,TIA_Control_MEMORY_MAP.XI_RF4_DRV_CH2_MSB        ,TIA_Control_MEMORY_MAP.XI_RF4_DRV_CH2_LSB);  
    MI2C1_WordADR_HLBW_PB34(Acacia_TIA12_SADR,0x1014,TIA_Control_MEMORY_MAP.XI_RF6_CH2_MSB            ,TIA_Control_MEMORY_MAP.XI_RF6_CH2_LSB);   
    //---------------------------------------------------------------------------//
    // Channel3 CONTROL
    //---------------------------------------------------------------------------//    
    MI2C1_WordADR_HLBW_PB34(Acacia_TIA34_SADR,0x0000,TIA_Control_MEMORY_MAP.XI_IBIAS10_VGA_CH3_MSB    ,TIA_Control_MEMORY_MAP.XI_IBIAS10_VGA_CH3_LSB);
    MI2C1_WordADR_HLBW_PB34(Acacia_TIA34_SADR,0x0002,TIA_Control_MEMORY_MAP.XI_AGC1_CH3_MSB           ,TIA_Control_MEMORY_MAP.XI_AGC1_CH3_LSB);
    MI2C1_WordADR_HLBW_PB34(Acacia_TIA34_SADR,0x0006,TIA_Control_MEMORY_MAP.XI_IBIAS32_DRIVER_CH3_MSB ,TIA_Control_MEMORY_MAP.XI_IBIAS32_DRIVER_CH3_LSB);   
    MI2C1_WordADR_HLBW_PB34(Acacia_TIA34_SADR,0x000A,TIA_Control_MEMORY_MAP.XI_RF1_TIA_CH3_MSB        ,TIA_Control_MEMORY_MAP.XI_RF1_TIA_CH3_LSB);   
    MI2C1_WordADR_HLBW_PB34(Acacia_TIA34_SADR,0x000C,TIA_Control_MEMORY_MAP.TIA_VLDO_TIA_VREF_MSB_CH3 ,TIA_Control_MEMORY_MAP.TIA_VLDO_TIA_VREF_LSB_CH3);  
    MI2C1_WordADR_HLBW_PB34(Acacia_TIA34_SADR,0x000E,TIA_Control_MEMORY_MAP.XI_RF3_PREDRV_CH3_MSB     ,TIA_Control_MEMORY_MAP.XI_RF3_PREDRV_CH3_LSB);  
    MI2C1_WordADR_HLBW_PB34(Acacia_TIA34_SADR,0x0010,TIA_Control_MEMORY_MAP.XI_RF4_DRV_CH3_MSB        ,TIA_Control_MEMORY_MAP.XI_RF4_DRV_CH3_LSB);  
    MI2C1_WordADR_HLBW_PB34(Acacia_TIA34_SADR,0x0014,TIA_Control_MEMORY_MAP.XI_RF6_CH3_MSB            ,TIA_Control_MEMORY_MAP.XI_RF6_CH3_LSB);  
    //---------------------------------------------------------------------------//
    // Channel4 CONTROL
    //---------------------------------------------------------------------------//    
    MI2C1_WordADR_HLBW_PB34(Acacia_TIA34_SADR,0x1000,TIA_Control_MEMORY_MAP.XI_IBIAS10_VGA_CH4_MSB    ,TIA_Control_MEMORY_MAP.XI_IBIAS10_VGA_CH4_LSB);
    MI2C1_WordADR_HLBW_PB34(Acacia_TIA34_SADR,0x1002,TIA_Control_MEMORY_MAP.XI_AGC1_CH4_MSB           ,TIA_Control_MEMORY_MAP.XI_AGC1_CH4_LSB);
    MI2C1_WordADR_HLBW_PB34(Acacia_TIA34_SADR,0x1006,TIA_Control_MEMORY_MAP.XI_IBIAS32_DRIVER_CH4_MSB ,TIA_Control_MEMORY_MAP.XI_IBIAS32_DRIVER_CH4_LSB);   
    MI2C1_WordADR_HLBW_PB34(Acacia_TIA34_SADR,0x100A,TIA_Control_MEMORY_MAP.XI_RF1_TIA_CH4_MSB        ,TIA_Control_MEMORY_MAP.XI_RF1_TIA_CH4_LSB);   
    MI2C1_WordADR_HLBW_PB34(Acacia_TIA34_SADR,0x100C,TIA_Control_MEMORY_MAP.TIA_VLDO_TIA_VREF_MSB_CH4 ,TIA_Control_MEMORY_MAP.TIA_VLDO_TIA_VREF_LSB_CH4);  
    MI2C1_WordADR_HLBW_PB34(Acacia_TIA34_SADR,0x100E,TIA_Control_MEMORY_MAP.XI_RF3_PREDRV_CH4_MSB     ,TIA_Control_MEMORY_MAP.XI_RF3_PREDRV_CH4_LSB);  
    MI2C1_WordADR_HLBW_PB34(Acacia_TIA34_SADR,0x1010,TIA_Control_MEMORY_MAP.XI_RF4_DRV_CH4_MSB        ,TIA_Control_MEMORY_MAP.XI_RF4_DRV_CH4_LSB);  
    MI2C1_WordADR_HLBW_PB34(Acacia_TIA34_SADR,0x1014,TIA_Control_MEMORY_MAP.XI_RF6_CH4_MSB            ,TIA_Control_MEMORY_MAP.XI_RF6_CH4_LSB);   
}

void Acacia_TIA_GetSP()
{
    TIA_Control_MEMORY_MAP.TIA_SP_PDO_REF_CH1 = ( Acacia_TIA_SP_GETDATA(1,1)/10 );
    TIA_Control_MEMORY_MAP.TIA_SP_PDO_P_CH1   = ( Acacia_TIA_SP_GETDATA(1,2)/10 );
    TIA_Control_MEMORY_MAP.TIA_SP_PDO_N_CH1   = ( Acacia_TIA_SP_GETDATA(1,3)/10 );
    TIA_Control_MEMORY_MAP.TIA_SP_OA_REF_CH1  = ( Acacia_TIA_SP_GETDATA(1,4)/10 );
    TIA_Control_MEMORY_MAP.TIA_SP_GCAMP_CH1   = ( Acacia_TIA_SP_GETDATA(1,5)/10 );
    TIA_Control_MEMORY_MAP.TIA_SP_PKD_CH1     = ( Acacia_TIA_SP_GETDATA(1,6)/10 );
    
    TIA_Control_MEMORY_MAP.TIA_SP_PDO_REF_CH2 = ( Acacia_TIA_SP_GETDATA(2,1)/10 );
    TIA_Control_MEMORY_MAP.TIA_SP_PDO_P_CH2   = ( Acacia_TIA_SP_GETDATA(2,2)/10 );
    TIA_Control_MEMORY_MAP.TIA_SP_PDO_N_CH2   = ( Acacia_TIA_SP_GETDATA(2,3)/10 );
    TIA_Control_MEMORY_MAP.TIA_SP_OA_REF_CH2  = ( Acacia_TIA_SP_GETDATA(2,4)/10 );
    TIA_Control_MEMORY_MAP.TIA_SP_GCAMP_CH2   = ( Acacia_TIA_SP_GETDATA(2,5)/10 );
    TIA_Control_MEMORY_MAP.TIA_SP_PKD_CH2     = ( Acacia_TIA_SP_GETDATA(2,6)/10 );
    
    TIA_Control_MEMORY_MAP.TIA_SP_PDO_REF_CH3 = ( Acacia_TIA_SP_GETDATA(3,1)/10 );
    TIA_Control_MEMORY_MAP.TIA_SP_PDO_P_CH3   = ( Acacia_TIA_SP_GETDATA(3,2)/10 );
    TIA_Control_MEMORY_MAP.TIA_SP_PDO_N_CH3   = ( Acacia_TIA_SP_GETDATA(3,3)/10 );
    TIA_Control_MEMORY_MAP.TIA_SP_OA_REF_CH3  = ( Acacia_TIA_SP_GETDATA(3,4)/10 );
    TIA_Control_MEMORY_MAP.TIA_SP_GCAMP_CH3   = ( Acacia_TIA_SP_GETDATA(3,5)/10 );
    TIA_Control_MEMORY_MAP.TIA_SP_PKD_CH3     = ( Acacia_TIA_SP_GETDATA(3,6)/10 );
    
    TIA_Control_MEMORY_MAP.TIA_SP_PDO_REF_CH4 = ( Acacia_TIA_SP_GETDATA(4,1)/10 );
    TIA_Control_MEMORY_MAP.TIA_SP_PDO_P_CH4   = ( Acacia_TIA_SP_GETDATA(4,2)/10 );
    TIA_Control_MEMORY_MAP.TIA_SP_PDO_N_CH4   = ( Acacia_TIA_SP_GETDATA(4,3)/10 );
    TIA_Control_MEMORY_MAP.TIA_SP_OA_REF_CH4  = ( Acacia_TIA_SP_GETDATA(4,4)/10 );
    TIA_Control_MEMORY_MAP.TIA_SP_GCAMP_CH4   = ( Acacia_TIA_SP_GETDATA(4,5)/10 );
    TIA_Control_MEMORY_MAP.TIA_SP_PKD_CH4     = ( Acacia_TIA_SP_GETDATA(4,6)/10 );
}

